package com.mobilityone.onecall.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.mobilityone.onecall.config.Constants;
import com.mobilityone.onecall.security.SecurityUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A SimPortInVo.
 */
@Entity
@Table(name = "sim_port_in_vo")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SimPortInVo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "msisdn")
    private String msisdn;

    @Column(name = "sim_number")
    private String simNumber;

    @Column(name = "status")
    private String status;

    @Column(name = "created_date")
    private ZonedDateTime createdDate = ZonedDateTime.now();

    @Column(name = "modified_date")
    private ZonedDateTime modifiedDate = ZonedDateTime.now();

    @Column(name = "created_by")
    private String createdBy = SecurityUtils.getCurrentUserLogin().orElse(Constants.ANONYMOUS_USER);

    @Column(name = "modified_by")
    private String modifiedBy = SecurityUtils.getCurrentUserLogin().orElse(Constants.ANONYMOUS_USER);

    @ManyToOne
    @JsonIgnoreProperties("simPortInVos")
    private SimPortIn simPortIn;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public SimPortInVo msisdn(String msisdn) {
        this.msisdn = msisdn;
        return this;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getSimNumber() {
        return simNumber;
    }

    public SimPortInVo simNumber(String simNumber) {
        this.simNumber = simNumber;
        return this;
    }

    public void setSimNumber(String simNumber) {
        this.simNumber = simNumber;
    }

    public String getStatus() {
        return status;
    }

    public SimPortInVo status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public SimPortInVo createdDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public ZonedDateTime getModifiedDate() {
        return modifiedDate;
    }

    public SimPortInVo modifiedDate(ZonedDateTime modifiedDate) {
        this.modifiedDate = modifiedDate;
        return this;
    }

    public void setModifiedDate(ZonedDateTime modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public SimPortInVo createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public SimPortInVo modifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
        return this;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public SimPortIn getSimPortIn() {
        return simPortIn;
    }

    public SimPortInVo simPortIn(SimPortIn simPortIn) {
        this.simPortIn = simPortIn;
        return this;
    }

    public void setSimPortIn(SimPortIn simPortIn) {
        this.simPortIn = simPortIn;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SimPortInVo simPortInVo = (SimPortInVo) o;
        if (simPortInVo.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), simPortInVo.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SimPortInVo{" +
            "id=" + getId() +
            ", msisdn='" + getMsisdn() + "'" +
            ", simNumber='" + getSimNumber() + "'" +
            ", status='" + getStatus() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", modifiedDate='" + getModifiedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", modifiedBy='" + getModifiedBy() + "'" +
            "}";
    }
}
