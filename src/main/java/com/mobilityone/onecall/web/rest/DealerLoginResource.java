package com.mobilityone.onecall.web.rest;
import com.mobilityone.onecall.service.DealerLoginService;
import com.mobilityone.onecall.service.dto.LoginDTO;
import com.mobilityone.onecall.service.dto.LoginRespDTO;
import com.mobilityone.onecall.web.rest.errors.BadRequestAlertException;
import com.mobilityone.onecall.web.rest.util.HeaderUtil;
import com.mobilityone.onecall.web.rest.util.PaginationUtil;
import com.mobilityone.onecall.service.dto.DealerLoginDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing DealerLogin.
 */
@RestController
@RequestMapping("/api")
public class DealerLoginResource {

    private final Logger log = LoggerFactory.getLogger(DealerLoginResource.class);

    private static final String ENTITY_NAME = "oneCallDealerLogin";

    private final DealerLoginService dealerLoginService;

    public DealerLoginResource(DealerLoginService dealerLoginService) {
        this.dealerLoginService = dealerLoginService;
    }

    /**
     * POST  /dealer-logins : Create a new dealerLogin.
     *
     * @param dealerLoginDTO the dealerLoginDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new dealerLoginDTO, or with status 400 (Bad Request) if the dealerLogin has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    //@PostMapping("/dealer-logins")
    public ResponseEntity<DealerLoginDTO> createDealerLogin(@RequestBody DealerLoginDTO dealerLoginDTO) throws URISyntaxException {
        log.debug("REST request to save DealerLogin : {}", dealerLoginDTO);
        if (dealerLoginDTO.getId() != null) {
            throw new BadRequestAlertException("A new dealerLogin cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DealerLoginDTO result = dealerLoginService.save(dealerLoginDTO);
        return ResponseEntity.created(new URI("/api/dealer-logins/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /dealer-logins : Updates an existing dealerLogin.
     *
     * @param dealerLoginDTO the dealerLoginDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated dealerLoginDTO,
     * or with status 400 (Bad Request) if the dealerLoginDTO is not valid,
     * or with status 500 (Internal Server Error) if the dealerLoginDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    //@PutMapping("/dealer-logins")
    public ResponseEntity<DealerLoginDTO> updateDealerLogin(@RequestBody DealerLoginDTO dealerLoginDTO) throws URISyntaxException {
        log.debug("REST request to update DealerLogin : {}", dealerLoginDTO);
        if (dealerLoginDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DealerLoginDTO result = dealerLoginService.save(dealerLoginDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, dealerLoginDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /dealer-logins : get all the dealerLogins.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of dealerLogins in body
     */
    @GetMapping("/dealer-logins")
    public ResponseEntity<List<DealerLoginDTO>> getAllDealerLogins(Pageable pageable) {
        log.debug("REST request to get a page of DealerLogins");
        Page<DealerLoginDTO> page = dealerLoginService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/dealer-logins");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /dealer-logins/:id : get the "id" dealerLogin.
     *
     * @param id the id of the dealerLoginDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the dealerLoginDTO, or with status 404 (Not Found)
     */
    @GetMapping("/dealer-logins/{id}")
    public ResponseEntity<DealerLoginDTO> getDealerLogin(@PathVariable Long id) {
        log.debug("REST request to get DealerLogin : {}", id);
        Optional<DealerLoginDTO> dealerLoginDTO = dealerLoginService.findOne(id);
        return ResponseUtil.wrapOrNotFound(dealerLoginDTO);
    }

    /**
     * DELETE  /dealer-logins/:id : delete the "id" dealerLogin.
     *
     * @param id the id of the dealerLoginDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    //@DeleteMapping("/dealer-logins/{id}")
    public ResponseEntity<Void> deleteDealerLogin(@PathVariable Long id) {
        log.debug("REST request to delete DealerLogin : {}", id);
        dealerLoginService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }


    @PostMapping("/dealer-login")
    public ResponseEntity login(@Valid @RequestBody LoginDTO loginDTO) {
        log.debug("REST request to Login an agent");

        LoginRespDTO login = dealerLoginService.login(loginDTO);

        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON_UTF8).body(login);
    }

}
