package com.mobilityone.onecall.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TtBalabceRespDTO {
    private String accountStatus;
    private String msisdn;
    private String iccid;
    private String creditBalance;
    private String creditExpiry;
    private String graceExpiry;
    private String apiKey;
    private String code;
    private String message;
    private String tariffPlan;
    private String ttBigPoint;
    private List<TtMsidnPlanDTO> plans = new ArrayList<>();
    private List<TtMsidnPlanDTO> freebies = new ArrayList<>();

    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    public String getIccid() {
        return iccid;
    }

    public void setIccid(String iccid) {
        this.iccid = iccid;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCreditBalance() {
        return creditBalance;
    }

    public void setCreditBalance(String creditBalance) {
        this.creditBalance = creditBalance;
    }

    public String getCreditExpiry() {
        return creditExpiry;
    }

    public void setCreditExpiry(String creditExpiry) {
        this.creditExpiry = creditExpiry;
    }

    public String getGraceExpiry() {
        return graceExpiry;
    }

    public void setGraceExpiry(String graceExpiry) {
        this.graceExpiry = graceExpiry;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getTariffPlan() {
        return tariffPlan;
    }

    public void setTariffPlan(String tariffPlan) {
        this.tariffPlan = tariffPlan;
    }

    public String getTtBigPoint() {
        return ttBigPoint;
    }

    public void setTtBigPoint(String ttBigPoint) {
        this.ttBigPoint = ttBigPoint;
    }

    public List<TtMsidnPlanDTO> getPlans() {
        return plans;
    }

    public void setPlans(List<TtMsidnPlanDTO> plans) {
        this.plans = plans;
    }

    public List<TtMsidnPlanDTO> getFreebies() {
        return freebies;
    }

    public void setFreebies(List<TtMsidnPlanDTO> freebies) {
        this.freebies = freebies;
    }

    @Override
    public String toString() {
        return "TtBalabceRespDTO{" +
            "accountStatus='" + accountStatus + '\'' +
            ", msisdn='" + msisdn + '\'' +
            ", creditBalance='" + creditBalance + '\'' +
            ", creditExpiry='" + creditExpiry + '\'' +
            ", graceExpiry='" + graceExpiry + '\'' +
            ", apiKey='" + apiKey + '\'' +
            ", code='" + code + '\'' +
            ", message='" + message + '\'' +
            ", tariffPlan='" + tariffPlan + '\'' +
            ", ttBigPoint='" + ttBigPoint + '\'' +
            ", plans=" + plans +
            ", freebies=" + freebies +
            '}';
    }
}
