package com.mobilityone.onecall.repository;

import com.mobilityone.onecall.domain.SimRegister;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


/**
 * Spring Data  repository for the SimRegister entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SimRegisterRepository extends JpaRepository<SimRegister, Long> {

    Page<SimRegister> findByAgentIdOrderByCreatedDateDesc(Pageable pageable, Long agentId);

    List<SimRegister> findByCreatedBy(String createdBy);



    @Query(nativeQuery = true, value = "select s.id as id, s.simSerialNumber as sim_Serial_Number, s.simMobileNumber as sim_Mobile_Number, " +
        "s.typeRegistration as type_Registration, s.status as status , s.statusDate as status_Date from ( " +
        "select piv.sim_port_in_id as id, piv.sim_number as simSerialNumber , piv.msisdn as simMobileNumber , 'port_in' as typeRegistration , " +
        "piv.status  as status , date_format(CONVERT_TZ(piv.modified_date ,'+00:00','+08:00'), '%d-%m-%Y %T') as statusDate from sim_port_in_vo piv where piv.created_by = :userName " +
        "union all " +
        "select sr.id as id, sr.sim_number as simSerialNumber , sr.msisdn as simMobileNumber ,  'register' as typeRegistration ,  " +
        "sr.status as status,  date_format(CONVERT_TZ(sr.created_date ,'+00:00','+08:00'), '%d-%m-%Y %T') as statusDate  " +
        "from sim_register sr where sr.created_by = :userName ) s;",
        countQuery = "select  count(s.simMobileNumber) from ( " +
            "select piv.msisdn as simMobileNumber from sim_port_in_vo piv where piv.created_by = :userName   " +
            "union all " +
            "select sr.msisdn  as simMobileNumber from sim_register sr where sr.created_by = :userName ) s;"
    )
    List<Map<String, Object>> simHistory(@Param("userName") String userName);




    @Query(nativeQuery = true, value = "select s.id as id, s.simSerialNumber as sim_Serial_Number, s.simMobileNumber as sim_Mobile_Number, " +
        "s.typeRegistration as type_Registration, s.status as status , s.statusDate as status_Date from ( " +
        "select piv.sim_port_in_id as id, piv.sim_number as simSerialNumber , piv.msisdn as simMobileNumber , 'port_in' as typeRegistration , " +
        "piv.status  as status , date_format(CONVERT_TZ(piv.modified_date ,'+00:00','+08:00'), '%d-%m-%Y %T') as statusDate " +
        "from sim_port_in_vo piv where piv.msisdn = :msisdn " +
        "union all " +
        "select sr.id as id, sr.sim_number as simSerialNumber , sr.msisdn as simMobileNumber ,  'register' as typeRegistration ,  " +
        "sr.status as status,  date_format(CONVERT_TZ(sr.created_date ,'+00:00','+08:00'), '%d-%m-%Y %T') as statusDate  " +
        "from sim_register sr where sr.msisdn = :msisdn ) s;",
        countQuery = "select  count(s.simMobileNumber) from ( " +
            "select piv.msisdn as simMobileNumber from sim_port_in_vo piv where piv.msisdn = :msisdn   " +
            "union all " +
            "select sr.msisdn  as simMobileNumber from sim_register sr where sr.msisdn = :msisdn ) s;"
    )
    List<Map<String, Object>> simStatusByMsisdn(@Param("msisdn") String msisdn);




    @Query("select count(sr) from SimRegister sr where sr.status = 'SUCCESSFUL' and date_format(CONVERT_TZ(sr.createdDate ,'+00:00','+08:00') , '%Y-%m-%d') = :date") //mysql
//    @Query("select count(sr) from SimRegister sr where sr.status = 'SUCCESSFUL' and FORMATDATETIME(DATEADD(HOUR, 8, sr.createdDate) ,'yyyy-MM-dd') = :date") //h2
    Integer countByDate(@Param("date") String date);




    @Query(value = "select sr.id as id, sr.agent_code as agentCode , sr.transaction_id as transactionId , sr.sim_number as simNumber , " +
        " sr.message as message , sr.msisdn as msisdn , sr.status as status , date_format(CONVERT_TZ(sr.created_date , '+00:00', '+08:00') , '%Y-%m-%d %T') as createdDate , " +
        " date_format(CONVERT_TZ(sr.modified_date , '+00:00', '+08:00') , '%Y-%m-%d %T') as modifiedDate , sr.created_by as createdBy, sr.modified_by as modifiedBy " +
        " from sim_register sr where date_format(CONVERT_TZ(sr.created_date , '+00:00', '+08:00') , '%Y-%m-%d') between :fromDate and :toDate", nativeQuery = true) //mysql
//    @Query(value = "select sr.id as id, sr.agent_code as agentCode , sr.transaction_id as transactionId , sr.sim_number as simNumber , " +
//        " sr.message as message , sr.msisdn as msisdn , sr.status as status , FORMATDATETIME(DATEADD(HOUR, 8, sr.created_date) , 'yyyy-MM-dd HH:mm:ss')  as createdDate , " +
//        " FORMATDATETIME(DATEADD(HOUR, 8, sr.modified_date) , 'yyyy-MM-dd HH:mm:ss') as modifiedDate , sr.created_by as createdBy, sr.modified_by as modifiedBy  from sim_register sr " +
//        " where FORMATDATETIME(DATEADD(HOUR, 8, sr.created_date) , 'yyyy-MM-dd') between :fromDate and :toDate", nativeQuery = true) //h2
    List<Map<String, Object>> findAllInRangeDate(@Param("fromDate") String fromDate, @Param("toDate") String toDate);




    @Query(value = "select sr.id as id, sr.sim_number as simNumber , sr.msisdn as msisdn , sr.status as ttstatus , " +
        " (case when sr.status = 'SUCCESSFUL' then 'OK' else null end ) as ewalletStatus , " +
        " date_format(CONVERT_TZ(sr.created_date , '+00:00', '+08:00'), '%Y-%m-%d %T') as createDate " +
        " from sim_register sr  " +
        " where sr.agent_code = :agentCode" , nativeQuery = true) //mysql
//    @Query(value = "select sr.id as id, sr.sim_number as simNumber , sr.msisdn as msisdn , sr.status as ttstatus , " +
//        " (case when sr.status = 'SUCCESSFUL' then 'OK' else null end ) as ewalletStatus , " +
//        " FORMATDATETIME(DATEADD(HOUR, 8, sr.created_date) , 'yyyy-MM-dd HH:mm:ss') as createDate " +
//        " from sim_register sr  " +
//        " where sr.agent_code = :agentCode" , nativeQuery = true)//h2
    List<Map<String, Object>> getSimStatusByAgentCode(@Param("agentCode") String agentCode);


    @Query(value = "select dates.createDate, reg.registerCount , port.portInCount , port.modifiedDate as portInSuccessDate from  " +
        " (select * from " +
        " (select date_format(CONVERT_TZ(spiv.created_date , '+00:00', '+08:00'), '%Y-%m-%d') as createDate " +
        " from sim_port_in_vo spiv " +
        " where spiv.status = 'SUCCESSFUL' " +
        " group by createDate " +
        "  union " +
        " select date_format(CONVERT_TZ(sr.created_date , '+00:00', '+08:00'), '%Y-%m-%d') as createDate " +
        " from sim_register sr " +
        " where sr.status = 'SUCCESSFUL' " +
        " group by createDate ) u " +
        " ) dates  " +
        " left join  " +
        " (select count(sr.id) registerCount , date_format(CONVERT_TZ(sr.created_date , '+00:00', '+08:00'), '%Y-%m-%d') as createDate " +
        " from sim_register sr " +
        " where sr.status = 'SUCCESSFUL' " +
        " group by createDate " +
        ") reg on dates.createDate = reg.createDate " +
        " left join  " +
        " (select count(spiv.id) portInCount , date_format(CONVERT_TZ(spiv.created_date , '+00:00', '+08:00'), '%Y-%m-%d') as createDate,  " +
        " date_format(CONVERT_TZ(spiv.modified_date , '+00:00', '+08:00'), '%Y-%m-%d') as modifiedDate " +
        " from sim_port_in_vo spiv  " +
        " where  spiv.status = 'SUCCESSFUL' " +
        " group by createDate " +
        " ) port on dates.createDate = port.createDate " +
        " where dates.createDate between :fromDate and :toDate", nativeQuery = true) //mysql
//    @Query(value = "select dates.createDate, reg.registerCount , port.portInCount , port.modifiedDate as portInSuccessDate from  " +
//        " (select * from " +
//        "  (select FORMATDATETIME(DATEADD(HOUR, 8, spiv.created_date) , 'yyyy-MM-dd') as createDate " +
//        "  from sim_port_in_vo spiv " +
//        "  where spiv.status = 'SUCCESSFUL' " +
//        "  group by createDate " +
//        "   union " +
//        "  select FORMATDATETIME(DATEADD(HOUR, 8, sr.created_date) , 'yyyy-MM-dd') as createDate " +
//        "  from sim_register sr " +
//        "  where sr.status = 'SUCCESSFUL' " +
//        "  group by createDate ) u " +
//        " ) dates  " +
//        " left join  " +
//        " (select count(sr.id) registerCount , FORMATDATETIME(DATEADD(HOUR, 8, sr.created_date) , 'yyyy-MM-dd') as createDate " +
//        "  from sim_register sr " +
//        "  where sr.status = 'SUCCESSFUL' " +
//        "  group by createDate " +
//        " ) reg on dates.createDate = reg.createDate " +
//        " left join  " +
//        " (select count(spiv.id) portInCount , FORMATDATETIME(DATEADD(HOUR, 8, spiv.created_date) , 'yyyy-MM-dd') as createDate,  " +
//        "  FORMATDATETIME(DATEADD(HOUR, 8, spiv.modified_date) , 'yyyy-MM-dd') as modifiedDate " +
//        "  from sim_port_in_vo spiv  " +
//        "  where  spiv.status = 'SUCCESSFUL' " +
//        "  group by createDate " +
//        " ) port on dates.createDate = port.createDate " +
//        " where dates.createDate between :fromDate and :toDate", nativeQuery = true) //h2
    List<Map<String, Object>> getDailyTotalSimInRange(@Param("fromDate") String fromDate, @Param("toDate") String toDate);




    @Query(value = "select count(sr.id) as registerCount , date_format(CONVERT_TZ(sr.created_date , '+00:00', '+08:00'), '%Y-%m-%d') as createDate  " +
        " from sim_register sr  " +
        " where sr.status = 'SUCCESSFUL' and sr.agent_code = :agentCode and date_format(CONVERT_TZ(sr.created_date , '+00:00', '+08:00'), '%Y-%m-%d') between :fromDate and :toDate  " +
        " group by createDate ", nativeQuery = true) //mysql
//    @Query(value = "select count(sr.id) registerCount , FORMATDATETIME(DATEADD(HOUR, 8, sr.created_date) , 'yyyy-MM-dd') as createDate  " +
//        " from sim_register sr  " +
//        " where sr.status = 'SUCCESSFUL' and sr.agent_code = :agentCode and FORMATDATETIME(DATEADD(HOUR, 8, sr.created_date) , 'yyyy-MM-dd') between :fromDate and :toDate  " +
//        " group by createDate ", nativeQuery = true) //h2
    List<Map<String, Object>> getAgentDailyTotalSimRegInRange(@Param("agentCode") String agentCode, @Param("fromDate") String fromDate, @Param("toDate") String toDate);


    @Query(value = "select sr.id as id, sr.agent_code as agentCode , sr.sim_number as simNumber , sr.id_number as ic_passport,  " +
        "   sr.msisdn as msisdn , date_format(CONVERT_TZ(sr.created_date , '+00:00', '+08:00') , '%Y-%m-%d %T') as createdDate ,   " +
        "   sr.created_by as createdBy   " +
        "   from sim_register sr where date_format(CONVERT_TZ(sr.created_date , '+00:00', '+08:00') , '%Y-%m-%d') between :fromDate and :toDate", nativeQuery = true) //mysql
    List<Map<String, Object>> getSimRegsInRange(@Param("fromDate") String fromDate, @Param("toDate") String toDate);
}
