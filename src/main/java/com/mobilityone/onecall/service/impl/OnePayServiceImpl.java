package com.mobilityone.onecall.service.impl;

import com.mobilityone.onecall.config.ApplicationProperties;
import com.mobilityone.onecall.domain.enumeration.SimStatus;
import com.mobilityone.onecall.service.OnePayService;
import com.mobilityone.onecall.service.SimRegisterService;
import com.mobilityone.onecall.service.dto.CustomerRegisterDTO;
import com.mobilityone.onecall.service.dto.SimRegisterDTO;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

@Service
public class OnePayServiceImpl implements OnePayService {

    private final Logger log = LoggerFactory.getLogger(OnePayServiceImpl.class);

    private final ApplicationProperties properties;

    private final SimRegisterService simRegisterService;

    public OnePayServiceImpl(ApplicationProperties properties, SimRegisterService simRegisterService) {
        this.properties = properties;
        this.simRegisterService = simRegisterService;
    }


    @Override
    public JSONObject registerCustomerByRegId(Long regId) {
        log.debug("Request to call TPM Customer Register by regId : {}", regId);

        Optional<SimRegisterDTO> simRegisterOptional = simRegisterService.findOne(regId);
        if (!simRegisterOptional.isPresent()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "sim register not found");
        }

        SimRegisterDTO simRegisterDTO = simRegisterOptional.get();
        JSONObject object = registerCustomer(simRegisterDTO);

        if (object == null) {
            throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, "calling TPM failed");
        }

        if (object.has("ErrCode") && "0".equals(object.getString("ErrCode"))) {
            simRegisterDTO.setStatus(SimStatus.SUCCESSFUL.toString());
            simRegisterDTO = simRegisterService.save(simRegisterDTO);
        }

        object.put("SimRegStatus", simRegisterDTO.getStatus());
        object.put("message", object.getString("ErrDesc"));

        return object;

    }

    @Override
    public JSONObject registerCustomer(SimRegisterDTO simRegisterDTO) {
        log.info("Start Register customer with TPM service.");
        HttpResponse response = null;
        try {
            HttpClient httpclient = HttpClients.createDefault();
            String url = properties.getTpmDealer().getUrl();
            log.info("TPM Url : {}", url);

            HttpPost httppost = new HttpPost(url);

// Request parameters and other properties.
            httppost.setEntity(new StringEntity(getSimRegisterAsJson(simRegisterDTO)));
            httppost.addHeader("content-type", "application/json");
            response = httpclient.execute(httppost);
            org.apache.http.HttpEntity entity = response.getEntity();
            String resBody = IOUtils.toString(entity.getContent(), StandardCharsets.UTF_8.name());
            log.info("==>>>> TPM Response Body: {}", resBody);
            return new JSONObject(resBody);
        } catch (IOException e) {
            throw new ResponseStatusException(HttpStatus.FAILED_DEPENDENCY, "Register customer with TPM Api Failed.", e);
        } catch (Exception e) {
            log.info("call TPm exception : {}", e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    private String getSimRegisterAsJson(SimRegisterDTO simRegisterDTO) {
        try {
            CustomerRegisterDTO customerRegisterDTO = new CustomerRegisterDTO();
            customerRegisterDTO.setID(simRegisterDTO.getIdNumber());
            customerRegisterDTO.setMSISDN("6".concat(simRegisterDTO.getMsisdn()));
            customerRegisterDTO.setName(simRegisterDTO.getFirstName().concat(" ").concat(simRegisterDTO.getLastName()));
            customerRegisterDTO.setDOB(simRegisterDTO.getDob().replace("-", ""));
            customerRegisterDTO.setGender("MALE".equalsIgnoreCase(simRegisterDTO.getGender().toString()) ? "M" : "F");
            customerRegisterDTO.setAddress(simRegisterDTO.getAddress());
            customerRegisterDTO.setSIMSerial(simRegisterDTO.getSimNumber());

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            customerRegisterDTO.setRegisterDT(simRegisterDTO.getCreatedDate().format(formatter).replace(" ", "T").concat("Z"));

            String photo = simRegisterDTO.getPhotos().iterator().next().getPhoto();
            customerRegisterDTO.setPhoto1(photo);
            customerRegisterDTO.setPhoto2(photo);

            customerRegisterDTO.setFunction(properties.getTpmDealer().getFunction());
            customerRegisterDTO.setPasscode(properties.getTpmDealer().getPasscode());
            customerRegisterDTO.setOrgID(properties.getTpmDealer().getOrgID());

            log.info("Sim Register Json for TPM : {}", customerRegisterDTO.getJson());

            return customerRegisterDTO.getJson().toString();
        } catch (Exception e){
            log.info("Create CustomerRegisterDTO Exception : {}", e.getMessage());
            throw e;
        }
    }
}
