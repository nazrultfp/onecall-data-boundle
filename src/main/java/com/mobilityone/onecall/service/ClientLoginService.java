package com.mobilityone.onecall.service;

import com.mobilityone.onecall.service.dto.ClientLoginRespDTO;
import com.mobilityone.onecall.service.dto.LoginDTO;

public interface ClientLoginService {

    ClientLoginRespDTO clientLogin(LoginDTO loginDTO);

}
