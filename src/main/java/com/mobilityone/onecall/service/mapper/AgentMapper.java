package com.mobilityone.onecall.service.mapper;

import com.mobilityone.onecall.domain.*;
import com.mobilityone.onecall.service.dto.AgentDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Agent and its DTO AgentDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AgentMapper extends EntityMapper<AgentDTO, Agent> {


    @Mapping(target = "simOrders", ignore = true)
    @Mapping(target = "simRegisters", ignore = true)
    @Mapping(target = "simPortIns", ignore = true)
    Agent toEntity(AgentDTO agentDTO);

    default Agent fromId(Long id) {
        if (id == null) {
            return null;
        }
        Agent agent = new Agent();
        agent.setId(id);
        return agent;
    }
}
