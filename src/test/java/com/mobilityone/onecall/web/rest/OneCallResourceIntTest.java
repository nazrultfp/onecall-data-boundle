package com.mobilityone.onecall.web.rest;

import com.mobilityone.onecall.OneCallApp;
import com.mobilityone.onecall.domain.OneCall;
import com.mobilityone.onecall.repository.OneCallRepository;
import com.mobilityone.onecall.service.OneCallService;
import com.mobilityone.onecall.service.TuneTalkService;
import com.mobilityone.onecall.service.dto.OneCallDTO;
import com.mobilityone.onecall.service.mapper.OneCallMapper;
import com.mobilityone.onecall.service.mapper.TopupMapper;
import com.mobilityone.onecall.web.rest.errors.ExceptionTranslator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;

import static com.mobilityone.onecall.web.rest.TestUtil.createFormattingConversionService;
import static com.mobilityone.onecall.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the OneCallResource REST controller.
 *
 * @see OneCallResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OneCallApp.class)
public class OneCallResourceIntTest {

    private static final String DEFAULT_CHANNEL_ID = "AAAAAAAAAA";
    private static final String UPDATED_CHANNEL_ID = "BBBBBBBBBB";

    private static final String DEFAULT_MERCHANT_ID = "AAAAAAAAAA";
    private static final String UPDATED_MERCHANT_ID = "BBBBBBBBBB";

    private static final String DEFAULT_MERCHANT_ORDER_NO = "AAAAAAAAAA";
    private static final String UPDATED_MERCHANT_ORDER_NO = "BBBBBBBBBB";

    private static final String DEFAULT_MSISDN = "AAAAAAAAAA";
    private static final String UPDATED_MSISDN = "BBBBBBBBBB";

    private static final Integer DEFAULT_TOPUP_DENO = 1;
    private static final Integer UPDATED_TOPUP_DENO = 2;

    private static final Long DEFAULT_TRANSACTION_ID = 1L;
    private static final Long UPDATED_TRANSACTION_ID = 2L;

    private static final String DEFAULT_API_KEY = "AAAAAAAAAA";
    private static final String UPDATED_API_KEY = "BBBBBBBBBB";

    private static final String DEFAULT_BALANCE = "AAAAAAAAAA";
    private static final String UPDATED_BALANCE = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_MESSAGE = "AAAAAAAAAA";
    private static final String UPDATED_MESSAGE = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_MODIFIED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_MODIFIED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_MODIFIED_BY = "BBBBBBBBBB";

    @Autowired
    private OneCallRepository oneCallRepository;

    @Autowired
    private OneCallMapper oneCallMapper;

    @Autowired
    private OneCallService oneCallService;

    @Autowired
    private TuneTalkService tuneTalkService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restOneCallMockMvc;

    private OneCall oneCall;

    private TopupMapper topupMapper;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final OneCallResource oneCallResource = new OneCallResource(oneCallService, tuneTalkService, topupMapper);
        this.restOneCallMockMvc = MockMvcBuilders.standaloneSetup(oneCallResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OneCall createEntity(EntityManager em) {
        OneCall oneCall = new OneCall()
            .channelId(DEFAULT_CHANNEL_ID)
            .merchantId(DEFAULT_MERCHANT_ID)
            .merchantOrderNo(DEFAULT_MERCHANT_ORDER_NO)
            .msisdn(DEFAULT_MSISDN)
            .topupDeno(DEFAULT_TOPUP_DENO)
            .transactionId(DEFAULT_TRANSACTION_ID)
            .apiKey(DEFAULT_API_KEY)
            .balance(DEFAULT_BALANCE)
            .code(DEFAULT_CODE)
            .message(DEFAULT_MESSAGE)
            .createdDate(DEFAULT_CREATED_DATE)
            .modifiedDate(DEFAULT_MODIFIED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .modifiedBy(DEFAULT_MODIFIED_BY);
        return oneCall;
    }

    @Before
    public void initTest() {
        oneCall = createEntity(em);
    }

    @Test
    @Transactional
    public void createOneCall() throws Exception {
        int databaseSizeBeforeCreate = oneCallRepository.findAll().size();

        // Create the OneCall
        OneCallDTO oneCallDTO = oneCallMapper.toDto(oneCall);
        restOneCallMockMvc.perform(post("/api/one-calls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(oneCallDTO)))
            .andExpect(status().isCreated());

        // Validate the OneCall in the database
        List<OneCall> oneCallList = oneCallRepository.findAll();
        assertThat(oneCallList).hasSize(databaseSizeBeforeCreate + 1);
        OneCall testOneCall = oneCallList.get(oneCallList.size() - 1);
        assertThat(testOneCall.getChannelId()).isEqualTo(DEFAULT_CHANNEL_ID);
        assertThat(testOneCall.getMerchantId()).isEqualTo(DEFAULT_MERCHANT_ID);
        assertThat(testOneCall.getMerchantOrderNo()).isEqualTo(DEFAULT_MERCHANT_ORDER_NO);
        assertThat(testOneCall.getMsisdn()).isEqualTo(DEFAULT_MSISDN);
        assertThat(testOneCall.getTopupDeno()).isEqualTo(DEFAULT_TOPUP_DENO);
        assertThat(testOneCall.getTransactionId()).isEqualTo(DEFAULT_TRANSACTION_ID);
        assertThat(testOneCall.getApiKey()).isEqualTo(DEFAULT_API_KEY);
        assertThat(testOneCall.getBalance()).isEqualTo(DEFAULT_BALANCE);
        assertThat(testOneCall.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testOneCall.getMessage()).isEqualTo(DEFAULT_MESSAGE);
        assertThat(testOneCall.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testOneCall.getModifiedDate()).isEqualTo(DEFAULT_MODIFIED_DATE);
        assertThat(testOneCall.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testOneCall.getModifiedBy()).isEqualTo(DEFAULT_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void createOneCallWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = oneCallRepository.findAll().size();

        // Create the OneCall with an existing ID
        oneCall.setId(1L);
        OneCallDTO oneCallDTO = oneCallMapper.toDto(oneCall);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOneCallMockMvc.perform(post("/api/one-calls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(oneCallDTO)))
            .andExpect(status().isBadRequest());

        // Validate the OneCall in the database
        List<OneCall> oneCallList = oneCallRepository.findAll();
        assertThat(oneCallList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkMerchantIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = oneCallRepository.findAll().size();
        // set the field null
        oneCall.setMerchantId(null);

        // Create the OneCall, which fails.
        OneCallDTO oneCallDTO = oneCallMapper.toDto(oneCall);

        restOneCallMockMvc.perform(post("/api/one-calls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(oneCallDTO)))
            .andExpect(status().isBadRequest());

        List<OneCall> oneCallList = oneCallRepository.findAll();
        assertThat(oneCallList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllOneCalls() throws Exception {
        // Initialize the database
        oneCallRepository.saveAndFlush(oneCall);

        // Get all the oneCallList
        restOneCallMockMvc.perform(get("/api/one-calls?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(oneCall.getId().intValue())))
            .andExpect(jsonPath("$.[*].channelId").value(hasItem(DEFAULT_CHANNEL_ID.toString())))
            .andExpect(jsonPath("$.[*].merchantId").value(hasItem(DEFAULT_MERCHANT_ID.toString())))
            .andExpect(jsonPath("$.[*].merchantOrderNo").value(hasItem(DEFAULT_MERCHANT_ORDER_NO.toString())))
            .andExpect(jsonPath("$.[*].msisdn").value(hasItem(DEFAULT_MSISDN.toString())))
            .andExpect(jsonPath("$.[*].topupDeno").value(hasItem(DEFAULT_TOPUP_DENO)))
            .andExpect(jsonPath("$.[*].transactionId").value(hasItem(DEFAULT_TRANSACTION_ID.intValue())))
            .andExpect(jsonPath("$.[*].apiKey").value(hasItem(DEFAULT_API_KEY.toString())))
            .andExpect(jsonPath("$.[*].balance").value(hasItem(DEFAULT_BALANCE.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].message").value(hasItem(DEFAULT_MESSAGE.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(sameInstant(DEFAULT_CREATED_DATE))))
            .andExpect(jsonPath("$.[*].modifiedDate").value(hasItem(sameInstant(DEFAULT_MODIFIED_DATE))))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].modifiedBy").value(hasItem(DEFAULT_MODIFIED_BY.toString())));
    }

    @Test
    @Transactional
    public void getOneCall() throws Exception {
        // Initialize the database
        oneCallRepository.saveAndFlush(oneCall);

        // Get the oneCall
        restOneCallMockMvc.perform(get("/api/one-calls/{id}", oneCall.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(oneCall.getId().intValue()))
            .andExpect(jsonPath("$.channelId").value(DEFAULT_CHANNEL_ID.toString()))
            .andExpect(jsonPath("$.merchantId").value(DEFAULT_MERCHANT_ID.toString()))
            .andExpect(jsonPath("$.merchantOrderNo").value(DEFAULT_MERCHANT_ORDER_NO.toString()))
            .andExpect(jsonPath("$.msisdn").value(DEFAULT_MSISDN.toString()))
            .andExpect(jsonPath("$.topupDeno").value(DEFAULT_TOPUP_DENO))
            .andExpect(jsonPath("$.transactionId").value(DEFAULT_TRANSACTION_ID.intValue()))
            .andExpect(jsonPath("$.apiKey").value(DEFAULT_API_KEY.toString()))
            .andExpect(jsonPath("$.balance").value(DEFAULT_BALANCE.toString()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.message").value(DEFAULT_MESSAGE.toString()))
            .andExpect(jsonPath("$.createdDate").value(sameInstant(DEFAULT_CREATED_DATE)))
            .andExpect(jsonPath("$.modifiedDate").value(sameInstant(DEFAULT_MODIFIED_DATE)))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.modifiedBy").value(DEFAULT_MODIFIED_BY.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingOneCall() throws Exception {
        // Get the oneCall
        restOneCallMockMvc.perform(get("/api/one-calls/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOneCall() throws Exception {
        // Initialize the database
        oneCallRepository.saveAndFlush(oneCall);

        int databaseSizeBeforeUpdate = oneCallRepository.findAll().size();

        // Update the oneCall
        OneCall updatedOneCall = oneCallRepository.findById(oneCall.getId()).get();
        // Disconnect from session so that the updates on updatedOneCall are not directly saved in db
        em.detach(updatedOneCall);
        updatedOneCall
            .channelId(UPDATED_CHANNEL_ID)
            .merchantId(UPDATED_MERCHANT_ID)
            .merchantOrderNo(UPDATED_MERCHANT_ORDER_NO)
            .msisdn(UPDATED_MSISDN)
            .topupDeno(UPDATED_TOPUP_DENO)
            .transactionId(UPDATED_TRANSACTION_ID)
            .apiKey(UPDATED_API_KEY)
            .balance(UPDATED_BALANCE)
            .code(UPDATED_CODE)
            .message(UPDATED_MESSAGE)
            .createdDate(UPDATED_CREATED_DATE)
            .modifiedDate(UPDATED_MODIFIED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .modifiedBy(UPDATED_MODIFIED_BY);
        OneCallDTO oneCallDTO = oneCallMapper.toDto(updatedOneCall);

        restOneCallMockMvc.perform(put("/api/one-calls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(oneCallDTO)))
            .andExpect(status().isOk());

        // Validate the OneCall in the database
        List<OneCall> oneCallList = oneCallRepository.findAll();
        assertThat(oneCallList).hasSize(databaseSizeBeforeUpdate);
        OneCall testOneCall = oneCallList.get(oneCallList.size() - 1);
        assertThat(testOneCall.getChannelId()).isEqualTo(UPDATED_CHANNEL_ID);
        assertThat(testOneCall.getMerchantId()).isEqualTo(UPDATED_MERCHANT_ID);
        assertThat(testOneCall.getMerchantOrderNo()).isEqualTo(UPDATED_MERCHANT_ORDER_NO);
        assertThat(testOneCall.getMsisdn()).isEqualTo(UPDATED_MSISDN);
        assertThat(testOneCall.getTopupDeno()).isEqualTo(UPDATED_TOPUP_DENO);
        assertThat(testOneCall.getTransactionId()).isEqualTo(UPDATED_TRANSACTION_ID);
        assertThat(testOneCall.getApiKey()).isEqualTo(UPDATED_API_KEY);
        assertThat(testOneCall.getBalance()).isEqualTo(UPDATED_BALANCE);
        assertThat(testOneCall.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testOneCall.getMessage()).isEqualTo(UPDATED_MESSAGE);
        assertThat(testOneCall.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testOneCall.getModifiedDate()).isEqualTo(UPDATED_MODIFIED_DATE);
        assertThat(testOneCall.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testOneCall.getModifiedBy()).isEqualTo(UPDATED_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void updateNonExistingOneCall() throws Exception {
        int databaseSizeBeforeUpdate = oneCallRepository.findAll().size();

        // Create the OneCall
        OneCallDTO oneCallDTO = oneCallMapper.toDto(oneCall);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOneCallMockMvc.perform(put("/api/one-calls")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(oneCallDTO)))
            .andExpect(status().isBadRequest());

        // Validate the OneCall in the database
        List<OneCall> oneCallList = oneCallRepository.findAll();
        assertThat(oneCallList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteOneCall() throws Exception {
        // Initialize the database
        oneCallRepository.saveAndFlush(oneCall);

        int databaseSizeBeforeDelete = oneCallRepository.findAll().size();

        // Delete the oneCall
        restOneCallMockMvc.perform(delete("/api/one-calls/{id}", oneCall.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<OneCall> oneCallList = oneCallRepository.findAll();
        assertThat(oneCallList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OneCall.class);
        OneCall oneCall1 = new OneCall();
        oneCall1.setId(1L);
        OneCall oneCall2 = new OneCall();
        oneCall2.setId(oneCall1.getId());
        assertThat(oneCall1).isEqualTo(oneCall2);
        oneCall2.setId(2L);
        assertThat(oneCall1).isNotEqualTo(oneCall2);
        oneCall1.setId(null);
        assertThat(oneCall1).isNotEqualTo(oneCall2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(OneCallDTO.class);
        OneCallDTO oneCallDTO1 = new OneCallDTO();
        oneCallDTO1.setId(1L);
        OneCallDTO oneCallDTO2 = new OneCallDTO();
        assertThat(oneCallDTO1).isNotEqualTo(oneCallDTO2);
        oneCallDTO2.setId(oneCallDTO1.getId());
        assertThat(oneCallDTO1).isEqualTo(oneCallDTO2);
        oneCallDTO2.setId(2L);
        assertThat(oneCallDTO1).isNotEqualTo(oneCallDTO2);
        oneCallDTO1.setId(null);
        assertThat(oneCallDTO1).isNotEqualTo(oneCallDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(oneCallMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(oneCallMapper.fromId(null)).isNull();
    }
}
