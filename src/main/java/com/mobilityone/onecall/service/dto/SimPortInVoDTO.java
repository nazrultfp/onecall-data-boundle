package com.mobilityone.onecall.service.dto;

import com.mobilityone.onecall.config.Constants;
import com.mobilityone.onecall.security.SecurityUtils;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A DTO for the SimPortInVo entity.
 */
public class SimPortInVoDTO implements Serializable {

    private Long id;

    private String msisdn;

    private String simNumber;

    private String status;

    private ZonedDateTime createdDate = ZonedDateTime.now();

    private ZonedDateTime modifiedDate = ZonedDateTime.now();

    private String createdBy = SecurityUtils.getCurrentUserLogin().orElse(Constants.ANONYMOUS_USER);

    private String modifiedBy = SecurityUtils.getCurrentUserLogin().orElse(Constants.ANONYMOUS_USER);


    private Long simPortInId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getSimNumber() {
        return simNumber;
    }

    public void setSimNumber(String simNumber) {
        this.simNumber = simNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public ZonedDateTime getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(ZonedDateTime modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Long getSimPortInId() {
        return simPortInId;
    }

    public void setSimPortInId(Long simPortInId) {
        this.simPortInId = simPortInId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SimPortInVoDTO simPortInVoDTO = (SimPortInVoDTO) o;
        if (simPortInVoDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), simPortInVoDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SimPortInVoDTO{" +
            "id=" + getId() +
            ", msisdn='" + getMsisdn() + "'" +
            ", simNumber='" + getSimNumber() + "'" +
            ", status='" + getStatus() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", modifiedDate='" + getModifiedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", modifiedBy='" + getModifiedBy() + "'" +
            ", simPortIn=" + getSimPortInId() +
            "}";
    }
}
