package com.mobilityone.onecall.service.dto;

import com.mobilityone.onecall.domain.enumeration.Status;

import java.time.ZonedDateTime;

public class TopupTransactionReportDTO {

    private ZonedDateTime transactionDate;
    private String mechantCode;
    private String merchantName;
    private Double amount;
    private Status status;
    private String message;

    public TopupTransactionReportDTO(ZonedDateTime transactionDate, String mechantCode, String merchantName, Double amount, Status status, String message) {
        this.transactionDate = transactionDate;
        this.mechantCode = mechantCode;
        this.merchantName = merchantName;
        this.amount = amount;
        this.status = status;
        this.message = message;
    }

    public ZonedDateTime getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(ZonedDateTime transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getMechantCode() {
        return mechantCode;
    }

    public void setMechantCode(String mechantCode) {
        this.mechantCode = mechantCode;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
