package com.mobilityone.onecall.service;

import com.mobilityone.onecall.service.dto.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Service Interface for managing OneCall.
 */
public interface OneCallService {

    /**
     * Save a oneCall.
     *
     * @param oneCallDTO the entity to save
     * @return the persisted entity
     */
    OneCallDTO save(OneCallDTO oneCallDTO);

    /**
     * Get all the oneCalls.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<OneCallDTO> findAll(Pageable pageable);


    /**
     * Get the "id" oneCall.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<OneCallDTO> findOne(Long id);

    /**
     * Delete the "id" oneCall.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    OneCallDTO doTopup(OneCallDTO oneCallDTO);

    OneCallDTO getTopupByTransId(TopupOrderDTO topupOrderDTO);

    OneCallDTO getTopupValuesByMsisdn(TopupValuesDTO topupValuesDTO);

    TtBalabceRespDTO getBalanceByIccid(String iccid);

    TtBalabceRespDTO getBalanceByImsi(String imsi);

    TtBalabceRespDTO getBalanceByMsisdn(String msisdn);

    TuneTalkRespDTO lookupByEmail(String email);

    Page<OneCallDTO> getAllTransactionsByMerchantId(Pageable pageable, String merchantId);

    Object simOrder(SimOrderReqDTO simOrderReqDTO);

    Object simPortIn(SimPortInReqDTO simPortInReqDTO);

    Object simRegistration(SimRegReqDTO simRegReqDTO);

    Object simRegistrationWithoutKYC(SimRegReqDTO simRegReqDTO);

    SimPortInVoDTO setPortInStatusByMsisdn(String msisdn, String statusResp);

    List<Map<String, Object>> getSimHistory();

    SubscriptionPlanDTO performSubscription(SubscriptionReqDTO subscriptionReqDTO);

    TuneTalkRespDTO bigShotConvert(BigShotConvertReqDTO bigShotConvertReqDTO);

    List<Map<String, Object>> getSimStatusByMsisdn(String msisdn);

    List<Map<String, Object>> getDailyTotalTopupInRangeForClient(String client, String fromDate, String toDate);
}
