package com.mobilityone.onecall.repository;

import com.mobilityone.onecall.domain.SimOrder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the SimOrder entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SimOrderRepository extends JpaRepository<SimOrder, Long> {

    Page<SimOrder> findByAgentIdOrderByCreatedDateDesc(Pageable pageable, Long agentId);
}
