package com.mobilityone.onecall.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mobilityone.onecall.config.Constants;
import org.json.JSONObject;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class SimRegReqDTO {

    private Long agentId;

//    @NotNull
//    private String agentCode;

    @NotNull(message = "address can't be null")
    @Size(max = 255, message = "address must be less than 255 characters long.")
    private String address;

    private String blockedNumber;

    @NotNull(message = "city can't be null")
    @Size(max = 255, message = "city must be less than 255 characters long.")
    @Pattern(regexp = "^[A-Z ]*$", message = "city must be Uppercase alphabets")
    private String city;

    @NotNull(message = "country can't be null")
    @Size(max = 255, message = "country must be less than 255 characters long.")
    @Pattern(regexp = "^[A-Z ]*$", message = "country must be uppercase alphabets")
    private String country;


    @NotNull(message = "dob can't be null")
    private String dob;

    @NotNull(message = "emailAddr can't be null")
    @Size(max = 255, message = "emailAddr must be less than 255 characters long.")
    @Pattern(regexp = Constants.EMAIL_REGEX, message = "emailAddr must be a valid email address")
    private String emailAddr;

    @NotNull(message = "firstName can't be null")
    @Size(max = 255, message = "firstName must be less than 255 characters long.")
//    @Pattern(regexp = "^[a-zA-Z ]*$", message = "firstName must be alphabets")
    private String firstName;

    @NotNull(message = "gender can't be null")
    @Pattern(regexp = "MALE|FEMALE", message = "gender must be MALE or FEMALE")
    private String gender;

    @NotNull(message = "idNumber can't be null")
    @Size(max = 64, message = "idNumber must be less than 64 characters long.")
//    @Pattern(regexp = "^[0-9]*$", message = "Only Number is valid")
    private String idNumber;

    @NotNull(message = "idType can't be null")
    @Pattern(regexp = "MYKAD|PASSPORT", message = "idType must be MYKAD or PASSPORT")
    private String idType;

    @NotNull(message = "lastName can't be null")
    @Size(max = 255, message = "lastName must be less than 255 characters long.")
//    @Pattern(regexp = "^[a-zA-Z ]*$", message = "lastName must be alphabets")
    private String lastName;

    @NotNull(message = "nationality can't be null")
    @Size(max = 64, message = "nationality must be less than 64 characters long.")
    @Pattern(regexp = "^[A-Z ]*$", message = "nationality must be uppercase alphabets")
    private String nationality;
//    private String partnerDealerCode;


    private String photo;

    @NotNull(message = "postCode can't be null")
    @Pattern(regexp = "^[0-9]{5}$", message = "postCode must be 5 numbers")
    private String postCode;

    @NotNull(message = "simNumber can't be null")
//    @Pattern(regexp = "^(89)[0-9]{17,18}$", message = "simNumber must be 19 to 20 numbers and start with 89")
    private String simNumber;

    @NotNull(message = "state can't be null")
    @Size(max = 64, message = "state must be less than 64 characters long.")
    @Pattern(regexp = "^[A-Z ]*$", message = "state must be uppercase alphabets")
    private String state;

    private String transactionId;

    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

//    public String getAgentCode() {
//        return agentCode;
//    }
//
//    public void setAgentCode(String agentCode) {
//        this.agentCode = agentCode;
//    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBlockedNumber() {
        return blockedNumber;
    }

    public void setBlockedNumber(String blockedNumber) {
        this.blockedNumber = blockedNumber;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getEmailAddr() {
        return emailAddr;
    }

    public void setEmailAddr(String emailAddr) {
        this.emailAddr = emailAddr;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

//    public String getPartnerDealerCode() {
//        return partnerDealerCode;
//    }
//
//    public void setPartnerDealerCode(String partnerDealerCode) {
//        this.partnerDealerCode = partnerDealerCode;
//    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getSimNumber() {
        return simNumber;
    }

    public void setSimNumber(String simNumber) {
        this.simNumber = simNumber;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    @JsonIgnore
    public JSONObject getJson() {

        JSONObject jsonObject = new JSONObject();

        if (this.address != null) jsonObject.put("address", this.address);
        if (this.blockedNumber != null) jsonObject.put("blockedNumber", this.blockedNumber);
        if (this.city != null) jsonObject.put("city", this.city);
        if (this.country != null) jsonObject.put("country", this.country);
        if (this.dob != null) jsonObject.put("dob", this.dob);
        if (this.emailAddr != null) jsonObject.put("emailAddr", this.emailAddr);
        if (this.firstName != null) jsonObject.put("firstName", this.firstName);
        if (this.gender != null) jsonObject.put("gender", this.gender);
        if (this.idNumber != null) jsonObject.put("idNumber", this.idNumber);
        if (this.idType != null) jsonObject.put("idType", this.idType);
        if (this.lastName != null) jsonObject.put("lastName", this.lastName);
        if (this.nationality != null) jsonObject.put("nationality", this.nationality);
//        if (this.partnerDealerCode != null) jsonObject.put("partnerDealerCode", this.partnerDealerCode);
        if (this.photo != null) jsonObject.put("photos", new String[] {this.photo});
        if (this.postCode != null) jsonObject.put("postCode", this.postCode);
        if (this.simNumber != null) jsonObject.put("simNumber", this.simNumber);
        if (this.state != null) jsonObject.put("state", this.state);
        if (this.transactionId != null) jsonObject.put("transactionId", this.transactionId);

        return jsonObject;
    }

    @Override
    public String toString() {
        return "SimRegReqDTO{" +
            "agentId=" + agentId +
//            ", agentCode='" + agentCode + '\'' +
            ", address='" + address + '\'' +
            ", blockedNumber='" + blockedNumber + '\'' +
            ", city='" + city + '\'' +
            ", country='" + country + '\'' +
            ", dob='" + dob + '\'' +
            ", emailAddr='" + emailAddr + '\'' +
            ", firstName='" + firstName + '\'' +
            ", gender='" + gender + '\'' +
            ", idNumber='" + idNumber + '\'' +
            ", idType='" + idType + '\'' +
            ", lastName='" + lastName + '\'' +
            ", nationality='" + nationality + '\'' +
//            ", photo='" + photo + '\'' +
            ", postCode='" + postCode + '\'' +
            ", simNumber='" + simNumber + '\'' +
            ", state='" + state + '\'' +
            ", transactionId='" + transactionId + '\'' +
            '}';
    }
}
