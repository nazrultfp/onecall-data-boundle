package com.mobilityone.onecall.web.rest;

import com.mobilityone.onecall.service.TuneTalkDealerService;
import com.mobilityone.onecall.service.dto.*;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * REST controller for managing Dealer.
 */
//@RestController
//@RequestMapping("/api")
public class DealerResource {

    private final Logger log = LoggerFactory.getLogger(DealerResource.class);

    private static final String ENTITY_NAME = "oneCallDealer";

    private final TuneTalkDealerService dealerService;

    public DealerResource(TuneTalkDealerService dealerService) {
        this.dealerService = dealerService;
    }


    /**
     * @param historyReqDTO
     * @return Values for type include {register, replacement, portIn}
     */
    @ApiOperation(value = "Get History for Sim Registration, Sim Replacement and Sim Port In")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Request Successful!"),
        @ApiResponse(code = 401, message = "Empty Secret Key!"),
        @ApiResponse(code = 403, message = "Invalid Secret Key! or Restricted Access!"),
        @ApiResponse(code = 500, message = "Request Failed!")
    })
    @PostMapping(value = "/dealers/history", produces = "application/json")
    public ResponseEntity<?> simHistory(@Valid @RequestBody HistoryReqDTO historyReqDTO) {
        log.info("Request to get SIM History for : {}", historyReqDTO.toString());

        JSONObject jsonObject = dealerService.simHistory(historyReqDTO);

        log.info("Response : {}", jsonObject.toString());

        return ResponseEntity.status(HttpStatus.valueOf(Math.abs(Integer.parseInt(jsonObject.getString("code")))))
            .header("Content-Type: application/json; charset=UTF-8").body(jsonObject.toString());
    }

    @ApiOperation(value = "Get Port In Status By MSISDN")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Request Successful!"),
        @ApiResponse(code = 401, message = "Empty Secret Key!"),
        @ApiResponse(code = 403, message = "Invalid Secret Key! or Restricted Access!"),
        @ApiResponse(code = 500, message = "Request Failed!")
    })
    @GetMapping(value = "/dealers/port-in-status/{msisdn}", produces = "application/json")
    public ResponseEntity<?> portInStatus(@PathVariable String msisdn) {

        log.info("Get Port In Status By MSISDN : {}", msisdn);

        JSONObject jsonObject = dealerService.portInStatus(msisdn);

        log.info("Response : {}", jsonObject.toString());

        return ResponseEntity.status(HttpStatus.valueOf(Math.abs(Integer.parseInt(jsonObject.getString("code")))))
            .header("Content-Type: application/json; charset=UTF-8").body(jsonObject.toString());
    }

    @ApiOperation(value = "Cancel Sim Port In")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Request Successful!"),
        @ApiResponse(code = 401, message = "Empty Secret Key!"),
        @ApiResponse(code = 403, message = "Invalid Secret Key! or Restricted Access!"),
        @ApiResponse(code = 500, message = "Request Failed!")
    })
    @PostMapping(value = "/dealers/cancel-port-in", produces = "application/json")
    public ResponseEntity<?> simCancelPortIn(@Valid @RequestBody SimCancelPortInReqDTO simCancelPortInReqDTO) {
        log.info("Cancel Sim Port In : {}", simCancelPortInReqDTO.toString());

        JSONObject jsonObject = dealerService.simCancelPortIn(simCancelPortInReqDTO);

        log.info("Response : {}", jsonObject.toString());

        return ResponseEntity.status(HttpStatus.valueOf(Math.abs(Integer.parseInt(jsonObject.getString("code")))))
            .header("Content-Type: application/json; charset=UTF-8").body(jsonObject.toString());
    }

    @ApiOperation(value = "Resubmit Sim Port In")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Request Successful!"),
        @ApiResponse(code = 401, message = "Empty Secret Key!"),
        @ApiResponse(code = 403, message = "Invalid Secret Key! or Restricted Access!"),
        @ApiResponse(code = 500, message = "Request Failed!")
    })
    @GetMapping(value = "/dealers/sim-resubmit/{portInId}", produces = "application/json")
    public ResponseEntity<TuneTalkRespDTO> simResubmitPortIn(@PathVariable Long portInId) {
        log.info("Resubmit Sim Port In : {}", portInId.toString());

        TuneTalkRespDTO tuneTalkRespDTO = dealerService.simResubmitPortIn(portInId);

        log.info("Response : {}", tuneTalkRespDTO.toString());

        return ResponseEntity.status(HttpStatus.valueOf(Math.abs(Integer.parseInt(tuneTalkRespDTO.getCode()))))
            .header("Content-Type: application/json; charset=UTF-8").body(tuneTalkRespDTO);
    }

    @ApiOperation(value = "Perform Subscription PT 1")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Request Successful!"),
        @ApiResponse(code = 401, message = "Empty Secret Key!"),
        @ApiResponse(code = 403, message = "Invalid Secret Key! or Restricted Access!"),
        @ApiResponse(code = 500, message = "Request Failed!")
    })
    @PostMapping(value = "/dealers/subscription", produces = "application/json")
    public ResponseEntity<TuneTalkRespDTO> performSubscription(@Valid @RequestBody DealerSubscriptionReqDTO dealerSubscriptionReqDTO) {
        log.info("Perform Subscription PT 1 : {}", dealerSubscriptionReqDTO.toString());

        TuneTalkRespDTO tuneTalkRespDTO = dealerService.performSubscription(dealerSubscriptionReqDTO);

        log.info("Response : {}", tuneTalkRespDTO.toString());

        return ResponseEntity.status(HttpStatus.valueOf(Math.abs(Integer.parseInt(tuneTalkRespDTO.getCode()))))
            .header("Content-Type: application/json; charset=UTF-8").body(tuneTalkRespDTO);
    }

    @ApiOperation(value = "Get Subscription Status By Transaction Id")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Request Successful!"),
        @ApiResponse(code = 401, message = "Empty Secret Key!"),
        @ApiResponse(code = 403, message = "Invalid Secret Key! or Restricted Access!"),
        @ApiResponse(code = 500, message = "Request Failed!")
    })
    @GetMapping(value = "/dealers/subscription-status/{transactionId}", produces = "application/json")
    public ResponseEntity<?> getSubscriptionStatusByTransactionId(@PathVariable String transactionId) {
        log.info("Get Subscription Status By Transaction Id : {}", transactionId);

        JSONObject jsonObject = dealerService.getSubscriptionStatusByTransactionId(transactionId);

        log.info("Response : {}", jsonObject.toString());

        return ResponseEntity.status(HttpStatus.valueOf(Math.abs(Integer.parseInt(jsonObject.getString("code")))))
            .header("Content-Type: application/json; charset=UTF-8").body(jsonObject.toString());
    }

    @ApiOperation(value = "Get Telco List")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Request Successful!"),
        @ApiResponse(code = 401, message = "Empty Secret Key!"),
        @ApiResponse(code = 403, message = "Invalid Secret Key! or Restricted Access!"),
        @ApiResponse(code = 500, message = "Request Failed!")
    })
    @GetMapping(value = "/dealers/telco-list", produces = "application/json")
    public ResponseEntity<?> getTelcoList() {
        log.info("Get Telco List");

        JSONObject jsonObject = dealerService.getTelcoList();

        log.info("Response : {}", jsonObject.toString());

        return ResponseEntity.status(HttpStatus.valueOf(Math.abs(Integer.parseInt(jsonObject.getString("code")))))
            .header("Content-Type: application/json; charset=UTF-8").body(jsonObject.toString());
    }

    @ApiOperation(value = "Perform Top up")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Request Successful!"),
        @ApiResponse(code = 401, message = "Empty Secret Key!"),
        @ApiResponse(code = 403, message = "Invalid Secret Key! or Restricted Access!"),
        @ApiResponse(code = 500, message = "Request Failed!")
    })
    @PostMapping(value = "/dealers/topup", produces = "application/json")
    public ResponseEntity<TuneTalkTopupRespDTO> performTopup(@Valid @RequestBody DealerTopupReqDTO dealerTopupReqDTO) {
        log.info("Perform topup : {}", dealerTopupReqDTO.toString());

        TuneTalkTopupRespDTO tuneTalkTopupRespDTO = dealerService.performTopup(dealerTopupReqDTO);

        log.info("Response : {}", tuneTalkTopupRespDTO);

        return ResponseEntity.status(HttpStatus.valueOf(Math.abs(Integer.parseInt(tuneTalkTopupRespDTO.getCode()))))
            .header("Content-Type: application/json; charset=UTF-8").body(tuneTalkTopupRespDTO);
    }

    @ApiOperation(value = "Get Topup Deno List")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Request Successful!"),
        @ApiResponse(code = 401, message = "Empty Secret Key!"),
        @ApiResponse(code = 403, message = "Invalid Secret Key! or Restricted Access!"),
        @ApiResponse(code = 500, message = "Request Failed!")
    })
    @GetMapping(value = "/dealers/topupValues/{msisdn}", produces = "application/json")
    public ResponseEntity<?> getTopupValues(@PathVariable String msisdn) {
        log.info("Get Topup Deno List for MSISDN : {}", msisdn);

        JSONObject jsonObject = dealerService.getTopupValues(msisdn);

        log.info("Response : {}", jsonObject.toString());

        return ResponseEntity.status(HttpStatus.valueOf(Math.abs(Integer.parseInt(jsonObject.getString("code")))))
            .header("Content-Type: application/json; charset=UTF-8").body(jsonObject.toString());
    }
}
