package com.mobilityone.onecall.service.dto;

import javax.validation.constraints.NotNull;
import java.util.Objects;

public class TopupDTO {

    private String channelId;

    @NotNull
    private String merchantId;

    private String merchantOrderNo;

    private String msisdn;

    private String topupDeno;

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantOrderNo() {
        return merchantOrderNo;
    }

    public void setMerchantOrderNo(String merchantOrderNo) {
        this.merchantOrderNo = merchantOrderNo;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getTopupDeno() {
        return topupDeno;
    }

    public void setTopupDeno(String topupDeno) {
        this.topupDeno = topupDeno;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TopupDTO topupDTO = (TopupDTO) o;
        return Objects.equals(channelId, topupDTO.channelId) &&
            Objects.equals(merchantId, topupDTO.merchantId) &&
            Objects.equals(merchantOrderNo, topupDTO.merchantOrderNo) &&
            Objects.equals(msisdn, topupDTO.msisdn) &&
            Objects.equals(topupDeno, topupDTO.topupDeno);
    }

    @Override
    public int hashCode() {
        return Objects.hash(channelId, merchantId, merchantOrderNo, msisdn, topupDeno);
    }

    @Override
    public String toString() {
        return "TopupDTO{" +
            ", channelId='" + channelId + '\'' +
            ", merchantId='" + merchantId + '\'' +
            ", merchantOrderNo='" + merchantOrderNo + '\'' +
            ", msisdn='" + msisdn + '\'' +
            ", topupDeno='" + topupDeno + '\'' +
            '}';
    }
}
