package com.mobilityone.onecall.web.rest;

import com.mobilityone.onecall.service.SimPortInService;
import com.mobilityone.onecall.web.rest.errors.BadRequestAlertException;
import com.mobilityone.onecall.web.rest.util.HeaderUtil;
import com.mobilityone.onecall.web.rest.util.PaginationUtil;
import com.mobilityone.onecall.service.dto.SimPortInDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * REST controller for managing SimPortIn.
 */
@RestController
@RequestMapping("/api")
public class SimPortInResource {

    private final Logger log = LoggerFactory.getLogger(SimPortInResource.class);

    private static final String ENTITY_NAME = "oneCallSimPortIn";

    private final SimPortInService simPortInService;

    public SimPortInResource(SimPortInService simPortInService) {
        this.simPortInService = simPortInService;
    }

    /**
     * POST  /sim-port-ins : Create a new simPortIn.
     *
     * @param simPortInDTO the simPortInDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new simPortInDTO, or with status 400 (Bad Request) if the simPortIn has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    //@PostMapping("/sim-port-ins")
    public ResponseEntity<SimPortInDTO> createSimPortIn(@RequestBody SimPortInDTO simPortInDTO) throws URISyntaxException {
        log.debug("REST request to save SimPortIn : {}", simPortInDTO);
        if (simPortInDTO.getId() != null) {
            throw new BadRequestAlertException("A new simPortIn cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SimPortInDTO result = simPortInService.save(simPortInDTO);
        return ResponseEntity.created(new URI("/api/sim-port-ins/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /sim-port-ins : Updates an existing simPortIn.
     *
     * @param simPortInDTO the simPortInDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated simPortInDTO,
     * or with status 400 (Bad Request) if the simPortInDTO is not valid,
     * or with status 500 (Internal Server Error) if the simPortInDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    //@PutMapping("/sim-port-ins")
    public ResponseEntity<SimPortInDTO> updateSimPortIn(@RequestBody SimPortInDTO simPortInDTO) throws URISyntaxException {
        log.debug("REST request to update SimPortIn : {}", simPortInDTO);
        if (simPortInDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SimPortInDTO result = simPortInService.save(simPortInDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, simPortInDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /sim-port-ins : get all the simPortIns.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of simPortIns in body
     */
    @GetMapping("/sim-port-ins")
    public ResponseEntity<List<SimPortInDTO>> getAllSimPortIns(Pageable pageable) {
        log.debug("REST request to get a page of SimPortIns");
        Page<SimPortInDTO> page = simPortInService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sim-port-ins");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /sim-port-ins/:id : get the "id" simPortIn.
     *
     * @param id the id of the simPortInDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the simPortInDTO, or with status 404 (Not Found)
     */
    @GetMapping("/sim-port-ins/{id}")
    public ResponseEntity<SimPortInDTO> getSimPortIn(@PathVariable Long id) {
        log.debug("REST request to get SimPortIn : {}", id);
        Optional<SimPortInDTO> simPortInDTO = simPortInService.findOne(id);
        return ResponseUtil.wrapOrNotFound(simPortInDTO);
    }

    /**
     * DELETE  /sim-port-ins/:id : delete the "id" simPortIn.
     *
     * @param id the id of the simPortInDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    //@DeleteMapping("/sim-port-ins/{id}")
    public ResponseEntity<Void> deleteSimPortIn(@PathVariable Long id) {
        log.debug("REST request to delete SimPortIn : {}", id);
        simPortInService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }


    //@GetMapping("/sim-port-ins/by-agent/{agentId}")
    public ResponseEntity<List<SimPortInDTO>> getAllSimPortInsByAgent(Pageable pageable, @PathVariable Long agentId) {
        log.debug("REST request to get a page of SimPortIns by agent Id : {}", agentId);
        Page<SimPortInDTO> page = simPortInService.findAllByAgentId(pageable, agentId);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sim-port-ins/by-agent");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }


    @GetMapping("/sim-port-ins/recall-port-in/{portInId}")
    public ResponseEntity<SimPortInDTO> recallSimPortIn(@PathVariable Long portInId) {
        log.debug("REST request to recall TT SimPortIn by PortIn Id : {}", portInId);

        SimPortInDTO simPortInDTO = simPortInService.recallSimPortIn(portInId);

        int abs = Math.abs(Integer.parseInt(simPortInDTO.getCode()));
        abs = abs < 0 ? abs * -1 : abs;
        log.info("code abs : {}", abs);


        return ResponseEntity.status(HttpStatus.valueOf(abs))
            .header("Content-Type: application/json; charset=UTF-8").body(simPortInDTO);
    }


    @GetMapping("/sim-port-ins/total-by-date/{year}/{month}/{day}")
    public ResponseEntity<String> getTotalPortInsByDate(@PathVariable Integer year, @PathVariable Integer month, @PathVariable Integer day) {
        log.info("REST request to get total number of port-in sims for {}-{}-{}.", year, month, day);

        JSONObject object = simPortInService.getTotalPortInsByDate(year, month, day);

        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON_UTF8).body(object.toString(2));
    }


    @GetMapping("/sim-port-ins/port-in-by/{agentCode}")
    public ResponseEntity<List<SimPortInDTO>> getAllByAgentCode(@PathVariable String agentCode){
        log.info("REST request to get all port in sims by agent code : {}", agentCode);

        List<SimPortInDTO> list = simPortInService.getAllByAgentCode(agentCode);

        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON_UTF8).body(list);
    }


    @GetMapping("/sim-port-ins/in-range/{fromDate}/{toDate}")
    public ResponseEntity<List<Map<String, Object>>> getAllInRangeDate(@PathVariable String fromDate , @PathVariable String toDate){
        log.info("REST request to get all port in sims in range from {} to {}", fromDate, toDate);

        List<Map<String, Object>> list = simPortInService.getAllInRangeDate(fromDate, toDate);

        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON_UTF8).body(list);
    }


    @GetMapping("/sim-port-ins/agent-daily-total-in-range/{agentCode}/{fromDate}/{toDate}")
    public ResponseEntity<List<Map<String, Object>>> getAgentDailyTotalSimRegInRange(@PathVariable String agentCode, @PathVariable String fromDate , @PathVariable String toDate){
        log.info("REST request to get agent {} daily total port in sims in range from {} to {}", agentCode, fromDate, toDate);

        List<Map<String, Object>> list = simPortInService.getAgentDailyTotalSimRegInRange(agentCode, fromDate,toDate);

        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON_UTF8).body(list);
    }
}
