package com.mobilityone.onecall.service;

import com.mobilityone.onecall.service.dto.SimRegReqDTO;
import com.mobilityone.onecall.service.dto.SimRegisterDTO;

import com.mobilityone.onecall.service.dto.SimRegisterReportDTO;
import org.json.JSONObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Service Interface for managing SimRegister.
 */
public interface SimRegisterService {

    /**
     * Save a simRegister.
     *
     * @param simRegisterDTO the entity to save
     * @return the persisted entity
     */
    SimRegisterDTO save(SimRegisterDTO simRegisterDTO);

    /**
     * Get all the simRegisters.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<SimRegisterDTO> findAll(Pageable pageable);


    /**
     * Get the "id" simRegister.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<SimRegisterDTO> findOne(Long id);

//    Optional<SimRegisterDTO> findOneByRegId(Long regId);

    /**
     * Delete the "id" simRegister.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    SimRegisterDTO save(SimRegReqDTO simRegReqDTO);

    SimRegisterDTO save(Long simRegId, JSONObject jsonObject);

//    SimRegisterDTO save(SimRegReqDTO simRegReqDTO, JSONObject jsonObject);

    Page<SimRegisterDTO> findByAgentId(Pageable pageable, Long agentId);

    List<Map<String, Object>> simHistory();

    List<Map<String, Object>> getSimStatusByMsisdn(String msisdn);

    Page<SimRegisterReportDTO> getReportOfAllSimRegisters(Pageable pageable);

    JSONObject getTotalNoByDate(Integer year, Integer month, Integer day);

    List<SimRegisterReportDTO> getAllByAgentCode(String agentCode);

    List<Map<String, Object>> getAllInRangeDate(String fromDate, String toDate);

    List<Map<String, Object>> getSimStatusByAgentCode(String agentCode);

    List<Map<String, Object>> getDailyTotalSimInRange(String fromDate, String toDate);

    List<Map<String, Object>> getAgentDailyTotalSimRegInRange(String agentCode, String fromDate, String toDate);

    List<Map<String, Object>> getSimRegsInRange(String fromDate, String toDate);
}
