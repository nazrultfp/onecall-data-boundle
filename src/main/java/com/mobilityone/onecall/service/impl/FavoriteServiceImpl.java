package com.mobilityone.onecall.service.impl;

import com.mobilityone.onecall.service.FavoriteService;
import com.mobilityone.onecall.domain.Favorite;
import com.mobilityone.onecall.repository.FavoriteRepository;
import com.mobilityone.onecall.service.dto.FavoriteDTO;
import com.mobilityone.onecall.service.mapper.FavoriteMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

/**
 * Service Implementation for managing Favorite.
 */
@Service
@Transactional
public class FavoriteServiceImpl implements FavoriteService {

    private final Logger log = LoggerFactory.getLogger(FavoriteServiceImpl.class);

    private final FavoriteRepository favoriteRepository;

    private final FavoriteMapper favoriteMapper;

    public FavoriteServiceImpl(FavoriteRepository favoriteRepository, FavoriteMapper favoriteMapper) {
        this.favoriteRepository = favoriteRepository;
        this.favoriteMapper = favoriteMapper;
    }

    /**
     * Save a favorite.
     *
     * @param favoriteDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public FavoriteDTO save(FavoriteDTO favoriteDTO) {
        log.debug("Request to save Favorite : {}", favoriteDTO);
        Favorite favorite = favoriteMapper.toEntity(favoriteDTO);
        favorite = favoriteRepository.save(favorite);
        return favoriteMapper.toDto(favorite);
    }

    @Override
    public FavoriteDTO update(FavoriteDTO favoriteDTO) {
        Optional<Favorite> favoriteOpt = favoriteRepository.findById(favoriteDTO.getId());
        if (!favoriteOpt.isPresent())
        {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "favorite not found");
        }

        Favorite favorite = favoriteOpt.get();

        if (favorite.getPhoneNo() != null && favoriteDTO.getPhoneNo() != null && !favoriteDTO.getPhoneNo().equalsIgnoreCase(favorite.getPhoneNo()))
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "phoneNo doesn't match");
        if (favoriteDTO.getProdCode() != null)
            favorite.setProdCode(favoriteDTO.getProdCode());
        if (favoriteDTO.getProdSubCode() != null)
            favorite.setProdSubCode(favoriteDTO.getProdSubCode());
        if (favoriteDTO.getCatType() != null)
            favorite.setCatType(favoriteDTO.getCatType());
        if (favoriteDTO.getProdCodeAlt() != null)
            favorite.setProdCodeAlt(favoriteDTO.getProdCodeAlt());
        if (favoriteDTO.getProdDesc() != null)
            favorite.setProdDesc(favoriteDTO.getProdDesc());
        if (favoriteDTO.getProdDescLong() != null)
            favorite.setProdDescLong(favoriteDTO.getProdDescLong());
        if (favoriteDTO.getAccountNumber() != null)
            favorite.setAccountNumber(favoriteDTO.getAccountNumber());

        favorite = favoriteRepository.save(favorite);

        log.debug("Saved Data : {}", favorite);

        return favoriteMapper.toDto(favorite);
    }

    /**
     * Get all the favorites.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<FavoriteDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Favorites");
        return favoriteRepository.findAll(pageable)
            .map(favoriteMapper::toDto);
    }


    /**
     * Get one favorite by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<FavoriteDTO> findOne(Long id) {
        log.debug("Request to get Favorite : {}", id);
        return favoriteRepository.findById(id)
            .map(favoriteMapper::toDto);
    }

    /**
     * Delete the favorite by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Favorite : {}", id);        favoriteRepository.deleteById(id);
    }

    @Override
    public Page<FavoriteDTO> findPhoneNoFavorites(Pageable pageable, String phoneNo) {
        log.debug("Request to get all Favorites for phoneNo : {}", phoneNo);
        return favoriteRepository.findByPhoneNo(pageable, phoneNo)
            .map(favoriteMapper::toDto);
    }
}
