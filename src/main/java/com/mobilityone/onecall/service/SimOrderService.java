package com.mobilityone.onecall.service;

import com.mobilityone.onecall.service.dto.SimOrderDTO;
import com.mobilityone.onecall.service.dto.SimOrderReqDTO;
import org.json.JSONObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing SimOrder.
 */
public interface SimOrderService {

    /**
     * Save a simOrder.
     *
     * @param simOrderDTO the entity to save
     * @return the persisted entity
     */
    SimOrderDTO save(SimOrderDTO simOrderDTO);

    /**
     * Get all the simOrders.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<SimOrderDTO> findAll(Pageable pageable);


    /**
     * Get the "id" simOrder.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<SimOrderDTO> findOne(Long id);

    /**
     * Delete the "id" simOrder.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    SimOrderDTO save(SimOrderReqDTO simOrderReqDTO);

    SimOrderDTO save(Long simOrderReqDTO, JSONObject jsonObject);

    Page<SimOrderDTO> findAllByAgentId(Pageable pageable, Long agentId);
}
