package com.mobilityone.onecall.service.mapper;

import com.mobilityone.onecall.domain.SimOrder;
import com.mobilityone.onecall.service.dto.SimOrderDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity SimOrder and its DTO SimOrderDTO.
 */
@Mapper(componentModel = "spring", uses = {AgentMapper.class})
public interface SimOrderMapper extends EntityMapper<SimOrderDTO, SimOrder> {

    @Mapping(source = "agent.id", target = "agentId")
    SimOrderDTO toDto(SimOrder simOrder);

    @Mapping(source = "agentId", target = "agent")
    SimOrder toEntity(SimOrderDTO simOrderDTO);

    default SimOrder fromId(Long id) {
        if (id == null) {
            return null;
        }
        SimOrder simOrder = new SimOrder();
        simOrder.setId(id);
        return simOrder;
    }
}
