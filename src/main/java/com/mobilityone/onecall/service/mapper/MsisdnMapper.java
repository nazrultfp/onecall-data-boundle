package com.mobilityone.onecall.service.mapper;

import com.mobilityone.onecall.domain.MsisdnValue;
import com.mobilityone.onecall.service.dto.TuneTalkValueDTO;
import org.mapstruct.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@Mapper(componentModel = "spring", uses = {})
public class MsisdnMapper implements EntityMapper<TuneTalkValueDTO, MsisdnValue> {

    private final Logger log = LoggerFactory.getLogger(MsisdnMapper.class);

    @Override
    public MsisdnValue toEntity(TuneTalkValueDTO dto) {
        MsisdnValue value = new MsisdnValue();

        value.setAmount(Double.valueOf(dto.getAmount()));
        value.setFree(dto.getFree());
        value.setFreeData(dto.getFreeData());
        value.setFreeSms(dto.getFreeSms());
        value.setFreeVoice(dto.getFreeVoice());
        value.setDataId(dto.getId());
        value.setPoint(dto.getPoint());
        value.setValidity(dto.getValidity());

        return value;
    }

    @Override
    public TuneTalkValueDTO toDto(MsisdnValue entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<MsisdnValue> toEntity(List<TuneTalkValueDTO> dtoList) {
        return dtoList.stream().map(new MsisdnMapper()::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<TuneTalkValueDTO> toDto(List<MsisdnValue> entityList) {
        throw new UnsupportedOperationException();
    }
}
