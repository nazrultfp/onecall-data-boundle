package com.mobilityone.onecall.web.rest;

import com.mobilityone.onecall.service.SimRegisterService;
import com.mobilityone.onecall.service.dto.SimRegisterReportDTO;
import com.mobilityone.onecall.web.rest.errors.BadRequestAlertException;
import com.mobilityone.onecall.web.rest.util.HeaderUtil;
import com.mobilityone.onecall.web.rest.util.PaginationUtil;
import com.mobilityone.onecall.service.dto.SimRegisterDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * REST controller for managing SimRegister.
 */
@RestController
@RequestMapping("/api")
public class SimRegisterResource {

    private final Logger log = LoggerFactory.getLogger(SimRegisterResource.class);

    private static final String ENTITY_NAME = "oneCallSimRegister";

    private final SimRegisterService simRegisterService;

    public SimRegisterResource(SimRegisterService simRegisterService) {
        this.simRegisterService = simRegisterService;
    }

    /**
     * POST  /sim-registers : Create a new simRegister.
     *
     * @param simRegisterDTO the simRegisterDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new simRegisterDTO, or with status 400 (Bad Request) if the simRegister has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    //@PostMapping("/sim-registers")
    public ResponseEntity<SimRegisterDTO> createSimRegister(@RequestBody SimRegisterDTO simRegisterDTO) throws URISyntaxException {
        log.debug("REST request to save SimRegister : {}", simRegisterDTO);
        if (simRegisterDTO.getId() != null) {
            throw new BadRequestAlertException("A new simRegister cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SimRegisterDTO result = simRegisterService.save(simRegisterDTO);
        return ResponseEntity.created(new URI("/api/sim-registers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /sim-registers : Updates an existing simRegister.
     *
     * @param simRegisterDTO the simRegisterDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated simRegisterDTO,
     * or with status 400 (Bad Request) if the simRegisterDTO is not valid,
     * or with status 500 (Internal Server Error) if the simRegisterDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    //@PutMapping("/sim-registers")
    public ResponseEntity<SimRegisterDTO> updateSimRegister(@RequestBody SimRegisterDTO simRegisterDTO) throws URISyntaxException {
        log.debug("REST request to update SimRegister : {}", simRegisterDTO);
        if (simRegisterDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SimRegisterDTO result = simRegisterService.save(simRegisterDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, simRegisterDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /sim-registers : get all the simRegisters.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of simRegisters in body
     */
    @GetMapping("/sim-registers")
    public ResponseEntity<List<SimRegisterDTO>> getAllSimRegisters(Pageable pageable) {
        log.debug("REST request to get a page of SimRegisters");
        Page<SimRegisterDTO> page = simRegisterService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sim-registers");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /sim-registers/:id : get the "id" simRegister.
     *
     * @param id the id of the simRegisterDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the simRegisterDTO, or with status 404 (Not Found)
     */
    @GetMapping("/sim-registers/{id}")
    public ResponseEntity<SimRegisterDTO> getSimRegister(@PathVariable Long id) {
        log.debug("REST request to get SimRegister : {}", id);
        Optional<SimRegisterDTO> simRegisterDTO = simRegisterService.findOne(id);
        return ResponseUtil.wrapOrNotFound(simRegisterDTO);
    }

    /**
     * DELETE  /sim-registers/:id : delete the "id" simRegister.
     *
     * @param id the id of the simRegisterDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    //@DeleteMapping("/sim-registers/{id}")
    public ResponseEntity<Void> deleteSimRegister(@PathVariable Long id) {
        log.debug("REST request to delete SimRegister : {}", id);
        simRegisterService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/sim-registers/by-agent/{agentId}")
    public ResponseEntity<List<SimRegisterDTO>> getAllSimRegistersByAgent(Pageable pageable, @PathVariable Long agentId) {
        log.debug("REST request to get a page of SimRegisters");
        Page<SimRegisterDTO> page = simRegisterService.findByAgentId(pageable, agentId);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sim-registers/by-agent");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }


    @GetMapping("/sim-registers/report-all")
    public ResponseEntity<List<SimRegisterReportDTO>> getReportOfAllSimRegisters(Pageable pageable) {
        log.debug("REST request to get a page of SimRegisters");
        Page<SimRegisterReportDTO> page = simRegisterService.getReportOfAllSimRegisters(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sim-registers/report-all");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }


    @GetMapping("/sim-registers/total-by-date/{year}/{month}/{day}")
    public ResponseEntity<String> getTotalNoByDate(@PathVariable Integer year, @PathVariable Integer month, @PathVariable Integer day) {
        log.info("REST request to get total number of registered sims for {}-{}-{}.", year, month, day);

        JSONObject object = simRegisterService.getTotalNoByDate(year,month,day);

        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON_UTF8).body(object.toString(2));
    }


    @GetMapping("/sim-registers/registered-by/{agentCode}")
    public ResponseEntity<List<SimRegisterReportDTO>> getAllByAgentCode(@PathVariable String agentCode){
        log.info("REST request to get all registered sims by agent code : {}", agentCode);

        List<SimRegisterReportDTO> list = simRegisterService.getAllByAgentCode(agentCode);

        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON_UTF8).body(list);
    }


    @GetMapping("/sim-registers/in-range/{fromDate}/{toDate}")
    public ResponseEntity<List<Map<String, Object>>> getAllInRangeDate(@PathVariable String fromDate , @PathVariable String toDate){
        log.info("REST request to get all registered sims in range from {} to {}", fromDate, toDate);

        List<Map<String, Object>> list = simRegisterService.getAllInRangeDate(fromDate, toDate);

        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON_UTF8).body(list);
    }



    @GetMapping("/sim-registers/status-by/{agentCode}")
    public ResponseEntity<List<Map<String, Object>>> getSimStatusByAgentCode(@PathVariable String agentCode){
        log.info("REST request to get all registered sims status by agent code : {}", agentCode);

        List<Map<String, Object>> list = simRegisterService.getSimStatusByAgentCode(agentCode);

        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON_UTF8).body(list);
    }


    @GetMapping("/sim-registers/daily-total-in-range/{fromDate}/{toDate}")
    public ResponseEntity<List<Map<String, Object>>> getDailyTotalSimInRange(@PathVariable String fromDate , @PathVariable String toDate){
        log.info("REST request to get daily total registered sims and port in sims in range from {} to {}", fromDate, toDate);

        List<Map<String, Object>> list = simRegisterService.getDailyTotalSimInRange(fromDate,toDate);

        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON_UTF8).body(list);
    }


    @GetMapping("/sim-registers/agent-daily-total-in-range/{agentCode}/{fromDate}/{toDate}")
    public ResponseEntity<List<Map<String, Object>>> getAgentDailyTotalSimRegInRange(@PathVariable String agentCode, @PathVariable String fromDate , @PathVariable String toDate){
        log.info("REST request to get agent {} daily total registered sims in range from {} to {}", agentCode, fromDate, toDate);

        List<Map<String, Object>> list = simRegisterService.getAgentDailyTotalSimRegInRange(agentCode, fromDate,toDate);

        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON_UTF8).body(list);
    }


    @GetMapping("/sim-registers/simregs-in-range/{fromDate}/{toDate}")
    public ResponseEntity<List<Map<String, Object>>> getSimRegsInRange(@PathVariable String fromDate , @PathVariable String toDate){
        log.info("REST request to get registered sims in range from {} to {}", fromDate, toDate);

        List<Map<String, Object>> list = simRegisterService.getSimRegsInRange(fromDate,toDate);

        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON_UTF8).body(list);
    }
}
