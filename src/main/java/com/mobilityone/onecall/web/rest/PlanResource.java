package com.mobilityone.onecall.web.rest;

import com.mobilityone.onecall.service.PlanService;
import com.mobilityone.onecall.web.rest.errors.BadRequestAlertException;
import com.mobilityone.onecall.web.rest.util.HeaderUtil;
import com.mobilityone.onecall.web.rest.util.PaginationUtil;
import com.mobilityone.onecall.service.dto.PlanDTO;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Plan.
 */
@RestController
@RequestMapping("/api")
public class PlanResource {

    private final Logger log = LoggerFactory.getLogger(PlanResource.class);

    private static final String ENTITY_NAME = "oneCallPlan";

    private final PlanService planService;

    public PlanResource(PlanService planService) {
        this.planService = planService;
    }

    /**
     * POST  /plans : Create a new plan.
     *
     * @param planDTO the planDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new planDTO, or with status 400 (Bad Request) if the plan has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    //@PostMapping("/plans")
    public ResponseEntity<PlanDTO> createPlan(@RequestBody PlanDTO planDTO) throws URISyntaxException {
        log.debug("REST request to save Plan : {}", planDTO);
        if (planDTO.getId() != null) {
            throw new BadRequestAlertException("A new plan cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PlanDTO result = planService.save(planDTO);
        return ResponseEntity.created(new URI("/api/plans/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /plans : Updates an existing plan.
     *
     * @param planDTO the planDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated planDTO,
     * or with status 400 (Bad Request) if the planDTO is not valid,
     * or with status 500 (Internal Server Error) if the planDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    //@PutMapping("/plans")
    public ResponseEntity<PlanDTO> updatePlan(@RequestBody PlanDTO planDTO) throws URISyntaxException {
        log.debug("REST request to update Plan : {}", planDTO);
        if (planDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PlanDTO result = planService.save(planDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, planDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /plans : get all the plans.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of plans in body
     */
    @ApiOperation(value = "get all plans")
    @GetMapping("/plans")
    public ResponseEntity<List<PlanDTO>> getAllPlans(Pageable pageable) {
        log.debug("REST request to get a page of Plans");
        Page<PlanDTO> page = planService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/plans");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /onecall-plans : get all the onecall plans.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of plans in body
     */
    @ApiOperation(value = "get all onecall plans")
    @GetMapping("/onecall-plans")
    public ResponseEntity<List<PlanDTO>> getAllOneCallPlans(Pageable pageable) {
        log.debug("REST request to get a page of onecall Plans");
        Page<PlanDTO> page = planService.findAllOneCallPlans(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/onecall-plans");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /plans/:id : get the "id" plan.
     *
     * @param id the id of the planDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the planDTO, or with status 404 (Not Found)
     */
    @ApiOperation(value = "get a plan by id")
    @GetMapping("/plans/{id}")
    public ResponseEntity<PlanDTO> getPlan(@PathVariable Long id) {
        log.debug("REST request to get Plan : {}", id);
        Optional<PlanDTO> planDTO = planService.findOne(id);
        return ResponseUtil.wrapOrNotFound(planDTO);
    }

    /**
     * DELETE  /plans/:id : delete the "id" plan.
     *
     * @param id the id of the planDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    //@DeleteMapping("/plans/{id}")
    public ResponseEntity<Void> deletePlan(@PathVariable Long id) {
        log.debug("REST request to delete Plan : {}", id);
        planService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
