package com.mobilityone.onecall.web.rest;

import com.mobilityone.onecall.service.PlanTransactionService;
import com.mobilityone.onecall.service.dto.PlanTransactionDTO;
import com.mobilityone.onecall.service.dto.PlanTransactionReportDTO;
import com.mobilityone.onecall.web.rest.errors.BadRequestAlertException;
import com.mobilityone.onecall.web.rest.util.HeaderUtil;
import com.mobilityone.onecall.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing PlanTransaction.
 */
@RestController
@RequestMapping("/api")
public class PlanTransactionResource {

    private final Logger log = LoggerFactory.getLogger(PlanTransactionResource.class);

    private static final String ENTITY_NAME = "oneCallPlanTransaction";

    private final PlanTransactionService planTransactionService;

    public PlanTransactionResource(PlanTransactionService planTransactionService) {
        this.planTransactionService = planTransactionService;
    }

    /**
     * POST  /plan-transactions : Create a new planTransaction.
     *
     * @param planTransactionDTO the planTransactionDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new planTransactionDTO, or with status 400 (Bad Request) if the planTransaction has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    //@PostMapping("/plan-transactions")
    public ResponseEntity<PlanTransactionDTO> createPlanTransaction(@RequestBody PlanTransactionDTO planTransactionDTO) throws URISyntaxException {
        log.debug("REST request to save PlanTransaction : {}", planTransactionDTO);
        if (planTransactionDTO.getId() != null) {
            throw new BadRequestAlertException("A new planTransaction cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PlanTransactionDTO result = planTransactionService.save(planTransactionDTO);
        return ResponseEntity.created(new URI("/api/plan-transactions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /plan-transactions : Updates an existing planTransaction.
     *
     * @param planTransactionDTO the planTransactionDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated planTransactionDTO,
     * or with status 400 (Bad Request) if the planTransactionDTO is not valid,
     * or with status 500 (Internal Server Error) if the planTransactionDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    //@PutMapping("/plan-transactions")
    public ResponseEntity<PlanTransactionDTO> updatePlanTransaction(@RequestBody PlanTransactionDTO planTransactionDTO) throws URISyntaxException {
        log.debug("REST request to update PlanTransaction : {}", planTransactionDTO);
        if (planTransactionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PlanTransactionDTO result = planTransactionService.save(planTransactionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, planTransactionDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /plan-transactions : get all the planTransactions.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of planTransactions in body
     */
    @GetMapping("/plan-transactions")
    public ResponseEntity<List<PlanTransactionDTO>> getAllPlanTransactions(Pageable pageable) {
        log.debug("REST request to get a page of PlanTransactions");
        Page<PlanTransactionDTO> page = planTransactionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/plan-transactions");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /plan-transactions/:id : get the "id" planTransaction.
     *
     * @param id the id of the planTransactionDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the planTransactionDTO, or with status 404 (Not Found)
     */
    @GetMapping("/plan-transactions/{id}")
    public ResponseEntity<PlanTransactionDTO> getPlanTransaction(@PathVariable Long id) {
        log.debug("REST request to get PlanTransaction : {}", id);
        Optional<PlanTransactionDTO> planTransactionDTO = planTransactionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(planTransactionDTO);
    }

    /**
     * DELETE  /plan-transactions/:id : delete the "id" planTransaction.
     *
     * @param id the id of the planTransactionDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    //@DeleteMapping("/plan-transactions/{id}")
    public ResponseEntity<Void> deletePlanTransaction(@PathVariable Long id) {
        log.debug("REST request to delete PlanTransaction : {}", id);
        planTransactionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * GET  /plan-transactions/report : get the planTransaction report.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of planTransactions in body
     */
    @GetMapping("/plan-transactions/report")
    public ResponseEntity<List<PlanTransactionReportDTO>> getPlanTransactionReport(Pageable pageable) {
        log.debug("REST request to get a page of PlanTransactions");
        Page<PlanTransactionReportDTO> page = planTransactionService.getPlanTransactionReport(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/plan-transactions/report");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
