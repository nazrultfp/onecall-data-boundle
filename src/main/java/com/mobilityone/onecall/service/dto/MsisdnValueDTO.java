package com.mobilityone.onecall.service.dto;
import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the MsisdnValue entity.
 */
public class MsisdnValueDTO implements Serializable {

    private Long id;

    private Double amount;

    private String free;

    private String freeData;

    private String freeSms;

    private String freeVoice;

    private String dataId;

    private String point;

    private String validity;

    private ZonedDateTime createdDate;

    private ZonedDateTime modifiedDate;

    private String createdBy;

    private String modifiedBy;


    private Long oneCallId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getFree() {
        return free;
    }

    public void setFree(String free) {
        this.free = free;
    }

    public String getFreeData() {
        return freeData;
    }

    public void setFreeData(String freeData) {
        this.freeData = freeData;
    }

    public String getFreeSms() {
        return freeSms;
    }

    public void setFreeSms(String freeSms) {
        this.freeSms = freeSms;
    }

    public String getFreeVoice() {
        return freeVoice;
    }

    public void setFreeVoice(String freeVoice) {
        this.freeVoice = freeVoice;
    }

    public String getDataId() {
        return dataId;
    }

    public void setDataId(String dataId) {
        this.dataId = dataId;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public String getValidity() {
        return validity;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public ZonedDateTime getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(ZonedDateTime modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Long getOneCallId() {
        return oneCallId;
    }

    public void setOneCallId(Long oneCallId) {
        this.oneCallId = oneCallId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MsisdnValueDTO msisdnValueDTO = (MsisdnValueDTO) o;
        if (msisdnValueDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), msisdnValueDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "MsisdnValueDTO{" +
            "id=" + getId() +
            ", amount=" + getAmount() +
            ", free='" + getFree() + "'" +
            ", freeData='" + getFreeData() + "'" +
            ", freeSms='" + getFreeSms() + "'" +
            ", freeVoice='" + getFreeVoice() + "'" +
            ", dataId='" + getDataId() + "'" +
            ", point='" + getPoint() + "'" +
            ", validity='" + getValidity() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", modifiedDate='" + getModifiedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", modifiedBy='" + getModifiedBy() + "'" +
            ", oneCall=" + getOneCallId() +
            "}";
    }
}
