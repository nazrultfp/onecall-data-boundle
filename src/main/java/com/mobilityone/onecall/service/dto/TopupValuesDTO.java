package com.mobilityone.onecall.service.dto;

import javax.validation.constraints.NotNull;

public class TopupValuesDTO {

    private String channelId;

    @NotNull
    private String merchantId;

    @NotNull
    private String msisdn;

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    @Override
    public String toString() {
        return "TopupValuesDTO{" +
            "channelId='" + channelId + '\'' +
            ", merchantId='" + merchantId + '\'' +
            ", msisdn='" + msisdn + '\'' +
            '}';
    }
}
