package com.mobilityone.onecall.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A MsisdnValue.
 */
@Entity
@Table(name = "msisdn_value")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class MsisdnValue implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "amount")
    private Double amount;

    @Column(name = "free")
    private String free;

    @Column(name = "free_data")
    private String freeData;

    @Column(name = "free_sms")
    private String freeSms;

    @Column(name = "free_voice")
    private String freeVoice;

    @Column(name = "data_id")
    private String dataId;

    @Column(name = "point")
    private String point;

    @Column(name = "validity")
    private String validity;

    @Column(name = "created_date")
    private ZonedDateTime createdDate;

    @Column(name = "modified_date")
    private ZonedDateTime modifiedDate;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "modified_by")
    private String modifiedBy;

    @ManyToOne
    @JoinColumn(name = "one_call_id")
    @JsonIgnoreProperties("msisdnValues")
    private OneCall oneCall;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getAmount() {
        return amount;
    }

    public MsisdnValue amount(Double amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getFree() {
        return free;
    }

    public MsisdnValue free(String free) {
        this.free = free;
        return this;
    }

    public void setFree(String free) {
        this.free = free;
    }

    public String getFreeData() {
        return freeData;
    }

    public MsisdnValue freeData(String freeData) {
        this.freeData = freeData;
        return this;
    }

    public void setFreeData(String freeData) {
        this.freeData = freeData;
    }

    public String getFreeSms() {
        return freeSms;
    }

    public MsisdnValue freeSms(String freeSms) {
        this.freeSms = freeSms;
        return this;
    }

    public void setFreeSms(String freeSms) {
        this.freeSms = freeSms;
    }

    public String getFreeVoice() {
        return freeVoice;
    }

    public MsisdnValue freeVoice(String freeVoice) {
        this.freeVoice = freeVoice;
        return this;
    }

    public void setFreeVoice(String freeVoice) {
        this.freeVoice = freeVoice;
    }

    public String getDataId() {
        return dataId;
    }

    public MsisdnValue dataId(String dataId) {
        this.dataId = dataId;
        return this;
    }

    public void setDataId(String dataId) {
        this.dataId = dataId;
    }

    public String getPoint() {
        return point;
    }

    public MsisdnValue point(String point) {
        this.point = point;
        return this;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public String getValidity() {
        return validity;
    }

    public MsisdnValue validity(String validity) {
        this.validity = validity;
        return this;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public MsisdnValue createdDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public ZonedDateTime getModifiedDate() {
        return modifiedDate;
    }

    public MsisdnValue modifiedDate(ZonedDateTime modifiedDate) {
        this.modifiedDate = modifiedDate;
        return this;
    }

    public void setModifiedDate(ZonedDateTime modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public MsisdnValue createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public MsisdnValue modifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
        return this;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public OneCall getOneCall() {
        return oneCall;
    }

    public MsisdnValue oneCall(OneCall oneCall) {
        this.oneCall = oneCall;
        return this;
    }

    public void setOneCall(OneCall oneCall) {
        this.oneCall = oneCall;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MsisdnValue msisdnValue = (MsisdnValue) o;
        if (msisdnValue.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), msisdnValue.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "MsisdnValue{" +
            "id=" + getId() +
            ", amount=" + getAmount() +
            ", free='" + getFree() + "'" +
            ", freeData='" + getFreeData() + "'" +
            ", freeSms='" + getFreeSms() + "'" +
            ", freeVoice='" + getFreeVoice() + "'" +
            ", dataId='" + getDataId() + "'" +
            ", point='" + getPoint() + "'" +
            ", validity='" + getValidity() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", modifiedDate='" + getModifiedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", modifiedBy='" + getModifiedBy() + "'" +
            "}";
    }
}
