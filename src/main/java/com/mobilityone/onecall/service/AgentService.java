package com.mobilityone.onecall.service;

import com.mobilityone.onecall.service.dto.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

/**
 * Service Interface for managing Agent.
 */
public interface AgentService {

    /**
     * Save a agent.
     *
     * @param agentDTO the entity to save
     * @return the persisted entity
     */
    AgentDTO save(AgentDTO agentDTO);

    /**
     * Get all the agents.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<AgentDTO> findAll(Pageable pageable);


    /**
     * Get the "id" agent.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<AgentDTO> findOne(Long id);

    /**
     * Delete the "id" agent.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    ResponseEntity registerAgent(AgentDTO agentDTO);

    LoginRespDTO login(LoginDTO loginDTO);

    AgentDTO updateAgentAddress(AgentUpdateDTO agentUpdateDTO);

    void resetPassword(ResetPasswordDTO resetPasswordDTO);

    LoginRespDTO loginAgent(LoginDTO loginDTO);
}
