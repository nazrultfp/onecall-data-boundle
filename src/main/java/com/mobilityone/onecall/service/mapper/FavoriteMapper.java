package com.mobilityone.onecall.service.mapper;

import com.mobilityone.onecall.domain.*;
import com.mobilityone.onecall.service.dto.FavoriteDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Favorite and its DTO FavoriteDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface FavoriteMapper extends EntityMapper<FavoriteDTO, Favorite> {



    default Favorite fromId(Long id) {
        if (id == null) {
            return null;
        }
        Favorite favorite = new Favorite();
        favorite.setId(id);
        return favorite;
    }
}
