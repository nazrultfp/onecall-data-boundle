package com.mobilityone.onecall.service;

import com.mobilityone.onecall.service.dto.DealerLoginDTO;

import com.mobilityone.onecall.service.dto.LoginDTO;
import com.mobilityone.onecall.service.dto.LoginRespDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing DealerLogin.
 */
public interface DealerLoginService {

    /**
     * Save a dealerLogin.
     *
     * @param dealerLoginDTO the entity to save
     * @return the persisted entity
     */
    DealerLoginDTO save(DealerLoginDTO dealerLoginDTO);

    /**
     * Get all the dealerLogins.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<DealerLoginDTO> findAll(Pageable pageable);


    /**
     * Get the "id" dealerLogin.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<DealerLoginDTO> findOne(Long id);

    /**
     * Delete the "id" dealerLogin.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    LoginRespDTO login(LoginDTO loginDTO);
}
