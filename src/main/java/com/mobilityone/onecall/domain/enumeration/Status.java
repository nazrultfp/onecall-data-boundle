package com.mobilityone.onecall.domain.enumeration;

/**
 * The Status enumeration.
 */
public enum Status {
    SUCCESSFUL, UNSUCCESSFUL
}
