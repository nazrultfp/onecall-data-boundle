package com.mobilityone.onecall.service.impl;

import com.mobilityone.onecall.config.ApplicationProperties;
import com.mobilityone.onecall.domain.OneCall;
import com.mobilityone.onecall.domain.enumeration.SimStatus;
import com.mobilityone.onecall.repository.OneCallRepository;
import com.mobilityone.onecall.security.SecurityUtils;
import com.mobilityone.onecall.service.*;
import com.mobilityone.onecall.service.dto.*;
import com.mobilityone.onecall.service.mapper.OneCallMapper;
import com.mobilityone.onecall.web.rest.util.CommonUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.server.ResponseStatusException;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;

/**
 * Service Implementation for managing OneCall.
 */
@Service
@Transactional
public class OneCallServiceImpl implements OneCallService {

    private final Logger log = LoggerFactory.getLogger(OneCallServiceImpl.class);

    private static final String SUCCESSFUL = "200";

    private final ApplicationProperties properties;

    private final OneCallRepository oneCallRepository;

    private final OneCallMapper oneCallMapper;

    private final TuneTalkService tuneTalkService;

    private final OnePayService onePayService;

    private final AgentService agentService;

    private final SimOrderService simOrderService;

    private final SimPortInService simPortInService;

    private final SimPortInVoService simPortInVoService;

    private final SimRegisterService simRegisterService;

    private final SubscriptionPlanService subscriptionPlanService;

    private final BigShotService bigShotService;

    public OneCallServiceImpl(ApplicationProperties properties, OneCallRepository oneCallRepository, OneCallMapper oneCallMapper, TuneTalkService tuneTalkService,
                              OnePayService onePayService, AgentService agentService, SimOrderService simOrderService, SimPortInService simPortInService,
                              SimPortInVoService simPortInVoService, SimRegisterService simRegisterService, SubscriptionPlanService subscriptionPlanService, BigShotService bigShotService) {
        this.properties = properties;
        this.oneCallRepository = oneCallRepository;
        this.oneCallMapper = oneCallMapper;
        this.tuneTalkService = tuneTalkService;
        this.onePayService = onePayService;
        this.agentService = agentService;
        this.simOrderService = simOrderService;
        this.simPortInService = simPortInService;
        this.simPortInVoService = simPortInVoService;
        this.simRegisterService = simRegisterService;
        this.subscriptionPlanService = subscriptionPlanService;
        this.bigShotService = bigShotService;
    }

    /**
     * Save a oneCall.
     *
     * @param oneCallDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public OneCallDTO save(OneCallDTO oneCallDTO) {
        log.debug("Request to save OneCall : {}", oneCallDTO);
        OneCall oneCall = oneCallMapper.toEntity(oneCallDTO);
        oneCall = oneCallRepository.save(oneCall);
        return oneCallMapper.toDto(oneCall);
    }

    /**
     * Get all the oneCalls.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<OneCallDTO> findAll(Pageable pageable) {
        log.debug("Request to get all OneCalls");
        return oneCallRepository.findAll(pageable)
            .map(oneCallMapper::toDto);
    }


    /**
     * Get one oneCall by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<OneCallDTO> findOne(Long id) {
        log.debug("Request to get OneCall : {}", id);
        return oneCallRepository.findById(id)
            .map(oneCallMapper::toDto);
    }

    /**
     * Delete the oneCall by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete OneCall : {}", id);
        oneCallRepository.deleteById(id);
    }

    @Override
    public OneCallDTO doTopup(OneCallDTO oneCallDTO) {

        Optional<OneCall> oneCall = oneCallRepository.findByMerchantIdAndMerchantOrderNoAndCode(oneCallDTO.getMerchantId(), oneCallDTO.getMerchantOrderNo(), SUCCESSFUL);
        if(oneCall.isPresent()){
            throw new ResponseStatusException(HttpStatus.FOUND, "This request has been done successfully.");
        }

        log.debug("mapped data to OneCallDTO");

        oneCallDTO.setTransactionId(CommonUtils.get15DigitId());
        oneCallDTO.setCreatedBy(SecurityUtils.getCurrentUserLogin().get());
        oneCallDTO.setCreatedDate(ZonedDateTime.now());

//        OneCall oneCall = oneCallRepository.save(oneCallMapper.toEntity(oneCallDTO));
        log.debug("Current Data of OneCall: {}", oneCallDTO.toString());

        log.debug("Before calling Tune Talk");

//        oneCallDTO = oneCallMapper.toDto(oneCall);
        try {
            oneCallDTO = tuneTalkService.callTopup(oneCallDTO);
        } catch (HttpClientErrorException e) {
            throw new ResponseStatusException(e.getStatusCode(), "Connection to Tune Talk Api Failed.");
        }

        oneCallDTO.setModifiedBy(SecurityUtils.getCurrentUserLogin().get());
        oneCallDTO.setModifiedDate(ZonedDateTime.now());

        OneCall result = oneCallRepository.save(oneCallMapper.toEntity(oneCallDTO));
        log.debug("save Responsed Data of OneCall: {}", result.toString());
//        OneCallDTO result = oneCallDTO;
        return oneCallMapper.toDto(result);
    }

    @Override
    public OneCallDTO getTopupByTransId(TopupOrderDTO topupOrderDTO) {
        log.debug("Before Finding MerchantOrderNo");
        OneCall oneCall = null;
        try {
            Optional<OneCall> oneByMerchantOrderNo = oneCallRepository
                .findTopByMerchantIdAndMerchantOrderNoOrderByCreatedDateDesc(topupOrderDTO.getMerchantId(),
                    topupOrderDTO.getMerchantOrderNo());
            oneCall = oneByMerchantOrderNo.get();
        } catch (NoSuchElementException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "MerchantOrderNo Not Found");
        }

        if (oneCall == null || oneCall.getId() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "MerchantOrderNo Not Found");
        }

        OneCallDTO result = null;

        log.debug("Call Tune Talk Service For topup By TransId");
        try {
            result = tuneTalkService.getTopupByTransId(oneCallMapper.toDto(oneCall));

        } catch (HttpClientErrorException e) {
            throw new ResponseStatusException(e.getStatusCode(), "Connection to Tune Talk Api Failed.");
        }
//        OneCallDTO result = oneCallMapper.toDto(oneCall);
//        result.setMessage("OK");
        return result;
    }

    @Override
    public OneCallDTO getTopupValuesByMsisdn(TopupValuesDTO topupValuesDTO) {

        log.info("Call TopupValues from Tune Talk");

        OneCall result = null;

        try {
            result = tuneTalkService.getTopupValuesByMsisdn(topupValuesDTO.getMsisdn());
        } catch (HttpClientErrorException e) {
            throw new ResponseStatusException(e.getStatusCode(), "Connection to Tune Talk Api Failed.");
        }

        log.info("Set OneCall to Service Result");
        result.setChannelId(topupValuesDTO.getChannelId());
        result.setMerchantId(topupValuesDTO.getMerchantId());
        result.setMsisdn(topupValuesDTO.getMsisdn());
        result.setCreatedDate(ZonedDateTime.now());
        result.setCreatedBy(SecurityUtils.getCurrentUserLogin().get());
        result.setModifiedDate(ZonedDateTime.now());
        result.setModifiedBy(SecurityUtils.getCurrentUserLogin().get());

        log.info("Save Result : {}", result.toString());
        OneCall save = oneCallRepository.save(result);

        return oneCallMapper.toDto(save);
    }

    @Override
    public TtBalabceRespDTO getBalanceByIccid(String iccid) {
        log.info("Get Balance for {} Iccid.", iccid);
        TtBalabceRespDTO balance = tuneTalkService.getBalanceByIccid(iccid);
//        log.info(balance.toString());
        return balance;
    }

    @Override
    public TtBalabceRespDTO getBalanceByImsi(String imsi) {
        log.info("Get Balance for {} IMSI.", imsi);
        TtBalabceRespDTO balance = tuneTalkService.getBalanceByIccid(imsi);
//        log.info(balance.toString());
        return balance;
    }

    @Override
    public TtBalabceRespDTO getBalanceByMsisdn(String msisdn) {
        log.info("Get Balance for {} Msisdn.", msisdn);
        TtBalabceRespDTO balanceByMsisdn = tuneTalkService.getBalanceByMsisdn(msisdn);
//        log.info(balanceByMsisdn.toString());
        return balanceByMsisdn;
    }

    @Override
    public TuneTalkRespDTO lookupByEmail(String email) {
        log.info("Look Up for Big Shot by Email : {}", email);
        TuneTalkRespDTO respDTO = tuneTalkService.lookupBigShotByEmail(email);
        log.info(respDTO.toString());
        return respDTO;
    }

    @Override
    public Page<OneCallDTO> getAllTransactionsByMerchantId(Pageable pageable, String merchantId) {
        return oneCallRepository.findByMerchantIdAndCode(pageable, merchantId, SUCCESSFUL).map(oneCallMapper::toDto);
    }

    @Override
    public Object simOrder(SimOrderReqDTO simOrderReqDTO) {
        log.info("Request to perform Sim Order : {}", simOrderReqDTO);

//        Optional<AgentDTO> agent = agentService.findOne(simOrderReqDTO.getAgentId());
//        if (!agent.isPresent()){
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "agent nou found");
//        }

        SimOrderDTO simOrderDTO = simOrderService.save(simOrderReqDTO);
        JSONObject jsonObject = tuneTalkService.simOrder(simOrderReqDTO);

        String code = jsonObject.getString("code");
        if (!code.equals("200")){
            simOrderService.delete(simOrderDTO.getId());
            return jsonObject;
        }

        simOrderDTO = simOrderService.save(simOrderDTO.getId(), jsonObject);

        return simOrderDTO;
    }

    @Override
    public Object simPortIn(SimPortInReqDTO simPortInReqDTO) {
        log.info("Request to Post SIM Port In : {}", simPortInReqDTO);

//        Optional<AgentDTO> agentOptional = agentService.findOne(simPortInReqDTO.getAgentId());
//        if (!agentOptional.isPresent()){
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "agent not found");
//        }

        SimPortInDTO simPortInDTO = simPortInService.save(simPortInReqDTO);
        JSONObject jsonObject = tuneTalkService.simPortIn(simPortInReqDTO.getJson());


//        if (jsonObject.has("code") && !jsonObject.getString("code").equals("200")){
//            simPortInService.delete(simPortInDTO.getId());
//            simPortInDTO.getSimPortInVos().forEach(simPortInVoDTO -> simPortInVoDTO.setStatus(SimStatus.PORT_IN_FAILED.toString()));
//            simPortInService.save(simPortInDTO)
//            return jsonObject;
//        }

        simPortInDTO = simPortInService.save(simPortInDTO.getId(), jsonObject);

        return simPortInDTO;
    }

    @Override
    public Object simRegistration(SimRegReqDTO simRegReqDTO) {
        log.info("Request to register SIM : {}", simRegReqDTO);

//        Optional<AgentDTO> agentOptional = agentService.findOne(simRegReqDTO.getAgentId());
//        if (!agentOptional.isPresent()){
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "agent not found");
//        }

        simRegReqDTO.setTransactionId(CommonUtils.getNumericCode(15));
//        simRegReqDTO.setPartnerDealerCode(properties.getPartnerDealerCode());

        SimRegisterDTO simRegisterDTO = simRegisterService.save(simRegReqDTO);
        JSONObject jsonObject = tuneTalkService.simRegistration(simRegReqDTO.getJson());

        String code = jsonObject.getString("code");
        if (!code.equals("200")){
            simRegisterService.delete(simRegisterDTO.getId());
            return jsonObject;
        }

        simRegisterDTO = simRegisterService.save(simRegisterDTO.getId(), jsonObject);

        JSONObject tpmRegisterRespDTO = onePayService.registerCustomer(simRegisterDTO);

        if (tpmRegisterRespDTO.has("ErrCode") && "0".equals(tpmRegisterRespDTO.getString("ErrCode"))){
            simRegisterDTO.setStatus(SimStatus.SUCCESSFUL.toString());
            simRegisterDTO = simRegisterService.save(simRegisterDTO);
        }

        return simRegisterDTO;
    }

    @Override
    public Object simRegistrationWithoutKYC(SimRegReqDTO simRegReqDTO) {
        log.info("Request to register SIM without KYC : {}", simRegReqDTO);

        simRegReqDTO.setTransactionId(CommonUtils.getNumericCode(15));
//        simRegReqDTO.setPartnerDealerCode(properties.getPartnerDealerCode());

        SimRegisterDTO simRegisterDTO = simRegisterService.save(simRegReqDTO);
        JSONObject jsonObject = tuneTalkService.simRegistration(simRegReqDTO.getJson());

        String code = jsonObject.getString("code");
        if (!code.equals("200")){
            simRegisterService.delete(simRegisterDTO.getId());
            return jsonObject;
        } else {
            simRegisterDTO.setStatus(SimStatus.SUCCESSFUL.toString());
            simRegisterDTO = simRegisterService.save(simRegisterDTO);
        }

        simRegisterDTO = simRegisterService.save(simRegisterDTO.getId(), jsonObject);

        return simRegisterDTO;
    }

    /*private TTSimRegReqDTO convertSimRegReqDtoToTTSimRegReq(SimRegReqDTO simRegReqDTO) {
        TTSimRegReqDTO ttSimRegReqDTO = new TTSimRegReqDTO();
        ttSimRegReqDTO.setTransactionId(simRegReqDTO.getTransactionId());
        ttSimRegReqDTO.setFirstName(simRegReqDTO.getFirstName());
        ttSimRegReqDTO.setLastName(simRegReqDTO.getLastName());
        ttSimRegReqDTO.setGender(simRegReqDTO.getGender());
        ttSimRegReqDTO.setDob(simRegReqDTO.getDob());
        ttSimRegReqDTO.setIdNumber(simRegReqDTO.getIdNumber());
        ttSimRegReqDTO.setIdType(simRegReqDTO.getIdType());
        ttSimRegReqDTO.setNationality(simRegReqDTO.getNationality());
        ttSimRegReqDTO.setAddress(simRegReqDTO.getAddress());
        ttSimRegReqDTO.setEmailAddr(simRegReqDTO.getEmailAddr());
        ttSimRegReqDTO.setBlockedNumber(simRegReqDTO.getBlockedNumber());
        ttSimRegReqDTO.setCity(simRegReqDTO.getCity());
        ttSimRegReqDTO.setState(simRegReqDTO.getState());
        ttSimRegReqDTO.setCountry(simRegReqDTO.getCountry());
        ttSimRegReqDTO.setPostCode(simRegReqDTO.getPostCode());
        ttSimRegReqDTO.setSimNumber(simRegReqDTO.getSimNumber());
        ttSimRegReqDTO.setPartnerDealerCode(simRegReqDTO.getPartnerDealerCode());
        ttSimRegReqDTO.getPhotos().add(simRegReqDTO.getPhoto());

        log.debug("TTSimRegReqDTO : {}", ttSimRegReqDTO);


        return ttSimRegReqDTO;
    }*/

    @Override
    public SimPortInVoDTO setPortInStatusByMsisdn(String msisdn, String statusResp) {
        return simPortInVoService.findByMsisdn(msisdn, statusResp);
    }

    @Override
    public List<Map<String, Object>> getSimHistory() {
        log.debug("Request to get all Sims histories");

        return simRegisterService.simHistory();
    }

    @Override
    public SubscriptionPlanDTO performSubscription(SubscriptionReqDTO subscriptionReqDTO) {
        log.info("Request to Perform subscription : {}", subscriptionReqDTO.toString());

        checkPlanValidity(subscriptionReqDTO);

        TuneTalkRespDTO tuneTalkRespDTO = tuneTalkService.performSubscription(subscriptionReqDTO);

        return subscriptionPlanService.save(subscriptionReqDTO, tuneTalkRespDTO);
    }

    private void checkPlanValidity(SubscriptionReqDTO subscriptionReqDTO) {

        TtBalabceRespDTO balanceByMsisdn = getBalanceByMsisdn(subscriptionReqDTO.getMsisdn());

        if (balanceByMsisdn.getPlans() != null) {
            boolean anyMatch = balanceByMsisdn.getPlans().stream().anyMatch(s ->
                s.getName().equalsIgnoreCase(subscriptionReqDTO.getKeyword()));

            if (anyMatch)
                subscriptionReqDTO.setKeyword(subscriptionReqDTO.getKeyword().trim().concat(" ADD"));
        }

    }

    @Override
    public TuneTalkRespDTO bigShotConvert(BigShotConvertReqDTO bigShotConvertReqDTO) {
        log.info("Convert BIG Points. {}", bigShotConvertReqDTO.toString());

        TuneTalkRespDTO tuneTalkRespDTO = tuneTalkService.bigShotConvert(bigShotConvertReqDTO);


        BigShotDTO bigShotDTO = bigShotService.save(bigShotConvertReqDTO, tuneTalkRespDTO);

        return tuneTalkRespDTO;
    }

    @Override
    public List<Map<String, Object>> getSimStatusByMsisdn(String msisdn) {
        log.info("Request to get sim status for msisdn : {}", msisdn);

        return simRegisterService.getSimStatusByMsisdn(msisdn);
    }

    @Override
    public List<Map<String, Object>> getDailyTotalTopupInRangeForClient(String client, String fromDate, String toDate) {
        log.info("Request to get client {} daily total topup in date range from {} to {}", client, fromDate, toDate);

        List<Map<String, Object>> list = oneCallRepository.getDailyTotalTopupInRangeForClient("%".concat(client).concat("%"), fromDate, toDate);

        log.info("result : {}", list);

        return list;
    }
}
