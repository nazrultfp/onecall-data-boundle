package com.mobilityone.onecall.service.dto;

public class TuneTalkValueDTO {
    private String id;
    private String amount;
    private String free;
    private String freeData;
    private String freeSms;
    private String freeVoice;
    private String point;
    private String validity;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getFree() {
        return free;
    }

    public void setFree(String free) {
        this.free = free;
    }

    public String getFreeData() {
        return freeData;
    }

    public void setFreeData(String freeData) {
        this.freeData = freeData;
    }

    public String getFreeSms() {
        return freeSms;
    }

    public void setFreeSms(String freeSms) {
        this.freeSms = freeSms;
    }

    public String getFreeVoice() {
        return freeVoice;
    }

    public void setFreeVoice(String freeVoice) {
        this.freeVoice = freeVoice;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public String getValidity() {
        return validity;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }

    @Override
    public String toString() {
        return "TuneTalkValueDTO{" +
            "id='" + id + '\'' +
            ", amount='" + amount + '\'' +
            ", free='" + free + '\'' +
            ", freeData='" + freeData + '\'' +
            ", freeSms='" + freeSms + '\'' +
            ", freeVoice='" + freeVoice + '\'' +
            ", point='" + point + '\'' +
            ", validity='" + validity + '\'' +
            '}';
    }
}
