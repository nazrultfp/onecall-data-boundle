package com.mobilityone.onecall.service.mapper;

import com.mobilityone.onecall.domain.SimRegister;
import com.mobilityone.onecall.service.dto.SimRegisterReportDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {AgentMapper.class})
public interface SimRegisterReportMapper extends EntityMapper<SimRegisterReportDTO, SimRegister> {

    @Mapping(source = "agent.id", target = "agentId")
    SimRegisterReportDTO toDto(SimRegister simRegister);

    @Mapping(target = "photos", ignore = true)
    @Mapping(source = "agentId", target = "agent")
    SimRegister toEntity(SimRegisterReportDTO simRegisterReportDTO);

    default SimRegister fromId(Long id) {
        if (id == null) {
            return null;
        }
        SimRegister simRegister = new SimRegister();
        simRegister.setId(id);
        return simRegister;
    }
}
