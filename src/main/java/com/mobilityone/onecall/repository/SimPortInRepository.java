package com.mobilityone.onecall.repository;

import com.mobilityone.onecall.domain.SimPortIn;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


/**
 * Spring Data  repository for the SimPortIn entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SimPortInRepository extends JpaRepository<SimPortIn, Long> {

    Page<SimPortIn> findByAgentIdOrderByCreatedDateDesc(Pageable pageable, Long agentId);

    List<SimPortIn> findByAgentCode(String agentCode);

    @Query("select count(spi) from SimPortIn spi where date_format(CONVERT_TZ(spi.createdDate ,'+00:00','+08:00') , '%Y-%m-%d') = :date") // mysql
//    @Query("select count(spi) from SimPortIn spi where FORMATDATETIME(DATEADD(HOUR, 8, spi.createdDate) , 'yyyy-MM-dd') = :date") // h2
    Integer countByDate(@Param("date") String date);




    @Query(value = "select spi.id as id, spi.agent_code as agentCode, spv.sim_number as simNumber, spi.message as message, " +
        " spv.msisdn as msisdn, sr.msisdn as onecallMsisdn , spv.status as status , date_format(CONVERT_TZ(spv.created_date , '+00:00', '+08:00') , '%Y-%m-%d %T') as createdDate , " +
        " date_format(CONVERT_TZ(spv.modified_date , '+00:00', '+08:00') , '%Y-%m-%d %T') as modifiedDate , spv.created_by as createdBy, spv.modified_by as modifiedBy " +
        " from sim_port_in spi join sim_port_in_vo spv on spi.id = spv.sim_port_in_id " +
        " join sim_register sr on spv.sim_number = sr.sim_number " +
        " where date_format(CONVERT_TZ(spv.modified_date , '+00:00', '+08:00') , '%Y-%m-%d') between :fromDate and :toDate", nativeQuery = true) //mysql
//    @Query(value = "select spi.id as id, spi.agent_code as agentCode, spv.sim_number as simNumber, spi.message as message, " +
//        " spv.msisdn as msisdn, sr.msisdn as onecallMsisdn , spv.status as status , FORMATDATETIME(DATEADD(HOUR, 8, spv.created_date) , 'yyyy-MM-dd HH:mm:ss') as createdDate ,  " +
//        " FORMATDATETIME(DATEADD(HOUR, 8, spv.modified_date) , 'yyyy-MM-dd HH:mm:ss') as modifiedDate , spv.created_by as createdBy, spv.modified_by as modifiedBy from sim_port_in spi " +
//        " join sim_port_in_vo spv on spi.id = spv.sim_port_in_id " +
//        " join sim_register sr on spv.sim_number = sr.sim_number " +
//        " where FORMATDATETIME(DATEADD(HOUR, 8, spv.modified_date) , 'yyyy-MM-dd')  between :fromDate and :toDate", nativeQuery = true) //h2
    List<Map<String, Object>> findAllInRangeDate(@Param("fromDate") String fromDate, @Param("toDate") String toDate);



    @Query(value = "select count(spiv.id) portInCount , date_format(CONVERT_TZ(spiv.modified_date , '+00:00', '+08:00'), '%Y-%m-%d') as modifiedDate  " +
        " from sim_port_in spi inner join sim_port_in_vo spiv  on spi.id = spiv.sim_port_in_id  " +
        " where  spiv.status = 'SUCCESSFUL' and spi.agent_code = :agentCode and date_format(CONVERT_TZ(spiv.modified_date , '+00:00', '+08:00'), '%Y-%m-%d') between :fromDate and :toDate  " +
        " group by modifiedDate", nativeQuery = true) //mysql
//    @Query(value = "select count(spiv.id) portInCount , FORMATDATETIME(DATEADD(HOUR, 8, spiv.modified_date) , 'yyyy-MM-dd') as modifiedDate  " +
//        " from sim_port_in spi inner join sim_port_in_vo spiv  on spi.id = spiv.sim_port_in_id  " +
//        " where  spiv.status = 'SUCCESSFUL' and spi.agent_code = :agentCode and FORMATDATETIME(DATEADD(HOUR, 8, spiv.modified_date) , 'yyyy-MM-dd') between :fromDate and :toDate  " +
//        " group by modifiedDate", nativeQuery = true) //h2
    List<Map<String, Object>> getAgentDailyTotalSimRegInRange(@Param("agentCode") String agentCode, @Param("fromDate") String fromDate, @Param("toDate") String toDate);
}
