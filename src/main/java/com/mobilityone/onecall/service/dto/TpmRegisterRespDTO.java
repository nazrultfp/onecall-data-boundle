package com.mobilityone.onecall.service.dto;

public class TpmRegisterRespDTO {
    private String ErrCode;
    private String ErrDesc;

    public String getErrCode() {
        return ErrCode;
    }

    public void setErrCode(String errCode) {
        ErrCode = errCode;
    }

    public String getErrDesc() {
        return ErrDesc;
    }

    public void setErrDesc(String errDesc) {
        ErrDesc = errDesc;
    }
}
