package com.mobilityone.onecall.service.impl;

import com.mobilityone.onecall.config.Constants;
import com.mobilityone.onecall.domain.enumeration.SimStatus;
import com.mobilityone.onecall.security.SecurityUtils;
import com.mobilityone.onecall.service.SimPortInVoService;
import com.mobilityone.onecall.domain.SimPortInVo;
import com.mobilityone.onecall.repository.SimPortInVoRepository;
import com.mobilityone.onecall.service.dto.SimPortInVoDTO;
import com.mobilityone.onecall.service.mapper.SimPortInVoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.time.ZonedDateTime;
import java.util.Optional;

/**
 * Service Implementation for managing SimPortInVo.
 */
@Service
@Transactional
public class SimPortInVoServiceImpl implements SimPortInVoService {

    private final Logger log = LoggerFactory.getLogger(SimPortInVoServiceImpl.class);

    private final SimPortInVoRepository simPortInVoRepository;

    private final SimPortInVoMapper simPortInVoMapper;

    public SimPortInVoServiceImpl(SimPortInVoRepository simPortInVoRepository, SimPortInVoMapper simPortInVoMapper) {
        this.simPortInVoRepository = simPortInVoRepository;
        this.simPortInVoMapper = simPortInVoMapper;
    }

    /**
     * Save a simPortInVo.
     *
     * @param simPortInVoDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public SimPortInVoDTO save(SimPortInVoDTO simPortInVoDTO) {
        log.debug("Request to save SimPortInVo : {}", simPortInVoDTO);
        SimPortInVo simPortInVo = simPortInVoMapper.toEntity(simPortInVoDTO);
        simPortInVo = simPortInVoRepository.save(simPortInVo);
        return simPortInVoMapper.toDto(simPortInVo);
    }

    /**
     * Get all the simPortInVos.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<SimPortInVoDTO> findAll(Pageable pageable) {
        log.debug("Request to get all SimPortInVos");
        return simPortInVoRepository.findAll(pageable)
            .map(simPortInVoMapper::toDto);
    }


    /**
     * Get one simPortInVo by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<SimPortInVoDTO> findOne(Long id) {
        log.debug("Request to get SimPortInVo : {}", id);
        return simPortInVoRepository.findById(id)
            .map(simPortInVoMapper::toDto);
    }

    /**
     * Delete the simPortInVo by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete SimPortInVo : {}", id);        simPortInVoRepository.deleteById(id);
    }

    @Override
    public SimPortInVoDTO findByMsisdn(String msisdn, String statusResp) {
        log.info("Sim Port In Status for msisdn : {} is status : {}", msisdn, statusResp);

        Optional<SimPortInVo> byMsisdn = simPortInVoRepository.findFirstByMsisdn(msisdn);
        if (!byMsisdn.isPresent()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "msisdn not found");
        }

        SimPortInVo simPortInVo = byMsisdn.get();
        if (isStatusRejected(statusResp) && !simPortInVo.getStatus().equals(SimStatus.REJECTED))
            simPortInVo.setStatus(SimStatus.REJECTED.toString());
        else if (isStatusSuccessful(statusResp) && !simPortInVo.getStatus().equals(SimStatus.SUCCESSFUL))
            simPortInVo.setStatus(SimStatus.SUCCESSFUL.toString());
        else
            return simPortInVoMapper.toDto(simPortInVo);

        simPortInVo.setModifiedDate(ZonedDateTime.now());
        simPortInVo.setModifiedBy(SecurityUtils.getCurrentUserLogin().orElse(Constants.ANONYMOUS_USER));

        simPortInVo = simPortInVoRepository.save(simPortInVo);

        log.info("Changed Data : {}", simPortInVo);

        return simPortInVoMapper.toDto(simPortInVo);
    }

    private boolean isStatusSuccessful(String statusResp) {
        return statusResp.toLowerCase().contains("complete") || statusResp.toLowerCase().contains("successful");
    }

    private boolean isStatusRejected(String statusResp) {
        return statusResp.toLowerCase().contains("failed") || statusResp.toLowerCase().contains("unsuccessful") || statusResp.toLowerCase().contains("rejected");
    }
}
