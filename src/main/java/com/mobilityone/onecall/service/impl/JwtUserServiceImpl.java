package com.mobilityone.onecall.service.impl;

import com.mobilityone.onecall.domain.ClientUser;
import com.mobilityone.onecall.domain.DealerLogin;
import com.mobilityone.onecall.repository.ClientUserRepository;
import com.mobilityone.onecall.repository.DealerLoginRepository;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class JwtUserServiceImpl implements UserDetailsService {

    private final ClientUserRepository userRepository;

    private final DealerLoginRepository dealerLoginRepository;

    public JwtUserServiceImpl(ClientUserRepository userRepository, DealerLoginRepository dealerLoginRepository) {
        this.userRepository = userRepository;
        this.dealerLoginRepository = dealerLoginRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Optional<ClientUser> clientUser = userRepository.findByClientUserName(username);
        if (clientUser.isPresent()) {
            User.UserBuilder builder = org.springframework.security.core.userdetails.User.withUsername(username);
            builder.password(new BCryptPasswordEncoder().encode(clientUser.get().getClientPassword()));
            builder.roles(clientUser.get().getClientAuthority());
            return builder.build();
        }

        Optional<DealerLogin> dealerLoginOptional = dealerLoginRepository.findByDealerUserNameAndTokenExpiredFalse(username);
        if (dealerLoginOptional.isPresent()){
            DealerLogin dealerLogin = dealerLoginOptional.get();
//            if (dealerLogin.getToken() dealerLogin.getTokenExpireDate().compareTo(ZonedDateTime.now()) > 0){
                User.UserBuilder builder = org.springframework.security.core.userdetails.User.withUsername(username);
                builder.password(new BCryptPasswordEncoder().encode(dealerLogin.getDealerPassword()));
                builder.roles("USER");
                return builder.build();
//            }
        }

        throw new UsernameNotFoundException("User not found.");
    }
}
