package com.mobilityone.onecall.service;

import com.mobilityone.onecall.domain.OneCall;
import com.mobilityone.onecall.service.dto.*;
import org.json.JSONObject;
import org.springframework.web.client.HttpClientErrorException;


public interface TuneTalkService {

    TtBalabceRespDTO getBalanceByIccid(String iccid);

    TtBalabceRespDTO getBalanceByImsiNumber(String imsi);

    TtBalabceRespDTO getBalanceByMsisdn(String msisdn);

    TuneTalkRespDTO bigShotConvert(BigShotConvertReqDTO bigShotConvertReqDTO);

    TuneTalkRespDTO lookupBigShotByEmail(String email);

    TuneTalkRespDTO cancelLocationByMsidn(String msisdn);

    DebitBalanceRespDTO debitBalance(DebitBalanceReqDTO debitBalanceReqDTO);

    JSONObject getMsisdnListByIdNumber(String idNumber);

    JSONObject getPortStatusByMsisdn(String msisdn);

    JSONObject getPreferNumberList(String prefix);

    JSONObject getMemberProfileByIccid(String iccid);

    JSONObject getMemberProfileByImsiNumber(String imsi);

    JSONObject getMemberProfileByMsisdn(String msisdn);

    TuneTalkRespDTO checkSimRegisteredStatus(String simNumber);

    JSONObject simOrder(SimOrderReqDTO simOrderReqDTO);

    JSONObject simPortIn(JSONObject reqObject);

    JSONObject simRegistration(JSONObject simRegReqDTO);

    TuneTalkRespDTO performSubscription(SubscriptionReqDTO subscriptionReqDTO);

    JSONObject getSubscriptionStatusByTransactionId(String transactionId);

    JSONObject getTelcoList();

    OneCallDTO callTopup(OneCallDTO oneCallDTO) throws HttpClientErrorException;

    OneCallDTO getTopupByTransId(OneCallDTO transactionId) throws HttpClientErrorException;

    OneCall getTopupValuesByMsisdn(String oneCallDTO) throws HttpClientErrorException;

    TuneTalkRespDTO updateTopupValidityForMsisdn(String msisdn, Integer days);
}
