package com.mobilityone.onecall.service.dto;

import java.util.ArrayList;
import java.util.List;

public class DealerSubscriptionReqDTO {

    private String address;
    private String city;
    private String country;
    private String creditId;
    private String date;
    private String dob;
    private String emailAddr;
    private String firstName;
    private String gender;
    private String idNumber;
    private String idType;
    private String lastName;
    private String latitude;
    private String longitude;
    private String middleName;
    private String msisdn;
    private String nationality;
    private String partnerDealerCode;
    private List<String> photos = new ArrayList<>();
    private String planName;
    private String postCode;
    private String simNumber;
    private String state;
    private String transactionId;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCreditId() {
        return creditId;
    }

    public void setCreditId(String creditId) {
        this.creditId = creditId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getEmailAddr() {
        return emailAddr;
    }

    public void setEmailAddr(String emailAddr) {
        this.emailAddr = emailAddr;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getPartnerDealerCode() {
        return partnerDealerCode;
    }

    public void setPartnerDealerCode(String partnerDealerCode) {
        this.partnerDealerCode = partnerDealerCode;
    }

    public List<String> getPhotos() {
        return photos;
    }

    public void setPhotos(List<String> photos) {
        this.photos = photos;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getSimNumber() {
        return simNumber;
    }

    public void setSimNumber(String simNumber) {
        this.simNumber = simNumber;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    @Override
    public String toString() {
        return "DealerSubscriptionReqDTO{" +
            "address='" + address + '\'' +
            ", city='" + city + '\'' +
            ", country='" + country + '\'' +
            ", creditId='" + creditId + '\'' +
            ", date='" + date + '\'' +
            ", dob='" + dob + '\'' +
            ", emailAddr='" + emailAddr + '\'' +
            ", firstName='" + firstName + '\'' +
            ", gender='" + gender + '\'' +
            ", idNumber='" + idNumber + '\'' +
            ", idType='" + idType + '\'' +
            ", lastName='" + lastName + '\'' +
            ", latitude='" + latitude + '\'' +
            ", longitude='" + longitude + '\'' +
            ", middleName='" + middleName + '\'' +
            ", msisdn='" + msisdn + '\'' +
            ", nationality='" + nationality + '\'' +
            ", partnerDealerCode='" + partnerDealerCode + '\'' +
            ", photos=" + photos +
            ", planName='" + planName + '\'' +
            ", postCode='" + postCode + '\'' +
            ", simNumber='" + simNumber + '\'' +
            ", state='" + state + '\'' +
            ", transactionId='" + transactionId + '\'' +
            '}';
    }
}
