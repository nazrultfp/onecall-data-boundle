package com.mobilityone.onecall.web.rest;

import com.mobilityone.onecall.OneCallApp;

import com.mobilityone.onecall.domain.SimOrder;
import com.mobilityone.onecall.repository.SimOrderRepository;
import com.mobilityone.onecall.service.SimOrderService;
import com.mobilityone.onecall.service.dto.SimOrderDTO;
import com.mobilityone.onecall.service.mapper.SimOrderMapper;
import com.mobilityone.onecall.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;


import static com.mobilityone.onecall.web.rest.TestUtil.sameInstant;
import static com.mobilityone.onecall.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SimOrderResource REST controller.
 *
 * @see SimOrderResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OneCallApp.class)
public class SimOrderResourceIntTest {

    private static final String DEFAULT_AGENT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_AGENT_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_CITY = "AAAAAAAAAA";
    private static final String UPDATED_CITY = "BBBBBBBBBB";

    private static final String DEFAULT_CONTACT = "AAAAAAAAAA";
    private static final String UPDATED_CONTACT = "BBBBBBBBBB";

    private static final String DEFAULT_COUNTRY = "AAAAAAAAAA";
    private static final String UPDATED_COUNTRY = "BBBBBBBBBB";

    private static final String DEFAULT_COURIER_TRACKING_URL = "AAAAAAAAAA";
    private static final String UPDATED_COURIER_TRACKING_URL = "BBBBBBBBBB";

    private static final Long DEFAULT_DIGITAL_PLAN_ID = 1L;
    private static final Long UPDATED_DIGITAL_PLAN_ID = 2L;

    private static final String DEFAULT_DONOR_TELCO = "AAAAAAAAAA";
    private static final String UPDATED_DONOR_TELCO = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_LAT_LONG = "AAAAAAAAAA";
    private static final String UPDATED_LAT_LONG = "BBBBBBBBBB";

    private static final String DEFAULT_MSISDN = "AAAAAAAAAA";
    private static final String UPDATED_MSISDN = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_POSTCODE = "AAAAAAAAAA";
    private static final String UPDATED_POSTCODE = "BBBBBBBBBB";

    private static final String DEFAULT_SOURCE = "AAAAAAAAAA";
    private static final String UPDATED_SOURCE = "BBBBBBBBBB";

    private static final String DEFAULT_STATE = "AAAAAAAAAA";
    private static final String UPDATED_STATE = "BBBBBBBBBB";

    private static final Double DEFAULT_TOPUP_AMOUNT = 1D;
    private static final Double UPDATED_TOPUP_AMOUNT = 2D;

    private static final String DEFAULT_API_KEY = "AAAAAAAAAA";
    private static final String UPDATED_API_KEY = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_MESSAGE = "AAAAAAAAAA";
    private static final String UPDATED_MESSAGE = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_MODIFIED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_MODIFIED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_MODIFIED_BY = "BBBBBBBBBB";

    @Autowired
    private SimOrderRepository simOrderRepository;

    @Autowired
    private SimOrderMapper simOrderMapper;

    @Autowired
    private SimOrderService simOrderService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSimOrderMockMvc;

    private SimOrder simOrder;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SimOrderResource simOrderResource = new SimOrderResource(simOrderService);
        this.restSimOrderMockMvc = MockMvcBuilders.standaloneSetup(simOrderResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SimOrder createEntity(EntityManager em) {
        SimOrder simOrder = new SimOrder()
            .agentCode(DEFAULT_AGENT_CODE)
            .address(DEFAULT_ADDRESS)
            .city(DEFAULT_CITY)
            .contact(DEFAULT_CONTACT)
            .country(DEFAULT_COUNTRY)
            .courierTrackingUrl(DEFAULT_COURIER_TRACKING_URL)
            .digitalPlanId(DEFAULT_DIGITAL_PLAN_ID)
            .donorTelco(DEFAULT_DONOR_TELCO)
            .email(DEFAULT_EMAIL)
            .latLong(DEFAULT_LAT_LONG)
            .msisdn(DEFAULT_MSISDN)
            .name(DEFAULT_NAME)
            .postcode(DEFAULT_POSTCODE)
            .source(DEFAULT_SOURCE)
            .state(DEFAULT_STATE)
            .topupAmount(DEFAULT_TOPUP_AMOUNT)
            .apiKey(DEFAULT_API_KEY)
            .code(DEFAULT_CODE)
            .message(DEFAULT_MESSAGE)
            .createdDate(DEFAULT_CREATED_DATE)
            .modifiedDate(DEFAULT_MODIFIED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .modifiedBy(DEFAULT_MODIFIED_BY);
        return simOrder;
    }

    @Before
    public void initTest() {
        simOrder = createEntity(em);
    }

    @Test
    @Transactional
    public void createSimOrder() throws Exception {
        int databaseSizeBeforeCreate = simOrderRepository.findAll().size();

        // Create the SimOrder
        SimOrderDTO simOrderDTO = simOrderMapper.toDto(simOrder);
        restSimOrderMockMvc.perform(post("/api/sim-orders")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(simOrderDTO)))
            .andExpect(status().isCreated());

        // Validate the SimOrder in the database
        List<SimOrder> simOrderList = simOrderRepository.findAll();
        assertThat(simOrderList).hasSize(databaseSizeBeforeCreate + 1);
        SimOrder testSimOrder = simOrderList.get(simOrderList.size() - 1);
        assertThat(testSimOrder.getAgentCode()).isEqualTo(DEFAULT_AGENT_CODE);
        assertThat(testSimOrder.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testSimOrder.getCity()).isEqualTo(DEFAULT_CITY);
        assertThat(testSimOrder.getContact()).isEqualTo(DEFAULT_CONTACT);
        assertThat(testSimOrder.getCountry()).isEqualTo(DEFAULT_COUNTRY);
        assertThat(testSimOrder.getCourierTrackingUrl()).isEqualTo(DEFAULT_COURIER_TRACKING_URL);
        assertThat(testSimOrder.getDigitalPlanId()).isEqualTo(DEFAULT_DIGITAL_PLAN_ID);
        assertThat(testSimOrder.getDonorTelco()).isEqualTo(DEFAULT_DONOR_TELCO);
        assertThat(testSimOrder.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testSimOrder.getLatLong()).isEqualTo(DEFAULT_LAT_LONG);
        assertThat(testSimOrder.getMsisdn()).isEqualTo(DEFAULT_MSISDN);
        assertThat(testSimOrder.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testSimOrder.getPostcode()).isEqualTo(DEFAULT_POSTCODE);
        assertThat(testSimOrder.getSource()).isEqualTo(DEFAULT_SOURCE);
        assertThat(testSimOrder.getState()).isEqualTo(DEFAULT_STATE);
        assertThat(testSimOrder.getTopupAmount()).isEqualTo(DEFAULT_TOPUP_AMOUNT);
        assertThat(testSimOrder.getApiKey()).isEqualTo(DEFAULT_API_KEY);
        assertThat(testSimOrder.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testSimOrder.getMessage()).isEqualTo(DEFAULT_MESSAGE);
        assertThat(testSimOrder.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testSimOrder.getModifiedDate()).isEqualTo(DEFAULT_MODIFIED_DATE);
        assertThat(testSimOrder.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testSimOrder.getModifiedBy()).isEqualTo(DEFAULT_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void createSimOrderWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = simOrderRepository.findAll().size();

        // Create the SimOrder with an existing ID
        simOrder.setId(1L);
        SimOrderDTO simOrderDTO = simOrderMapper.toDto(simOrder);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSimOrderMockMvc.perform(post("/api/sim-orders")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(simOrderDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SimOrder in the database
        List<SimOrder> simOrderList = simOrderRepository.findAll();
        assertThat(simOrderList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSimOrders() throws Exception {
        // Initialize the database
        simOrderRepository.saveAndFlush(simOrder);

        // Get all the simOrderList
        restSimOrderMockMvc.perform(get("/api/sim-orders?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(simOrder.getId().intValue())))
            .andExpect(jsonPath("$.[*].agentCode").value(hasItem(DEFAULT_AGENT_CODE.toString())))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY.toString())))
            .andExpect(jsonPath("$.[*].contact").value(hasItem(DEFAULT_CONTACT.toString())))
            .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY.toString())))
            .andExpect(jsonPath("$.[*].courierTrackingUrl").value(hasItem(DEFAULT_COURIER_TRACKING_URL.toString())))
            .andExpect(jsonPath("$.[*].digitalPlanId").value(hasItem(DEFAULT_DIGITAL_PLAN_ID.intValue())))
            .andExpect(jsonPath("$.[*].donorTelco").value(hasItem(DEFAULT_DONOR_TELCO.toString())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].latLong").value(hasItem(DEFAULT_LAT_LONG.toString())))
            .andExpect(jsonPath("$.[*].msisdn").value(hasItem(DEFAULT_MSISDN.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].postcode").value(hasItem(DEFAULT_POSTCODE.toString())))
            .andExpect(jsonPath("$.[*].source").value(hasItem(DEFAULT_SOURCE.toString())))
            .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE.toString())))
            .andExpect(jsonPath("$.[*].topupAmount").value(hasItem(DEFAULT_TOPUP_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].apiKey").value(hasItem(DEFAULT_API_KEY.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].message").value(hasItem(DEFAULT_MESSAGE.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(sameInstant(DEFAULT_CREATED_DATE))))
            .andExpect(jsonPath("$.[*].modifiedDate").value(hasItem(sameInstant(DEFAULT_MODIFIED_DATE))))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].modifiedBy").value(hasItem(DEFAULT_MODIFIED_BY.toString())));
    }
    
    @Test
    @Transactional
    public void getSimOrder() throws Exception {
        // Initialize the database
        simOrderRepository.saveAndFlush(simOrder);

        // Get the simOrder
        restSimOrderMockMvc.perform(get("/api/sim-orders/{id}", simOrder.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(simOrder.getId().intValue()))
            .andExpect(jsonPath("$.agentCode").value(DEFAULT_AGENT_CODE.toString()))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS.toString()))
            .andExpect(jsonPath("$.city").value(DEFAULT_CITY.toString()))
            .andExpect(jsonPath("$.contact").value(DEFAULT_CONTACT.toString()))
            .andExpect(jsonPath("$.country").value(DEFAULT_COUNTRY.toString()))
            .andExpect(jsonPath("$.courierTrackingUrl").value(DEFAULT_COURIER_TRACKING_URL.toString()))
            .andExpect(jsonPath("$.digitalPlanId").value(DEFAULT_DIGITAL_PLAN_ID.intValue()))
            .andExpect(jsonPath("$.donorTelco").value(DEFAULT_DONOR_TELCO.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.latLong").value(DEFAULT_LAT_LONG.toString()))
            .andExpect(jsonPath("$.msisdn").value(DEFAULT_MSISDN.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.postcode").value(DEFAULT_POSTCODE.toString()))
            .andExpect(jsonPath("$.source").value(DEFAULT_SOURCE.toString()))
            .andExpect(jsonPath("$.state").value(DEFAULT_STATE.toString()))
            .andExpect(jsonPath("$.topupAmount").value(DEFAULT_TOPUP_AMOUNT.doubleValue()))
            .andExpect(jsonPath("$.apiKey").value(DEFAULT_API_KEY.toString()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.message").value(DEFAULT_MESSAGE.toString()))
            .andExpect(jsonPath("$.createdDate").value(sameInstant(DEFAULT_CREATED_DATE)))
            .andExpect(jsonPath("$.modifiedDate").value(sameInstant(DEFAULT_MODIFIED_DATE)))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.modifiedBy").value(DEFAULT_MODIFIED_BY.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSimOrder() throws Exception {
        // Get the simOrder
        restSimOrderMockMvc.perform(get("/api/sim-orders/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSimOrder() throws Exception {
        // Initialize the database
        simOrderRepository.saveAndFlush(simOrder);

        int databaseSizeBeforeUpdate = simOrderRepository.findAll().size();

        // Update the simOrder
        SimOrder updatedSimOrder = simOrderRepository.findById(simOrder.getId()).get();
        // Disconnect from session so that the updates on updatedSimOrder are not directly saved in db
        em.detach(updatedSimOrder);
        updatedSimOrder
            .agentCode(UPDATED_AGENT_CODE)
            .address(UPDATED_ADDRESS)
            .city(UPDATED_CITY)
            .contact(UPDATED_CONTACT)
            .country(UPDATED_COUNTRY)
            .courierTrackingUrl(UPDATED_COURIER_TRACKING_URL)
            .digitalPlanId(UPDATED_DIGITAL_PLAN_ID)
            .donorTelco(UPDATED_DONOR_TELCO)
            .email(UPDATED_EMAIL)
            .latLong(UPDATED_LAT_LONG)
            .msisdn(UPDATED_MSISDN)
            .name(UPDATED_NAME)
            .postcode(UPDATED_POSTCODE)
            .source(UPDATED_SOURCE)
            .state(UPDATED_STATE)
            .topupAmount(UPDATED_TOPUP_AMOUNT)
            .apiKey(UPDATED_API_KEY)
            .code(UPDATED_CODE)
            .message(UPDATED_MESSAGE)
            .createdDate(UPDATED_CREATED_DATE)
            .modifiedDate(UPDATED_MODIFIED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .modifiedBy(UPDATED_MODIFIED_BY);
        SimOrderDTO simOrderDTO = simOrderMapper.toDto(updatedSimOrder);

        restSimOrderMockMvc.perform(put("/api/sim-orders")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(simOrderDTO)))
            .andExpect(status().isOk());

        // Validate the SimOrder in the database
        List<SimOrder> simOrderList = simOrderRepository.findAll();
        assertThat(simOrderList).hasSize(databaseSizeBeforeUpdate);
        SimOrder testSimOrder = simOrderList.get(simOrderList.size() - 1);
        assertThat(testSimOrder.getAgentCode()).isEqualTo(UPDATED_AGENT_CODE);
        assertThat(testSimOrder.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testSimOrder.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testSimOrder.getContact()).isEqualTo(UPDATED_CONTACT);
        assertThat(testSimOrder.getCountry()).isEqualTo(UPDATED_COUNTRY);
        assertThat(testSimOrder.getCourierTrackingUrl()).isEqualTo(UPDATED_COURIER_TRACKING_URL);
        assertThat(testSimOrder.getDigitalPlanId()).isEqualTo(UPDATED_DIGITAL_PLAN_ID);
        assertThat(testSimOrder.getDonorTelco()).isEqualTo(UPDATED_DONOR_TELCO);
        assertThat(testSimOrder.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testSimOrder.getLatLong()).isEqualTo(UPDATED_LAT_LONG);
        assertThat(testSimOrder.getMsisdn()).isEqualTo(UPDATED_MSISDN);
        assertThat(testSimOrder.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testSimOrder.getPostcode()).isEqualTo(UPDATED_POSTCODE);
        assertThat(testSimOrder.getSource()).isEqualTo(UPDATED_SOURCE);
        assertThat(testSimOrder.getState()).isEqualTo(UPDATED_STATE);
        assertThat(testSimOrder.getTopupAmount()).isEqualTo(UPDATED_TOPUP_AMOUNT);
        assertThat(testSimOrder.getApiKey()).isEqualTo(UPDATED_API_KEY);
        assertThat(testSimOrder.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testSimOrder.getMessage()).isEqualTo(UPDATED_MESSAGE);
        assertThat(testSimOrder.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testSimOrder.getModifiedDate()).isEqualTo(UPDATED_MODIFIED_DATE);
        assertThat(testSimOrder.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testSimOrder.getModifiedBy()).isEqualTo(UPDATED_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void updateNonExistingSimOrder() throws Exception {
        int databaseSizeBeforeUpdate = simOrderRepository.findAll().size();

        // Create the SimOrder
        SimOrderDTO simOrderDTO = simOrderMapper.toDto(simOrder);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSimOrderMockMvc.perform(put("/api/sim-orders")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(simOrderDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SimOrder in the database
        List<SimOrder> simOrderList = simOrderRepository.findAll();
        assertThat(simOrderList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSimOrder() throws Exception {
        // Initialize the database
        simOrderRepository.saveAndFlush(simOrder);

        int databaseSizeBeforeDelete = simOrderRepository.findAll().size();

        // Delete the simOrder
        restSimOrderMockMvc.perform(delete("/api/sim-orders/{id}", simOrder.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<SimOrder> simOrderList = simOrderRepository.findAll();
        assertThat(simOrderList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SimOrder.class);
        SimOrder simOrder1 = new SimOrder();
        simOrder1.setId(1L);
        SimOrder simOrder2 = new SimOrder();
        simOrder2.setId(simOrder1.getId());
        assertThat(simOrder1).isEqualTo(simOrder2);
        simOrder2.setId(2L);
        assertThat(simOrder1).isNotEqualTo(simOrder2);
        simOrder1.setId(null);
        assertThat(simOrder1).isNotEqualTo(simOrder2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SimOrderDTO.class);
        SimOrderDTO simOrderDTO1 = new SimOrderDTO();
        simOrderDTO1.setId(1L);
        SimOrderDTO simOrderDTO2 = new SimOrderDTO();
        assertThat(simOrderDTO1).isNotEqualTo(simOrderDTO2);
        simOrderDTO2.setId(simOrderDTO1.getId());
        assertThat(simOrderDTO1).isEqualTo(simOrderDTO2);
        simOrderDTO2.setId(2L);
        assertThat(simOrderDTO1).isNotEqualTo(simOrderDTO2);
        simOrderDTO1.setId(null);
        assertThat(simOrderDTO1).isNotEqualTo(simOrderDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(simOrderMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(simOrderMapper.fromId(null)).isNull();
    }
}
