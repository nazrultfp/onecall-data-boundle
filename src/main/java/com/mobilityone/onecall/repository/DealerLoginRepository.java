package com.mobilityone.onecall.repository;

import com.mobilityone.onecall.domain.DealerLogin;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Spring Data  repository for the DealerLogin entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DealerLoginRepository extends JpaRepository<DealerLogin, Long> {

    Optional<DealerLogin> findByDealerUserNameAndTokenExpiredFalse(String username);

    Optional<DealerLogin> findFirstByRespLstAgentCode(String agentCode);
}
