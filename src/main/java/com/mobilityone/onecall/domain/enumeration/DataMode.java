package com.mobilityone.onecall.domain.enumeration;

/**
 * The DataMode enumeration.
 */
public enum DataMode {
    VOICE, DATA
}
