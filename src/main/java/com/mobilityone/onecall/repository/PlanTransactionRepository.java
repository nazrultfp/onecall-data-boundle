package com.mobilityone.onecall.repository;

import com.mobilityone.onecall.domain.PlanTransaction;
import com.mobilityone.onecall.service.dto.PlanTransactionReportDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the PlanTransaction entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PlanTransactionRepository extends JpaRepository<PlanTransaction, Long> {

    @Query("select new com.mobilityone.onecall.service.dto.PlanTransactionReportDTO(pt.transactionDate , pt.orderType , pt.orderCode , m.merchantCode , m.merchantName , " +
        "p.description , count(pt) , pt.totalAmount , pt.amount , pt.discount , pt.discountType , " +
        "pt.processingFee , pt.netAmount , pt.status , pt.transactionId) from PlanTransaction pt " +
        "inner join Merchant m on pt.merchant.id = m.id " +
        "inner join Plan p on pt.plan.id = p.id " +
        "group by m.id")
    Page<PlanTransactionReportDTO> getPlanTransactionReport(Pageable pageable);
}
