package com.mobilityone.onecall.service.impl;

import com.mobilityone.onecall.service.SubscriptionPlanService;
import com.mobilityone.onecall.domain.SubscriptionPlan;
import com.mobilityone.onecall.repository.SubscriptionPlanRepository;
import com.mobilityone.onecall.service.dto.SubscriptionPlanDTO;
import com.mobilityone.onecall.service.dto.SubscriptionReqDTO;
import com.mobilityone.onecall.service.dto.TuneTalkRespDTO;
import com.mobilityone.onecall.service.mapper.SubscriptionPlanMapper;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Service Implementation for managing SubscriptionPlan.
 */
@Service
@Transactional
public class SubscriptionPlanServiceImpl implements SubscriptionPlanService {

    private final Logger log = LoggerFactory.getLogger(SubscriptionPlanServiceImpl.class);

    private final SubscriptionPlanRepository subscriptionPlanRepository;

    private final SubscriptionPlanMapper subscriptionPlanMapper;

    public SubscriptionPlanServiceImpl(SubscriptionPlanRepository subscriptionPlanRepository, SubscriptionPlanMapper subscriptionPlanMapper) {
        this.subscriptionPlanRepository = subscriptionPlanRepository;
        this.subscriptionPlanMapper = subscriptionPlanMapper;
    }

    /**
     * Save a subscriptionPlan.
     *
     * @param subscriptionPlanDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public SubscriptionPlanDTO save(SubscriptionPlanDTO subscriptionPlanDTO) {
        log.debug("Request to save SubscriptionPlan : {}", subscriptionPlanDTO);
        SubscriptionPlan subscriptionPlan = subscriptionPlanMapper.toEntity(subscriptionPlanDTO);
        subscriptionPlan = subscriptionPlanRepository.save(subscriptionPlan);
        return subscriptionPlanMapper.toDto(subscriptionPlan);
    }

    /**
     * Get all the subscriptionPlans.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<SubscriptionPlanDTO> findAll(Pageable pageable) {
        log.debug("Request to get all SubscriptionPlans");
        return subscriptionPlanRepository.findAll(pageable)
            .map(subscriptionPlanMapper::toDto);
    }


    /**
     * Get one subscriptionPlan by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<SubscriptionPlanDTO> findOne(Long id) {
        log.debug("Request to get SubscriptionPlan : {}", id);
        return subscriptionPlanRepository.findById(id)
            .map(subscriptionPlanMapper::toDto);
    }

    /**
     * Delete the subscriptionPlan by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete SubscriptionPlan : {}", id);
        subscriptionPlanRepository.deleteById(id);
    }

    @Override
    public SubscriptionPlanDTO save(SubscriptionReqDTO subscriptionReqDTO, TuneTalkRespDTO tuneTalkRespDTO) {
        log.debug("Request to save SubscriptionPlan by req : {} and resp : {}", subscriptionReqDTO, tuneTalkRespDTO);

        SubscriptionPlanDTO planDTO = new SubscriptionPlanDTO();
        planDTO.setKeyword(subscriptionReqDTO.getKeyword());
        planDTO.setMsisdn(subscriptionReqDTO.getMsisdn());
        planDTO.setTransactionId(subscriptionReqDTO.getTransactionId());
        planDTO.setApiKey(tuneTalkRespDTO.getApiKey());
        planDTO.setCode(tuneTalkRespDTO.getCode());
        planDTO.setMessage(tuneTalkRespDTO.getMessage());

        planDTO = save(planDTO);

        log.info("saved data : {}", planDTO);

        return planDTO;
    }

    @Override
    public SubscriptionPlanDTO findByTransactionId(String transactionId) {
        log.debug("REST request to get list of SubscriptionPlans by transactionId : {}" , transactionId);
        Optional<SubscriptionPlan> subscriptionPlan = subscriptionPlanRepository.findByTransactionId(transactionId);
        if (!subscriptionPlan.isPresent()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Subscription not found");
        }

        return subscriptionPlanMapper.toDto(subscriptionPlan.get());
    }

    @Override
    public Page<SubscriptionPlanDTO> findSuccessfulByMsisdn(Pageable pageable, String msisdn) {
        log.debug("Request to get successful SubscriptionPlan by msisdn : {}" , msisdn);
        return subscriptionPlanRepository.findByMsisdnAndCode(pageable, msisdn,"200")
            .map(subscriptionPlanMapper::toDto);
    }

    @Override
    public List<Map<String, Object>> report() {
        log.debug("Request to get all SubscriptionPlan with their Keywords.");

        List<Map<String, Object>> report = subscriptionPlanRepository.report();

        return report;
    }

    @Override
    public JSONObject getTotalPlansByPackageAndDate(String pakage, Integer year, Integer month, Integer day) {
        log.info("Request to get total number of subscriptions for package : {} at {}-{}-{}.", pakage, year, month, day);

        if (month < 1 || month > 12 || day < 1 || day > 31) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "date is not correct");
        }

        String date = String.valueOf(year);
        date = date.concat("-").concat(String.format("%02d", month));
        date = date.concat("-").concat(String.format("%02d", day));

        log.info("date format : {}", date);

        Integer count = subscriptionPlanRepository.countByPackageAndDate(pakage, date);

        JSONObject object = new JSONObject();
        object.put("date", date);
        object.put("package", pakage);
        object.put("total", count);

        log.info("total number of port-in sims : {}", object);

        return object;
    }

    @Override
    public List<Map<String, Object>> getTotalPlansByPackage(String pakage) {
        log.info("Request to get total number of subscriptions for package : {} group by date", pakage);

        List<Map<String, Object>> list = subscriptionPlanRepository.getTotalPlansByPackage(pakage);

        log.info("result : {}", list);

        return list;
    }

    @Override
    public List<Map<String, Object>> getTotalPlansGroupByPackageAndDate() {
        log.info("REST request to get total number of subscriptions group by package and date");

        List<Map<String, Object>> list = subscriptionPlanRepository.getTotalPlansGroupByPackageAndDate();

        log.info("result : {}", list);

        return list;
    }

    @Override
    public List<Map<String, Object>> getTotalPlansByDateRange(String fromDate, String toDate) {
        log.info("Request to get total number of subscriptions group by package and date from {} to {}", fromDate, toDate);

        List<Map<String, Object>> list = subscriptionPlanRepository.getTotalPlansByDateRange(fromDate, toDate);

        log.info("result : {}", list);

        return list;
    }

    @Override
    public List<Map<String, Object>> getDailyTotalSaleInRange(String fromDate, String toDate) {
        log.info("Request to get daily total sales of subscriptions in date range from {} to {}", fromDate, toDate);

        List<Map<String, Object>> list = subscriptionPlanRepository.getDailyTotalSaleInRange(fromDate, toDate);

        log.info("result : {}", list);

        return list;
    }

    @Override
    public List<Map<String, Object>> getDailyTotalSaleInRangeForClient(String client, String fromDate, String toDate) {
        log.info("REST request to get client {} daily total sales of subscriptions in date range from {} to {}", client, fromDate, toDate);

        List<Map<String, Object>> list = subscriptionPlanRepository.getDailyTotalSaleInRangeForClient("%".concat(client).concat("%"), fromDate, toDate);

        log.info("result : {}", list);

        return list;
    }

    @Override
    public List<Map<String, Object>> getPlansInRange(String fromDate, String toDate) {
        log.info("Request to get subscription plans in date range from {} to {}", fromDate, toDate);

        List<Map<String, Object>> list = subscriptionPlanRepository.getPlansInRange(fromDate, toDate);

        log.info("result : {}", list);

        return list;
    }
}
