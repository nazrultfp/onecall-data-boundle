package com.mobilityone.onecall.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mobilityone.onecall.config.ApplicationProperties;
import com.mobilityone.onecall.domain.OneCall;
import com.mobilityone.onecall.service.TuneTalkService;
import com.mobilityone.onecall.service.dto.*;
import com.mobilityone.onecall.service.mapper.TopupValuesMapper;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Calendar;

@Service
public class TuneTalkServiceImpl implements TuneTalkService {
    private final Logger log = LoggerFactory.getLogger(TuneTalkServiceImpl.class);

    private final ApplicationProperties applicationProperties;

    private final TopupValuesMapper topupValuesMapper;

    public TuneTalkServiceImpl(ApplicationProperties applicationProperties, TopupValuesMapper topupValuesMapper) {
        this.applicationProperties = applicationProperties;
        this.topupValuesMapper = topupValuesMapper;
    }

    @Override
    public TtBalabceRespDTO getBalanceByIccid(String iccid) {
        log.info("Call Tune Talk Service to get member balance by Iccid");

        JSONObject respObject = null;

        String url = applicationProperties.getUrl().getTtBaseUrl() + "/balance/" + iccid + "/iccid";

        try {

            ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

            respObject = getFromTuneTalk(url, Boolean.TRUE);

            TtBalabceRespDTO respDTO = mapper.readValue(respObject.toString(), TtBalabceRespDTO.class);
            log.info("Response mapped : {}", respDTO);

            return respDTO;
        } catch (IOException e) {
            if (respObject.has("code") && !respObject.getString("code").equals("200")){
                throw new ResponseStatusException(HttpStatus.valueOf(Integer.parseInt(respObject.getString("code"))), respObject.getString("message"));
            }
            throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, "mapping data failed");
        }

//        RestTemplate restTemplate = new RestTemplate();
//
//        // request header
//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.APPLICATION_JSON);
//        headers.set("secretKey", applicationProperties.getKey().getTtBaseSecret());
//
//        final HttpEntity<?> entity = new HttpEntity<>(headers);
//
//        log.info("URL : {}", url);
//
//        ResponseEntity<TtBalabceRespDTO> exchange = null;
//        try {
//            exchange = restTemplate.exchange(url, HttpMethod.GET, entity, TtBalabceRespDTO.class);
//        } catch (HttpClientErrorException e) {
//            throw new ResponseStatusException(e.getStatusCode(), "Connection to Tune Talk Api Failed.");
//        }
//
//        return exchange.getBody();
    }

    @Override
    public TtBalabceRespDTO getBalanceByImsiNumber(String imsi) {
        log.info("Call Tune Talk Service to get member balance by IMSI Number");

        JSONObject respObject = null;

        String url = applicationProperties.getUrl().getTtBaseUrl() + "/balance/" + imsi;

        try {

            ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

            respObject = getFromTuneTalk(url, Boolean.TRUE);

            TtBalabceRespDTO respDTO = mapper.readValue(respObject.toString(), TtBalabceRespDTO.class);
            log.info("Response mapped : {}", respDTO);

            return respDTO;
        } catch (IOException e) {
            if (respObject.has("code") && !respObject.getString("code").equals("200")){
                throw new ResponseStatusException(HttpStatus.valueOf(Integer.parseInt(respObject.getString("code"))), respObject.getString("message"));
            }
            throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, "mapping data failed");
        }

//        RestTemplate restTemplate = new RestTemplate();
//
//        // request header
//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.APPLICATION_JSON);
//        headers.set("secretKey", applicationProperties.getKey().getTtBaseSecret());
//
//        final HttpEntity<?> entity = new HttpEntity<>(headers);
//
//        log.info("URL : {}", url);
//
//        ResponseEntity<TtBalabceRespDTO> exchange = null;
//        try {
//            exchange = restTemplate.exchange(url, HttpMethod.GET, entity, TtBalabceRespDTO.class);
//        } catch (HttpClientErrorException e) {
//            throw new ResponseStatusException(e.getStatusCode(), "Connection to Tune Talk Api Failed.");
//        }
//
//        return exchange.getBody();
    }

    @Override
    public TtBalabceRespDTO getBalanceByMsisdn(String msisdn) {
        log.info("Call Tune Talk to get Balance for {} Msisdn.", msisdn);

        JSONObject respObject = null;

        final String url = applicationProperties.getUrl().getTtBaseUrl() + "/balance/" + msisdn + "/msisdn";

        try {

            ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

            respObject = getFromTuneTalk(url, Boolean.TRUE);


            TtBalabceRespDTO respDTO = mapper.readValue(respObject.toString(), TtBalabceRespDTO.class);
            log.info("Response mapped : {}", respDTO);

            return respDTO;
        } catch (IOException e) {
            if (respObject.has("code") && !respObject.getString("code").equals("200")){
                throw new ResponseStatusException(HttpStatus.valueOf(Integer.parseInt(respObject.getString("code"))), respObject.getString("message"));
            }
            throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, "mapping data failed");
        }

//        RestTemplate restTemplate = new RestTemplate();
//
//        // request header
//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.APPLICATION_JSON);
//        headers.set("secretKey", applicationProperties.getKey().getTtBaseSecret());
//
//        final HttpEntity<?> entity = new HttpEntity<>(headers);
//
//        log.info("URL : {}", url);
//
//        ResponseEntity<TtBalabceRespDTO> exchange = null;
//        try {
//            exchange = restTemplate.exchange(url, HttpMethod.GET, entity, TtBalabceRespDTO.class);
//        } catch (HttpClientErrorException e) {
//            throw new ResponseStatusException(e.getStatusCode(), "Connection to Tune Talk Api Failed.", e);
//        }
//
//        return exchange.getBody();
    }

    @Override
    public TuneTalkRespDTO bigShotConvert(BigShotConvertReqDTO bigShotConvertReqDTO) {
        log.info("Convert BIG Points : {}", bigShotConvertReqDTO.toString());

        JSONObject respObject = null;

        final String url = applicationProperties.getUrl().getTtBaseUrl() + "/bigshot/convert";

        try {

            ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

            respObject = getFromTuneTalk(url, Boolean.TRUE);

            TuneTalkRespDTO respDTO = mapper.readValue(respObject.toString(), TuneTalkRespDTO.class);
            log.info("Response mapped : {}", respDTO);

            return respDTO;
        } catch (IOException e) {
            if (respObject.has("code") && !respObject.getString("code").equals("200")){
                throw new ResponseStatusException(HttpStatus.valueOf(Integer.parseInt(respObject.getString("code"))), respObject.getString("message"));
            }
            throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, "mapping data failed");
        }


//        RestTemplate restTemplate = new RestTemplate();
//
//        // request header
//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.APPLICATION_JSON);
//        headers.set("secretKey", applicationProperties.getKey().getTtBaseSecret());
//
//        final HttpEntity<?> entity = new HttpEntity<>(bigShotConvertReqDTO, headers);
//
//        log.info("URL : {}", url);
//
//        ResponseEntity<TuneTalkRespDTO> exchange = null;
//        try {
//            exchange = restTemplate.exchange(url, HttpMethod.GET, entity, TuneTalkRespDTO.class);
//        } catch (HttpClientErrorException e) {
//            throw new ResponseStatusException(e.getStatusCode(), "Connection to Tune Talk Api Failed.");
//        }
//
//        return exchange.getBody();

    }

    @Override
    public TuneTalkRespDTO lookupBigShotByEmail(String email) {
        log.info("Look up Tune Talk for Big Shot by Email : {}", email);

        JSONObject respObject = null;

        final String url = applicationProperties.getUrl().getTtBaseUrl() + "/bigshot/lookup";

        try {
            ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

            JSONObject object = new JSONObject();
            object = object.put("email", email);

            log.info("Request Json : {}", object.toString());

            respObject = postToTuneTalk(url, object);

            TuneTalkRespDTO respDTO = mapper.readValue(respObject.toString(), TuneTalkRespDTO.class);
            log.info("Response mapped : {}", respDTO);

            return respDTO;
        } catch (IOException e) {
            if (respObject.has("code") && !respObject.getString("code").equals("200")){
                throw new ResponseStatusException(HttpStatus.valueOf(Integer.parseInt(respObject.getString("code"))), respObject.getString("message"));
            }
            throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, "mapping data failed");
        }

//        RestTemplate restTemplate = new RestTemplate();
//
//        // request header
//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.APPLICATION_JSON);
//        headers.set("secretKey", applicationProperties.getKey().getTtBaseSecret());
//
//        JsonObject object = new JsonObject();
//        object = object.add("email", email);
//
//        final HttpEntity<?> entity = new HttpEntity<>(object, headers);
//
//        log.info("URL : {}", url);
//
//        ResponseEntity<TuneTalkRespDTO> exchange = null;
//        try {
//            exchange = restTemplate.exchange(url, HttpMethod.POST, entity, TuneTalkRespDTO.class);
//        } catch (HttpClientErrorException e) {
//            throw new ResponseStatusException(e.getStatusCode(), "Connection to Tune Talk Api Failed.");
//        }
//
//        return exchange.getBody();
    }

    @Override
    public TuneTalkRespDTO cancelLocationByMsidn(String msisdn) {
        log.info("Cancel Location By Msisdn : {}", msisdn);

        JSONObject respObject = null;

        final String url = applicationProperties.getUrl().getTtBaseUrl() + "/cancelLocation/" + msisdn;

        try {

            ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

            respObject = getFromTuneTalk(url, Boolean.TRUE);

            TuneTalkRespDTO respDTO = mapper.readValue(respObject.toString(), TuneTalkRespDTO.class);
            log.info("Response mapped : {}", respDTO);

            return respDTO;
        } catch (IOException e) {
            if (respObject.has("code") && !respObject.getString("code").equals("200")){
                throw new ResponseStatusException(HttpStatus.valueOf(Integer.parseInt(respObject.getString("code"))), respObject.getString("message"));
            }
            throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, "mapping data failed");
        }

//        RestTemplate restTemplate = new RestTemplate();
//
//        // request header
//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.APPLICATION_JSON);
//        headers.set("secretKey", applicationProperties.getKey().getTtBaseSecret());
//
//        final HttpEntity<?> entity = new HttpEntity<>(headers);
//
//        log.info("URL : {}", url);
//
//        ResponseEntity<TuneTalkRespDTO> exchange = null;
//        try {
//            exchange = restTemplate.exchange(url, HttpMethod.GET, entity, TuneTalkRespDTO.class);
//        } catch (HttpClientErrorException e) {
//            throw new ResponseStatusException(e.getStatusCode(), "Connection to Tune Talk Api Failed.");
//        }
//
//        return exchange.getBody();

    }

    @Override
    public DebitBalanceRespDTO debitBalance(DebitBalanceReqDTO debitBalanceReqDTO) {
        log.info("Request to debit the balance : {}", debitBalanceReqDTO.toString());

        JSONObject respObject = null;

        final String url = applicationProperties.getUrl().getTtBaseUrl() + "/debit";

        try {
            ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

            String reqStr = mapper.writeValueAsString(debitBalanceReqDTO);
            log.info("Request Json : {}", reqStr);

            respObject = postToTuneTalk(url, new JSONObject(reqStr));

            DebitBalanceRespDTO respDTO = mapper.readValue(respObject.toString(), DebitBalanceRespDTO.class);
            log.info("Response mapped : {}", respDTO);

            return respDTO;
        } catch (IOException e) {
            if (respObject.has("code") && !respObject.getString("code").equals("200")){
                throw new ResponseStatusException(HttpStatus.valueOf(Integer.parseInt(respObject.getString("code"))), respObject.getString("message"));
            }
            throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, "mapping data failed");
        }

//        RestTemplate restTemplate = new RestTemplate();
//
//        // request header
//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.APPLICATION_JSON);
//        headers.set("secretKey", applicationProperties.getKey().getTtBaseSecret());
//
//        final HttpEntity<?> entity = new HttpEntity<>(debitBalanceReqDTO, headers);
//
//        log.info("URL : {}", url);
//
//        ResponseEntity<DebitBalanceRespDTO> exchange = null;
//        try {
//            exchange = restTemplate.exchange(url, HttpMethod.POST, entity, DebitBalanceRespDTO.class);
//        } catch (HttpClientErrorException e) {
//            throw new ResponseStatusException(e.getStatusCode(), "Connection to Tune Talk Api Failed.");
//        }
//
//        return exchange.getBody();
    }

    @Override
    public JSONObject getMsisdnListByIdNumber(String idNumber) {
        log.info("Request to get Msisdn List By Id Number : {}", idNumber);

        final String url = applicationProperties.getUrl().getTtBaseUrl() + "/msisdn/" + idNumber;

        return getFromTuneTalk(url, Boolean.TRUE);

//        RestTemplate restTemplate = new RestTemplate();
//
//        // request header
//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.APPLICATION_JSON);
//        headers.set("secretKey", applicationProperties.getKey().getTtBaseSecret());
//
//        final HttpEntity<?> entity = new HttpEntity<>(headers);
//
//        log.info("URL : {}", url);
//
//        ResponseEntity<String> exchange = null;
//        try {
//            exchange = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
//        } catch (HttpClientErrorException e) {
//            throw new ResponseStatusException(e.getStatusCode(), "Connection to Tune Talk Api Failed.");
//        }
//
//        JSONObject jsonObject = new JSONObject(exchange.getBody());
//
//        return jsonObject;
    }

    @Override
    public JSONObject getPortStatusByMsisdn(String msisdn) {
        log.info("Request to get Port Status By Msisdn : {}", msisdn);

        final String url = applicationProperties.getUrl().getTtBaseUrl() + "/portInStatus/" + msisdn;

        return getFromTuneTalk(url, Boolean.TRUE);

//        RestTemplate restTemplate = new RestTemplate();
//
//        // request header
//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.APPLICATION_JSON);
//        headers.set("secretKey", applicationProperties.getKey().getTtBaseSecret());
//
//        final HttpEntity<?> entity = new HttpEntity<>(headers);
//
//        log.info("URL : {}", url);
//
//        ResponseEntity<String> exchange = null;
//        try {
//            exchange = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
//        } catch (HttpClientErrorException e) {
//            throw new ResponseStatusException(e.getStatusCode(), "Connection to Tune Talk Api Failed.");
//        }
//
//        JSONObject jsonObject = new JSONObject(exchange.getBody());
//
//        return jsonObject;
    }

    @Override
    public JSONObject getPreferNumberList(String prefix) {
        log.info("Request to get Prefer Number List for : {}", prefix);

        final String url = applicationProperties.getUrl().getTtBaseUrl() + "/preferNumber/" + prefix;

        return getFromTuneTalk(url, Boolean.TRUE);

//        RestTemplate restTemplate = new RestTemplate();
//
//        // request header
//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.APPLICATION_JSON);
//        headers.set("secretKey", applicationProperties.getKey().getTtBaseSecret());
//
//        final HttpEntity<?> entity = new HttpEntity<>(headers);
//
//        log.info("URL : {}", url);
//
//        ResponseEntity<String> exchange = null;
//        try {
//            exchange = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
//        } catch (HttpClientErrorException e) {
//            throw new ResponseStatusException(e.getStatusCode(), "Connection to Tune Talk Api Failed.");
//        }
//
//        JSONObject jsonObject = new JSONObject(exchange.getBody());
//
//        return jsonObject;
    }

    @Override
    public JSONObject getMemberProfileByIccid(String iccid) {
        log.info("Request to get Member Profile By ICCID : {}", iccid);

        final String url = applicationProperties.getUrl().getTtBaseUrl() + "/profile/" + iccid + "/iccid";

        return getFromTuneTalk(url, Boolean.TRUE);

//        RestTemplate restTemplate = new RestTemplate();
//
//        // request header
//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.APPLICATION_JSON);
//        headers.set("secretKey", applicationProperties.getKey().getTtBaseSecret());
//
//        final HttpEntity<?> entity = new HttpEntity<>(headers);
//
//        log.info("URL : {}", url);
//
//        ResponseEntity<String> exchange = null;
//        try {
//            exchange = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
//        } catch (HttpClientErrorException e) {
//            throw new ResponseStatusException(e.getStatusCode(), "Connection to Tune Talk Api Failed.");
//        }
//
//        JSONObject jsonObject = new JSONObject(exchange.getBody());
//
//        return jsonObject;
    }

    @Override
    public JSONObject getMemberProfileByImsiNumber(String imsi) {
        log.info("Request to get Member Profile By IMSI Number : {}", imsi);

        final String url = applicationProperties.getUrl().getTtBaseUrl() + "/profile/" + imsi + "/imsi";

        return getFromTuneTalk(url, Boolean.TRUE);

//        RestTemplate restTemplate = new RestTemplate();
//
//        // request header
//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.APPLICATION_JSON);
//        headers.set("secretKey", applicationProperties.getKey().getTtBaseSecret());
//
//        final HttpEntity<?> entity = new HttpEntity<>(headers);
//
//        log.info("URL : {}", url);
//
//        ResponseEntity<String> exchange = null;
//        try {
//            exchange = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
//        } catch (HttpClientErrorException e) {
//            throw new ResponseStatusException(e.getStatusCode(), "Connection to Tune Talk Api Failed.");
//        }
//
//        JSONObject jsonObject = new JSONObject(exchange.getBody());
//
//        return jsonObject;
    }

    @Override
    public JSONObject getMemberProfileByMsisdn(String msisdn) {
        log.info("Request to get Member Profile By Msisdn : {}", msisdn);

        final String url = applicationProperties.getUrl().getTtBaseUrl() + "/profile/" + msisdn + "/msisdn";

        return getFromTuneTalk(url, Boolean.TRUE);

//        RestTemplate restTemplate = new RestTemplate();
//
//        // request header
//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.APPLICATION_JSON);
//        headers.set("secretKey", applicationProperties.getKey().getTtBaseSecret());
//
//        final HttpEntity<?> entity = new HttpEntity<>(headers);
//
//        log.info("URL : {}", url);
//
//        ResponseEntity<String> exchange = null;
//        try {
//            exchange = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
//        } catch (HttpClientErrorException e) {
//            throw new ResponseStatusException(e.getStatusCode(), "Connection to Tune Talk Api Failed.");
//        }
//
//        JSONObject jsonObject = new JSONObject(exchange.getBody());
//
//        return jsonObject;
    }

    @Override
    public TuneTalkRespDTO checkSimRegisteredStatus(String simNumber) {
        log.info("Request to check SIM Registered Status By SIM Number : {}", simNumber);

        JSONObject respObject = null;

        final String url = applicationProperties.getUrl().getTtBaseUrl() + "/sim/query/" + simNumber;

        try {

            ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

            respObject = getFromTuneTalk(url, Boolean.TRUE);

            TuneTalkRespDTO respDTO = mapper.readValue(respObject.toString(), TuneTalkRespDTO.class);
            log.info("Response mapped : {}", respDTO);

            return respDTO;
        } catch (IOException e) {
            if (respObject.has("code") && !respObject.getString("code").equals("200")){
                throw new ResponseStatusException(HttpStatus.valueOf(Integer.parseInt(respObject.getString("code"))), respObject.getString("message"));
            }
            throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, "mapping data failed");
        }

//        RestTemplate restTemplate = new RestTemplate();
//
//        // request header
//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.APPLICATION_JSON);
//        headers.set("secretKey", applicationProperties.getKey().getTtBaseSecret());
//
//        final HttpEntity<?> entity = new HttpEntity<>(headers);
//
//        log.info("URL : {}", url);
//
//        ResponseEntity<TuneTalkRespDTO> exchange = null;
//        try {
//            exchange = restTemplate.exchange(url, HttpMethod.GET, entity, TuneTalkRespDTO.class);
//        } catch (HttpClientErrorException e) {
//            throw new ResponseStatusException(e.getStatusCode(), "Connection to Tune Talk Api Failed.");
//        }
//
//        return exchange.getBody();
    }

    @Override
    public JSONObject simOrder(SimOrderReqDTO simOrderReqDTO) {
        log.info("Request to Order SIM : {}", simOrderReqDTO.toString());

        final String url = applicationProperties.getUrl().getTtBaseUrl() + "/simOrder";

        try {
            ObjectMapper mapper = new ObjectMapper();
            String reqObject = mapper.writeValueAsString(simOrderReqDTO);
            log.info("Request Json : {}", reqObject);

            return postToTuneTalk(url, new JSONObject(reqObject));
        } catch (JsonProcessingException e) {
            throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, "mapping data failed");
        }


//        RestTemplate restTemplate = new RestTemplate();
//
//        // request header
//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.APPLICATION_JSON);
//        headers.set("secretKey", applicationProperties.getKey().getTtBaseSecret());
//
//        final HttpEntity<?> entity = new HttpEntity<>(simOrderReqDTO, headers);
//
//        log.info("URL : {}", url);
//
//        ResponseEntity<String> exchange = null;
//        try {
//            exchange = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
//        } catch (HttpClientErrorException e) {
//            throw new ResponseStatusException(e.getStatusCode(), "Connection to Tune Talk Api Failed.");
//        }
//
//        JSONObject jsonObject = new JSONObject(exchange.getBody());
//
//        return jsonObject;
    }

    @Override
    public JSONObject simPortIn(JSONObject reqObject) {
        log.info("Request to Post SIM Port In : {}", reqObject.toString());

        final String url = applicationProperties.getUrl().getTtBaseUrl() + "/simPortIn";

        return postToTuneTalk(url, reqObject);

//        RestTemplate restTemplate = new RestTemplate();
//
//        // request header
//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.APPLICATION_JSON);
//        headers.set("secretKey", applicationProperties.getKey().getTtBaseSecret());
//
//        final HttpEntity<?> entity = new HttpEntity<>(simPortInReqDTO, headers);
//
//        log.info("URL : {}", url);
//
//        ResponseEntity<TuneTalkRespDTO> exchange = null;
//        try {
//            exchange = restTemplate.exchange(url, HttpMethod.POST, entity, TuneTalkRespDTO.class);
//        } catch (HttpClientErrorException e) {
//            throw new ResponseStatusException(e.getStatusCode(), "Connection to Tune Talk Api Failed.");
//        }

//        HttpResponse response = null;
//        try {
//            HttpClient httpclient = HttpClients.createDefault();
//            HttpPost httppost = new HttpPost(url);
//
//// Request parameters and other properties.
//            httppost.setEntity(new StringEntity(reqObject.toString()));
//            httppost.addHeader("content-type", "application/json");
//            httppost.addHeader("secretKey", applicationProperties.getKey().getTtBaseSecret());
//            response = httpclient.execute(httppost);
//            org.apache.http.HttpEntity entity = response.getEntity();
//            String resBody = IOUtils.toString(entity.getContent(), StandardCharsets.UTF_8.name());
//            ObjectMapper mapper = new ObjectMapper();
//            TuneTalkRespDTO tuneTalkRespDTO = mapper.readValue(resBody, TuneTalkRespDTO.class);
//            log.info("==>>>> PortIn Resp Body: {}", resBody);
//            return new JSONObject(resBody);
//        } catch (IOException e) {
//            throw new ResponseStatusException(HttpStatus.FAILED_DEPENDENCY, "Send data to Tune Talk Api Failed.", e);
//        }
    }

    @Override
    public JSONObject simRegistration(JSONObject reqObject) {
        log.info("Request to Register SIM : {}", reqObject.toString());

        final String url = applicationProperties.getUrl().getTtBaseUrl() + "/simReg";

        return postToTuneTalk(url, reqObject);


//        RestTemplate restTemplate = new RestTemplate();
//
//        // request header
//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.APPLICATION_JSON);
//        headers.set("secretKey", applicationProperties.getKey().getTtBaseSecret());
//
//        final HttpEntity<?> entity = new HttpEntity<>(reqObject, headers);
//
//        log.info("URL : {}", url);
//        log.info("Request Entity : {}", entity);
//
//        String exchange = null;
//        try {
//            exchange = restTemplate.postForObject(url, entity, String.class);
////        }catch (RestClientException e){
//        } catch (HttpClientErrorException e) {
//            log.debug("ERROR Status Code: {}", e.getStatusCode());
//            log.debug("ERROR Message: {}", e.getMessage());
////            log.debug("ERROR : {}", e.getStackTrace());
//            throw new ResponseStatusException(e.getStatusCode(), "Send data to Tune Talk Api Failed.", e);
//        }

//        JSONObject jsonObject = new JSONObject(exchange);

//        log.debug("TT Response : {}", jsonObject);

//        return jsonObject;


//Execute and get the response.
//        HttpResponse response = null;
//        try {
//            HttpClient httpclient = HttpClients.createDefault();
//            HttpPost httppost = new HttpPost(url);
//
//// Request parameters and other properties.
//            httppost.setEntity(new StringEntity(reqObject.toString()));
//            httppost.addHeader("content-type", "application/json");
//            httppost.addHeader("secretKey", applicationProperties.getKey().getTtBaseSecret());
//            response = httpclient.execute(httppost);
//            org.apache.http.HttpEntity entity = response.getEntity();
//            String resBody = IOUtils.toString(entity.getContent(), StandardCharsets.UTF_8.name());
//            log.info("==>>>> Body: {}", resBody);
//            return new JSONObject(resBody);
//        } catch (IOException e) {
//            throw new ResponseStatusException(HttpStatus.FAILED_DEPENDENCY, "Send data to Tune Talk Api Failed.", e);
//        }
    }

    @Override
    public TuneTalkRespDTO performSubscription(SubscriptionReqDTO subscriptionReqDTO) {
        log.info("Request to Post SIM Port In : {}", subscriptionReqDTO.toString());

        JSONObject respObject = null;

        final String url = applicationProperties.getUrl().getTtBaseUrl() + "/subscription";

        try {
            ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

            String reqStr = mapper.writeValueAsString(subscriptionReqDTO);
            log.info("Request Json : {}", reqStr);

            respObject = postToTuneTalk(url, new JSONObject(reqStr));

            TuneTalkRespDTO tuneTalkRespDTO = mapper.readValue(respObject.toString(), TuneTalkRespDTO.class);
            log.info("Response mapped : {}", tuneTalkRespDTO);

            return tuneTalkRespDTO;
        } catch (IOException e) {
            if (respObject.has("code") && !respObject.getString("code").equals("200")){
                throw new ResponseStatusException(HttpStatus.valueOf(Integer.parseInt(respObject.getString("code"))), respObject.getString("message"));
            }
            throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, "mapping data failed");
        }


//        RestTemplate restTemplate = new RestTemplate();
//
//        // request header
//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.APPLICATION_JSON);
//        headers.set("secretKey", applicationProperties.getKey().getTtBaseSecret());
//
//        final HttpEntity<?> entity = new HttpEntity<>(subscriptionReqDTO, headers);
//
//        log.info("URL : {}", url);
//
//        ResponseEntity<TuneTalkRespDTO> exchange = null;
//        try {
//            exchange = restTemplate.exchange(url, HttpMethod.POST, entity, TuneTalkRespDTO.class);
//        } catch (HttpClientErrorException e) {
//            throw new ResponseStatusException(e.getStatusCode(), "Connection to Tune Talk Api Failed.");
//        }
//
//        return exchange.getBody();
    }

    @Override
    public JSONObject getSubscriptionStatusByTransactionId(String transactionId) {
        log.info("Request to Get Subscription Status By Transaction ID : {}", transactionId);

        final String url = applicationProperties.getUrl().getTtBaseUrl() + "/subscriptionStatus/" + transactionId;

        return getFromTuneTalk(url, Boolean.TRUE);

//        RestTemplate restTemplate = new RestTemplate();
//
//        // request header
//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.APPLICATION_JSON);
//        headers.set("secretKey", applicationProperties.getKey().getTtBaseSecret());
//
//        final HttpEntity<?> entity = new HttpEntity<>(headers);
//
//        log.info("URL : {}", url);
//
//        ResponseEntity<String> exchange = null;
//        try {
//            exchange = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
//        } catch (HttpClientErrorException e) {
//            throw new ResponseStatusException(e.getStatusCode(), "Connection to Tune Talk Api Failed.");
//        }
//
//        JSONObject jsonObject = new JSONObject(exchange.getBody());
//
//        return jsonObject;
    }

    @Override
    public JSONObject getTelcoList() {
        log.info("Request to Get Telco List");

        final String url = applicationProperties.getUrl().getTtBaseUrl() + "/telco";

        return getFromTuneTalk(url, Boolean.FALSE);

//        RestTemplate restTemplate = new RestTemplate();
//
//        // request header
//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.APPLICATION_JSON);
//
//        final HttpEntity<?> entity = new HttpEntity<>(headers);
//
//        log.info("URL : {}", url);
//
//        ResponseEntity<String> exchange = null;
//        try {
//            exchange = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
//        } catch (HttpClientErrorException e) {
//            throw new ResponseStatusException(e.getStatusCode(), "Connection to Tune Talk Api Failed.");
//        }
//
//        JSONObject jsonObject = new JSONObject(exchange.getBody());
//
//        return jsonObject;
    }

    @Override
    public OneCallDTO callTopup(OneCallDTO oneCallDTO) {
        log.info("Request to Topup from TT : {}", oneCallDTO);

        log.info("Before Calling Tune Talk Api " + Calendar.getInstance().getTime());

        JSONObject respObject = null;

        final String url = applicationProperties.getUrl().getTtBaseUrl() + "/topup";

        try {
            ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

            // request body
            TuneTalkTopupReqDTO reqDTO = new TuneTalkTopupReqDTO();
            reqDTO.setMsisdn(oneCallDTO.getMsisdn());
            reqDTO.setTopUpDeno(oneCallDTO.getTopupDeno());
            reqDTO.setTransactionId(oneCallDTO.getTransactionId().toString());

            String reqStr = mapper.writeValueAsString(reqDTO);
            log.info("Request Json : {}", reqStr);

            respObject= postToTuneTalk(url, new JSONObject(reqStr));

            TuneTalkTopupRespDTO resp = mapper.readValue(respObject.toString(), TuneTalkTopupRespDTO.class);
            log.info("Response mapped : {}", resp);

            log.info("After Calling Tune Talk Api " + Calendar.getInstance().getTime());
            log.info("Tune Talk Response Data : " + resp.toString());
            log.info("Map Response Data to OneCallDTO");

            oneCallDTO.setApiKey(resp.getApiKey());
            oneCallDTO.setBalance(resp.getBalance());
            oneCallDTO.setCode(resp.getCode());
            oneCallDTO.setMessage(resp.getMessage());

            return oneCallDTO;
        } catch (IOException e) {
            if (respObject.has("code") && !respObject.getString("code").equals("200")){
                throw new ResponseStatusException(HttpStatus.valueOf(Integer.parseInt(respObject.getString("code"))), respObject.getString("message"));
            }
            throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, "mapping data failed");
        }

//        RestTemplate restTemplate = new RestTemplate();
//
//        // request header
//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.APPLICATION_JSON);
//        headers.set("secretKey", applicationProperties.getKey().getTtBaseSecret());
//
//        // request body
//        TuneTalkTopupReqDTO reqDTO = new TuneTalkTopupReqDTO();
//        reqDTO.setMsisdn(oneCallDTO.getMsisdn());
//        reqDTO.setTopUpDeno(oneCallDTO.getTopupDeno());
//        reqDTO.setTransactionId(oneCallDTO.getTransactionId().toString());
//
//        log.debug("Request: " + reqDTO.toString());
//
//        final HttpEntity<?> entity = new HttpEntity<>(reqDTO, headers);
//        ResponseEntity<TuneTalkTopupRespDTO> exchange = null;
//        try {
//            exchange = restTemplate.exchange(url, HttpMethod.POST, entity, TuneTalkTopupRespDTO.class);
//
//        } catch (HttpClientErrorException e) {
//            throw new ResponseStatusException(e.getStatusCode(), "Connection to Tune Talk Api Failed.");
//        }
//
//        TuneTalkTopupRespDTO resp = exchange.getBody();
//        log.debug("Resonse: " + resp.toString());
//
//        log.info("After Calling Tune Talk Api " + Calendar.getInstance().getTime());
//        log.info("Tune Talk Response Data : " + resp.toString());
//        log.info("Map Response Data to OneCallDTO");
//
//        oneCallDTO.setApiKey(resp.getApiKey());
//        oneCallDTO.setBalance(resp.getBalance());
//        oneCallDTO.setCode(resp.getCode());
//        oneCallDTO.setMessage(resp.getMessage());
//
//
//        return oneCallDTO;
    }

    @Override
    public OneCallDTO getTopupByTransId(OneCallDTO oneCallDTO) {

        log.debug("Before Calling Tune Talk Api " + Calendar.getInstance().getTime());

        JSONObject respObject = null;

        String url = applicationProperties.getUrl().getTtBaseUrl() + "/topup/query/" + oneCallDTO.getTransactionId().toString();

        try {

            ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

            respObject = getFromTuneTalk(url, Boolean.TRUE);

            TuneTalkRespDTO respDTO = mapper.readValue(respObject.toString(), TuneTalkRespDTO.class);
            log.info("Response mapped : {}", respDTO);

            log.debug("After Calling Tune Talk Api " + Calendar.getInstance().getTime());
            log.debug("Set Response Data to OneCallDTO");

            oneCallDTO.setApiKey(respDTO.getApiKey());
            oneCallDTO.setCode(respDTO.getCode());
            oneCallDTO.setMessage(respDTO.getMessage());

            return oneCallDTO;
        } catch (IOException e) {
            if (respObject.has("code") && !respObject.getString("code").equals("200")){
                throw new ResponseStatusException(HttpStatus.valueOf(Integer.parseInt(respObject.getString("code"))), respObject.getString("message"));
            }
            throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, "mapping data failed");
        }

//        RestTemplate restTemplate = new RestTemplate();
//
//        // request header
//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.APPLICATION_JSON);
//        headers.set("secretKey", applicationProperties.getKey().getTtBaseSecret());
//
////        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url)
////            .queryParam("transactionId", oneCallDTO.getTransactionId().toString());
//
//        final HttpEntity<?> entity = new HttpEntity<>(headers);
//
//        log.info("URL : {}", url);
//
//        ResponseEntity<TuneTalkRespDTO> exchange = null;
//        try {
//            exchange = restTemplate.exchange(url, HttpMethod.GET, entity, TuneTalkRespDTO.class);
//        } catch (HttpClientErrorException e) {
//            throw new ResponseStatusException(e.getStatusCode(), "Connection to Tune Talk Api Failed.");
//        }
//
//        TuneTalkRespDTO respDTO = exchange.getBody();
//
//        log.debug("After Calling Tune Talk Api " + Calendar.getInstance().getTime());
//        log.debug("Set Response Data to OneCallDTO");
//        oneCallDTO.setApiKey(respDTO.getApiKey());
//        oneCallDTO.setCode(respDTO.getCode());
//        oneCallDTO.setMessage(respDTO.getMessage());
//        return oneCallDTO;
    }

    @Override
    public OneCall getTopupValuesByMsisdn(String msisdn)  {

        log.info("Call Tune Talk Topup Values Service");

        JSONObject respObject = null;

        String url = applicationProperties.getUrl().getTtBaseUrl() + "/topupValues/" + msisdn;

        try {

            ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

            respObject = getFromTuneTalk(url, Boolean.TRUE);

            TuneTalkMsisdnRespDTO respDTO = mapper.readValue(respObject.toString(), TuneTalkMsisdnRespDTO.class);
            log.info("Response mapped : {}", respDTO);

            log.info("Convert Service Result to OneCall");

            return topupValuesMapper.toEntity(respDTO);
        } catch (IOException e) {
            if (respObject.has("code") && !respObject.getString("code").equals("200")){
                throw new ResponseStatusException(HttpStatus.valueOf(Integer.parseInt(respObject.getString("code"))), respObject.getString("message"));
            }
            throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, "mapping data failed");
        }

//        RestTemplate restTemplate = new RestTemplate();
//
//        // request header
//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.APPLICATION_JSON);
//        headers.set("secretKey", applicationProperties.getKey().getTtBaseSecret());
//
//        final HttpEntity<?> entity = new HttpEntity<>(headers);
//
//        log.info("URL : {}", url);
//
//        ResponseEntity<TuneTalkMsisdnRespDTO> exchange = null;
//        try {
//            exchange = restTemplate.exchange(url, HttpMethod.GET, entity, TuneTalkMsisdnRespDTO.class);
//        } catch (HttpClientErrorException e) {
//            throw new ResponseStatusException(e.getStatusCode(), "Connection to Tune Talk Api Failed.");
//        }
//
//        log.info("Convert Service Result to OneCall");
//        OneCall dto = topupValuesMapper.toEntity(exchange.getBody());
//        return dto;
    }

    @Override
    public TuneTalkRespDTO updateTopupValidityForMsisdn(String msisdn, Integer days) {
        log.info("Request to Update Topup Validity {} days for {}", days.toString(), msisdn);

        JSONObject respObject = null;

        final String url = applicationProperties.getUrl().getTtBaseUrl() + "/updateValidity/" + msisdn + "/" + days.toString();

        try {

            ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

            respObject = getFromTuneTalk(url, Boolean.TRUE);

            TuneTalkRespDTO respDTO = mapper.readValue(respObject.toString(), TuneTalkRespDTO.class);
            log.info("Response mapped : {}", respDTO);

            return respDTO;
        } catch (IOException e) {
            if (respObject.has("code") && !respObject.getString("code").equals("200")){
                throw new ResponseStatusException(HttpStatus.valueOf(Integer.parseInt(respObject.getString("code"))), respObject.getString("message"));
            }
            throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, "mapping data failed");
        }

//        RestTemplate restTemplate = new RestTemplate();
//
//        // request header
//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.APPLICATION_JSON);
//        headers.set("secretKey", applicationProperties.getKey().getTtBaseSecret());
//
//        final HttpEntity<?> entity = new HttpEntity<>(headers);
//
//        log.info("URL : {}", url);
//
//        ResponseEntity<TuneTalkRespDTO> exchange = null;
//        try {
//            exchange = restTemplate.exchange(url, HttpMethod.GET, entity, TuneTalkRespDTO.class);
//        } catch (HttpClientErrorException e) {
//            throw new ResponseStatusException(e.getStatusCode(), "Connection to Tune Talk Api Failed.");
//        }
//
//        return exchange.getBody();
    }


    private JSONObject postToTuneTalk(String url, JSONObject reqObject) {
        log.info("URL : {}", url);

        try {
            HttpClient httpclient = HttpClients.createDefault();
            HttpPost httppost = new HttpPost(url);

            httppost.setEntity(new StringEntity(reqObject.toString()));
            httppost.addHeader("content-type", "application/json");
            httppost.addHeader("secretKey", applicationProperties.getKey().getTtBaseSecret());
            HttpResponse response = httpclient.execute(httppost);
            org.apache.http.HttpEntity entity = response.getEntity();
            String resBody = IOUtils.toString(entity.getContent(), StandardCharsets.UTF_8.name());
            log.info("==>>>> TT Post Response : {}", resBody);
            return new JSONObject(resBody);
        } catch (IOException e) {
            throw new ResponseStatusException(HttpStatus.FAILED_DEPENDENCY, "Send data to Tune Talk Api Failed.", e);
        }
    }


    private JSONObject getFromTuneTalk(String url, boolean hasSecret) {
        log.info("URL : {}", url);

        try {
            HttpClient httpClient = HttpClients.createDefault();
            HttpGet httpGet = new HttpGet(url);

            httpGet.addHeader("content-type", "application/json");
            if (hasSecret)
                httpGet.addHeader("secretKey", applicationProperties.getKey().getTtBaseSecret());
            HttpResponse response = httpClient.execute(httpGet);
            org.apache.http.HttpEntity entity = response.getEntity();
            String respBody = IOUtils.toString(entity.getContent(), StandardCharsets.UTF_8.name());
            log.info("==>>>> TT Get Response : {}", respBody);
            return new JSONObject(respBody);
        } catch (IOException e) {
            throw new ResponseStatusException(HttpStatus.FAILED_DEPENDENCY, "Get data from Tune Talk Api Failed.", e);
        }
    }
}
