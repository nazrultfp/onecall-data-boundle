package com.mobilityone.onecall.service.impl;

import com.mobilityone.onecall.config.ApplicationProperties;
import com.mobilityone.onecall.domain.ClientUser;
import com.mobilityone.onecall.repository.ClientUserRepository;
import com.mobilityone.onecall.service.ClientLoginService;
import com.mobilityone.onecall.service.dto.ClientLoginRespDTO;
import com.mobilityone.onecall.service.dto.LoginDTO;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ClientLoginServiceImpl implements ClientLoginService {

    private final Logger log = LoggerFactory.getLogger(ClientLoginServiceImpl.class);

    private final static String ROLE_USER = "ROLE_USER";

    private final ApplicationProperties properties;

    private final ClientUserRepository userRepository;

    private final AuthenticationManager authenticationManager;

    public ClientLoginServiceImpl(ApplicationProperties properties, ClientUserRepository userRepository, AuthenticationManager authenticationManager) {
        this.properties = properties;
        this.userRepository = userRepository;
        this.authenticationManager = authenticationManager;
    }

    @Override
    public ClientLoginRespDTO clientLogin(LoginDTO loginDTO) {
        log.debug("Request to Login Client : {}", loginDTO);

        try {

            Optional<ClientUser> clientUser = userRepository.findByClientUserNameAndClientPassword(loginDTO.getUsername(),loginDTO.getPassword());
            if (!clientUser.isPresent()) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "username or password is not correct");
            }

            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginDTO.getUsername(), loginDTO.getPassword(),
                Collections.singletonList(new SimpleGrantedAuthority(ROLE_USER))));

            ClientLoginRespDTO respDTO = new ClientLoginRespDTO();
            respDTO.setAccess_token(doGenerateToken(loginDTO.getUsername()));
            respDTO.setToken_type(properties.getAuthentication().getTokenPrefix());
            respDTO.setExpires_in(System.currentTimeMillis() + (60 * 60 * 1000));

            return respDTO;
        } catch (DisabledException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "USER_DISABLED");
        } catch (BadCredentialsException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "INVALID_CREDENTIALS");
        }
    }

    private String doGenerateToken(String userName) {
        List<SimpleGrantedAuthority> roleUser = Collections.singletonList(new SimpleGrantedAuthority(ROLE_USER));

        byte[] signingKey = properties.getAuthentication().getJwtSecret().getBytes();

        String token = Jwts.builder()
            .signWith(Keys.hmacShaKeyFor(signingKey), SignatureAlgorithm.HS512)
            .setHeaderParam("typ", properties.getAuthentication().getTokenType())
            .setIssuer(properties.getAuthentication().getTokenIssuer())
            .setAudience(properties.getAuthentication().getTokenAudience())
            .setSubject(userName)
            .setExpiration(new Date(System.currentTimeMillis() + (24 * 60 * 60 * 1000))) // now + 1 hour
            .claim("rol", roleUser)
            .compact();
        return token;
    }
}
