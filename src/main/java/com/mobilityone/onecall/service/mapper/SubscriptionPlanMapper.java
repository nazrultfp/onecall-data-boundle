package com.mobilityone.onecall.service.mapper;

import com.mobilityone.onecall.domain.*;
import com.mobilityone.onecall.service.dto.SubscriptionPlanDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity SubscriptionPlan and its DTO SubscriptionPlanDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SubscriptionPlanMapper extends EntityMapper<SubscriptionPlanDTO, SubscriptionPlan> {



    default SubscriptionPlan fromId(Long id) {
        if (id == null) {
            return null;
        }
        SubscriptionPlan subscriptionPlan = new SubscriptionPlan();
        subscriptionPlan.setId(id);
        return subscriptionPlan;
    }
}
