package com.mobilityone.onecall.service;

import com.mobilityone.onecall.service.dto.SimPortInDTO;
import com.mobilityone.onecall.service.dto.SimPortInReqDTO;
import org.json.JSONObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Service Interface for managing SimPortIn.
 */
public interface SimPortInService {

    /**
     * Save a simPortIn.
     *
     * @param simPortInDTO the entity to save
     * @return the persisted entity
     */
    SimPortInDTO save(SimPortInDTO simPortInDTO);

    /**
     * Get all the simPortIns.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<SimPortInDTO> findAll(Pageable pageable);


    /**
     * Get the "id" simPortIn.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<SimPortInDTO> findOne(Long id);

    /**
     * Delete the "id" simPortIn.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    SimPortInDTO save(SimPortInReqDTO simPortInReqDTO);

    SimPortInDTO save(Long simPortInId, JSONObject tuneTalkRespDTO);

    Page<SimPortInDTO> findAllByAgentId(Pageable pageable, Long agentId);

    SimPortInDTO recallSimPortIn(Long portInId);

    JSONObject getTotalPortInsByDate(Integer year, Integer month, Integer day);

    List<SimPortInDTO> getAllByAgentCode(String agentCode);

    List<Map<String, Object>> getAllInRangeDate(String fromDate, String toDate);

    List<Map<String, Object>> getAgentDailyTotalSimRegInRange(String agentCode, String fromDate, String toDate);
}
