package com.mobilityone.onecall.service.mapper;

import com.mobilityone.onecall.domain.*;
import com.mobilityone.onecall.service.dto.BigShotDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity BigShot and its DTO BigShotDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface BigShotMapper extends EntityMapper<BigShotDTO, BigShot> {



    default BigShot fromId(Long id) {
        if (id == null) {
            return null;
        }
        BigShot bigShot = new BigShot();
        bigShot.setId(id);
        return bigShot;
    }
}
