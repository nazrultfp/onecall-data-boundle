package com.mobilityone.onecall.service.mapper;

import com.mobilityone.onecall.domain.*;
import com.mobilityone.onecall.service.dto.OneCallDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity OneCall and its DTO OneCallDTO.
 */
@Mapper(componentModel = "spring", uses = {MsisdnValueMapper.class})
public interface OneCallMapper extends EntityMapper<OneCallDTO, OneCall> {


//    @Mapping(target = "msisdnValues", ignore = true)
    OneCall toEntity(OneCallDTO oneCallDTO);

    default OneCall fromId(Long id) {
        if (id == null) {
            return null;
        }
        OneCall oneCall = new OneCall();
        oneCall.setId(id);
        return oneCall;
    }
}
