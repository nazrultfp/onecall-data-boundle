package com.mobilityone.onecall.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.mobilityone.onecall.domain.enumeration.PlanRenewal;

import com.mobilityone.onecall.domain.enumeration.PlanType;

import com.mobilityone.onecall.domain.enumeration.DataMode;

/**
 * A Plan.
 */
@Entity
@Table(name = "plan")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Plan implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "keyword")
    private String keyword;

    @Enumerated(EnumType.STRING)
    @Column(name = "plan_renewal")
    private PlanRenewal planRenewal;

    @Enumerated(EnumType.STRING)
    @Column(name = "plan_type")
    private PlanType planType;

    @Column(name = "amount")
    private String amount;

    @Column(name = "price")
    private String price;

    @Enumerated(EnumType.STRING)
    @Column(name = "data_mode")
    private DataMode dataMode;

    @Column(name = "expire_days")
    private String expireDays;

    @Column(name = "description")
    private String description;

    @Column(name = "jhi_enable")
    private Boolean enable;

    @Column(name = "title_a")
    private String titleA;

    @Column(name = "subtitle_a")
    private String subtitleA;

    @Column(name = "title_b")
    private String titleB;

    @Column(name = "subtitle_b")
    private String subtitleB;

    @Column(name = "unlimited_call")
    private Boolean unlimitedCall;

    @OneToMany(mappedBy = "plan")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<PlanTransaction> planTransactions = new HashSet<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKeyword() {
        return keyword;
    }

    public Plan keyword(String keyword) {
        this.keyword = keyword;
        return this;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public PlanRenewal getPlanRenewal() {
        return planRenewal;
    }

    public Plan planRenewal(PlanRenewal planRenewal) {
        this.planRenewal = planRenewal;
        return this;
    }

    public void setPlanRenewal(PlanRenewal planRenewal) {
        this.planRenewal = planRenewal;
    }

    public PlanType getPlanType() {
        return planType;
    }

    public Plan planType(PlanType planType) {
        this.planType = planType;
        return this;
    }

    public void setPlanType(PlanType planType) {
        this.planType = planType;
    }

    public String getAmount() {
        return amount;
    }

    public Plan amount(String amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPrice() {
        return price;
    }

    public Plan price(String price) {
        this.price = price;
        return this;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public DataMode getDataMode() {
        return dataMode;
    }

    public Plan dataMode(DataMode dataMode) {
        this.dataMode = dataMode;
        return this;
    }

    public void setDataMode(DataMode dataMode) {
        this.dataMode = dataMode;
    }

    public String getExpireDays() {
        return expireDays;
    }

    public Plan expireDays(String expireDays) {
        this.expireDays = expireDays;
        return this;
    }

    public void setExpireDays(String expireDays) {
        this.expireDays = expireDays;
    }

    public String getDescription() {
        return description;
    }

    public Plan description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean isEnable() {
        return enable;
    }

    public Plan enable(Boolean enable) {
        this.enable = enable;
        return this;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public String getTitleA() {
        return titleA;
    }

    public Plan titleA(String titleA) {
        this.titleA = titleA;
        return this;
    }

    public void setTitleA(String titleA) {
        this.titleA = titleA;
    }

    public String getSubtitleA() {
        return subtitleA;
    }

    public Plan subtitleA(String subtitleA) {
        this.subtitleA = subtitleA;
        return this;
    }

    public void setSubtitleA(String subtitleA) {
        this.subtitleA = subtitleA;
    }

    public String getTitleB() {
        return titleB;
    }

    public Plan titleB(String titleB) {
        this.titleB = titleB;
        return this;
    }

    public void setTitleB(String titleB) {
        this.titleB = titleB;
    }

    public String getSubtitleB() {
        return subtitleB;
    }

    public Plan subtitleB(String subtitleB) {
        this.subtitleB = subtitleB;
        return this;
    }

    public void setSubtitleB(String subtitleB) {
        this.subtitleB = subtitleB;
    }

    public Boolean isUnlimitedCall() {
        return unlimitedCall;
    }

    public Plan unlimitedCall(Boolean unlimitedCall) {
        this.unlimitedCall = unlimitedCall;
        return this;
    }

    public void setUnlimitedCall(Boolean unlimitedCall) {
        this.unlimitedCall = unlimitedCall;
    }

    public Set<PlanTransaction> getPlanTransactions() {
        return planTransactions;
    }

    public Plan planTransactions(Set<PlanTransaction> planTransactions) {
        this.planTransactions = planTransactions;
        return this;
    }

    public Plan addPlanTransactions(PlanTransaction planTransaction) {
        this.planTransactions.add(planTransaction);
        planTransaction.setPlan(this);
        return this;
    }

    public Plan removePlanTransactions(PlanTransaction planTransaction) {
        this.planTransactions.remove(planTransaction);
        planTransaction.setPlan(null);
        return this;
    }

    public void setPlanTransactions(Set<PlanTransaction> planTransactions) {
        this.planTransactions = planTransactions;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Plan plan = (Plan) o;
        if (plan.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), plan.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Plan{" +
            "id=" + getId() +
            ", keyword='" + getKeyword() + "'" +
            ", planRenewal='" + getPlanRenewal() + "'" +
            ", planType='" + getPlanType() + "'" +
            ", amount='" + getAmount() + "'" +
            ", price='" + getPrice() + "'" +
            ", dataMode='" + getDataMode() + "'" +
            ", expireDays='" + getExpireDays() + "'" +
            ", description='" + getDescription() + "'" +
            ", enable='" + isEnable() + "'" +
            ", titleA='" + getTitleA() + "'" +
            ", subtitleA='" + getSubtitleA() + "'" +
            ", titleB='" + getTitleB() + "'" +
            ", subtitleB='" + getSubtitleB() + "'" +
            ", unlimitedCall='" + isUnlimitedCall() + "'" +
            "}";
    }
}
