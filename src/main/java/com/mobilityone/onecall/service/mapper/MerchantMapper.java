package com.mobilityone.onecall.service.mapper;

import com.mobilityone.onecall.domain.Merchant;
import com.mobilityone.onecall.service.dto.MerchantDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity Merchant and its DTO MerchantDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MerchantMapper extends EntityMapper<MerchantDTO, Merchant> {


    @Mapping(target = "planTransactions", ignore = true)
    @Mapping(target = "topupTransactions", ignore = true)
    Merchant toEntity(MerchantDTO merchantDTO);

    default Merchant fromId(Long id) {
        if (id == null) {
            return null;
        }
        Merchant merchant = new Merchant();
        merchant.setId(id);
        return merchant;
    }
}
