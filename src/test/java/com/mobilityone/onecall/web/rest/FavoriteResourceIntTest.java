package com.mobilityone.onecall.web.rest;

import com.mobilityone.onecall.OneCallApp;

import com.mobilityone.onecall.domain.Favorite;
import com.mobilityone.onecall.repository.FavoriteRepository;
import com.mobilityone.onecall.service.FavoriteService;
import com.mobilityone.onecall.service.dto.FavoriteDTO;
import com.mobilityone.onecall.service.mapper.FavoriteMapper;
import com.mobilityone.onecall.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.mobilityone.onecall.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the FavoriteResource REST controller.
 *
 * @see FavoriteResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OneCallApp.class)
public class FavoriteResourceIntTest {

    private static final String DEFAULT_PHONE_NO = "AAAAAAAAAA";
    private static final String UPDATED_PHONE_NO = "BBBBBBBBBB";

    private static final String DEFAULT_PROD_CODE = "AAAAAAAAAA";
    private static final String UPDATED_PROD_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_PROD_SUB_CODE = "AAAAAAAAAA";
    private static final String UPDATED_PROD_SUB_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_CAT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_CAT_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_PROD_DESC = "AAAAAAAAAA";
    private static final String UPDATED_PROD_DESC = "BBBBBBBBBB";

    private static final String DEFAULT_PROD_CODE_ALT = "AAAAAAAAAA";
    private static final String UPDATED_PROD_CODE_ALT = "BBBBBBBBBB";

    private static final String DEFAULT_PROD_DESC_LONG = "AAAAAAAAAA";
    private static final String UPDATED_PROD_DESC_LONG = "BBBBBBBBBB";

    private static final String DEFAULT_ACCOUNT_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_ACCOUNT_NUMBER = "BBBBBBBBBB";

    @Autowired
    private FavoriteRepository favoriteRepository;

    @Autowired
    private FavoriteMapper favoriteMapper;

    @Autowired
    private FavoriteService favoriteService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restFavoriteMockMvc;

    private Favorite favorite;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final FavoriteResource favoriteResource = new FavoriteResource(favoriteService);
        this.restFavoriteMockMvc = MockMvcBuilders.standaloneSetup(favoriteResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Favorite createEntity(EntityManager em) {
        Favorite favorite = new Favorite()
            .phoneNo(DEFAULT_PHONE_NO)
            .prodCode(DEFAULT_PROD_CODE)
            .prodSubCode(DEFAULT_PROD_SUB_CODE)
            .catType(DEFAULT_CAT_TYPE)
            .prodDesc(DEFAULT_PROD_DESC)
            .prodCodeAlt(DEFAULT_PROD_CODE_ALT)
            .prodDescLong(DEFAULT_PROD_DESC_LONG)
            .accountNumber(DEFAULT_ACCOUNT_NUMBER);
        return favorite;
    }

    @Before
    public void initTest() {
        favorite = createEntity(em);
    }

    @Test
    @Transactional
    public void createFavorite() throws Exception {
        int databaseSizeBeforeCreate = favoriteRepository.findAll().size();

        // Create the Favorite
        FavoriteDTO favoriteDTO = favoriteMapper.toDto(favorite);
        restFavoriteMockMvc.perform(post("/api/favorites")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(favoriteDTO)))
            .andExpect(status().isCreated());

        // Validate the Favorite in the database
        List<Favorite> favoriteList = favoriteRepository.findAll();
        assertThat(favoriteList).hasSize(databaseSizeBeforeCreate + 1);
        Favorite testFavorite = favoriteList.get(favoriteList.size() - 1);
        assertThat(testFavorite.getPhoneNo()).isEqualTo(DEFAULT_PHONE_NO);
        assertThat(testFavorite.getProdCode()).isEqualTo(DEFAULT_PROD_CODE);
        assertThat(testFavorite.getProdSubCode()).isEqualTo(DEFAULT_PROD_SUB_CODE);
        assertThat(testFavorite.getCatType()).isEqualTo(DEFAULT_CAT_TYPE);
        assertThat(testFavorite.getProdDesc()).isEqualTo(DEFAULT_PROD_DESC);
        assertThat(testFavorite.getProdCodeAlt()).isEqualTo(DEFAULT_PROD_CODE_ALT);
        assertThat(testFavorite.getProdDescLong()).isEqualTo(DEFAULT_PROD_DESC_LONG);
        assertThat(testFavorite.getAccountNumber()).isEqualTo(DEFAULT_ACCOUNT_NUMBER);
    }

    @Test
    @Transactional
    public void createFavoriteWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = favoriteRepository.findAll().size();

        // Create the Favorite with an existing ID
        favorite.setId(1L);
        FavoriteDTO favoriteDTO = favoriteMapper.toDto(favorite);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFavoriteMockMvc.perform(post("/api/favorites")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(favoriteDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Favorite in the database
        List<Favorite> favoriteList = favoriteRepository.findAll();
        assertThat(favoriteList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllFavorites() throws Exception {
        // Initialize the database
        favoriteRepository.saveAndFlush(favorite);

        // Get all the favoriteList
        restFavoriteMockMvc.perform(get("/api/favorites?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(favorite.getId().intValue())))
            .andExpect(jsonPath("$.[*].phoneNo").value(hasItem(DEFAULT_PHONE_NO.toString())))
            .andExpect(jsonPath("$.[*].prodCode").value(hasItem(DEFAULT_PROD_CODE.toString())))
            .andExpect(jsonPath("$.[*].prodSubCode").value(hasItem(DEFAULT_PROD_SUB_CODE.toString())))
            .andExpect(jsonPath("$.[*].catType").value(hasItem(DEFAULT_CAT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].prodDesc").value(hasItem(DEFAULT_PROD_DESC.toString())))
            .andExpect(jsonPath("$.[*].prodCodeAlt").value(hasItem(DEFAULT_PROD_CODE_ALT.toString())))
            .andExpect(jsonPath("$.[*].prodDescLong").value(hasItem(DEFAULT_PROD_DESC_LONG.toString())))
            .andExpect(jsonPath("$.[*].accountNumber").value(hasItem(DEFAULT_ACCOUNT_NUMBER.toString())));
    }
    
    @Test
    @Transactional
    public void getFavorite() throws Exception {
        // Initialize the database
        favoriteRepository.saveAndFlush(favorite);

        // Get the favorite
        restFavoriteMockMvc.perform(get("/api/favorites/{id}", favorite.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(favorite.getId().intValue()))
            .andExpect(jsonPath("$.phoneNo").value(DEFAULT_PHONE_NO.toString()))
            .andExpect(jsonPath("$.prodCode").value(DEFAULT_PROD_CODE.toString()))
            .andExpect(jsonPath("$.prodSubCode").value(DEFAULT_PROD_SUB_CODE.toString()))
            .andExpect(jsonPath("$.catType").value(DEFAULT_CAT_TYPE.toString()))
            .andExpect(jsonPath("$.prodDesc").value(DEFAULT_PROD_DESC.toString()))
            .andExpect(jsonPath("$.prodCodeAlt").value(DEFAULT_PROD_CODE_ALT.toString()))
            .andExpect(jsonPath("$.prodDescLong").value(DEFAULT_PROD_DESC_LONG.toString()))
            .andExpect(jsonPath("$.accountNumber").value(DEFAULT_ACCOUNT_NUMBER.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingFavorite() throws Exception {
        // Get the favorite
        restFavoriteMockMvc.perform(get("/api/favorites/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFavorite() throws Exception {
        // Initialize the database
        favoriteRepository.saveAndFlush(favorite);

        int databaseSizeBeforeUpdate = favoriteRepository.findAll().size();

        // Update the favorite
        Favorite updatedFavorite = favoriteRepository.findById(favorite.getId()).get();
        // Disconnect from session so that the updates on updatedFavorite are not directly saved in db
        em.detach(updatedFavorite);
        updatedFavorite
            .phoneNo(UPDATED_PHONE_NO)
            .prodCode(UPDATED_PROD_CODE)
            .prodSubCode(UPDATED_PROD_SUB_CODE)
            .catType(UPDATED_CAT_TYPE)
            .prodDesc(UPDATED_PROD_DESC)
            .prodCodeAlt(UPDATED_PROD_CODE_ALT)
            .prodDescLong(UPDATED_PROD_DESC_LONG)
            .accountNumber(UPDATED_ACCOUNT_NUMBER);
        FavoriteDTO favoriteDTO = favoriteMapper.toDto(updatedFavorite);

        restFavoriteMockMvc.perform(put("/api/favorites")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(favoriteDTO)))
            .andExpect(status().isOk());

        // Validate the Favorite in the database
        List<Favorite> favoriteList = favoriteRepository.findAll();
        assertThat(favoriteList).hasSize(databaseSizeBeforeUpdate);
        Favorite testFavorite = favoriteList.get(favoriteList.size() - 1);
        assertThat(testFavorite.getPhoneNo()).isEqualTo(UPDATED_PHONE_NO);
        assertThat(testFavorite.getProdCode()).isEqualTo(UPDATED_PROD_CODE);
        assertThat(testFavorite.getProdSubCode()).isEqualTo(UPDATED_PROD_SUB_CODE);
        assertThat(testFavorite.getCatType()).isEqualTo(UPDATED_CAT_TYPE);
        assertThat(testFavorite.getProdDesc()).isEqualTo(UPDATED_PROD_DESC);
        assertThat(testFavorite.getProdCodeAlt()).isEqualTo(UPDATED_PROD_CODE_ALT);
        assertThat(testFavorite.getProdDescLong()).isEqualTo(UPDATED_PROD_DESC_LONG);
        assertThat(testFavorite.getAccountNumber()).isEqualTo(UPDATED_ACCOUNT_NUMBER);
    }

    @Test
    @Transactional
    public void updateNonExistingFavorite() throws Exception {
        int databaseSizeBeforeUpdate = favoriteRepository.findAll().size();

        // Create the Favorite
        FavoriteDTO favoriteDTO = favoriteMapper.toDto(favorite);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFavoriteMockMvc.perform(put("/api/favorites")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(favoriteDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Favorite in the database
        List<Favorite> favoriteList = favoriteRepository.findAll();
        assertThat(favoriteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteFavorite() throws Exception {
        // Initialize the database
        favoriteRepository.saveAndFlush(favorite);

        int databaseSizeBeforeDelete = favoriteRepository.findAll().size();

        // Delete the favorite
        restFavoriteMockMvc.perform(delete("/api/favorites/{id}", favorite.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Favorite> favoriteList = favoriteRepository.findAll();
        assertThat(favoriteList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Favorite.class);
        Favorite favorite1 = new Favorite();
        favorite1.setId(1L);
        Favorite favorite2 = new Favorite();
        favorite2.setId(favorite1.getId());
        assertThat(favorite1).isEqualTo(favorite2);
        favorite2.setId(2L);
        assertThat(favorite1).isNotEqualTo(favorite2);
        favorite1.setId(null);
        assertThat(favorite1).isNotEqualTo(favorite2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(FavoriteDTO.class);
        FavoriteDTO favoriteDTO1 = new FavoriteDTO();
        favoriteDTO1.setId(1L);
        FavoriteDTO favoriteDTO2 = new FavoriteDTO();
        assertThat(favoriteDTO1).isNotEqualTo(favoriteDTO2);
        favoriteDTO2.setId(favoriteDTO1.getId());
        assertThat(favoriteDTO1).isEqualTo(favoriteDTO2);
        favoriteDTO2.setId(2L);
        assertThat(favoriteDTO1).isNotEqualTo(favoriteDTO2);
        favoriteDTO1.setId(null);
        assertThat(favoriteDTO1).isNotEqualTo(favoriteDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(favoriteMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(favoriteMapper.fromId(null)).isNull();
    }
}
