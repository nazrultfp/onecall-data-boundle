package com.mobilityone.onecall.repository;

import com.mobilityone.onecall.domain.SubscriptionPlan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;


/**
 * Spring Data  repository for the SubscriptionPlan entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SubscriptionPlanRepository extends JpaRepository<SubscriptionPlan, Long> {

    Optional<SubscriptionPlan> findByTransactionId(String transactionId);

    Page<SubscriptionPlan> findByMsisdnAndCode(Pageable pageable, String msisdn, String code);

    @Query(value = "select " +
        " sp.id as id, " +
        " sp.keyword as keyword, " +
        " sp.msisdn as msisdn, " +
        " sp.transaction_id as transaction_Id, " +
        " sp.created_date as create_Date, " +
        " sp.created_by as create_By, " +
        " p.amount as amount, " +
        " p.price as price, " +
        " p.description as description, " +
        " p.jhi_enable as enable " +
        " from subscription_plan as sp " +
        " left join Plan as p on " +
        " sp.keyword = p.keyword",
        nativeQuery = true)
    List<Map<String, Object>> report();


    @Query("select count(sp) from SubscriptionPlan sp where sp.code = '200' and sp.keyword = :pakage and " +
        " date_format(CONVERT_TZ(sp.createdDate ,'+00:00','+08:00') , '%Y-%m-%d') = :date")
        //mysql
//    @Query("select count(sp) from SubscriptionPlan sp where sp.code = '200' and sp.keyword = :pakage and " +
//        " FORMATDATETIME(DATEADD(HOUR, 8, sp.createdDate) , 'yyyy-MM-dd') = :date") //h2
    Integer countByPackageAndDate(@Param("pakage") String pakage, @Param("date") String date);


    @Query(value = "select sp.keyword , date_format(CONVERT_TZ(sp.created_Date ,'+00:00','+08:00') , '%Y-%m-%d') as date , " +
        " count(*) as total from Subscription_Plan sp where sp.code = '200' and sp.keyword = :pakage  group by date ", nativeQuery = true)
        //mysql
//    @Query(value = "select sp.keyword as keyword, FORMATDATETIME(DATEADD(HOUR, 8, sp.created_Date) , 'yyyy-MM-dd') as date, count(*) as total " +
//        " from Subscription_Plan sp where sp.code = '200' and sp.keyword = :pakage  group by date " , nativeQuery = true) //h2
    List<Map<String, Object>> getTotalPlansByPackage(@Param("pakage") String pakage);


    @Query(value = "select sp.keyword , date_format(CONVERT_TZ(sp.created_Date ,'+00:00','+08:00') , '%Y-%m-%d') as date , count(*) as total  " +
        " from Subscription_Plan sp where sp.code = '200' and date_format(CONVERT_TZ(sp.created_Date ,'+00:00','+08:00') , '%Y-%m-%d') between :fromDate and :toDate " +
        "group by date , sp.keyword ", nativeQuery = true)
        //mysql
//    @Query(value = "select sp.keyword , FORMATDATETIME(DATEADD(HOUR, 8, sp.created_Date) , 'yyyy-MM-dd') as date , count(*) as total  " +
//        " from Subscription_Plan sp where sp.code = '200' and FORMATDATETIME(DATEADD(HOUR, 8, sp.created_Date) , 'yyyy-MM-dd') between :fromDate and :toDate " +
//        "group by date , sp.keyword ", nativeQuery = true) //h2
    List<Map<String, Object>> getTotalPlansByDateRange(@Param("fromDate") String fromDate, @Param("toDate") String toDate);


    @Query(value = "select sp.keyword , date_format(CONVERT_TZ(sp.created_Date ,'+00:00','+08:00') , '%Y-%m-%d') as date , count(*) as total " +
        "from Subscription_Plan sp where sp.code = '200'  group by date , sp.keyword ", nativeQuery = true)
        //mysql
//    @Query(value = "select sp.keyword , FORMATDATETIME(DATEADD(HOUR, 8, sp.created_Date) , 'yyyy-MM-dd') as date , count(*)  as total " +
//        "from Subscription_Plan sp where sp.code = '200' group by date , sp.keyword ", nativeQuery = true) //h2
    List<Map<String, Object>> getTotalPlansGroupByPackageAndDate();


    @Query(value = "select sum(p.price) as totalPrice , date_format(CONVERT_TZ(sp.created_date , '+00:00', '+08:00'), '%Y-%m-%d') as createDate from subscription_plan sp " +
        " left join plan p on sp.keyword = p.keyword " +
        " where sp.code = '200' and date_format(CONVERT_TZ(sp.created_date , '+00:00', '+08:00'), '%Y-%m-%d') between :fromDate and :toDate " +
        " group by date_format(CONVERT_TZ(sp.created_date , '+00:00', '+08:00'), '%Y-%m-%d')", nativeQuery = true)
        //mysql
//    @Query(value = "select sum(cast(p.price as int)) as totalPrice , FORMATDATETIME(DATEADD(HOUR, 8, sp.created_Date) , 'yyyy-MM-dd') as createDate from subscription_plan sp " +
//        " left join plan p on sp.keyword = p.keyword " +
//        " where sp.code = '200' and FORMATDATETIME(DATEADD(HOUR, 8, sp.created_Date) , 'yyyy-MM-dd') between :fromDate and :toDate" +
//        " group by FORMATDATETIME(DATEADD(HOUR, 8, sp.created_Date) , 'yyyy-MM-dd') ", nativeQuery = true) //h2
    List<Map<String, Object>> getDailyTotalSaleInRange(@Param("fromDate") String fromDate, @Param("toDate") String toDate);


    @Query(value = "select sum(p.price) as totalPrice , date_format(CONVERT_TZ(sp.created_date , '+00:00', '+08:00'), '%Y-%m-%d') as createDate from subscription_plan sp " +
        " left join plan p on sp.keyword = p.keyword  " +
        " where sp.code = '200' and sp.created_by like :client and date_format(CONVERT_TZ(sp.created_date , '+00:00', '+08:00'), '%Y-%m-%d') between :fromDate and :toDate " +
        " group by createDate ", nativeQuery = true)
        //mysql
//    @Query(value = "select sum(cast(p.price as int)) as totalPrice , FORMATDATETIME(DATEADD(HOUR, 8, sp.created_Date) , 'yyyy-MM-dd') as createDate from subscription_plan sp " +
//        " left join plan p on sp.keyword = p.keyword " +
//        " where sp.code = '200' and sp.created_by like :client and FORMATDATETIME(DATEADD(HOUR, 8, sp.created_Date) , 'yyyy-MM-dd') between :fromDate and :toDate" +
//        " group by FORMATDATETIME(DATEADD(HOUR, 8, sp.created_Date) , 'yyyy-MM-dd') ", nativeQuery = true) //h2
    List<Map<String, Object>> getDailyTotalSaleInRangeForClient(@Param("client") String client, @Param("fromDate") String fromDate, @Param("toDate") String toDate);


    @Query(value = "select  sp.id as id, sp.created_by as create_By,  sp.msisdn as msisdn,  sp.keyword as plan,  " +
        "date_format(CONVERT_TZ(sp.created_date , '+00:00', '+08:00') , '%Y-%m-%d %T') as create_Date, p.price as price ," +
        "(CASE  WHEN sp.code = '200' THEN 'SUCCESSFUL' " +
        "    ELSE 'FAILED' END ) as status " +
        " from subscription_plan as sp  left join Plan as p on  (sp.keyword = p.keyword or sp.keyword = concat(p.keyword , ' ADD') ) " +
        " where date_format(CONVERT_TZ(sp.created_date , '+00:00', '+08:00') , '%Y-%m-%d') between :fromDate and :toDate ", nativeQuery = true) //mysql
    List<Map<String, Object>> getPlansInRange(@Param("fromDate") String fromDate, @Param("toDate") String toDate);
}
