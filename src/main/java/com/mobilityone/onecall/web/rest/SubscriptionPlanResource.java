package com.mobilityone.onecall.web.rest;
import com.mobilityone.onecall.service.SubscriptionPlanService;
import com.mobilityone.onecall.web.rest.errors.BadRequestAlertException;
import com.mobilityone.onecall.web.rest.util.HeaderUtil;
import com.mobilityone.onecall.web.rest.util.PaginationUtil;
import com.mobilityone.onecall.service.dto.SubscriptionPlanDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * REST controller for managing SubscriptionPlan.
 */
@RestController
@RequestMapping("/api")
public class SubscriptionPlanResource {

    private final Logger log = LoggerFactory.getLogger(SubscriptionPlanResource.class);

    private static final String ENTITY_NAME = "oneCallSubscriptionPlan";

    private final SubscriptionPlanService subscriptionPlanService;

    public SubscriptionPlanResource(SubscriptionPlanService subscriptionPlanService) {
        this.subscriptionPlanService = subscriptionPlanService;
    }

    /**
     * POST  /subscription-plans : Create a new subscriptionPlan.
     *
     * @param subscriptionPlanDTO the subscriptionPlanDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new subscriptionPlanDTO, or with status 400 (Bad Request) if the subscriptionPlan has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    //@PostMapping("/subscription-plans")
    public ResponseEntity<SubscriptionPlanDTO> createSubscriptionPlan(@RequestBody SubscriptionPlanDTO subscriptionPlanDTO) throws URISyntaxException {
        log.debug("REST request to save SubscriptionPlan : {}", subscriptionPlanDTO);
        if (subscriptionPlanDTO.getId() != null) {
            throw new BadRequestAlertException("A new subscriptionPlan cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SubscriptionPlanDTO result = subscriptionPlanService.save(subscriptionPlanDTO);
        return ResponseEntity.created(new URI("/api/subscription-plans/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /subscription-plans : Updates an existing subscriptionPlan.
     *
     * @param subscriptionPlanDTO the subscriptionPlanDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated subscriptionPlanDTO,
     * or with status 400 (Bad Request) if the subscriptionPlanDTO is not valid,
     * or with status 500 (Internal Server Error) if the subscriptionPlanDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    //@PutMapping("/subscription-plans")
    public ResponseEntity<SubscriptionPlanDTO> updateSubscriptionPlan(@RequestBody SubscriptionPlanDTO subscriptionPlanDTO) throws URISyntaxException {
        log.debug("REST request to update SubscriptionPlan : {}", subscriptionPlanDTO);
        if (subscriptionPlanDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SubscriptionPlanDTO result = subscriptionPlanService.save(subscriptionPlanDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, subscriptionPlanDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /subscription-plans : get all the subscriptionPlans.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of subscriptionPlans in body
     */
    @GetMapping("/subscription-plans")
    public ResponseEntity<List<SubscriptionPlanDTO>> getAllSubscriptionPlans(Pageable pageable) {
        log.debug("REST request to get a page of SubscriptionPlans");
        Page<SubscriptionPlanDTO> page = subscriptionPlanService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/subscription-plans");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /subscription-plans/:id : get the "id" subscriptionPlan.
     *
     * @param id the id of the subscriptionPlanDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the subscriptionPlanDTO, or with status 404 (Not Found)
     */
    @GetMapping("/subscription-plans/{id}")
    public ResponseEntity<SubscriptionPlanDTO> getSubscriptionPlan(@PathVariable Long id) {
        log.debug("REST request to get SubscriptionPlan : {}", id);
        Optional<SubscriptionPlanDTO> subscriptionPlanDTO = subscriptionPlanService.findOne(id);
        return ResponseUtil.wrapOrNotFound(subscriptionPlanDTO);
    }

    /**
     * DELETE  /subscription-plans/:id : delete the "id" subscriptionPlan.
     *
     * @param id the id of the subscriptionPlanDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    //@DeleteMapping("/subscription-plans/{id}")
    public ResponseEntity<Void> deleteSubscriptionPlan(@PathVariable Long id) {
        log.debug("REST request to delete SubscriptionPlan : {}", id);
        subscriptionPlanService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }


    @GetMapping("/subscription-plans/by-transaction-id/{transactionId}")
    public ResponseEntity<SubscriptionPlanDTO> getAllSubscriptionPlansByTransactionId(@PathVariable String transactionId) {
        log.debug("REST request to get a SubscriptionPlan by transactionId : {}" , transactionId);
        SubscriptionPlanDTO subscriptionPlanDTO = subscriptionPlanService.findByTransactionId(transactionId);
        return ResponseEntity.ok().body(subscriptionPlanDTO);
    }


    @GetMapping("/subscription-plans/by-msisdn/{msisdn}")
    public ResponseEntity<List<SubscriptionPlanDTO>> getSuccessfulSubscriptionsByMsisdn(Pageable pageable, @PathVariable String msisdn){
        log.debug("REST request to get successful SubscriptionPlan by msisdn : {}" , msisdn);

        Page<SubscriptionPlanDTO> page = subscriptionPlanService.findSuccessfulByMsisdn(pageable, msisdn);

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/subscription-plans/by-msisdn");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }


    @GetMapping("/subscription-plans/report")
    public ResponseEntity<List<Map<String, Object>>> reportSubscriptionPlans(){
        log.debug("REST request to get all SubscriptionPlan with their Keywords." );

        List<Map<String, Object>> report = subscriptionPlanService.report();

//        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(report, "/api/subscription-plans/report");
//        return ResponseEntity.ok().headers(headers).body(report.getContent());
        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON_UTF8).body(report);
    }



    @GetMapping("/subscription-plans/total-by-package-date/{pakage}/{year}/{month}/{day}")
    public ResponseEntity<String> getTotalPlansByPackageAndDate(@PathVariable String pakage, @PathVariable Integer year, @PathVariable Integer month, @PathVariable Integer day){
        log.info("REST request to get total number of subscriptions for package : {} at {}-{}-{}.", pakage, year, month, day);

        JSONObject object = subscriptionPlanService.getTotalPlansByPackageAndDate(pakage, year,month,day);

        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON_UTF8).body(object.toString(2));
    }


    @GetMapping("/subscription-plans/total-by-package/{pakage}")
    public ResponseEntity<List<Map<String, Object>>> getTotalPlansByPackage(@PathVariable String pakage){
        log.info("REST request to get total number of subscriptions for package : {} group by date", pakage);

        List<Map<String, Object>> object = subscriptionPlanService.getTotalPlansByPackage(pakage);

        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON_UTF8).body(object);
    }


    @GetMapping("/subscription-plans/total-by-date-range/{fromDate}/{toDate}")
    public ResponseEntity<List<Map<String, Object>>> getTotalPlansByDateRange(@PathVariable String fromDate, @PathVariable String toDate){
        log.info("REST request to get total number of subscriptions group by package and date from {} to {}", fromDate, toDate);

        List<Map<String, Object>> object = subscriptionPlanService.getTotalPlansByDateRange(fromDate,toDate);

        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON_UTF8).body(object);
    }


    @GetMapping("/subscription-plans/total-groupby-package-date")
    public ResponseEntity<List<Map<String, Object>>> getTotalPlansGroupByPackageAndDate(){
        log.info("REST request to get total number of subscriptions group by package and date");

        List<Map<String, Object>> object = subscriptionPlanService.getTotalPlansGroupByPackageAndDate();

        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON_UTF8).body(object);
    }


    @GetMapping("/subscription-plans/daily-sale-in-range/{fromDate}/{toDate}")
    public ResponseEntity<List<Map<String, Object>>> getDailyTotalSaleInRange(@PathVariable String fromDate, @PathVariable String toDate){
        log.info("REST request to get daily total sales of subscriptions in date range from {} to {}", fromDate, toDate);

        List<Map<String, Object>> object = subscriptionPlanService.getDailyTotalSaleInRange(fromDate,toDate);

        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON_UTF8).body(object);
    }


    @GetMapping("/subscription-plans/daily-client-sale-in-range/{client}/{fromDate}/{toDate}")
    public ResponseEntity<List<Map<String, Object>>> getDailyTotalSaleInRangeForClient(@PathVariable String client, @PathVariable String fromDate, @PathVariable String toDate){
        log.info("REST request to get client {} daily total sales of subscriptions in date range from {} to {}", client, fromDate, toDate);

        List<Map<String, Object>> object = subscriptionPlanService.getDailyTotalSaleInRangeForClient(client, fromDate,toDate);

        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON_UTF8).body(object);
    }


    @GetMapping("/subscription-plans/plans-in-range/{fromDate}/{toDate}")
    public ResponseEntity<List<Map<String, Object>>> getPlansInRange(@PathVariable String fromDate, @PathVariable String toDate){
        log.info("REST request to get subscription plans in date range from {} to {}", fromDate, toDate);

        List<Map<String, Object>> object = subscriptionPlanService.getPlansInRange(fromDate,toDate);

        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON_UTF8).body(object);
    }
}
