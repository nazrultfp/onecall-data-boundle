package com.mobilityone.onecall.web.rest;
import com.mobilityone.onecall.service.MsisdnValueService;
import com.mobilityone.onecall.web.rest.errors.BadRequestAlertException;
import com.mobilityone.onecall.web.rest.util.HeaderUtil;
import com.mobilityone.onecall.web.rest.util.PaginationUtil;
import com.mobilityone.onecall.service.dto.MsisdnValueDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing MsisdnValue.
 */
@RestController
@RequestMapping("/api")
public class MsisdnValueResource {

    private final Logger log = LoggerFactory.getLogger(MsisdnValueResource.class);

    private static final String ENTITY_NAME = "oneCallMsisdnValue";

    private final MsisdnValueService msisdnValueService;

    public MsisdnValueResource(MsisdnValueService msisdnValueService) {
        this.msisdnValueService = msisdnValueService;
    }

    /**
     * POST  /msisdn-values : Create a new msisdnValue.
     *
     * @param msisdnValueDTO the msisdnValueDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new msisdnValueDTO, or with status 400 (Bad Request) if the msisdnValue has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
//    @PostMapping("/msisdn-values")
    public ResponseEntity<MsisdnValueDTO> createMsisdnValue(@RequestBody MsisdnValueDTO msisdnValueDTO) throws URISyntaxException {
        log.debug("REST request to save MsisdnValue : {}", msisdnValueDTO);
        if (msisdnValueDTO.getId() != null) {
            throw new BadRequestAlertException("A new msisdnValue cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MsisdnValueDTO result = msisdnValueService.save(msisdnValueDTO);
        return ResponseEntity.created(new URI("/api/msisdn-values/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /msisdn-values : Updates an existing msisdnValue.
     *
     * @param msisdnValueDTO the msisdnValueDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated msisdnValueDTO,
     * or with status 400 (Bad Request) if the msisdnValueDTO is not valid,
     * or with status 500 (Internal Server Error) if the msisdnValueDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
//   @PutMapping("/msisdn-values")
    public ResponseEntity<MsisdnValueDTO> updateMsisdnValue(@RequestBody MsisdnValueDTO msisdnValueDTO) throws URISyntaxException {
        log.debug("REST request to update MsisdnValue : {}", msisdnValueDTO);
        if (msisdnValueDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        MsisdnValueDTO result = msisdnValueService.save(msisdnValueDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, msisdnValueDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /msisdn-values : get all the msisdnValues.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of msisdnValues in body
     */
    @GetMapping("/msisdn-values")
    public ResponseEntity<List<MsisdnValueDTO>> getAllMsisdnValues(Pageable pageable) {
        log.debug("REST request to get a page of MsisdnValues");
        Page<MsisdnValueDTO> page = msisdnValueService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/msisdn-values");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /msisdn-values/:id : get the "id" msisdnValue.
     *
     * @param id the id of the msisdnValueDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the msisdnValueDTO, or with status 404 (Not Found)
     */
    @GetMapping("/msisdn-values/{id}")
    public ResponseEntity<MsisdnValueDTO> getMsisdnValue(@PathVariable Long id) {
        log.debug("REST request to get MsisdnValue : {}", id);
        Optional<MsisdnValueDTO> msisdnValueDTO = msisdnValueService.findOne(id);
        return ResponseUtil.wrapOrNotFound(msisdnValueDTO);
    }

    /**
     * DELETE  /msisdn-values/:id : delete the "id" msisdnValue.
     *
     * @param id the id of the msisdnValueDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
//    @DeleteMapping("/msisdn-values/{id}")
    public ResponseEntity<Void> deleteMsisdnValue(@PathVariable Long id) {
        log.debug("REST request to delete MsisdnValue : {}", id);
        msisdnValueService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
