package com.mobilityone.onecall.service.dto;

import javax.validation.constraints.NotNull;
import java.util.Objects;

public class TopupOrderDTO {

    private String channelId;

    @NotNull
    private String merchantId;

    @NotNull
    private String merchantOrderNo;

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantOrderNo() {
        return merchantOrderNo;
    }

    public void setMerchantOrderNo(String merchantOrderNo) {
        this.merchantOrderNo = merchantOrderNo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TopupOrderDTO that = (TopupOrderDTO) o;
        return Objects.equals(channelId, that.channelId) &&
            merchantId.equals(that.merchantId) &&
            merchantOrderNo.equals(that.merchantOrderNo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(channelId, merchantId, merchantOrderNo);
    }

    @Override
    public String toString() {
        return "TopupOrderDTO{" +
            "channelId='" + channelId + '\'' +
            ", merchantId='" + merchantId + '\'' +
            ", merchantOrderNo='" + merchantOrderNo + '\'' +
            '}';
    }
}
