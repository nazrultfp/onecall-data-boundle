package com.mobilityone.onecall.service.dto;

import org.json.JSONArray;
import org.json.JSONObject;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class SimPortInReqDTO {

    private Long agentId;
//    @NotNull
//    private String agentCode;
    private String donorTelco;
    private String idNumber;

    private List<SimPortInVosReqDTO> simPortInVos = new ArrayList<>();

    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

//    public String getAgentCode() {
//        return agentCode;
//    }
//
//    public void setAgentCode(String agentCode) {
//        this.agentCode = agentCode;
//    }

    public String getDonorTelco() {
        return donorTelco;
    }

    public void setDonorTelco(String donorTelco) {
        this.donorTelco = donorTelco;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public List<SimPortInVosReqDTO> getSimPortInVos() {
        return simPortInVos;
    }

    public void setSimPortInVos(List<SimPortInVosReqDTO> simPortInVos) {
        this.simPortInVos = simPortInVos;
    }

    public JSONObject getJson() {

        JSONObject jsonObject = new JSONObject();

        if (this.donorTelco != null) jsonObject.put("donorTelco", this.donorTelco);
        if (this.idNumber != null) jsonObject.put("idNumber", this.idNumber);
        if (this.simPortInVos.size() > 0){
            JSONArray array = new JSONArray();
            for (SimPortInVosReqDTO value : this.simPortInVos){
                JSONObject object = new JSONObject();
                object.put("msisdn", value.getMsisdn());
                object.put("simNumber", value.getSimNumber());
                array.put(object);
            }
            jsonObject.put("simPortInVos", array);
        }

        return jsonObject;
    }

    @Override
    public String toString() {
        return "SimPortInReqDTO{" +
            "agentId=" + agentId +
//            ", agentCode='" + agentCode + '\'' +
            ", donorTelco='" + donorTelco + '\'' +
            ", idNumber='" + idNumber + '\'' +
            ", simPortInVos=" + simPortInVos +
            '}';
    }
}
