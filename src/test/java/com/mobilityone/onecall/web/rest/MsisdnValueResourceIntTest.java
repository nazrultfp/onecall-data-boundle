package com.mobilityone.onecall.web.rest;

import com.mobilityone.onecall.OneCallApp;

import com.mobilityone.onecall.domain.MsisdnValue;
import com.mobilityone.onecall.repository.MsisdnValueRepository;
import com.mobilityone.onecall.service.MsisdnValueService;
import com.mobilityone.onecall.service.dto.MsisdnValueDTO;
import com.mobilityone.onecall.service.mapper.MsisdnValueMapper;
import com.mobilityone.onecall.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;


import static com.mobilityone.onecall.web.rest.TestUtil.sameInstant;
import static com.mobilityone.onecall.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the MsisdnValueResource REST controller.
 *
 * @see MsisdnValueResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OneCallApp.class)
public class MsisdnValueResourceIntTest {

    private static final Double DEFAULT_AMOUNT = 1D;
    private static final Double UPDATED_AMOUNT = 2D;

    private static final String DEFAULT_FREE = "AAAAAAAAAA";
    private static final String UPDATED_FREE = "BBBBBBBBBB";

    private static final String DEFAULT_FREE_DATA = "AAAAAAAAAA";
    private static final String UPDATED_FREE_DATA = "BBBBBBBBBB";

    private static final String DEFAULT_FREE_SMS = "AAAAAAAAAA";
    private static final String UPDATED_FREE_SMS = "BBBBBBBBBB";

    private static final String DEFAULT_FREE_VOICE = "AAAAAAAAAA";
    private static final String UPDATED_FREE_VOICE = "BBBBBBBBBB";

    private static final String DEFAULT_DATA_ID = "AAAAAAAAAA";
    private static final String UPDATED_DATA_ID = "BBBBBBBBBB";

    private static final String DEFAULT_POINT = "AAAAAAAAAA";
    private static final String UPDATED_POINT = "BBBBBBBBBB";

    private static final String DEFAULT_VALIDITY = "AAAAAAAAAA";
    private static final String UPDATED_VALIDITY = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_MODIFIED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_MODIFIED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_MODIFIED_BY = "BBBBBBBBBB";

    @Autowired
    private MsisdnValueRepository msisdnValueRepository;

    @Autowired
    private MsisdnValueMapper msisdnValueMapper;

    @Autowired
    private MsisdnValueService msisdnValueService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restMsisdnValueMockMvc;

    private MsisdnValue msisdnValue;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MsisdnValueResource msisdnValueResource = new MsisdnValueResource(msisdnValueService);
        this.restMsisdnValueMockMvc = MockMvcBuilders.standaloneSetup(msisdnValueResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MsisdnValue createEntity(EntityManager em) {
        MsisdnValue msisdnValue = new MsisdnValue()
            .amount(DEFAULT_AMOUNT)
            .free(DEFAULT_FREE)
            .freeData(DEFAULT_FREE_DATA)
            .freeSms(DEFAULT_FREE_SMS)
            .freeVoice(DEFAULT_FREE_VOICE)
            .dataId(DEFAULT_DATA_ID)
            .point(DEFAULT_POINT)
            .validity(DEFAULT_VALIDITY)
            .createdDate(DEFAULT_CREATED_DATE)
            .modifiedDate(DEFAULT_MODIFIED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .modifiedBy(DEFAULT_MODIFIED_BY);
        return msisdnValue;
    }

    @Before
    public void initTest() {
        msisdnValue = createEntity(em);
    }

    @Test
    @Transactional
    public void createMsisdnValue() throws Exception {
        int databaseSizeBeforeCreate = msisdnValueRepository.findAll().size();

        // Create the MsisdnValue
        MsisdnValueDTO msisdnValueDTO = msisdnValueMapper.toDto(msisdnValue);
        restMsisdnValueMockMvc.perform(post("/api/msisdn-values")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(msisdnValueDTO)))
            .andExpect(status().isCreated());

        // Validate the MsisdnValue in the database
        List<MsisdnValue> msisdnValueList = msisdnValueRepository.findAll();
        assertThat(msisdnValueList).hasSize(databaseSizeBeforeCreate + 1);
        MsisdnValue testMsisdnValue = msisdnValueList.get(msisdnValueList.size() - 1);
        assertThat(testMsisdnValue.getAmount()).isEqualTo(DEFAULT_AMOUNT);
        assertThat(testMsisdnValue.getFree()).isEqualTo(DEFAULT_FREE);
        assertThat(testMsisdnValue.getFreeData()).isEqualTo(DEFAULT_FREE_DATA);
        assertThat(testMsisdnValue.getFreeSms()).isEqualTo(DEFAULT_FREE_SMS);
        assertThat(testMsisdnValue.getFreeVoice()).isEqualTo(DEFAULT_FREE_VOICE);
        assertThat(testMsisdnValue.getDataId()).isEqualTo(DEFAULT_DATA_ID);
        assertThat(testMsisdnValue.getPoint()).isEqualTo(DEFAULT_POINT);
        assertThat(testMsisdnValue.getValidity()).isEqualTo(DEFAULT_VALIDITY);
        assertThat(testMsisdnValue.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testMsisdnValue.getModifiedDate()).isEqualTo(DEFAULT_MODIFIED_DATE);
        assertThat(testMsisdnValue.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testMsisdnValue.getModifiedBy()).isEqualTo(DEFAULT_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void createMsisdnValueWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = msisdnValueRepository.findAll().size();

        // Create the MsisdnValue with an existing ID
        msisdnValue.setId(1L);
        MsisdnValueDTO msisdnValueDTO = msisdnValueMapper.toDto(msisdnValue);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMsisdnValueMockMvc.perform(post("/api/msisdn-values")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(msisdnValueDTO)))
            .andExpect(status().isBadRequest());

        // Validate the MsisdnValue in the database
        List<MsisdnValue> msisdnValueList = msisdnValueRepository.findAll();
        assertThat(msisdnValueList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllMsisdnValues() throws Exception {
        // Initialize the database
        msisdnValueRepository.saveAndFlush(msisdnValue);

        // Get all the msisdnValueList
        restMsisdnValueMockMvc.perform(get("/api/msisdn-values?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(msisdnValue.getId().intValue())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].free").value(hasItem(DEFAULT_FREE.toString())))
            .andExpect(jsonPath("$.[*].freeData").value(hasItem(DEFAULT_FREE_DATA.toString())))
            .andExpect(jsonPath("$.[*].freeSms").value(hasItem(DEFAULT_FREE_SMS.toString())))
            .andExpect(jsonPath("$.[*].freeVoice").value(hasItem(DEFAULT_FREE_VOICE.toString())))
            .andExpect(jsonPath("$.[*].dataId").value(hasItem(DEFAULT_DATA_ID.toString())))
            .andExpect(jsonPath("$.[*].point").value(hasItem(DEFAULT_POINT.toString())))
            .andExpect(jsonPath("$.[*].validity").value(hasItem(DEFAULT_VALIDITY.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(sameInstant(DEFAULT_CREATED_DATE))))
            .andExpect(jsonPath("$.[*].modifiedDate").value(hasItem(sameInstant(DEFAULT_MODIFIED_DATE))))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].modifiedBy").value(hasItem(DEFAULT_MODIFIED_BY.toString())));
    }
    
    @Test
    @Transactional
    public void getMsisdnValue() throws Exception {
        // Initialize the database
        msisdnValueRepository.saveAndFlush(msisdnValue);

        // Get the msisdnValue
        restMsisdnValueMockMvc.perform(get("/api/msisdn-values/{id}", msisdnValue.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(msisdnValue.getId().intValue()))
            .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT.doubleValue()))
            .andExpect(jsonPath("$.free").value(DEFAULT_FREE.toString()))
            .andExpect(jsonPath("$.freeData").value(DEFAULT_FREE_DATA.toString()))
            .andExpect(jsonPath("$.freeSms").value(DEFAULT_FREE_SMS.toString()))
            .andExpect(jsonPath("$.freeVoice").value(DEFAULT_FREE_VOICE.toString()))
            .andExpect(jsonPath("$.dataId").value(DEFAULT_DATA_ID.toString()))
            .andExpect(jsonPath("$.point").value(DEFAULT_POINT.toString()))
            .andExpect(jsonPath("$.validity").value(DEFAULT_VALIDITY.toString()))
            .andExpect(jsonPath("$.createdDate").value(sameInstant(DEFAULT_CREATED_DATE)))
            .andExpect(jsonPath("$.modifiedDate").value(sameInstant(DEFAULT_MODIFIED_DATE)))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.modifiedBy").value(DEFAULT_MODIFIED_BY.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingMsisdnValue() throws Exception {
        // Get the msisdnValue
        restMsisdnValueMockMvc.perform(get("/api/msisdn-values/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMsisdnValue() throws Exception {
        // Initialize the database
        msisdnValueRepository.saveAndFlush(msisdnValue);

        int databaseSizeBeforeUpdate = msisdnValueRepository.findAll().size();

        // Update the msisdnValue
        MsisdnValue updatedMsisdnValue = msisdnValueRepository.findById(msisdnValue.getId()).get();
        // Disconnect from session so that the updates on updatedMsisdnValue are not directly saved in db
        em.detach(updatedMsisdnValue);
        updatedMsisdnValue
            .amount(UPDATED_AMOUNT)
            .free(UPDATED_FREE)
            .freeData(UPDATED_FREE_DATA)
            .freeSms(UPDATED_FREE_SMS)
            .freeVoice(UPDATED_FREE_VOICE)
            .dataId(UPDATED_DATA_ID)
            .point(UPDATED_POINT)
            .validity(UPDATED_VALIDITY)
            .createdDate(UPDATED_CREATED_DATE)
            .modifiedDate(UPDATED_MODIFIED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .modifiedBy(UPDATED_MODIFIED_BY);
        MsisdnValueDTO msisdnValueDTO = msisdnValueMapper.toDto(updatedMsisdnValue);

        restMsisdnValueMockMvc.perform(put("/api/msisdn-values")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(msisdnValueDTO)))
            .andExpect(status().isOk());

        // Validate the MsisdnValue in the database
        List<MsisdnValue> msisdnValueList = msisdnValueRepository.findAll();
        assertThat(msisdnValueList).hasSize(databaseSizeBeforeUpdate);
        MsisdnValue testMsisdnValue = msisdnValueList.get(msisdnValueList.size() - 1);
        assertThat(testMsisdnValue.getAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testMsisdnValue.getFree()).isEqualTo(UPDATED_FREE);
        assertThat(testMsisdnValue.getFreeData()).isEqualTo(UPDATED_FREE_DATA);
        assertThat(testMsisdnValue.getFreeSms()).isEqualTo(UPDATED_FREE_SMS);
        assertThat(testMsisdnValue.getFreeVoice()).isEqualTo(UPDATED_FREE_VOICE);
        assertThat(testMsisdnValue.getDataId()).isEqualTo(UPDATED_DATA_ID);
        assertThat(testMsisdnValue.getPoint()).isEqualTo(UPDATED_POINT);
        assertThat(testMsisdnValue.getValidity()).isEqualTo(UPDATED_VALIDITY);
        assertThat(testMsisdnValue.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testMsisdnValue.getModifiedDate()).isEqualTo(UPDATED_MODIFIED_DATE);
        assertThat(testMsisdnValue.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testMsisdnValue.getModifiedBy()).isEqualTo(UPDATED_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void updateNonExistingMsisdnValue() throws Exception {
        int databaseSizeBeforeUpdate = msisdnValueRepository.findAll().size();

        // Create the MsisdnValue
        MsisdnValueDTO msisdnValueDTO = msisdnValueMapper.toDto(msisdnValue);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMsisdnValueMockMvc.perform(put("/api/msisdn-values")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(msisdnValueDTO)))
            .andExpect(status().isBadRequest());

        // Validate the MsisdnValue in the database
        List<MsisdnValue> msisdnValueList = msisdnValueRepository.findAll();
        assertThat(msisdnValueList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteMsisdnValue() throws Exception {
        // Initialize the database
        msisdnValueRepository.saveAndFlush(msisdnValue);

        int databaseSizeBeforeDelete = msisdnValueRepository.findAll().size();

        // Delete the msisdnValue
        restMsisdnValueMockMvc.perform(delete("/api/msisdn-values/{id}", msisdnValue.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<MsisdnValue> msisdnValueList = msisdnValueRepository.findAll();
        assertThat(msisdnValueList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MsisdnValue.class);
        MsisdnValue msisdnValue1 = new MsisdnValue();
        msisdnValue1.setId(1L);
        MsisdnValue msisdnValue2 = new MsisdnValue();
        msisdnValue2.setId(msisdnValue1.getId());
        assertThat(msisdnValue1).isEqualTo(msisdnValue2);
        msisdnValue2.setId(2L);
        assertThat(msisdnValue1).isNotEqualTo(msisdnValue2);
        msisdnValue1.setId(null);
        assertThat(msisdnValue1).isNotEqualTo(msisdnValue2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(MsisdnValueDTO.class);
        MsisdnValueDTO msisdnValueDTO1 = new MsisdnValueDTO();
        msisdnValueDTO1.setId(1L);
        MsisdnValueDTO msisdnValueDTO2 = new MsisdnValueDTO();
        assertThat(msisdnValueDTO1).isNotEqualTo(msisdnValueDTO2);
        msisdnValueDTO2.setId(msisdnValueDTO1.getId());
        assertThat(msisdnValueDTO1).isEqualTo(msisdnValueDTO2);
        msisdnValueDTO2.setId(2L);
        assertThat(msisdnValueDTO1).isNotEqualTo(msisdnValueDTO2);
        msisdnValueDTO1.setId(null);
        assertThat(msisdnValueDTO1).isNotEqualTo(msisdnValueDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(msisdnValueMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(msisdnValueMapper.fromId(null)).isNull();
    }
}
