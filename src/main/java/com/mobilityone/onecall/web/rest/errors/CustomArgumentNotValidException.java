package com.mobilityone.onecall.web.rest.errors;

import org.json.JSONObject;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class CustomArgumentNotValidException {

    @ExceptionHandler(MethodArgumentNotValidException.class)
//    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ResponseEntity<String> processValidationError(MethodArgumentNotValidException ex) {
        BindingResult result = ex.getBindingResult();
        List<FieldError> fieldErrors = result.getFieldErrors();
//        ErrorVM dto = new ErrorVM(fieldErrors.get(0).getDefaultMessage());
//        ErrorVM dto = new ErrorVM(ErrorConstants.ERR_VALIDATION);
//        for (FieldError fieldError : fieldErrors) {
//            dto.add(fieldError.getObjectName(), fieldError.getField(), fieldError.getDefaultMessage());
//        }
//        return dto;

        JSONObject error = new JSONObject();
        error.put("error_description", fieldErrors.get(0).getDefaultMessage());
        error.put("error", fieldErrors.get(0).getField());

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=UTF-8");

        return new ResponseEntity<String>(error.toString(2), headers, HttpStatus.BAD_REQUEST);
    }
}
