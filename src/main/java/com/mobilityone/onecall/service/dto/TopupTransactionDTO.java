package com.mobilityone.onecall.service.dto;

import com.mobilityone.onecall.domain.enumeration.Status;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A DTO for the TopupTransaction entity.
 */
public class TopupTransactionDTO implements Serializable {

    private Long id;

    private Double amount;

    private ZonedDateTime transactionDate;

    private String transactionType;

    private Status status;

    private String message;


    private Long merchantId;

    public TopupTransactionDTO() {
    }

    public TopupTransactionDTO(Double amount, ZonedDateTime transactionDate, Status status, String message, Long merchantId) {
        this.amount = amount;
        this.transactionDate = transactionDate;
        this.status = status;
        this.message = message;
        this.merchantId = merchantId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public ZonedDateTime getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(ZonedDateTime transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TopupTransactionDTO topupTransactionDTO = (TopupTransactionDTO) o;
        if (topupTransactionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), topupTransactionDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TopupTransactionDTO{" +
            "id=" + getId() +
            ", amount=" + getAmount() +
            ", transactionDate='" + getTransactionDate() + "'" +
            ", transactionType='" + getTransactionType() + "'" +
            ", status='" + getStatus() + "'" +
            ", message='" + getMessage() + "'" +
            ", merchant=" + getMerchantId() +
            "}";
    }
}
