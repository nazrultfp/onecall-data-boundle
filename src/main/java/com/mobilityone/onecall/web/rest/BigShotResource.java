package com.mobilityone.onecall.web.rest;
import com.mobilityone.onecall.service.BigShotService;
import com.mobilityone.onecall.web.rest.errors.BadRequestAlertException;
import com.mobilityone.onecall.web.rest.util.HeaderUtil;
import com.mobilityone.onecall.web.rest.util.PaginationUtil;
import com.mobilityone.onecall.service.dto.BigShotDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing BigShot.
 */
//@RestController
//@RequestMapping("/api")
public class BigShotResource {

    private final Logger log = LoggerFactory.getLogger(BigShotResource.class);

    private static final String ENTITY_NAME = "oneCallBigShot";

    private final BigShotService bigShotService;

    public BigShotResource(BigShotService bigShotService) {
        this.bigShotService = bigShotService;
    }

    /**
     * POST  /big-shots : Create a new bigShot.
     *
     * @param bigShotDTO the bigShotDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new bigShotDTO, or with status 400 (Bad Request) if the bigShot has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/big-shots")
    public ResponseEntity<BigShotDTO> createBigShot(@RequestBody BigShotDTO bigShotDTO) throws URISyntaxException {
        log.debug("REST request to save BigShot : {}", bigShotDTO);
        if (bigShotDTO.getId() != null) {
            throw new BadRequestAlertException("A new bigShot cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BigShotDTO result = bigShotService.save(bigShotDTO);
        return ResponseEntity.created(new URI("/api/big-shots/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /big-shots : Updates an existing bigShot.
     *
     * @param bigShotDTO the bigShotDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated bigShotDTO,
     * or with status 400 (Bad Request) if the bigShotDTO is not valid,
     * or with status 500 (Internal Server Error) if the bigShotDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/big-shots")
    public ResponseEntity<BigShotDTO> updateBigShot(@RequestBody BigShotDTO bigShotDTO) throws URISyntaxException {
        log.debug("REST request to update BigShot : {}", bigShotDTO);
        if (bigShotDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BigShotDTO result = bigShotService.save(bigShotDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, bigShotDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /big-shots : get all the bigShots.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of bigShots in body
     */
    @GetMapping("/big-shots")
    public ResponseEntity<List<BigShotDTO>> getAllBigShots(Pageable pageable) {
        log.debug("REST request to get a page of BigShots");
        Page<BigShotDTO> page = bigShotService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/big-shots");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /big-shots/:id : get the "id" bigShot.
     *
     * @param id the id of the bigShotDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the bigShotDTO, or with status 404 (Not Found)
     */
    @GetMapping("/big-shots/{id}")
    public ResponseEntity<BigShotDTO> getBigShot(@PathVariable Long id) {
        log.debug("REST request to get BigShot : {}", id);
        Optional<BigShotDTO> bigShotDTO = bigShotService.findOne(id);
        return ResponseUtil.wrapOrNotFound(bigShotDTO);
    }

    /**
     * DELETE  /big-shots/:id : delete the "id" bigShot.
     *
     * @param id the id of the bigShotDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/big-shots/{id}")
    public ResponseEntity<Void> deleteBigShot(@PathVariable Long id) {
        log.debug("REST request to delete BigShot : {}", id);
        bigShotService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
