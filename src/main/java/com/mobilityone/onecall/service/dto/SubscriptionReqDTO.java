package com.mobilityone.onecall.service.dto;

public class SubscriptionReqDTO {

    private String keyword;
    private String msisdn;
    private String transactionId;

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    @Override
    public String toString() {
        return "SubscriptionReqDTO{" +
            "keyword='" + keyword + '\'' +
            ", msisdn='" + msisdn + '\'' +
            ", transactionId='" + transactionId + '\'' +
            '}';
    }
}
