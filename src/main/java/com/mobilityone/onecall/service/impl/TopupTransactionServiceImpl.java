package com.mobilityone.onecall.service.impl;

import com.mobilityone.onecall.domain.TopupTransaction;
import com.mobilityone.onecall.repository.TopupTransactionRepository;
import com.mobilityone.onecall.service.TopupTransactionService;
import com.mobilityone.onecall.service.dto.TopupTransactionDTO;
import com.mobilityone.onecall.service.dto.TopupTransactionReportDTO;
import com.mobilityone.onecall.service.mapper.TopupTransactionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing TopupTransaction.
 */
@Service
@Transactional
public class TopupTransactionServiceImpl implements TopupTransactionService {

    private final Logger log = LoggerFactory.getLogger(TopupTransactionServiceImpl.class);

    private final TopupTransactionRepository topupTransactionRepository;

    private final TopupTransactionMapper topupTransactionMapper;

    public TopupTransactionServiceImpl(TopupTransactionRepository topupTransactionRepository, TopupTransactionMapper topupTransactionMapper) {
        this.topupTransactionRepository = topupTransactionRepository;
        this.topupTransactionMapper = topupTransactionMapper;
    }

    /**
     * Save a topupTransaction.
     *
     * @param topupTransactionDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public TopupTransactionDTO save(TopupTransactionDTO topupTransactionDTO) {
        log.debug("Request to save TopupTransaction : {}", topupTransactionDTO);
        TopupTransaction topupTransaction = topupTransactionMapper.toEntity(topupTransactionDTO);
        topupTransaction = topupTransactionRepository.save(topupTransaction);
        return topupTransactionMapper.toDto(topupTransaction);
    }

    /**
     * Get all the topupTransactions.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<TopupTransactionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all TopupTransactions");
        return topupTransactionRepository.findAll(pageable)
            .map(topupTransactionMapper::toDto);
    }


    /**
     * Get one topupTransaction by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<TopupTransactionDTO> findOne(Long id) {
        log.debug("Request to get TopupTransaction : {}", id);
        return topupTransactionRepository.findById(id)
            .map(topupTransactionMapper::toDto);
    }

    /**
     * Delete the topupTransaction by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete TopupTransaction : {}", id);
        topupTransactionRepository.deleteById(id);
    }

    @Override
    public Page<TopupTransactionReportDTO> getTopupReport(Pageable pageable) {
        log.debug("Request to get TopupTransaction report");
        return topupTransactionRepository.getTopupReport(pageable);
    }
}
