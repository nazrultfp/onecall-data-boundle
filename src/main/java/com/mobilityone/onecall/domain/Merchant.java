package com.mobilityone.onecall.domain;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A Merchant.
 */
@Entity
@Table(name = "merchant")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Merchant implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "merchant_name")
    private String merchantName;

    @Column(name = "merchant_code")
    private String merchantCode;

    @NotNull
    @Column(name = "phone_no", nullable = false)
    private String phoneNo;

    @NotNull
    @Column(name = "jhi_password", nullable = false)
    private String password;

    @Column(name = "registry_code")
    private String registryCode;

    @Column(name = "balance")
    private Double balance = 0d;

    @Column(name = "created_date")
    private ZonedDateTime createdDate;

    @Column(name = "modified_date")
    private ZonedDateTime modifiedDate;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "modified_by")
    private String modifiedBy;

    @OneToMany(mappedBy = "merchant")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<PlanTransaction> planTransactions = new HashSet<>();
    @OneToMany(mappedBy = "merchant")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<TopupTransaction> topupTransactions = new HashSet<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public Merchant merchantName(String merchantName) {
        this.merchantName = merchantName;
        return this;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getMerchantCode() {
        return merchantCode;
    }

    public Merchant merchantCode(String merchantCode) {
        this.merchantCode = merchantCode;
        return this;
    }

    public void setMerchantCode(String merchantCode) {
        this.merchantCode = merchantCode;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public Merchant phoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
        return this;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getPassword() {
        return password;
    }

    public Merchant password(String password) {
        this.password = password;
        return this;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRegistryCode() {
        return registryCode;
    }

    public Merchant registryCode(String registryCode) {
        this.registryCode = registryCode;
        return this;
    }

    public void setRegistryCode(String registryCode) {
        this.registryCode = registryCode;
    }

    public Double getBalance() {
        return balance;
    }

    public Merchant balance(Double balance) {
        this.balance = balance;
        return this;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public Merchant createdDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public ZonedDateTime getModifiedDate() {
        return modifiedDate;
    }

    public Merchant modifiedDate(ZonedDateTime modifiedDate) {
        this.modifiedDate = modifiedDate;
        return this;
    }

    public void setModifiedDate(ZonedDateTime modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public Merchant createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public Merchant modifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
        return this;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Set<PlanTransaction> getPlanTransactions() {
        return planTransactions;
    }

    public Merchant planTransactions(Set<PlanTransaction> planTransactions) {
        this.planTransactions = planTransactions;
        return this;
    }

    public Merchant addPlanTransactions(PlanTransaction planTransaction) {
        this.planTransactions.add(planTransaction);
        planTransaction.setMerchant(this);
        return this;
    }

    public Merchant removePlanTransactions(PlanTransaction planTransaction) {
        this.planTransactions.remove(planTransaction);
        planTransaction.setMerchant(null);
        return this;
    }

    public void setPlanTransactions(Set<PlanTransaction> planTransactions) {
        this.planTransactions = planTransactions;
    }

    public Set<TopupTransaction> getTopupTransactions() {
        return topupTransactions;
    }

    public Merchant topupTransactions(Set<TopupTransaction> topupTransactions) {
        this.topupTransactions = topupTransactions;
        return this;
    }

    public Merchant addTopupTransactions(TopupTransaction topupTransaction) {
        this.topupTransactions.add(topupTransaction);
        topupTransaction.setMerchant(this);
        return this;
    }

    public Merchant removeTopupTransactions(TopupTransaction topupTransaction) {
        this.topupTransactions.remove(topupTransaction);
        topupTransaction.setMerchant(null);
        return this;
    }

    public void setTopupTransactions(Set<TopupTransaction> topupTransactions) {
        this.topupTransactions = topupTransactions;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Merchant merchant = (Merchant) o;
        if (merchant.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), merchant.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Merchant{" +
            "id=" + getId() +
            ", merchantName='" + getMerchantName() + "'" +
            ", merchantCode='" + getMerchantCode() + "'" +
            ", phoneNo='" + getPhoneNo() + "'" +
            ", password='" + getPassword() + "'" +
            ", registryCode='" + getRegistryCode() + "'" +
            ", balance=" + getBalance() +
            ", createdDate='" + getCreatedDate() + "'" +
            ", modifiedDate='" + getModifiedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", modifiedBy='" + getModifiedBy() + "'" +
            "}";
    }
}
