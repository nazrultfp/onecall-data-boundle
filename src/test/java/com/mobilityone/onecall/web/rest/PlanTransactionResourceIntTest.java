package com.mobilityone.onecall.web.rest;

import com.mobilityone.onecall.OneCallApp;
import com.mobilityone.onecall.domain.PlanTransaction;
import com.mobilityone.onecall.domain.enumeration.DiscountType;
import com.mobilityone.onecall.domain.enumeration.Status;
import com.mobilityone.onecall.repository.PlanTransactionRepository;
import com.mobilityone.onecall.service.PlanTransactionService;
import com.mobilityone.onecall.service.dto.PlanTransactionDTO;
import com.mobilityone.onecall.service.mapper.PlanTransactionMapper;
import com.mobilityone.onecall.web.rest.errors.ExceptionTranslator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;

import static com.mobilityone.onecall.web.rest.TestUtil.createFormattingConversionService;
import static com.mobilityone.onecall.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
/**
 * Test class for the PlanTransactionResource REST controller.
 *
 * @see PlanTransactionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OneCallApp.class)
public class PlanTransactionResourceIntTest {

    private static final Double DEFAULT_AMOUNT = 1D;
    private static final Double UPDATED_AMOUNT = 2D;

    private static final String DEFAULT_MSISDN = "AAAAAAAAAA";
    private static final String UPDATED_MSISDN = "BBBBBBBBBB";

    private static final String DEFAULT_TRANSACTION_ID = "AAAAAAAAAA";
    private static final String UPDATED_TRANSACTION_ID = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_TRANSACTION_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_TRANSACTION_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Double DEFAULT_DISCOUNT = 1D;
    private static final Double UPDATED_DISCOUNT = 2D;

    private static final DiscountType DEFAULT_DISCOUNT_TYPE = DiscountType.AMOUNT;
    private static final DiscountType UPDATED_DISCOUNT_TYPE = DiscountType.PERCENT;

    private static final Double DEFAULT_PROCESSING_FEE = 1D;
    private static final Double UPDATED_PROCESSING_FEE = 2D;

    private static final Double DEFAULT_NET_AMOUNT = 1D;
    private static final Double UPDATED_NET_AMOUNT = 2D;

    private static final Double DEFAULT_TOTAL_AMOUNT = 1D;
    private static final Double UPDATED_TOTAL_AMOUNT = 2D;

    private static final String DEFAULT_ORDER_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_ORDER_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_ORDER_CODE = "AAAAAAAAAA";
    private static final String UPDATED_ORDER_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_API_KEY = "AAAAAAAAAA";
    private static final String UPDATED_API_KEY = "BBBBBBBBBB";

    private static final String DEFAULT_BALANCE = "AAAAAAAAAA";
    private static final String UPDATED_BALANCE = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_MESSAGE = "AAAAAAAAAA";
    private static final String UPDATED_MESSAGE = "BBBBBBBBBB";

    private static final Status DEFAULT_STATUS = Status.SUCCESSFUL;
    private static final Status UPDATED_STATUS = Status.UNSUCCESSFUL;

    private static final ZonedDateTime DEFAULT_CREATED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_MODIFIED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_MODIFIED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_MODIFIED_BY = "BBBBBBBBBB";

    @Autowired
    private PlanTransactionRepository planTransactionRepository;

    @Autowired
    private PlanTransactionMapper planTransactionMapper;

    @Autowired
    private PlanTransactionService planTransactionService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restPlanTransactionMockMvc;

    private PlanTransaction planTransaction;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PlanTransactionResource planTransactionResource = new PlanTransactionResource(planTransactionService);
        this.restPlanTransactionMockMvc = MockMvcBuilders.standaloneSetup(planTransactionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PlanTransaction createEntity(EntityManager em) {
        PlanTransaction planTransaction = new PlanTransaction()
            .amount(DEFAULT_AMOUNT)
            .msisdn(DEFAULT_MSISDN)
            .transactionId(DEFAULT_TRANSACTION_ID)
            .transactionDate(DEFAULT_TRANSACTION_DATE)
            .discount(DEFAULT_DISCOUNT)
            .discountType(DEFAULT_DISCOUNT_TYPE)
            .processingFee(DEFAULT_PROCESSING_FEE)
            .netAmount(DEFAULT_NET_AMOUNT)
            .totalAmount(DEFAULT_TOTAL_AMOUNT)
            .orderType(DEFAULT_ORDER_TYPE)
            .orderCode(DEFAULT_ORDER_CODE)
            .apiKey(DEFAULT_API_KEY)
            .balance(DEFAULT_BALANCE)
            .code(DEFAULT_CODE)
            .message(DEFAULT_MESSAGE)
            .status(DEFAULT_STATUS)
            .createdDate(DEFAULT_CREATED_DATE)
            .modifiedDate(DEFAULT_MODIFIED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .modifiedBy(DEFAULT_MODIFIED_BY);
        return planTransaction;
    }

    @Before
    public void initTest() {
        planTransaction = createEntity(em);
    }

    @Test
    @Transactional
    public void createPlanTransaction() throws Exception {
        int databaseSizeBeforeCreate = planTransactionRepository.findAll().size();

        // Create the PlanTransaction
        PlanTransactionDTO planTransactionDTO = planTransactionMapper.toDto(planTransaction);
        restPlanTransactionMockMvc.perform(post("/api/plan-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(planTransactionDTO)))
            .andExpect(status().isCreated());

        // Validate the PlanTransaction in the database
        List<PlanTransaction> planTransactionList = planTransactionRepository.findAll();
        assertThat(planTransactionList).hasSize(databaseSizeBeforeCreate + 1);
        PlanTransaction testPlanTransaction = planTransactionList.get(planTransactionList.size() - 1);
        assertThat(testPlanTransaction.getAmount()).isEqualTo(DEFAULT_AMOUNT);
        assertThat(testPlanTransaction.getMsisdn()).isEqualTo(DEFAULT_MSISDN);
        assertThat(testPlanTransaction.getTransactionId()).isEqualTo(DEFAULT_TRANSACTION_ID);
        assertThat(testPlanTransaction.getTransactionDate()).isEqualTo(DEFAULT_TRANSACTION_DATE);
        assertThat(testPlanTransaction.getDiscount()).isEqualTo(DEFAULT_DISCOUNT);
        assertThat(testPlanTransaction.getDiscountType()).isEqualTo(DEFAULT_DISCOUNT_TYPE);
        assertThat(testPlanTransaction.getProcessingFee()).isEqualTo(DEFAULT_PROCESSING_FEE);
        assertThat(testPlanTransaction.getNetAmount()).isEqualTo(DEFAULT_NET_AMOUNT);
        assertThat(testPlanTransaction.getTotalAmount()).isEqualTo(DEFAULT_TOTAL_AMOUNT);
        assertThat(testPlanTransaction.getOrderType()).isEqualTo(DEFAULT_ORDER_TYPE);
        assertThat(testPlanTransaction.getOrderCode()).isEqualTo(DEFAULT_ORDER_CODE);
        assertThat(testPlanTransaction.getApiKey()).isEqualTo(DEFAULT_API_KEY);
        assertThat(testPlanTransaction.getBalance()).isEqualTo(DEFAULT_BALANCE);
        assertThat(testPlanTransaction.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testPlanTransaction.getMessage()).isEqualTo(DEFAULT_MESSAGE);
        assertThat(testPlanTransaction.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testPlanTransaction.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testPlanTransaction.getModifiedDate()).isEqualTo(DEFAULT_MODIFIED_DATE);
        assertThat(testPlanTransaction.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testPlanTransaction.getModifiedBy()).isEqualTo(DEFAULT_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void createPlanTransactionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = planTransactionRepository.findAll().size();

        // Create the PlanTransaction with an existing ID
        planTransaction.setId(1L);
        PlanTransactionDTO planTransactionDTO = planTransactionMapper.toDto(planTransaction);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPlanTransactionMockMvc.perform(post("/api/plan-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(planTransactionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PlanTransaction in the database
        List<PlanTransaction> planTransactionList = planTransactionRepository.findAll();
        assertThat(planTransactionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllPlanTransactions() throws Exception {
        // Initialize the database
        planTransactionRepository.saveAndFlush(planTransaction);

        // Get all the planTransactionList
        restPlanTransactionMockMvc.perform(get("/api/plan-transactions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(planTransaction.getId().intValue())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].msisdn").value(hasItem(DEFAULT_MSISDN.toString())))
            .andExpect(jsonPath("$.[*].transactionId").value(hasItem(DEFAULT_TRANSACTION_ID.toString())))
            .andExpect(jsonPath("$.[*].transactionDate").value(hasItem(sameInstant(DEFAULT_TRANSACTION_DATE))))
            .andExpect(jsonPath("$.[*].discount").value(hasItem(DEFAULT_DISCOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].discountType").value(hasItem(DEFAULT_DISCOUNT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].processingFee").value(hasItem(DEFAULT_PROCESSING_FEE.doubleValue())))
            .andExpect(jsonPath("$.[*].netAmount").value(hasItem(DEFAULT_NET_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].totalAmount").value(hasItem(DEFAULT_TOTAL_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].orderType").value(hasItem(DEFAULT_ORDER_TYPE.toString())))
            .andExpect(jsonPath("$.[*].orderCode").value(hasItem(DEFAULT_ORDER_CODE.toString())))
            .andExpect(jsonPath("$.[*].apiKey").value(hasItem(DEFAULT_API_KEY.toString())))
            .andExpect(jsonPath("$.[*].balance").value(hasItem(DEFAULT_BALANCE.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].message").value(hasItem(DEFAULT_MESSAGE.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(sameInstant(DEFAULT_CREATED_DATE))))
            .andExpect(jsonPath("$.[*].modifiedDate").value(hasItem(sameInstant(DEFAULT_MODIFIED_DATE))))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].modifiedBy").value(hasItem(DEFAULT_MODIFIED_BY.toString())));
    }

    @Test
    @Transactional
    public void getPlanTransaction() throws Exception {
        // Initialize the database
        planTransactionRepository.saveAndFlush(planTransaction);

        // Get the planTransaction
        restPlanTransactionMockMvc.perform(get("/api/plan-transactions/{id}", planTransaction.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(planTransaction.getId().intValue()))
            .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT.doubleValue()))
            .andExpect(jsonPath("$.msisdn").value(DEFAULT_MSISDN.toString()))
            .andExpect(jsonPath("$.transactionId").value(DEFAULT_TRANSACTION_ID.toString()))
            .andExpect(jsonPath("$.transactionDate").value(sameInstant(DEFAULT_TRANSACTION_DATE)))
            .andExpect(jsonPath("$.discount").value(DEFAULT_DISCOUNT.doubleValue()))
            .andExpect(jsonPath("$.discountType").value(DEFAULT_DISCOUNT_TYPE.toString()))
            .andExpect(jsonPath("$.processingFee").value(DEFAULT_PROCESSING_FEE.doubleValue()))
            .andExpect(jsonPath("$.netAmount").value(DEFAULT_NET_AMOUNT.doubleValue()))
            .andExpect(jsonPath("$.totalAmount").value(DEFAULT_TOTAL_AMOUNT.doubleValue()))
            .andExpect(jsonPath("$.orderType").value(DEFAULT_ORDER_TYPE.toString()))
            .andExpect(jsonPath("$.orderCode").value(DEFAULT_ORDER_CODE.toString()))
            .andExpect(jsonPath("$.apiKey").value(DEFAULT_API_KEY.toString()))
            .andExpect(jsonPath("$.balance").value(DEFAULT_BALANCE.toString()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.message").value(DEFAULT_MESSAGE.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.createdDate").value(sameInstant(DEFAULT_CREATED_DATE)))
            .andExpect(jsonPath("$.modifiedDate").value(sameInstant(DEFAULT_MODIFIED_DATE)))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.modifiedBy").value(DEFAULT_MODIFIED_BY.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPlanTransaction() throws Exception {
        // Get the planTransaction
        restPlanTransactionMockMvc.perform(get("/api/plan-transactions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePlanTransaction() throws Exception {
        // Initialize the database
        planTransactionRepository.saveAndFlush(planTransaction);

        int databaseSizeBeforeUpdate = planTransactionRepository.findAll().size();

        // Update the planTransaction
        PlanTransaction updatedPlanTransaction = planTransactionRepository.findById(planTransaction.getId()).get();
        // Disconnect from session so that the updates on updatedPlanTransaction are not directly saved in db
        em.detach(updatedPlanTransaction);
        updatedPlanTransaction
            .amount(UPDATED_AMOUNT)
            .msisdn(UPDATED_MSISDN)
            .transactionId(UPDATED_TRANSACTION_ID)
            .transactionDate(UPDATED_TRANSACTION_DATE)
            .discount(UPDATED_DISCOUNT)
            .discountType(UPDATED_DISCOUNT_TYPE)
            .processingFee(UPDATED_PROCESSING_FEE)
            .netAmount(UPDATED_NET_AMOUNT)
            .totalAmount(UPDATED_TOTAL_AMOUNT)
            .orderType(UPDATED_ORDER_TYPE)
            .orderCode(UPDATED_ORDER_CODE)
            .apiKey(UPDATED_API_KEY)
            .balance(UPDATED_BALANCE)
            .code(UPDATED_CODE)
            .message(UPDATED_MESSAGE)
            .status(UPDATED_STATUS)
            .createdDate(UPDATED_CREATED_DATE)
            .modifiedDate(UPDATED_MODIFIED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .modifiedBy(UPDATED_MODIFIED_BY);
        PlanTransactionDTO planTransactionDTO = planTransactionMapper.toDto(updatedPlanTransaction);

        restPlanTransactionMockMvc.perform(put("/api/plan-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(planTransactionDTO)))
            .andExpect(status().isOk());

        // Validate the PlanTransaction in the database
        List<PlanTransaction> planTransactionList = planTransactionRepository.findAll();
        assertThat(planTransactionList).hasSize(databaseSizeBeforeUpdate);
        PlanTransaction testPlanTransaction = planTransactionList.get(planTransactionList.size() - 1);
        assertThat(testPlanTransaction.getAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testPlanTransaction.getMsisdn()).isEqualTo(UPDATED_MSISDN);
        assertThat(testPlanTransaction.getTransactionId()).isEqualTo(UPDATED_TRANSACTION_ID);
        assertThat(testPlanTransaction.getTransactionDate()).isEqualTo(UPDATED_TRANSACTION_DATE);
        assertThat(testPlanTransaction.getDiscount()).isEqualTo(UPDATED_DISCOUNT);
        assertThat(testPlanTransaction.getDiscountType()).isEqualTo(UPDATED_DISCOUNT_TYPE);
        assertThat(testPlanTransaction.getProcessingFee()).isEqualTo(UPDATED_PROCESSING_FEE);
        assertThat(testPlanTransaction.getNetAmount()).isEqualTo(UPDATED_NET_AMOUNT);
        assertThat(testPlanTransaction.getTotalAmount()).isEqualTo(UPDATED_TOTAL_AMOUNT);
        assertThat(testPlanTransaction.getOrderType()).isEqualTo(UPDATED_ORDER_TYPE);
        assertThat(testPlanTransaction.getOrderCode()).isEqualTo(UPDATED_ORDER_CODE);
        assertThat(testPlanTransaction.getApiKey()).isEqualTo(UPDATED_API_KEY);
        assertThat(testPlanTransaction.getBalance()).isEqualTo(UPDATED_BALANCE);
        assertThat(testPlanTransaction.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testPlanTransaction.getMessage()).isEqualTo(UPDATED_MESSAGE);
        assertThat(testPlanTransaction.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testPlanTransaction.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testPlanTransaction.getModifiedDate()).isEqualTo(UPDATED_MODIFIED_DATE);
        assertThat(testPlanTransaction.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testPlanTransaction.getModifiedBy()).isEqualTo(UPDATED_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void updateNonExistingPlanTransaction() throws Exception {
        int databaseSizeBeforeUpdate = planTransactionRepository.findAll().size();

        // Create the PlanTransaction
        PlanTransactionDTO planTransactionDTO = planTransactionMapper.toDto(planTransaction);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPlanTransactionMockMvc.perform(put("/api/plan-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(planTransactionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PlanTransaction in the database
        List<PlanTransaction> planTransactionList = planTransactionRepository.findAll();
        assertThat(planTransactionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePlanTransaction() throws Exception {
        // Initialize the database
        planTransactionRepository.saveAndFlush(planTransaction);

        int databaseSizeBeforeDelete = planTransactionRepository.findAll().size();

        // Delete the planTransaction
        restPlanTransactionMockMvc.perform(delete("/api/plan-transactions/{id}", planTransaction.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PlanTransaction> planTransactionList = planTransactionRepository.findAll();
        assertThat(planTransactionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PlanTransaction.class);
        PlanTransaction planTransaction1 = new PlanTransaction();
        planTransaction1.setId(1L);
        PlanTransaction planTransaction2 = new PlanTransaction();
        planTransaction2.setId(planTransaction1.getId());
        assertThat(planTransaction1).isEqualTo(planTransaction2);
        planTransaction2.setId(2L);
        assertThat(planTransaction1).isNotEqualTo(planTransaction2);
        planTransaction1.setId(null);
        assertThat(planTransaction1).isNotEqualTo(planTransaction2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PlanTransactionDTO.class);
        PlanTransactionDTO planTransactionDTO1 = new PlanTransactionDTO();
        planTransactionDTO1.setId(1L);
        PlanTransactionDTO planTransactionDTO2 = new PlanTransactionDTO();
        assertThat(planTransactionDTO1).isNotEqualTo(planTransactionDTO2);
        planTransactionDTO2.setId(planTransactionDTO1.getId());
        assertThat(planTransactionDTO1).isEqualTo(planTransactionDTO2);
        planTransactionDTO2.setId(2L);
        assertThat(planTransactionDTO1).isNotEqualTo(planTransactionDTO2);
        planTransactionDTO1.setId(null);
        assertThat(planTransactionDTO1).isNotEqualTo(planTransactionDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(planTransactionMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(planTransactionMapper.fromId(null)).isNull();
    }
}
