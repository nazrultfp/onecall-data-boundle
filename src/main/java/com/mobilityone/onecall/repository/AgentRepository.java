package com.mobilityone.onecall.repository;

import com.mobilityone.onecall.domain.Agent;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Spring Data  repository for the Agent entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AgentRepository extends JpaRepository<Agent, Long> {

    Optional<Agent> findFirstByPhoneNo(String phoneNo);
}
