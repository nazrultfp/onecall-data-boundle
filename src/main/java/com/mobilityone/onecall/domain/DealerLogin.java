package com.mobilityone.onecall.domain;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A DealerLogin.
 */
@Entity
@Table(name = "dealer_login")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DealerLogin implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "dealer_user_name")
    private String dealerUserName;

    @Column(name = "dealer_password")
    private String dealerPassword;

    @Column(name = "resp_error")
    private Boolean respError;

    @Column(name = "resp_error_desc")
    private String respErrorDesc;

    @Column(name = "resp_exist")
    private Boolean respExist;

    @Column(name = "resp_catch_desc")
    private String respCatchDesc;

    @Column(name = "resp_lst_msisdn")
    private String respLstMsisdn;

    @Column(name = "resp_lst_fullname")
    private String respLstFullname;

    @Column(name = "resp_lst_creditbalance")
    private String respLstCreditbalance;

    @Column(name = "resp_lst_agent_code")
    private String respLstAgentCode;

    @Lob
    @Column(name = "token")
    private String token;

    @Column(name = "token_role")
    private String tokenRole;

    @Column(name = "token_create_date")
    private ZonedDateTime tokenCreateDate;

    @Column(name = "token_expire_date")
    private ZonedDateTime tokenExpireDate;

    @Column(name = "token_expired")
    private Boolean tokenExpired;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDealerUserName() {
        return dealerUserName;
    }

    public DealerLogin dealerUserName(String dealerUserName) {
        this.dealerUserName = dealerUserName;
        return this;
    }

    public void setDealerUserName(String dealerUserName) {
        this.dealerUserName = dealerUserName;
    }

    public String getDealerPassword() {
        return dealerPassword;
    }

    public DealerLogin dealerPassword(String dealerPassword) {
        this.dealerPassword = dealerPassword;
        return this;
    }

    public void setDealerPassword(String dealerPassword) {
        this.dealerPassword = dealerPassword;
    }

    public Boolean isRespError() {
        return respError;
    }

    public DealerLogin respError(Boolean respError) {
        this.respError = respError;
        return this;
    }

    public void setRespError(Boolean respError) {
        this.respError = respError;
    }

    public String getRespErrorDesc() {
        return respErrorDesc;
    }

    public DealerLogin respErrorDesc(String respErrorDesc) {
        this.respErrorDesc = respErrorDesc;
        return this;
    }

    public void setRespErrorDesc(String respErrorDesc) {
        this.respErrorDesc = respErrorDesc;
    }

    public Boolean isRespExist() {
        return respExist;
    }

    public DealerLogin respExist(Boolean respExist) {
        this.respExist = respExist;
        return this;
    }

    public void setRespExist(Boolean respExist) {
        this.respExist = respExist;
    }

    public String getRespCatchDesc() {
        return respCatchDesc;
    }

    public DealerLogin respCatchDesc(String respCatchDesc) {
        this.respCatchDesc = respCatchDesc;
        return this;
    }

    public void setRespCatchDesc(String respCatchDesc) {
        this.respCatchDesc = respCatchDesc;
    }

    public String getRespLstMsisdn() {
        return respLstMsisdn;
    }

    public DealerLogin respLstMsisdn(String respLstMsisdn) {
        this.respLstMsisdn = respLstMsisdn;
        return this;
    }

    public void setRespLstMsisdn(String respLstMsisdn) {
        this.respLstMsisdn = respLstMsisdn;
    }

    public String getRespLstFullname() {
        return respLstFullname;
    }

    public DealerLogin respLstFullname(String respLstFullname) {
        this.respLstFullname = respLstFullname;
        return this;
    }

    public void setRespLstFullname(String respLstFullname) {
        this.respLstFullname = respLstFullname;
    }

    public String getRespLstCreditbalance() {
        return respLstCreditbalance;
    }

    public DealerLogin respLstCreditbalance(String respLstCreditbalance) {
        this.respLstCreditbalance = respLstCreditbalance;
        return this;
    }

    public void setRespLstCreditbalance(String respLstCreditbalance) {
        this.respLstCreditbalance = respLstCreditbalance;
    }

    public String getRespLstAgentCode() {
        return respLstAgentCode;
    }

    public DealerLogin respLstAgentCode(String respLstAgentCode) {
        this.respLstAgentCode = respLstAgentCode;
        return this;
    }

    public void setRespLstAgentCode(String respLstAgentCode) {
        this.respLstAgentCode = respLstAgentCode;
    }

    public String getToken() {
        return token;
    }

    public DealerLogin token(String token) {
        this.token = token;
        return this;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTokenRole() {
        return tokenRole;
    }

    public DealerLogin tokenRole(String tokenRole) {
        this.tokenRole = tokenRole;
        return this;
    }

    public void setTokenRole(String tokenRole) {
        this.tokenRole = tokenRole;
    }

    public ZonedDateTime getTokenCreateDate() {
        return tokenCreateDate;
    }

    public DealerLogin tokenCreateDate(ZonedDateTime tokenCreateDate) {
        this.tokenCreateDate = tokenCreateDate;
        return this;
    }

    public void setTokenCreateDate(ZonedDateTime tokenCreateDate) {
        this.tokenCreateDate = tokenCreateDate;
    }

    public ZonedDateTime getTokenExpireDate() {
        return tokenExpireDate;
    }

    public DealerLogin tokenExpireDate(ZonedDateTime tokenExpireDate) {
        this.tokenExpireDate = tokenExpireDate;
        return this;
    }

    public void setTokenExpireDate(ZonedDateTime tokenExpireDate) {
        this.tokenExpireDate = tokenExpireDate;
    }

    public Boolean isTokenExpired() {
        return tokenExpired;
    }

    public DealerLogin tokenExpired(Boolean tokenExpired) {
        this.tokenExpired = tokenExpired;
        return this;
    }

    public void setTokenExpired(Boolean tokenExpired) {
        this.tokenExpired = tokenExpired;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DealerLogin dealerLogin = (DealerLogin) o;
        if (dealerLogin.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), dealerLogin.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DealerLogin{" +
            "id=" + getId() +
            ", dealerUserName='" + getDealerUserName() + "'" +
            ", dealerPassword='" + getDealerPassword() + "'" +
            ", respError='" + isRespError() + "'" +
            ", respErrorDesc='" + getRespErrorDesc() + "'" +
            ", respExist='" + isRespExist() + "'" +
            ", respCatchDesc='" + getRespCatchDesc() + "'" +
            ", respLstMsisdn='" + getRespLstMsisdn() + "'" +
            ", respLstFullname='" + getRespLstFullname() + "'" +
            ", respLstCreditbalance='" + getRespLstCreditbalance() + "'" +
            ", respLstAgentCode='" + getRespLstAgentCode() + "'" +
            ", token='" + getToken() + "'" +
            ", tokenRole='" + getTokenRole() + "'" +
            ", tokenCreateDate='" + getTokenCreateDate() + "'" +
            ", tokenExpireDate='" + getTokenExpireDate() + "'" +
            ", tokenExpired='" + isTokenExpired() + "'" +
            "}";
    }
}
