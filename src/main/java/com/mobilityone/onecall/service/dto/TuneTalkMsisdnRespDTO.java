package com.mobilityone.onecall.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TuneTalkMsisdnRespDTO {

    private String apiKey;
    private String code;
    private String message;

    private List<TuneTalkValueDTO> data = new ArrayList<>();

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<TuneTalkValueDTO> getData() {
        return data;
    }

    public void setData(List<TuneTalkValueDTO> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "TuneTalkMsisdnRespDTO{" +
            "apiKey='" + apiKey + '\'' +
            ", code='" + code + '\'' +
            ", message='" + message + '\'' +
            ", data=" + data +
            '}';
    }
}
