package com.mobilityone.onecall.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to One Call.
 * <p>
 * Properties are configured in the application.yml file.
 * See {@link io.github.jhipster.config.JHipsterProperties} for a good example.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {

    private AppSetting appSetting;
    private String partnerDealerCode;
    private Url url;
    private Key key;
    private Keycloak keycloak;
    private UsersAdmin usersAdmin;
    private LoginTfp loginTfp;
    private Authentication authentication;
    private TpmDealer tpmDealer;

    public AppSetting getAppSetting() {
        return appSetting;
    }

    public void setAppSetting(AppSetting appSetting) {
        this.appSetting = appSetting;
    }

    public String getPartnerDealerCode() {
        return partnerDealerCode;
    }

    public void setPartnerDealerCode(String partnerDealerCode) {
        this.partnerDealerCode = partnerDealerCode;
    }

    public Url getUrl() {
        return url;
    }

    public void setUrl(Url url) {
        this.url = url;
    }

    public Key getKey() {
        return key;
    }

    public void setKey(Key key) {
        this.key = key;
    }

    public Keycloak getKeycloak() {
        return keycloak;
    }

    public void setKeycloak(Keycloak keycloak) {
        this.keycloak = keycloak;
    }

    public UsersAdmin getUsersAdmin() {
        return usersAdmin;
    }

    public void setUsersAdmin(UsersAdmin usersAdmin) {
        this.usersAdmin = usersAdmin;
    }

    public LoginTfp getLoginTfp() {
        return loginTfp;
    }

    public void setLoginTfp(LoginTfp loginTfp) {
        this.loginTfp = loginTfp;
    }

    public Authentication getAuthentication() {
        return authentication;
    }

    public void setAuthentication(Authentication authentication) {
        this.authentication = authentication;
    }

    public TpmDealer getTpmDealer() {
        return tpmDealer;
    }

    public void setTpmDealer(TpmDealer tpmDealer) {
        this.tpmDealer = tpmDealer;
    }

    public static class Url {
        private String ttBaseUrl;
        private String ttDealerUrl;

        public String getTtBaseUrl() {
            return ttBaseUrl;
        }

        public void setTtBaseUrl(String ttBaseUrl) {
            this.ttBaseUrl = ttBaseUrl;
        }

        public String getTtDealerUrl() {
            return ttDealerUrl;
        }

        public void setTtDealerUrl(String ttDealerUrl) {
            this.ttDealerUrl = ttDealerUrl;
        }
    }

    public static class Key {
        private String ttBaseSecret;
        private String ttDealerSecret;

        public String getTtBaseSecret() {
            return ttBaseSecret;
        }

        public void setTtBaseSecret(String ttBaseSecret) {
            this.ttBaseSecret = ttBaseSecret;
        }

        public String getTtDealerSecret() {
            return ttDealerSecret;
        }

        public void setTtDealerSecret(String ttDealerSecret) {
            this.ttDealerSecret = ttDealerSecret;
        }
    }

    public static class Keycloak {

        private String baseUrl;
        private String realm;
        private String grantType;
        private String clientId;
        private String clientSecret;
        private String scope;

        public String getBaseUrl() {
            return baseUrl;
        }

        public void setBaseUrl(String baseUrl) {
            this.baseUrl = baseUrl;
        }

        public String getRealm() {
            return realm;
        }

        public void setRealm(String realm) {
            this.realm = realm;
        }

        public String getGrantType() {
            return grantType;
        }

        public void setGrantType(String grantType) {
            this.grantType = grantType;
        }

        public String getClientId() {
            return clientId;
        }

        public void setClientId(String clientId) {
            this.clientId = clientId;
        }

        public String getClientSecret() {
            return clientSecret;
        }

        public void setClientSecret(String clientSecret) {
            this.clientSecret = clientSecret;
        }

        public String getScope() {
            return scope;
        }

        public void setScope(String scope) {
            this.scope = scope;
        }
    }

    public static class UsersAdmin {
        private String serverUrl;
        private String realm;
        private String clientId;
        private String username;
        private String password;

        public String getServerUrl() {
            return serverUrl;
        }

        public void setServerUrl(String serverUrl) {
            this.serverUrl = serverUrl;
        }

        public String getRealm() {
            return realm;
        }

        public void setRealm(String realm) {
            this.realm = realm;
        }

        public String getClientId() {
            return clientId;
        }

        public void setClientId(String clientId) {
            this.clientId = clientId;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }

    public static class LoginTfp {
        private String loginUrl;

        public String getLoginUrl() {
            return loginUrl;
        }

        public void setLoginUrl(String loginUrl) {
            this.loginUrl = loginUrl;
        }
    }

    public static class Authentication {
        private String jwtSecret;
        private String tokenHeader;
        private String tokenPrefix;
        private String tokenType;
        private String tokenIssuer;
        private String tokenAudience;

        public String getJwtSecret() {
            return jwtSecret;
        }

        public void setJwtSecret(String jwtSecret) {
            this.jwtSecret = jwtSecret;
        }

        public String getTokenHeader() {
            return tokenHeader;
        }

        public void setTokenHeader(String tokenHeader) {
            this.tokenHeader = tokenHeader;
        }

        public String getTokenPrefix() {
            return tokenPrefix;
        }

        public void setTokenPrefix(String tokenPrefix) {
            this.tokenPrefix = tokenPrefix;
        }

        public String getTokenType() {
            return tokenType;
        }

        public void setTokenType(String tokenType) {
            this.tokenType = tokenType;
        }

        public String getTokenIssuer() {
            return tokenIssuer;
        }

        public void setTokenIssuer(String tokenIssuer) {
            this.tokenIssuer = tokenIssuer;
        }

        public String getTokenAudience() {
            return tokenAudience;
        }

        public void setTokenAudience(String tokenAudience) {
            this.tokenAudience = tokenAudience;
        }
    }

    public static class AppSetting {

        private Integer versionAndroid;
        private String versionIos;
        private Boolean isAndroidCritical;
        private Boolean isIosCritical;
        private String tncUrl;
        private Integer telcoVersion;

        public Integer getVersionAndroid() {
            return versionAndroid;
        }

        public void setVersionAndroid(Integer versionAndroid) {
            this.versionAndroid = versionAndroid;
        }

        public String getVersionIos() {
            return versionIos;
        }

        public void setVersionIos(String versionIos) {
            this.versionIos = versionIos;
        }

        public Boolean getAndroidCritical() {
            return isAndroidCritical;
        }

        public void setIsAndroidCritical(Boolean androidCritical) {
            isAndroidCritical = androidCritical;
        }

        public Boolean getIosCritical() {
            return isIosCritical;
        }

        public void setIsIosCritical(Boolean iosCritical) {
            isIosCritical = iosCritical;
        }

        public String getTncUrl() {
            return tncUrl;
        }

        public void setTncUrl(String tncUrl) {
            this.tncUrl = tncUrl;
        }

        public Integer getTelcoVersion() {
            return telcoVersion;
        }

        public void setTelcoVersion(Integer telcoVersion) {
            this.telcoVersion = telcoVersion;
        }
    }

    public static class TpmDealer {
        private String url;
        private String id;
        private String passcode;
        private String function;
        private String orgID;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getPasscode() {
            return passcode;
        }

        public void setPasscode(String passcode) {
            this.passcode = passcode;
        }

        public String getFunction() {
            return function;
        }

        public void setFunction(String function) {
            this.function = function;
        }

        public String getOrgID() {
            return orgID;
        }

        public void setOrgID(String orgID) {
            this.orgID = orgID;
        }
    }
}
