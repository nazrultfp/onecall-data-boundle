package com.mobilityone.onecall.service.mapper;

import com.mobilityone.onecall.domain.OneCall;
import com.mobilityone.onecall.service.dto.TuneTalkMsisdnRespDTO;
import com.mobilityone.onecall.service.dto.TuneTalkValueDTO;
import org.mapstruct.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@Mapper(componentModel = "spring", uses = {MsisdnMapper.class})
public class TopupValuesMapper implements EntityMapper<TuneTalkMsisdnRespDTO, OneCall> {

    private final Logger log = LoggerFactory.getLogger(TopupValuesMapper.class);

    @Override
    public OneCall toEntity(TuneTalkMsisdnRespDTO dto) {
        OneCall oneCall = new OneCall();

        oneCall.setApiKey(dto.getApiKey());
        oneCall.setCode(dto.getCode());
        oneCall.setMessage(dto.getMessage());
        for (TuneTalkValueDTO data : dto.getData())
            oneCall.addMsisdnValues(new MsisdnMapper().toEntity(data));

        return oneCall;
    }

    @Override
    public TuneTalkMsisdnRespDTO toDto(OneCall entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<OneCall> toEntity(List<TuneTalkMsisdnRespDTO> dtoList) {
        return dtoList.stream().map(new TopupValuesMapper()::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<TuneTalkMsisdnRespDTO> toDto(List<OneCall> entityList) {
        throw new UnsupportedOperationException();
    }
}
