package com.mobilityone.onecall.web.rest;

import com.mobilityone.onecall.service.ClientLoginService;
import com.mobilityone.onecall.service.dto.ClientLoginRespDTO;
import com.mobilityone.onecall.service.dto.LoginDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ClientLoginResource {

    private final Logger log = LoggerFactory.getLogger(ClientLoginResource.class);

    private final ClientLoginService clientLoginService;

    public ClientLoginResource(ClientLoginService clientLoginService) {
        this.clientLoginService = clientLoginService;
    }

    @PostMapping("/client-login")
    public ResponseEntity<?> clientLogin(@RequestBody LoginDTO loginDTO){
        log.debug("REST Request to Login Client : {}", loginDTO);

        ClientLoginRespDTO respDTO = clientLoginService.clientLogin(loginDTO);

        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON_UTF8).body(respDTO);
    }
}
