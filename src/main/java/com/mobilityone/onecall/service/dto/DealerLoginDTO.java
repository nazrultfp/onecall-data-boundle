package com.mobilityone.onecall.service.dto;
import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Lob;

/**
 * A DTO for the DealerLogin entity.
 */
public class DealerLoginDTO implements Serializable {

    private Long id;

    private String dealerUserName;

    private String dealerPassword;

    private Boolean respError;

    private String respErrorDesc;

    private Boolean respExist;

    private String respCatchDesc;

    private String respLstMsisdn;

    private String respLstFullname;

    private String respLstCreditbalance;

    private String respLstAgentCode;

    @Lob
    private String token;

    private String tokenRole;

    private ZonedDateTime tokenCreateDate;

    private ZonedDateTime tokenExpireDate;

    private Boolean tokenExpired = Boolean.FALSE;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDealerUserName() {
        return dealerUserName;
    }

    public void setDealerUserName(String dealerUserName) {
        this.dealerUserName = dealerUserName;
    }

    public String getDealerPassword() {
        return dealerPassword;
    }

    public void setDealerPassword(String dealerPassword) {
        this.dealerPassword = dealerPassword;
    }

    public Boolean isRespError() {
        return respError;
    }

    public void setRespError(Boolean respError) {
        this.respError = respError;
    }

    public String getRespErrorDesc() {
        return respErrorDesc;
    }

    public void setRespErrorDesc(String respErrorDesc) {
        this.respErrorDesc = respErrorDesc;
    }

    public Boolean isRespExist() {
        return respExist;
    }

    public void setRespExist(Boolean respExist) {
        this.respExist = respExist;
    }

    public String getRespCatchDesc() {
        return respCatchDesc;
    }

    public void setRespCatchDesc(String respCatchDesc) {
        this.respCatchDesc = respCatchDesc;
    }

    public String getRespLstMsisdn() {
        return respLstMsisdn;
    }

    public void setRespLstMsisdn(String respLstMsisdn) {
        this.respLstMsisdn = respLstMsisdn;
    }

    public String getRespLstFullname() {
        return respLstFullname;
    }

    public void setRespLstFullname(String respLstFullname) {
        this.respLstFullname = respLstFullname;
    }

    public String getRespLstCreditbalance() {
        return respLstCreditbalance;
    }

    public void setRespLstCreditbalance(String respLstCreditbalance) {
        this.respLstCreditbalance = respLstCreditbalance;
    }

    public String getRespLstAgentCode() {
        return respLstAgentCode;
    }

    public void setRespLstAgentCode(String respLstAgentCode) {
        this.respLstAgentCode = respLstAgentCode;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTokenRole() {
        return tokenRole;
    }

    public void setTokenRole(String tokenRole) {
        this.tokenRole = tokenRole;
    }

    public ZonedDateTime getTokenCreateDate() {
        return tokenCreateDate;
    }

    public void setTokenCreateDate(ZonedDateTime tokenCreateDate) {
        this.tokenCreateDate = tokenCreateDate;
    }

    public ZonedDateTime getTokenExpireDate() {
        return tokenExpireDate;
    }

    public void setTokenExpireDate(ZonedDateTime tokenExpireDate) {
        this.tokenExpireDate = tokenExpireDate;
    }

    public Boolean isTokenExpired() {
        return tokenExpired;
    }

    public void setTokenExpired(Boolean tokenExpired) {
        this.tokenExpired = tokenExpired;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DealerLoginDTO dealerLoginDTO = (DealerLoginDTO) o;
        if (dealerLoginDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), dealerLoginDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DealerLoginDTO{" +
            "id=" + getId() +
            ", dealerUserName='" + getDealerUserName() + "'" +
            ", dealerPassword='" + getDealerPassword() + "'" +
            ", respError='" + isRespError() + "'" +
            ", respErrorDesc='" + getRespErrorDesc() + "'" +
            ", respExist='" + isRespExist() + "'" +
            ", respCatchDesc='" + getRespCatchDesc() + "'" +
            ", respLstMsisdn='" + getRespLstMsisdn() + "'" +
            ", respLstFullname='" + getRespLstFullname() + "'" +
            ", respLstCreditbalance='" + getRespLstCreditbalance() + "'" +
            ", respLstAgentCode='" + getRespLstAgentCode() + "'" +
            ", token='" + getToken() + "'" +
            ", tokenRole='" + getTokenRole() + "'" +
            ", tokenCreateDate='" + getTokenCreateDate() + "'" +
            ", tokenExpireDate='" + getTokenExpireDate() + "'" +
            ", tokenExpired='" + isTokenExpired() + "'" +
            "}";
    }
}
