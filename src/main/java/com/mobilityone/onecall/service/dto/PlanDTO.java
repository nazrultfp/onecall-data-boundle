package com.mobilityone.onecall.service.dto;
import java.io.Serializable;
import java.util.Objects;
import com.mobilityone.onecall.domain.enumeration.PlanRenewal;
import com.mobilityone.onecall.domain.enumeration.PlanType;
import com.mobilityone.onecall.domain.enumeration.DataMode;

/**
 * A DTO for the Plan entity.
 */
public class PlanDTO implements Serializable {

    private Long id;

    private String keyword;

    private PlanRenewal planRenewal;

    private PlanType planType;

    private String amount;

    private String price;

    private DataMode dataMode;

    private String expireDays;

    private String description;

    private Boolean enable;

    private String titleA;

    private String subtitleA;

    private String titleB;

    private String subtitleB;

    private Boolean unlimitedCall;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public PlanRenewal getPlanRenewal() {
        return planRenewal;
    }

    public void setPlanRenewal(PlanRenewal planRenewal) {
        this.planRenewal = planRenewal;
    }

    public PlanType getPlanType() {
        return planType;
    }

    public void setPlanType(PlanType planType) {
        this.planType = planType;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public DataMode getDataMode() {
        return dataMode;
    }

    public void setDataMode(DataMode dataMode) {
        this.dataMode = dataMode;
    }

    public String getExpireDays() {
        return expireDays;
    }

    public void setExpireDays(String expireDays) {
        this.expireDays = expireDays;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean isEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public String getTitleA() {
        return titleA;
    }

    public void setTitleA(String titleA) {
        this.titleA = titleA;
    }

    public String getSubtitleA() {
        return subtitleA;
    }

    public void setSubtitleA(String subtitleA) {
        this.subtitleA = subtitleA;
    }

    public String getTitleB() {
        return titleB;
    }

    public void setTitleB(String titleB) {
        this.titleB = titleB;
    }

    public String getSubtitleB() {
        return subtitleB;
    }

    public void setSubtitleB(String subtitleB) {
        this.subtitleB = subtitleB;
    }

    public Boolean isUnlimitedCall() {
        return unlimitedCall;
    }

    public void setUnlimitedCall(Boolean unlimitedCall) {
        this.unlimitedCall = unlimitedCall;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PlanDTO planDTO = (PlanDTO) o;
        if (planDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), planDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PlanDTO{" +
            "id=" + getId() +
            ", keyword='" + getKeyword() + "'" +
            ", planRenewal='" + getPlanRenewal() + "'" +
            ", planType='" + getPlanType() + "'" +
            ", amount='" + getAmount() + "'" +
            ", price='" + getPrice() + "'" +
            ", dataMode='" + getDataMode() + "'" +
            ", expireDays='" + getExpireDays() + "'" +
            ", description='" + getDescription() + "'" +
            ", enable='" + isEnable() + "'" +
            ", titleA='" + getTitleA() + "'" +
            ", subtitleA='" + getSubtitleA() + "'" +
            ", titleB='" + getTitleB() + "'" +
            ", subtitleB='" + getSubtitleB() + "'" +
            ", unlimitedCall='" + isUnlimitedCall() + "'" +
            "}";
    }
}
