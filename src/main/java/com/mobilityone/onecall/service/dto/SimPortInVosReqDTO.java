package com.mobilityone.onecall.service.dto;

public class SimPortInVosReqDTO {
    private String msisdn;
    private String simNumber;

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getSimNumber() {
        return simNumber;
    }

    public void setSimNumber(String simNumber) {
        this.simNumber = simNumber;
    }

    @Override
    public String toString() {
        return "SimPortInVosDTO{" +
            "msisdn='" + msisdn + '\'' +
            ", simNumber='" + simNumber + '\'' +
            '}';
    }
}
