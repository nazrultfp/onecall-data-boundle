package com.mobilityone.onecall.service;

import com.mobilityone.onecall.service.dto.FavoriteDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Favorite.
 */
public interface FavoriteService {

    /**
     * Save a favorite.
     *
     * @param favoriteDTO the entity to save
     * @return the persisted entity
     */
    FavoriteDTO save(FavoriteDTO favoriteDTO);

    FavoriteDTO update(FavoriteDTO favoriteDTO);

    /**
     * Get all the favorites.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<FavoriteDTO> findAll(Pageable pageable);


    /**
     * Get the "id" favorite.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<FavoriteDTO> findOne(Long id);

    /**
     * Delete the "id" favorite.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    Page<FavoriteDTO> findPhoneNoFavorites(Pageable pageable, String phoneNo);
}
