package com.mobilityone.onecall.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.mobilityone.onecall.config.Constants;
import com.mobilityone.onecall.security.SecurityUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A SimPortIn.
 */
@Entity
@Table(name = "sim_port_in")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SimPortIn implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "agent_code")
    private String agentCode;

    @Column(name = "donor_telco")
    private String donorTelco;

    @Column(name = "id_number")
    private String idNumber;

    @Column(name = "api_key")
    private String apiKey;

    @Column(name = "code")
    private String code;

    @Column(name = "message")
    private String message;

    @Column(name = "created_date")
    private ZonedDateTime createdDate = ZonedDateTime.now();

    @Column(name = "modified_date")
    private ZonedDateTime modifiedDate = ZonedDateTime.now();

    @Column(name = "created_by")
    private String createdBy = SecurityUtils.getCurrentUserLogin().orElse(Constants.ANONYMOUS_USER);

    @Column(name = "modified_by")
    private String modifiedBy = SecurityUtils.getCurrentUserLogin().orElse(Constants.ANONYMOUS_USER);

    @OneToMany(mappedBy = "simPortIn", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REMOVE})
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<SimPortInVo> simPortInVos = new HashSet<>();
    @ManyToOne
    @JsonIgnoreProperties("simPortIns")
    private Agent agent;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public SimPortIn agentCode(String agentCode) {
        this.agentCode = agentCode;
        return this;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getDonorTelco() {
        return donorTelco;
    }

    public SimPortIn donorTelco(String donorTelco) {
        this.donorTelco = donorTelco;
        return this;
    }

    public void setDonorTelco(String donorTelco) {
        this.donorTelco = donorTelco;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public SimPortIn idNumber(String idNumber) {
        this.idNumber = idNumber;
        return this;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getApiKey() {
        return apiKey;
    }

    public SimPortIn apiKey(String apiKey) {
        this.apiKey = apiKey;
        return this;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getCode() {
        return code;
    }

    public SimPortIn code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public SimPortIn message(String message) {
        this.message = message;
        return this;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public SimPortIn createdDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public ZonedDateTime getModifiedDate() {
        return modifiedDate;
    }

    public SimPortIn modifiedDate(ZonedDateTime modifiedDate) {
        this.modifiedDate = modifiedDate;
        return this;
    }

    public void setModifiedDate(ZonedDateTime modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public SimPortIn createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public SimPortIn modifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
        return this;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Set<SimPortInVo> getSimPortInVos() {
        return simPortInVos;
    }

    public SimPortIn simPortInVos(Set<SimPortInVo> simPortInVos) {
        this.simPortInVos = simPortInVos;
        return this;
    }

    public SimPortIn addSimPortInVos(SimPortInVo simPortInVo) {
        this.simPortInVos.add(simPortInVo);
        simPortInVo.setSimPortIn(this);
        return this;
    }

    public SimPortIn removeSimPortInVos(SimPortInVo simPortInVo) {
        this.simPortInVos.remove(simPortInVo);
        simPortInVo.setSimPortIn(null);
        return this;
    }

    public void setSimPortInVos(Set<SimPortInVo> simPortInVos) {
        this.simPortInVos = simPortInVos;
    }

    public Agent getAgent() {
        return agent;
    }

    public SimPortIn agent(Agent agent) {
        this.agent = agent;
        return this;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SimPortIn simPortIn = (SimPortIn) o;
        if (simPortIn.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), simPortIn.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SimPortIn{" +
            "id=" + getId() +
            ", agentCode='" + getAgentCode() + "'" +
            ", donorTelco='" + getDonorTelco() + "'" +
            ", idNumber='" + getIdNumber() + "'" +
            ", apiKey='" + getApiKey() + "'" +
            ", code='" + getCode() + "'" +
            ", message='" + getMessage() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", modifiedDate='" + getModifiedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", modifiedBy='" + getModifiedBy() + "'" +
            "}";
    }
}
