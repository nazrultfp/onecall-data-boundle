package com.mobilityone.onecall.service.impl;

import com.mobilityone.onecall.service.MsisdnValueService;
import com.mobilityone.onecall.domain.MsisdnValue;
import com.mobilityone.onecall.repository.MsisdnValueRepository;
import com.mobilityone.onecall.service.dto.MsisdnValueDTO;
import com.mobilityone.onecall.service.mapper.MsisdnValueMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing MsisdnValue.
 */
@Service
@Transactional
public class MsisdnValueServiceImpl implements MsisdnValueService {

    private final Logger log = LoggerFactory.getLogger(MsisdnValueServiceImpl.class);

    private final MsisdnValueRepository msisdnValueRepository;

    private final MsisdnValueMapper msisdnValueMapper;

    public MsisdnValueServiceImpl(MsisdnValueRepository msisdnValueRepository, MsisdnValueMapper msisdnValueMapper) {
        this.msisdnValueRepository = msisdnValueRepository;
        this.msisdnValueMapper = msisdnValueMapper;
    }

    /**
     * Save a msisdnValue.
     *
     * @param msisdnValueDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public MsisdnValueDTO save(MsisdnValueDTO msisdnValueDTO) {
        log.debug("Request to save MsisdnValue : {}", msisdnValueDTO);
        MsisdnValue msisdnValue = msisdnValueMapper.toEntity(msisdnValueDTO);
        msisdnValue = msisdnValueRepository.save(msisdnValue);
        return msisdnValueMapper.toDto(msisdnValue);
    }

    /**
     * Get all the msisdnValues.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<MsisdnValueDTO> findAll(Pageable pageable) {
        log.debug("Request to get all MsisdnValues");
        return msisdnValueRepository.findAll(pageable)
            .map(msisdnValueMapper::toDto);
    }


    /**
     * Get one msisdnValue by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<MsisdnValueDTO> findOne(Long id) {
        log.debug("Request to get MsisdnValue : {}", id);
        return msisdnValueRepository.findById(id)
            .map(msisdnValueMapper::toDto);
    }

    /**
     * Delete the msisdnValue by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete MsisdnValue : {}", id);        msisdnValueRepository.deleteById(id);
    }
}
