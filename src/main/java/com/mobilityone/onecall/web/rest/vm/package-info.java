/**
 * View Models used by Spring MVC REST controllers.
 */
package com.mobilityone.onecall.web.rest.vm;
