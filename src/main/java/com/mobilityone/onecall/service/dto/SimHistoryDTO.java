package com.mobilityone.onecall.service.dto;

public class SimHistoryDTO {

    private String simSerialNumber;
    private String simMobileNumber;
    private String typeRegistration;
    private String status;
    private String statusDate;

    public SimHistoryDTO() {
    }

    public SimHistoryDTO(String simSerialNumber, String simMobileNumber, String typeRegistration, String status, String statusDate) {
        this.simSerialNumber = simSerialNumber;
        this.simMobileNumber = simMobileNumber;
        this.typeRegistration = typeRegistration;
        this.status = status;
        this.statusDate = statusDate;
    }

    public String getSimSerialNumber() {
        return simSerialNumber;
    }

    public void setSimSerialNumber(String simSerialNumber) {
        this.simSerialNumber = simSerialNumber;
    }

    public String getSimMobileNumber() {
        return simMobileNumber;
    }

    public void setSimMobileNumber(String simMobileNumber) {
        this.simMobileNumber = simMobileNumber;
    }

    public String getTypeRegistration() {
        return typeRegistration;
    }

    public void setTypeRegistration(String typeRegistration) {
        this.typeRegistration = typeRegistration;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(String statusDate) {
        this.statusDate = statusDate;
    }

    @Override
    public String toString() {
        return "SimHistoryDTO{" +
            "simSerialNumber='" + simSerialNumber + '\'' +
            ", simMobileNumber='" + simMobileNumber + '\'' +
            ", typeRegistration='" + typeRegistration + '\'' +
            ", status='" + status + '\'' +
            ", statusDate='" + statusDate + '\'' +
            '}';
    }
}
