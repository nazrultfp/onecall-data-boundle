package com.mobilityone.onecall.config.jwtfilter;

import com.mobilityone.onecall.config.ApplicationProperties;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final Logger log = LoggerFactory.getLogger(JwtAuthenticationFilter.class);

    private final AuthenticationManager authenticationManager;

//    @Autowired
    private final ApplicationProperties properties;

    public JwtAuthenticationFilter(AuthenticationManager authenticationManager, ApplicationProperties properties) {
        this.authenticationManager = authenticationManager;
        this.properties = properties;
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response,
                                            FilterChain filterChain, Authentication authentication) {
        User user = ((User) authentication.getPrincipal());

        log.info("User : {}", user);

        List<String> roles = user.getAuthorities()
            .stream()
            .map(GrantedAuthority::getAuthority)
            .collect(Collectors.toList());

        roles.forEach(au -> System.out.println("-=> ROLE: " + au));

        byte[] signingKey = properties.getAuthentication().getJwtSecret().getBytes();

        log.info("signingKey : {}", signingKey);

        String token = Jwts.builder()
            .signWith(Keys.hmacShaKeyFor(signingKey), SignatureAlgorithm.HS512)
            .setHeaderParam("typ", properties.getAuthentication().getTokenType())
            .setIssuer(properties.getAuthentication().getTokenIssuer())
            .setAudience(properties.getAuthentication().getTokenAudience())
            .setSubject(user.getUsername())
            .setExpiration(new Date(System.currentTimeMillis() + 86400000)) // now + 1 day
            .claim("rol", roles)
            .compact();

        log.info("token : {}", token);

        response.addHeader(properties.getAuthentication().getTokenHeader(), properties.getAuthentication().getTokenPrefix() + " " + token);
    }
}
