package com.mobilityone.onecall.service.impl;

import com.mobilityone.onecall.service.BigShotService;
import com.mobilityone.onecall.domain.BigShot;
import com.mobilityone.onecall.repository.BigShotRepository;
import com.mobilityone.onecall.service.dto.BigShotConvertReqDTO;
import com.mobilityone.onecall.service.dto.BigShotDTO;
import com.mobilityone.onecall.service.dto.TuneTalkRespDTO;
import com.mobilityone.onecall.service.mapper.BigShotMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing BigShot.
 */
@Service
@Transactional
public class BigShotServiceImpl implements BigShotService {

    private final Logger log = LoggerFactory.getLogger(BigShotServiceImpl.class);

    private final BigShotRepository bigShotRepository;

    private final BigShotMapper bigShotMapper;

    public BigShotServiceImpl(BigShotRepository bigShotRepository, BigShotMapper bigShotMapper) {
        this.bigShotRepository = bigShotRepository;
        this.bigShotMapper = bigShotMapper;
    }

    /**
     * Save a bigShot.
     *
     * @param bigShotDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public BigShotDTO save(BigShotDTO bigShotDTO) {
        log.debug("Request to save BigShot : {}", bigShotDTO);
        BigShot bigShot = bigShotMapper.toEntity(bigShotDTO);
        bigShot = bigShotRepository.save(bigShot);
        return bigShotMapper.toDto(bigShot);
    }

    /**
     * Get all the bigShots.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<BigShotDTO> findAll(Pageable pageable) {
        log.debug("Request to get all BigShots");
        return bigShotRepository.findAll(pageable)
            .map(bigShotMapper::toDto);
    }


    /**
     * Get one bigShot by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<BigShotDTO> findOne(Long id) {
        log.debug("Request to get BigShot : {}", id);
        return bigShotRepository.findById(id)
            .map(bigShotMapper::toDto);
    }

    /**
     * Delete the bigShot by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete BigShot : {}", id);        bigShotRepository.deleteById(id);
    }

    @Override
    public BigShotDTO save(BigShotConvertReqDTO bigShotConvertReqDTO, TuneTalkRespDTO tuneTalkRespDTO) {
        log.debug("Request to save BigShot with Req : {} and Resp : {}", bigShotConvertReqDTO, tuneTalkRespDTO);

        BigShotDTO bigShotDTO = new BigShotDTO();
        bigShotDTO.setBigshotId(bigShotConvertReqDTO.getBigshotId());
        bigShotDTO.setMsisdn(bigShotConvertReqDTO.getMsisdn());
        bigShotDTO.setPoint(bigShotConvertReqDTO.getPoint());
        bigShotDTO.setTransactionId(bigShotConvertReqDTO.getTransactionId());
        bigShotDTO.setVersion(bigShotConvertReqDTO.getVersion());
        bigShotDTO.setApiKey(tuneTalkRespDTO.getApiKey());
        bigShotDTO.setCode(tuneTalkRespDTO.getCode());
        bigShotDTO.setMessage(tuneTalkRespDTO.getMessage());
        bigShotDTO = save(bigShotDTO);

        return bigShotDTO;
    }
}
