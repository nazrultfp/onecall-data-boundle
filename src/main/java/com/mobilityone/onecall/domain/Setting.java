package com.mobilityone.onecall.domain;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A Setting.
 */
@Entity
@Table(name = "setting")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Setting implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "version_android")
    private Integer versionAndroid;

    @Column(name = "version_ios")
    private String versionIos;

    @Column(name = "is_android_critical")
    private Boolean isAndroidCritical;

    @Column(name = "is_ios_critical")
    private Boolean isIosCritical;

    @Column(name = "tnc_url")
    private String tncUrl;

    @Column(name = "telco_version")
    private Integer telcoVersion;

    @Column(name = "created_date")
    private ZonedDateTime createdDate;

    @Column(name = "modified_date")
    private ZonedDateTime modifiedDate;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "modified_by")
    private String modifiedBy;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersionAndroid() {
        return versionAndroid;
    }

    public Setting versionAndroid(Integer versionAndroid) {
        this.versionAndroid = versionAndroid;
        return this;
    }

    public void setVersionAndroid(Integer versionAndroid) {
        this.versionAndroid = versionAndroid;
    }

    public String getVersionIos() {
        return versionIos;
    }

    public Setting versionIos(String versionIos) {
        this.versionIos = versionIos;
        return this;
    }

    public void setVersionIos(String versionIos) {
        this.versionIos = versionIos;
    }

    public Boolean isIsAndroidCritical() {
        return isAndroidCritical;
    }

    public Setting isAndroidCritical(Boolean isAndroidCritical) {
        this.isAndroidCritical = isAndroidCritical;
        return this;
    }

    public void setIsAndroidCritical(Boolean isAndroidCritical) {
        this.isAndroidCritical = isAndroidCritical;
    }

    public Boolean isIsIosCritical() {
        return isIosCritical;
    }

    public Setting isIosCritical(Boolean isIosCritical) {
        this.isIosCritical = isIosCritical;
        return this;
    }

    public void setIsIosCritical(Boolean isIosCritical) {
        this.isIosCritical = isIosCritical;
    }

    public String getTncUrl() {
        return tncUrl;
    }

    public Setting tncUrl(String tncUrl) {
        this.tncUrl = tncUrl;
        return this;
    }

    public void setTncUrl(String tncUrl) {
        this.tncUrl = tncUrl;
    }

    public Integer getTelcoVersion() {
        return telcoVersion;
    }

    public Setting telcoVersion(Integer telcoVersion) {
        this.telcoVersion = telcoVersion;
        return this;
    }

    public void setTelcoVersion(Integer telcoVersion) {
        this.telcoVersion = telcoVersion;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public Setting createdDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public ZonedDateTime getModifiedDate() {
        return modifiedDate;
    }

    public Setting modifiedDate(ZonedDateTime modifiedDate) {
        this.modifiedDate = modifiedDate;
        return this;
    }

    public void setModifiedDate(ZonedDateTime modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public Setting createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public Setting modifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
        return this;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Setting setting = (Setting) o;
        if (setting.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), setting.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Setting{" +
            "id=" + getId() +
            ", versionAndroid=" + getVersionAndroid() +
            ", versionIos='" + getVersionIos() + "'" +
            ", isAndroidCritical='" + isIsAndroidCritical() + "'" +
            ", isIosCritical='" + isIsIosCritical() + "'" +
            ", tncUrl='" + getTncUrl() + "'" +
            ", telcoVersion=" + getTelcoVersion() +
            ", createdDate='" + getCreatedDate() + "'" +
            ", modifiedDate='" + getModifiedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", modifiedBy='" + getModifiedBy() + "'" +
            "}";
    }
}
