package com.mobilityone.onecall.service.dto;

public class DealerTopupReqDTO {

    private String msisdn;
    private String partnerDealerCode;
    private Double topUpDeno;
    private String transactionId;

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getPartnerDealerCode() {
        return partnerDealerCode;
    }

    public void setPartnerDealerCode(String partnerDealerCode) {
        this.partnerDealerCode = partnerDealerCode;
    }

    public Double getTopUpDeno() {
        return topUpDeno;
    }

    public void setTopUpDeno(Double topUpDeno) {
        this.topUpDeno = topUpDeno;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    @Override
    public String toString() {
        return "DealerTopupReqDTO{" +
            "msisdn='" + msisdn + '\'' +
            ", partnerDealerCode='" + partnerDealerCode + '\'' +
            ", topUpDeno=" + topUpDeno +
            ", transactionId='" + transactionId + '\'' +
            '}';
    }
}
