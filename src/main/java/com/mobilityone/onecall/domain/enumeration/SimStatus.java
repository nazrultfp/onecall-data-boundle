package com.mobilityone.onecall.domain.enumeration;

public enum SimStatus {
    SUCCESSFUL,
    PENDING,
    KYC_PENDING,
    FAILED,
    REJECTED;
}
