package com.mobilityone.onecall.service.dto;

import com.mobilityone.onecall.config.Constants;
import com.mobilityone.onecall.security.SecurityUtils;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A DTO for the SimOrder entity.
 */
public class SimOrderDTO implements Serializable {

    private Long id;

    private String agentCode;

    private String address;

    private String city;

    private String contact;

    private String country;

    private String courierTrackingUrl;

    private Long digitalPlanId;

    private String donorTelco;

    private String email;

    private String latLong;

    private String msisdn;

    private String name;

    private String postcode;

    private String source;

    private String state;

    private Double topupAmount;

    private String apiKey;

    private String code;

    private String message;

    private ZonedDateTime createdDate = ZonedDateTime.now();

    private ZonedDateTime modifiedDate = ZonedDateTime.now();

    private String createdBy = SecurityUtils.getCurrentUserLogin().orElse(Constants.ANONYMOUS_USER);

    private String modifiedBy = SecurityUtils.getCurrentUserLogin().orElse(Constants.ANONYMOUS_USER);


    private Long agentId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCourierTrackingUrl() {
        return courierTrackingUrl;
    }

    public void setCourierTrackingUrl(String courierTrackingUrl) {
        this.courierTrackingUrl = courierTrackingUrl;
    }

    public Long getDigitalPlanId() {
        return digitalPlanId;
    }

    public void setDigitalPlanId(Long digitalPlanId) {
        this.digitalPlanId = digitalPlanId;
    }

    public String getDonorTelco() {
        return donorTelco;
    }

    public void setDonorTelco(String donorTelco) {
        this.donorTelco = donorTelco;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLatLong() {
        return latLong;
    }

    public void setLatLong(String latLong) {
        this.latLong = latLong;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Double getTopupAmount() {
        return topupAmount;
    }

    public void setTopupAmount(Double topupAmount) {
        this.topupAmount = topupAmount;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public ZonedDateTime getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(ZonedDateTime modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SimOrderDTO simOrderDTO = (SimOrderDTO) o;
        if (simOrderDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), simOrderDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SimOrderDTO{" +
            "id=" + getId() +
            ", agentCode='" + getAgentCode() + "'" +
            ", address='" + getAddress() + "'" +
            ", city='" + getCity() + "'" +
            ", contact='" + getContact() + "'" +
            ", country='" + getCountry() + "'" +
            ", courierTrackingUrl='" + getCourierTrackingUrl() + "'" +
            ", digitalPlanId=" + getDigitalPlanId() +
            ", donorTelco='" + getDonorTelco() + "'" +
            ", email='" + getEmail() + "'" +
            ", latLong='" + getLatLong() + "'" +
            ", msisdn='" + getMsisdn() + "'" +
            ", name='" + getName() + "'" +
            ", postcode='" + getPostcode() + "'" +
            ", source='" + getSource() + "'" +
            ", state='" + getState() + "'" +
            ", topupAmount=" + getTopupAmount() +
            ", apiKey='" + getApiKey() + "'" +
            ", code='" + getCode() + "'" +
            ", message='" + getMessage() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", modifiedDate='" + getModifiedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", modifiedBy='" + getModifiedBy() + "'" +
            ", agent=" + getAgentId() +
            "}";
    }
}
