package com.mobilityone.onecall.service.dto;

public class HistoryReqDTO {

    private String partnerDealerCode;
    private String type;

    public String getPartnerDealerCode() {
        return partnerDealerCode;
    }

    public void setPartnerDealerCode(String partnerDealerCode) {
        this.partnerDealerCode = partnerDealerCode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "HistoryReqDTO{" +
            "partnerDealerCode='" + partnerDealerCode + '\'' +
            ", type='" + type + '\'' +
            '}';
    }
}
