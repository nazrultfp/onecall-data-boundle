package com.mobilityone.onecall.service.dto;

import com.mobilityone.onecall.web.rest.util.CommonUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.NoSuchAlgorithmException;

public class CustomerRegisterDTO {

    private final Logger log = LoggerFactory.getLogger(CustomerRegisterDTO.class);

    private String ID;
    private String MSISDN;
    private String Name;
    private String DOB;
    private String gender;
    private String Address;
    private String SIMSerial;
    private String RegisterDT;
    private String Photo1;
    private String Photo2;
    private String Hash;

    //    @Value("${application.tpmDealer.passcode}")
    private String passcode;

    //    @Value("${application.tpmDealer.function}")
    private String function;

    //    @Value("${application.tpmDealer.orgID}")
    private String orgID;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getMSISDN() {
        return MSISDN;
    }

    public void setMSISDN(String MSISDN) {
        this.MSISDN = MSISDN;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getSIMSerial() {
        return SIMSerial;
    }

    public void setSIMSerial(String SIMSerial) {
        this.SIMSerial = SIMSerial;
    }

    public String getRegisterDT() {
        return RegisterDT;
    }

    public void setRegisterDT(String registerDT) {
        RegisterDT = registerDT;
    }

    public String getPhoto1() {
        return Photo1;
    }

    public void setPhoto1(String photo1) {
        Photo1 = photo1;
    }

    public String getPhoto2() {
        return Photo2;
    }

    public void setPhoto2(String photo2) {
        Photo2 = photo2;
    }

    public String getPasscode() {
        return passcode;
    }

    public void setPasscode(String passcode) {
        this.passcode = passcode;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public String getOrgID() {
        return orgID;
    }

    public void setOrgID(String orgID) {
        this.orgID = orgID;
    }

    public String getHash() {

        String str = this.orgID;
        if (this.ID != null) str = str.concat(this.ID);
        if (this.MSISDN != null) str = str.concat(MSISDN);
        if (this.Name != null) str = str.concat(this.Name);
        if (this.SIMSerial != null) str = str.concat(this.SIMSerial);
        str = str.concat(this.passcode);

        try {
            log.info("Hash str : {}", str);
            this.Hash = CommonUtils.generateSHA1(str);
            log.info("Hash : {}", this.Hash);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }

        return Hash;
    }

    public void setHash(String hash) {
        Hash = hash;
    }

    public JSONObject getJson() {
        JSONObject object = new JSONObject();

        object.put("Function", this.function);
        object.put("OrgID", this.orgID);
        if (this.ID != null) object.put("ID", this.ID);
        if (this.MSISDN != null) object.put("MSISDN", this.MSISDN);
        if (this.Name != null) object.put("Name", this.Name);
        if (this.DOB != null) object.put("DOB", this.DOB);
        if (this.gender != null) object.put("Gender", this.gender);
        if (this.Address != null) object.put("Address", this.Address);
        if (this.SIMSerial != null) object.put("SIMSerial", this.SIMSerial);
        if (this.RegisterDT != null) object.put("RegisterDT", this.RegisterDT);
        if (this.Photo1 != null) object.put("Photo1", this.Photo1);
        if (this.Photo2 != null) object.put("Photo2", this.Photo2);
        object.put("Hash", getHash());

        return object;
    }

    @Override
    public String toString() {
        return "CustomerRegisterDTO{" +
            "ID='" + ID + '\'' +
            ", MSISDN='" + MSISDN + '\'' +
            ", Name='" + Name + '\'' +
            ", Address='" + Address + '\'' +
            ", SIMSerial='" + SIMSerial + '\'' +
            ", RegisterDT='" + RegisterDT + '\'' +
            ", Photo1='" + Photo1 + '\'' +
            ", Photo2='" + Photo2 + '\'' +
            ", Hash='" + Hash + '\'' +
            '}';
    }
}
