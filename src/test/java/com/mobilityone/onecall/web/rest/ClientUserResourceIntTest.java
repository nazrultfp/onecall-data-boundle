package com.mobilityone.onecall.web.rest;

import com.mobilityone.onecall.OneCallApp;

import com.mobilityone.onecall.domain.ClientUser;
import com.mobilityone.onecall.repository.ClientUserRepository;
import com.mobilityone.onecall.service.ClientUserService;
import com.mobilityone.onecall.service.dto.ClientUserDTO;
import com.mobilityone.onecall.service.mapper.ClientUserMapper;
import com.mobilityone.onecall.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;


import static com.mobilityone.onecall.web.rest.TestUtil.sameInstant;
import static com.mobilityone.onecall.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ClientUserResource REST controller.
 *
 * @see ClientUserResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OneCallApp.class)
public class ClientUserResourceIntTest {

    private static final String DEFAULT_CLIENT_USER_NAME = "AAAAAAAAAA";
    private static final String UPDATED_CLIENT_USER_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CLIENT_PASSWORD = "AAAAAAAAAA";
    private static final String UPDATED_CLIENT_PASSWORD = "BBBBBBBBBB";

    private static final String DEFAULT_CLIENT_AUTHORITY = "AAAAAAAAAA";
    private static final String UPDATED_CLIENT_AUTHORITY = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_MODIFIED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_MODIFIED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_MODIFIED_BY = "BBBBBBBBBB";

    @Autowired
    private ClientUserRepository clientUserRepository;

    @Autowired
    private ClientUserMapper clientUserMapper;

    @Autowired
    private ClientUserService clientUserService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restClientUserMockMvc;

    private ClientUser clientUser;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ClientUserResource clientUserResource = new ClientUserResource(clientUserService);
        this.restClientUserMockMvc = MockMvcBuilders.standaloneSetup(clientUserResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ClientUser createEntity(EntityManager em) {
        ClientUser clientUser = new ClientUser()
            .clientUserName(DEFAULT_CLIENT_USER_NAME)
            .clientPassword(DEFAULT_CLIENT_PASSWORD)
            .clientAuthority(DEFAULT_CLIENT_AUTHORITY)
            .createdDate(DEFAULT_CREATED_DATE)
            .modifiedDate(DEFAULT_MODIFIED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .modifiedBy(DEFAULT_MODIFIED_BY);
        return clientUser;
    }

    @Before
    public void initTest() {
        clientUser = createEntity(em);
    }

    @Test
    @Transactional
    public void createClientUser() throws Exception {
        int databaseSizeBeforeCreate = clientUserRepository.findAll().size();

        // Create the ClientUser
        ClientUserDTO clientUserDTO = clientUserMapper.toDto(clientUser);
        restClientUserMockMvc.perform(post("/api/client-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientUserDTO)))
            .andExpect(status().isCreated());

        // Validate the ClientUser in the database
        List<ClientUser> clientUserList = clientUserRepository.findAll();
        assertThat(clientUserList).hasSize(databaseSizeBeforeCreate + 1);
        ClientUser testClientUser = clientUserList.get(clientUserList.size() - 1);
        assertThat(testClientUser.getClientUserName()).isEqualTo(DEFAULT_CLIENT_USER_NAME);
        assertThat(testClientUser.getClientPassword()).isEqualTo(DEFAULT_CLIENT_PASSWORD);
        assertThat(testClientUser.getClientAuthority()).isEqualTo(DEFAULT_CLIENT_AUTHORITY);
        assertThat(testClientUser.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testClientUser.getModifiedDate()).isEqualTo(DEFAULT_MODIFIED_DATE);
        assertThat(testClientUser.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testClientUser.getModifiedBy()).isEqualTo(DEFAULT_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void createClientUserWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = clientUserRepository.findAll().size();

        // Create the ClientUser with an existing ID
        clientUser.setId(1L);
        ClientUserDTO clientUserDTO = clientUserMapper.toDto(clientUser);

        // An entity with an existing ID cannot be created, so this API call must fail
        restClientUserMockMvc.perform(post("/api/client-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientUserDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ClientUser in the database
        List<ClientUser> clientUserList = clientUserRepository.findAll();
        assertThat(clientUserList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllClientUsers() throws Exception {
        // Initialize the database
        clientUserRepository.saveAndFlush(clientUser);

        // Get all the clientUserList
        restClientUserMockMvc.perform(get("/api/client-users?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(clientUser.getId().intValue())))
            .andExpect(jsonPath("$.[*].clientUserName").value(hasItem(DEFAULT_CLIENT_USER_NAME.toString())))
            .andExpect(jsonPath("$.[*].clientPassword").value(hasItem(DEFAULT_CLIENT_PASSWORD.toString())))
            .andExpect(jsonPath("$.[*].clientAuthority").value(hasItem(DEFAULT_CLIENT_AUTHORITY.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(sameInstant(DEFAULT_CREATED_DATE))))
            .andExpect(jsonPath("$.[*].modifiedDate").value(hasItem(sameInstant(DEFAULT_MODIFIED_DATE))))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].modifiedBy").value(hasItem(DEFAULT_MODIFIED_BY.toString())));
    }
    
    @Test
    @Transactional
    public void getClientUser() throws Exception {
        // Initialize the database
        clientUserRepository.saveAndFlush(clientUser);

        // Get the clientUser
        restClientUserMockMvc.perform(get("/api/client-users/{id}", clientUser.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(clientUser.getId().intValue()))
            .andExpect(jsonPath("$.clientUserName").value(DEFAULT_CLIENT_USER_NAME.toString()))
            .andExpect(jsonPath("$.clientPassword").value(DEFAULT_CLIENT_PASSWORD.toString()))
            .andExpect(jsonPath("$.clientAuthority").value(DEFAULT_CLIENT_AUTHORITY.toString()))
            .andExpect(jsonPath("$.createdDate").value(sameInstant(DEFAULT_CREATED_DATE)))
            .andExpect(jsonPath("$.modifiedDate").value(sameInstant(DEFAULT_MODIFIED_DATE)))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.modifiedBy").value(DEFAULT_MODIFIED_BY.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingClientUser() throws Exception {
        // Get the clientUser
        restClientUserMockMvc.perform(get("/api/client-users/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateClientUser() throws Exception {
        // Initialize the database
        clientUserRepository.saveAndFlush(clientUser);

        int databaseSizeBeforeUpdate = clientUserRepository.findAll().size();

        // Update the clientUser
        ClientUser updatedClientUser = clientUserRepository.findById(clientUser.getId()).get();
        // Disconnect from session so that the updates on updatedClientUser are not directly saved in db
        em.detach(updatedClientUser);
        updatedClientUser
            .clientUserName(UPDATED_CLIENT_USER_NAME)
            .clientPassword(UPDATED_CLIENT_PASSWORD)
            .clientAuthority(UPDATED_CLIENT_AUTHORITY)
            .createdDate(UPDATED_CREATED_DATE)
            .modifiedDate(UPDATED_MODIFIED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .modifiedBy(UPDATED_MODIFIED_BY);
        ClientUserDTO clientUserDTO = clientUserMapper.toDto(updatedClientUser);

        restClientUserMockMvc.perform(put("/api/client-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientUserDTO)))
            .andExpect(status().isOk());

        // Validate the ClientUser in the database
        List<ClientUser> clientUserList = clientUserRepository.findAll();
        assertThat(clientUserList).hasSize(databaseSizeBeforeUpdate);
        ClientUser testClientUser = clientUserList.get(clientUserList.size() - 1);
        assertThat(testClientUser.getClientUserName()).isEqualTo(UPDATED_CLIENT_USER_NAME);
        assertThat(testClientUser.getClientPassword()).isEqualTo(UPDATED_CLIENT_PASSWORD);
        assertThat(testClientUser.getClientAuthority()).isEqualTo(UPDATED_CLIENT_AUTHORITY);
        assertThat(testClientUser.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testClientUser.getModifiedDate()).isEqualTo(UPDATED_MODIFIED_DATE);
        assertThat(testClientUser.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testClientUser.getModifiedBy()).isEqualTo(UPDATED_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void updateNonExistingClientUser() throws Exception {
        int databaseSizeBeforeUpdate = clientUserRepository.findAll().size();

        // Create the ClientUser
        ClientUserDTO clientUserDTO = clientUserMapper.toDto(clientUser);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restClientUserMockMvc.perform(put("/api/client-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientUserDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ClientUser in the database
        List<ClientUser> clientUserList = clientUserRepository.findAll();
        assertThat(clientUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteClientUser() throws Exception {
        // Initialize the database
        clientUserRepository.saveAndFlush(clientUser);

        int databaseSizeBeforeDelete = clientUserRepository.findAll().size();

        // Delete the clientUser
        restClientUserMockMvc.perform(delete("/api/client-users/{id}", clientUser.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ClientUser> clientUserList = clientUserRepository.findAll();
        assertThat(clientUserList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ClientUser.class);
        ClientUser clientUser1 = new ClientUser();
        clientUser1.setId(1L);
        ClientUser clientUser2 = new ClientUser();
        clientUser2.setId(clientUser1.getId());
        assertThat(clientUser1).isEqualTo(clientUser2);
        clientUser2.setId(2L);
        assertThat(clientUser1).isNotEqualTo(clientUser2);
        clientUser1.setId(null);
        assertThat(clientUser1).isNotEqualTo(clientUser2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ClientUserDTO.class);
        ClientUserDTO clientUserDTO1 = new ClientUserDTO();
        clientUserDTO1.setId(1L);
        ClientUserDTO clientUserDTO2 = new ClientUserDTO();
        assertThat(clientUserDTO1).isNotEqualTo(clientUserDTO2);
        clientUserDTO2.setId(clientUserDTO1.getId());
        assertThat(clientUserDTO1).isEqualTo(clientUserDTO2);
        clientUserDTO2.setId(2L);
        assertThat(clientUserDTO1).isNotEqualTo(clientUserDTO2);
        clientUserDTO1.setId(null);
        assertThat(clientUserDTO1).isNotEqualTo(clientUserDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(clientUserMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(clientUserMapper.fromId(null)).isNull();
    }
}
