package com.mobilityone.onecall.service;

import com.mobilityone.onecall.service.dto.SimPortInVoDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing SimPortInVo.
 */
public interface SimPortInVoService {

    /**
     * Save a simPortInVo.
     *
     * @param simPortInVoDTO the entity to save
     * @return the persisted entity
     */
    SimPortInVoDTO save(SimPortInVoDTO simPortInVoDTO);

    /**
     * Get all the simPortInVos.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<SimPortInVoDTO> findAll(Pageable pageable);


    /**
     * Get the "id" simPortInVo.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<SimPortInVoDTO> findOne(Long id);

    /**
     * Delete the "id" simPortInVo.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    SimPortInVoDTO findByMsisdn(String msisdn, String statusResp);
}
