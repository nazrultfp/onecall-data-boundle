package com.mobilityone.onecall.service.impl;

import com.mobilityone.onecall.config.ApplicationProperties;
import com.mobilityone.onecall.service.TuneTalkDealerService;
import com.mobilityone.onecall.service.dto.*;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Service
public class TuneTalkDealerServiceImpl implements TuneTalkDealerService {

    private final Logger log = LoggerFactory.getLogger(TuneTalkDealerServiceImpl.class);

    private final ApplicationProperties applicationProperties;

    private static final String secretKey = "c13e3371464c5caebc3010c9a48b201e";

    public TuneTalkDealerServiceImpl(ApplicationProperties applicationProperties) {
        this.applicationProperties = applicationProperties;
    }

    @Override
    public JSONObject simHistory(HistoryReqDTO historyReqDTO) {
        log.info("get SIM History for : {}", historyReqDTO);

        final String url = applicationProperties.getUrl().getTtDealerUrl() + "/history";
        RestTemplate restTemplate = new RestTemplate();

        // request header
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("secretKey", applicationProperties.getKey().getTtDealerSecret());

        final HttpEntity<?> entity = new HttpEntity<>(historyReqDTO, headers);

        log.info("URL : {}", url);

        ResponseEntity<String> exchange = null;
        try {
            exchange = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
        } catch (HttpClientErrorException e) {
            throw new HttpClientErrorException(e.getStatusCode(), "Connection to Tune Talk Api Failed.");
        }

        JSONObject jsonObject = new JSONObject(exchange.getBody());

        return jsonObject;
    }

    @Override
    public JSONObject portInStatus(String msisdn) {
        log.info("Get Port In Status By MSISDN : {}", msisdn);

        final String url = applicationProperties.getUrl().getTtDealerUrl() + "/portInStatus/" + msisdn;
        RestTemplate restTemplate = new RestTemplate();

        // request header
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("secretKey", applicationProperties.getKey().getTtDealerSecret());

        final HttpEntity<?> entity = new HttpEntity<>(headers);

        log.info("URL : {}", url);

        ResponseEntity<String> exchange = null;
        try {
            exchange = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
        } catch (HttpClientErrorException e) {
            throw new HttpClientErrorException(e.getStatusCode(), "Connection to Tune Talk Api Failed.");
        }

        JSONObject jsonObject = new JSONObject(exchange.getBody());

        return jsonObject;
    }

    @Override
    public JSONObject simCancelPortIn(SimCancelPortInReqDTO simCancelPortInReqDTO) {
        log.info("Cancel Sim Port In : {}", simCancelPortInReqDTO);

        final String url = applicationProperties.getUrl().getTtDealerUrl() + "/simCancelPortIn";
        RestTemplate restTemplate = new RestTemplate();

        // request header
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("secretKey", applicationProperties.getKey().getTtDealerSecret());

        final HttpEntity<?> entity = new HttpEntity<>(simCancelPortInReqDTO, headers);

        log.info("URL : {}", url);

        ResponseEntity<String> exchange = null;
        try {
            exchange = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
        } catch (HttpClientErrorException e) {
            throw new HttpClientErrorException(e.getStatusCode(), "Connection to Tune Talk Api Failed.");
        }

        JSONObject jsonObject = new JSONObject(exchange.getBody());

        return jsonObject;
    }

    @Override
    public TuneTalkRespDTO simResubmitPortIn(Long portInId) {
        log.info("Resubmit Sim Port In : {}", portInId);

        final String url = applicationProperties.getUrl().getTtDealerUrl() + "/simResubmitPortIn/" + portInId.toString();
        RestTemplate restTemplate = new RestTemplate();

        // request header
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("secretKey", applicationProperties.getKey().getTtDealerSecret());

        final HttpEntity<?> entity = new HttpEntity<>(headers);

        log.info("URL : {}", url);

        ResponseEntity<TuneTalkRespDTO> exchange = null;
        try {
            exchange = restTemplate.exchange(url, HttpMethod.GET, entity, TuneTalkRespDTO.class);
        } catch (HttpClientErrorException e) {
            throw new HttpClientErrorException(e.getStatusCode(), "Connection to Tune Talk Api Failed.");
        }

        return exchange.getBody();
    }

    @Override
    public TuneTalkRespDTO performSubscription(DealerSubscriptionReqDTO dealerSubscriptionReqDTO) {
        log.info("Perform Subscription PT 1 : {}", dealerSubscriptionReqDTO);

        final String url = applicationProperties.getUrl().getTtDealerUrl() + "/subscription";
        RestTemplate restTemplate = new RestTemplate();

        // request header
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("secretKey", applicationProperties.getKey().getTtDealerSecret());

        final HttpEntity<?> entity = new HttpEntity<>(dealerSubscriptionReqDTO, headers);

        log.info("URL : {}", url);

        ResponseEntity<TuneTalkRespDTO> exchange = null;
        try {
            exchange = restTemplate.exchange(url, HttpMethod.POST, entity, TuneTalkRespDTO.class);
        } catch (HttpClientErrorException e) {
            throw new HttpClientErrorException(e.getStatusCode(), "Connection to Tune Talk Api Failed.");
        }

        return exchange.getBody();
    }

    @Override
    public JSONObject getSubscriptionStatusByTransactionId(String transactionId) {
        log.info("Get Subscription Status By Transaction Id : {}", transactionId);

        final String url = applicationProperties.getUrl().getTtDealerUrl() + "/subscriptionStatus/" + transactionId;
        RestTemplate restTemplate = new RestTemplate();

        // request header
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("secretKey", applicationProperties.getKey().getTtDealerSecret());

        final HttpEntity<?> entity = new HttpEntity<>(headers);

        log.info("URL : {}", url);

        ResponseEntity<String> exchange = null;
        try {
            exchange = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
        } catch (HttpClientErrorException e) {
            throw new HttpClientErrorException(e.getStatusCode(), "Connection to Tune Talk Api Failed.");
        }

        JSONObject jsonObject = new JSONObject(exchange.getBody());

        return jsonObject;
    }

    @Override
    public JSONObject getTelcoList() {
        log.info("Get Telco List");

        final String url = applicationProperties.getUrl().getTtDealerUrl() + "/telco";
        RestTemplate restTemplate = new RestTemplate();

        // request header
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        final HttpEntity<?> entity = new HttpEntity<>(headers);

        log.info("URL : {}", url);

        ResponseEntity<String> exchange = null;
        try {
            exchange = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
        } catch (HttpClientErrorException e) {
            throw new HttpClientErrorException(e.getStatusCode(), "Connection to Tune Talk Api Failed.");
        }

        JSONObject jsonObject = new JSONObject(exchange.getBody());

        return jsonObject;
    }

    @Override
    public TuneTalkTopupRespDTO performTopup(DealerTopupReqDTO dealerTopupReqDTO) {
        log.info("Perform topup : {}", dealerTopupReqDTO);

        final String url = applicationProperties.getUrl().getTtDealerUrl() + "/topup";
        RestTemplate restTemplate = new RestTemplate();

        // request header
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("secretKey", applicationProperties.getKey().getTtDealerSecret());

        final HttpEntity<?> entity = new HttpEntity<>(dealerTopupReqDTO, headers);

        log.info("URL : {}", url);

        ResponseEntity<TuneTalkTopupRespDTO> exchange = null;
        try {
            exchange = restTemplate.exchange(url, HttpMethod.POST, entity, TuneTalkTopupRespDTO.class);
        } catch (HttpClientErrorException e) {
            throw new HttpClientErrorException(e.getStatusCode(), "Connection to Tune Talk Api Failed.");
        }

        return exchange.getBody();
    }

    @Override
    public JSONObject getTopupValues(String msisdn) {
        log.info("Get Topup Deno List for MSISDN : {}", msisdn);

        final String url = applicationProperties.getUrl().getTtDealerUrl() + "/topupValues/" + msisdn;
        RestTemplate restTemplate = new RestTemplate();

        // request header
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("secretKey", applicationProperties.getKey().getTtDealerSecret());

        final HttpEntity<?> entity = new HttpEntity<>(headers);

        log.info("URL : {}", url);

        ResponseEntity<String> exchange = null;
        try {
            exchange = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
        } catch (HttpClientErrorException e) {
            throw new HttpClientErrorException(e.getStatusCode(), "Connection to Tune Talk Api Failed.");
        }

        JSONObject jsonObject = new JSONObject(exchange.getBody());

        return jsonObject;
    }
}
