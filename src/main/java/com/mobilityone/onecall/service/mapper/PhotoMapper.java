package com.mobilityone.onecall.service.mapper;

import com.mobilityone.onecall.domain.*;
import com.mobilityone.onecall.service.dto.PhotoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Photo and its DTO PhotoDTO.
 */
@Mapper(componentModel = "spring", uses = {SimRegisterMapper.class})
public interface PhotoMapper extends EntityMapper<PhotoDTO, Photo> {

    @Mapping(source = "simRegister.id", target = "simRegisterId")
    PhotoDTO toDto(Photo photo);

    @Mapping(source = "simRegisterId", target = "simRegister")
    Photo toEntity(PhotoDTO photoDTO);

    default Photo fromId(Long id) {
        if (id == null) {
            return null;
        }
        Photo photo = new Photo();
        photo.setId(id);
        return photo;
    }
}
