package com.mobilityone.onecall.service.dto;

public class TopupRespDTO {

    private String apiKey;

    private String balance;

    private String code;

    private String message;

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "TopupRespDTO{" +
            "apiKey='" + apiKey + '\'' +
            ", balance='" + balance + '\'' +
            ", code='" + code + '\'' +
            ", message='" + message + '\'' +
            '}';
    }
}
