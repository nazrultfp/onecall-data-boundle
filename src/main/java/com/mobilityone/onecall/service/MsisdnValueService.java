package com.mobilityone.onecall.service;

import com.mobilityone.onecall.service.dto.MsisdnValueDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing MsisdnValue.
 */
public interface MsisdnValueService {

    /**
     * Save a msisdnValue.
     *
     * @param msisdnValueDTO the entity to save
     * @return the persisted entity
     */
    MsisdnValueDTO save(MsisdnValueDTO msisdnValueDTO);

    /**
     * Get all the msisdnValues.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<MsisdnValueDTO> findAll(Pageable pageable);


    /**
     * Get the "id" msisdnValue.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<MsisdnValueDTO> findOne(Long id);

    /**
     * Delete the "id" msisdnValue.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
