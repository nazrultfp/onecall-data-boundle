package com.mobilityone.onecall.service;

import com.mobilityone.onecall.service.dto.SubscriptionPlanDTO;

import com.mobilityone.onecall.service.dto.SubscriptionReqDTO;
import com.mobilityone.onecall.service.dto.TuneTalkRespDTO;
import org.json.JSONObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Service Interface for managing SubscriptionPlan.
 */
public interface SubscriptionPlanService {

    /**
     * Save a subscriptionPlan.
     *
     * @param subscriptionPlanDTO the entity to save
     * @return the persisted entity
     */
    SubscriptionPlanDTO save(SubscriptionPlanDTO subscriptionPlanDTO);

    /**
     * Get all the subscriptionPlans.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<SubscriptionPlanDTO> findAll(Pageable pageable);


    /**
     * Get the "id" subscriptionPlan.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<SubscriptionPlanDTO> findOne(Long id);

    /**
     * Delete the "id" subscriptionPlan.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    SubscriptionPlanDTO save(SubscriptionReqDTO subscriptionReqDTO, TuneTalkRespDTO tuneTalkRespDTO);

    SubscriptionPlanDTO findByTransactionId(String transactionId);

    Page<SubscriptionPlanDTO> findSuccessfulByMsisdn(Pageable pageable, String msisdn);

    List<Map<String, Object>> report();

    JSONObject getTotalPlansByPackageAndDate(String pakage, Integer year, Integer month, Integer day);

    List<Map<String, Object>> getTotalPlansByPackage(String pakage);

    List<Map<String, Object>> getTotalPlansGroupByPackageAndDate();

    List<Map<String, Object>> getTotalPlansByDateRange(String fromDate, String toDate);

    List<Map<String, Object>> getDailyTotalSaleInRange(String fromDate, String toDate);

    List<Map<String, Object>> getDailyTotalSaleInRangeForClient(String client, String fromDate, String toDate);

    List<Map<String, Object>> getPlansInRange(String fromDate, String toDate);
}
