package com.mobilityone.onecall.service.impl;

import com.mobilityone.onecall.config.ApplicationProperties;
import com.mobilityone.onecall.config.Constants;
import com.mobilityone.onecall.security.SecurityUtils;
import com.mobilityone.onecall.service.SettingService;
import com.mobilityone.onecall.domain.Setting;
import com.mobilityone.onecall.repository.SettingRepository;
import com.mobilityone.onecall.service.dto.SettingDTO;
import com.mobilityone.onecall.service.mapper.SettingMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing Setting.
 */
@Service
@Transactional
public class SettingServiceImpl implements SettingService {

    private final Logger log = LoggerFactory.getLogger(SettingServiceImpl.class);

    private final ApplicationProperties properties;

    private final SettingRepository settingRepository;

    private final SettingMapper settingMapper;

    public SettingServiceImpl(ApplicationProperties properties, SettingRepository settingRepository, SettingMapper settingMapper) {
        this.properties = properties;
        this.settingRepository = settingRepository;
        this.settingMapper = settingMapper;
    }

    /**
     * Save a setting.
     *
     * @param settingDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public SettingDTO save(SettingDTO settingDTO) {
        log.debug("Request to save Setting : {}", settingDTO);
        Setting setting = settingMapper.toEntity(settingDTO);
        setting = settingRepository.save(setting);
        return settingMapper.toDto(setting);
    }

    /**
     * Get all the settings.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<SettingDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Settings");
        return settingRepository.findAll(pageable)
            .map(settingMapper::toDto);
    }


    /**
     * Get one setting by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<SettingDTO> findOne(Long id) {
        log.debug("Request to get Setting : {}", id);
        return settingRepository.findById(id)
            .map(settingMapper::toDto);
    }

    /**
     * Delete the setting by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Setting : {}", id);        settingRepository.deleteById(id);
    }

    @Override
    public void setDefaultValues() {
        List<Setting> settingList = settingRepository.findAll();
        if (settingList.size() <= 0){
            Setting setting = new Setting();
            setting.setVersionAndroid(properties.getAppSetting().getVersionAndroid());
            setting.setVersionIos(properties.getAppSetting().getVersionIos());
            setting.setTelcoVersion(properties.getAppSetting().getTelcoVersion());
            setting.setIsAndroidCritical(properties.getAppSetting().getAndroidCritical());
            setting.setIsIosCritical(properties.getAppSetting().getIosCritical());
            setting.setTncUrl(properties.getAppSetting().getTncUrl());
            setting.setCreatedDate(ZonedDateTime.now());
            setting.setCreatedBy(SecurityUtils.getCurrentUserLogin().orElse(Constants.SYSTEM_ACCOUNT));
            setting.setModifiedDate(ZonedDateTime.now());
            setting.setModifiedBy(SecurityUtils.getCurrentUserLogin().orElse(Constants.SYSTEM_ACCOUNT));
            Setting save = settingRepository.save(setting);
            log.info("Saved data : {}", save);
        }
    }
}
