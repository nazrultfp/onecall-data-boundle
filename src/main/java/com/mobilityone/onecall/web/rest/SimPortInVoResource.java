package com.mobilityone.onecall.web.rest;
import com.mobilityone.onecall.service.SimPortInVoService;
import com.mobilityone.onecall.web.rest.errors.BadRequestAlertException;
import com.mobilityone.onecall.web.rest.util.HeaderUtil;
import com.mobilityone.onecall.web.rest.util.PaginationUtil;
import com.mobilityone.onecall.service.dto.SimPortInVoDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SimPortInVo.
 */
//@RestController
//@RequestMapping("/api")
public class SimPortInVoResource {

    private final Logger log = LoggerFactory.getLogger(SimPortInVoResource.class);

    private static final String ENTITY_NAME = "oneCallSimPortInVo";

    private final SimPortInVoService simPortInVoService;

    public SimPortInVoResource(SimPortInVoService simPortInVoService) {
        this.simPortInVoService = simPortInVoService;
    }

    /**
     * POST  /sim-port-in-vos : Create a new simPortInVo.
     *
     * @param simPortInVoDTO the simPortInVoDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new simPortInVoDTO, or with status 400 (Bad Request) if the simPortInVo has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    //@PostMapping("/sim-port-in-vos")
    public ResponseEntity<SimPortInVoDTO> createSimPortInVo(@RequestBody SimPortInVoDTO simPortInVoDTO) throws URISyntaxException {
        log.debug("REST request to save SimPortInVo : {}", simPortInVoDTO);
        if (simPortInVoDTO.getId() != null) {
            throw new BadRequestAlertException("A new simPortInVo cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SimPortInVoDTO result = simPortInVoService.save(simPortInVoDTO);
        return ResponseEntity.created(new URI("/api/sim-port-in-vos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /sim-port-in-vos : Updates an existing simPortInVo.
     *
     * @param simPortInVoDTO the simPortInVoDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated simPortInVoDTO,
     * or with status 400 (Bad Request) if the simPortInVoDTO is not valid,
     * or with status 500 (Internal Server Error) if the simPortInVoDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    //@PutMapping("/sim-port-in-vos")
    public ResponseEntity<SimPortInVoDTO> updateSimPortInVo(@RequestBody SimPortInVoDTO simPortInVoDTO) throws URISyntaxException {
        log.debug("REST request to update SimPortInVo : {}", simPortInVoDTO);
        if (simPortInVoDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SimPortInVoDTO result = simPortInVoService.save(simPortInVoDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, simPortInVoDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /sim-port-in-vos : get all the simPortInVos.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of simPortInVos in body
     */
    //GetMapping("/sim-port-in-vos")
    public ResponseEntity<List<SimPortInVoDTO>> getAllSimPortInVos(Pageable pageable) {
        log.debug("REST request to get a page of SimPortInVos");
        Page<SimPortInVoDTO> page = simPortInVoService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sim-port-in-vos");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /sim-port-in-vos/:id : get the "id" simPortInVo.
     *
     * @param id the id of the simPortInVoDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the simPortInVoDTO, or with status 404 (Not Found)
     */
    //@GetMapping("/sim-port-in-vos/{id}")
    public ResponseEntity<SimPortInVoDTO> getSimPortInVo(@PathVariable Long id) {
        log.debug("REST request to get SimPortInVo : {}", id);
        Optional<SimPortInVoDTO> simPortInVoDTO = simPortInVoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(simPortInVoDTO);
    }

    /**
     * DELETE  /sim-port-in-vos/:id : delete the "id" simPortInVo.
     *
     * @param id the id of the simPortInVoDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    //@DeleteMapping("/sim-port-in-vos/{id}")
    public ResponseEntity<Void> deleteSimPortInVo(@PathVariable Long id) {
        log.debug("REST request to delete SimPortInVo : {}", id);
        simPortInVoService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
