package com.mobilityone.onecall.service.dto;

public class DebitBalanceReqDTO {

    private Double amount;
    private String msisdn;

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }
}
