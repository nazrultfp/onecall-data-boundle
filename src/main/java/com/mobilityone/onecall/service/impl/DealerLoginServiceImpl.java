package com.mobilityone.onecall.service.impl;

import com.mobilityone.onecall.config.ApplicationProperties;
import com.mobilityone.onecall.domain.DealerLogin;
import com.mobilityone.onecall.repository.DealerLoginRepository;
import com.mobilityone.onecall.service.DealerLoginService;
import com.mobilityone.onecall.service.TuneTalkService;
import com.mobilityone.onecall.service.dto.DealerLoginDTO;
import com.mobilityone.onecall.service.dto.LoginDTO;
import com.mobilityone.onecall.service.dto.LoginRespDTO;
import com.mobilityone.onecall.service.dto.TtBalabceRespDTO;
import com.mobilityone.onecall.service.mapper.DealerLoginMapper;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing DealerLogin.
 */
@Service
@Transactional
public class DealerLoginServiceImpl implements DealerLoginService {

    private final Logger log = LoggerFactory.getLogger(DealerLoginServiceImpl.class);

    private final ApplicationProperties properties;

    private final AuthenticationManager authenticationManager;

    private final DealerLoginRepository dealerLoginRepository;

    private final DealerLoginMapper dealerLoginMapper;

    private final TuneTalkService tuneTalkService;

    private final static String ROLE_USER = "ROLE_USER";

    public DealerLoginServiceImpl(ApplicationProperties properties, AuthenticationManager authenticationManager,
                                  DealerLoginRepository dealerLoginRepository, DealerLoginMapper dealerLoginMapper, TuneTalkService tuneTalkService) {
        this.properties = properties;
        this.authenticationManager = authenticationManager;
        this.dealerLoginRepository = dealerLoginRepository;
        this.dealerLoginMapper = dealerLoginMapper;
        this.tuneTalkService = tuneTalkService;
    }

    /**
     * Save a dealerLogin.
     *
     * @param dealerLoginDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public DealerLoginDTO save(DealerLoginDTO dealerLoginDTO) {
        log.debug("Request to save DealerLogin : {}", dealerLoginDTO);
        DealerLogin dealerLogin = dealerLoginMapper.toEntity(dealerLoginDTO);
        dealerLogin = dealerLoginRepository.save(dealerLogin);
        return dealerLoginMapper.toDto(dealerLogin);
    }

    /**
     * Get all the dealerLogins.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<DealerLoginDTO> findAll(Pageable pageable) {
        log.debug("Request to get all DealerLogins");
        return dealerLoginRepository.findAll(pageable)
            .map(dealerLoginMapper::toDto);
    }


    /**
     * Get one dealerLogin by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<DealerLoginDTO> findOne(Long id) {
        log.debug("Request to get DealerLogin : {}", id);
        return dealerLoginRepository.findById(id)
            .map(dealerLoginMapper::toDto);
    }

    /**
     * Delete the dealerLogin by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete DealerLogin : {}", id);        dealerLoginRepository.deleteById(id);
    }

    @Override
    public LoginRespDTO login(LoginDTO loginDTO) {
        log.debug("Start login process for: {}", loginDTO.getUsername());

        String loginUrl = properties.getLoginTfp().getLoginUrl();

//        RestTemplate restTemplate = new RestTemplate();

        // request header
//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.APPLICATION_JSON);
//        final HttpEntity<?> entity = new HttpEntity<>(headers);
//
        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(loginUrl)
            .queryParam("username", loginDTO.getUsername())
            .queryParam("password", loginDTO.getPassword());

        log.info("URL : {}", uriBuilder.toUriString());
//
//        ResponseEntity<DealerLoginRespDTO> responseEntity = restTemplate.exchange(
//            uriBuilder.toUriString(),
//            HttpMethod.GET,
//            entity,
//            DealerLoginRespDTO.class
//        );

        JSONObject jsonObject;
        try {
            HttpClient httpclient = HttpClients.createDefault();
            HttpGet httpGet = new HttpGet(uriBuilder.toUriString());
            httpGet.addHeader("content-type", "application/json");
            HttpResponse response = httpclient.execute(httpGet);
            org.apache.http.HttpEntity entity = response.getEntity();
            String resBody = IOUtils.toString(entity.getContent(), StandardCharsets.UTF_8.name());
            log.info("==>>>> Body: {}", resBody);
            jsonObject = new JSONObject(resBody);
//            if (jsonObject == null) {
//                throw new ResponseStatusException(HttpStatus.FAILED_DEPENDENCY, "no response from tfp");
//            }

            if (jsonObject.has("Exist") && jsonObject.getBoolean("Exist") == Boolean.FALSE) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "user doesn't exist");
            }
        } catch (IOException | JSONException e) {
            throw new ResponseStatusException(HttpStatus.FAILED_DEPENDENCY, "no response from tfp", e);
        }

//        DealerLoginRespDTO body = responseEntity.getBody();
//        log.info("TFP Responce : {}",body);
//        if (body.getExist().equals(Boolean.FALSE)) {
//            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "user not found.");
//        }

        DealerLoginDTO dealer = findDealer(loginDTO.getUsername());

        log.info("Dealer Data : {}", dealer);

        if (dealer == null){
            dealer = new DealerLoginDTO();
            dealer.setDealerUserName(loginDTO.getUsername());
            dealer.setDealerPassword(loginDTO.getPassword());
            dealer.setRespError(jsonObject.getBoolean("Error"));
            dealer.setRespErrorDesc(jsonObject.getString("ErrorDescription"));
            dealer.setRespCatchDesc(jsonObject.getString("CatchDescription"));
            dealer.setRespExist(jsonObject.getBoolean("Exist"));
            if (jsonObject.has("lst")) {
                JSONArray lst = jsonObject.getJSONArray("lst");
                JSONObject object = (JSONObject) lst.get(0);
                dealer.setRespLstMsisdn(object.getString("MSISDN"));
                dealer.setRespLstFullname(object.getString("Fullname"));
                dealer.setRespLstCreditbalance(object.getString("Creditbalance"));
                dealer.setRespLstAgentCode(object.getString("AgentCode"));
            }
            dealer = save(dealer);
        }

//        TtBalabceRespDTO balance = tuneTalkService.getBalanceByMsisdn(loginDTO.getUsername());
//        if (!balance.getCode().equals("200")){
//            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "msisdn is not onecall");
//        }

        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(dealer.getDealerUserName(), dealer.getDealerPassword(),
                Collections.singletonList(new SimpleGrantedAuthority(ROLE_USER))));

            if (dealer.getToken() == null) {
                dealer.setToken(doGenerateToken(dealer.getDealerUserName()));
                dealer.setTokenCreateDate(ZonedDateTime.now());
                dealer.setTokenExpireDate(ZonedDateTime.now().plusHours(1));
                dealer.setTokenExpired(Boolean.FALSE);
                dealer.setTokenRole(ROLE_USER);

                dealer = save(dealer);

                log.info("Dealer Saved Data : {}", dealer);
            }

            LoginRespDTO loginRespDTO = new LoginRespDTO();
            loginRespDTO.setAccess_token(dealer.getToken());
            loginRespDTO.setToken_type(properties.getAuthentication().getTokenPrefix());
            loginRespDTO.setFull_name(dealer.getRespLstFullname());
            loginRespDTO.setCredit_balance(dealer.getRespLstCreditbalance());
            loginRespDTO.setMsisdn(dealer.getRespLstMsisdn());
            loginRespDTO.setAgentCode(dealer.getRespLstAgentCode());

            log.info("Login Response Data : {}", loginRespDTO);

            return loginRespDTO;


        } catch (DisabledException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "USER_DISABLED");
        } catch (BadCredentialsException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "INVALID_CREDENTIALS");
        }
    }

    private String doGenerateToken(String userName) {
        List<SimpleGrantedAuthority> roleUser = Collections.singletonList(new SimpleGrantedAuthority(ROLE_USER));

        byte[] signingKey = properties.getAuthentication().getJwtSecret().getBytes();

        String token = Jwts.builder()
            .signWith(Keys.hmacShaKeyFor(signingKey), SignatureAlgorithm.HS512)
            .setHeaderParam("typ", properties.getAuthentication().getTokenType())
            .setIssuer(properties.getAuthentication().getTokenIssuer())
            .setAudience(properties.getAuthentication().getTokenAudience())
            .setSubject(userName)
            .setExpiration(new Date(System.currentTimeMillis() + (24 * 60 * 60 * 1000))) // now + 1 day
            .claim("rol", roleUser)
            .compact();
        return token;
    }

    private DealerLoginDTO findDealer(String username) {
        Optional<DealerLogin> dealerLoginOptional = dealerLoginRepository.findByDealerUserNameAndTokenExpiredFalse(username);
        if (dealerLoginOptional.isPresent()){
            DealerLogin dealerLogin = dealerLoginOptional.get();
           if (dealerLogin.getToken()!= null && ZonedDateTime.now().compareTo(dealerLogin.getTokenExpireDate()) > 0){
                   dealerLogin.setTokenExpired(Boolean.TRUE);
                   dealerLoginRepository.save(dealerLogin);
                   return null;
           }
           return  dealerLoginMapper.toDto(dealerLogin);
        }

        return null;
    }
}
