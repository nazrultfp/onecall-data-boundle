package com.mobilityone.onecall.repository;

import com.mobilityone.onecall.domain.ClientUser;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Spring Data  repository for the ClientUser entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClientUserRepository extends JpaRepository<ClientUser, Long> {

    Optional<ClientUser> findByClientUserName(String username);

    Optional<ClientUser> findByClientUserNameAndClientPassword(String username, String password);
}
