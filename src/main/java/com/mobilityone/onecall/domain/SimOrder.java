package com.mobilityone.onecall.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A SimOrder.
 */
@Entity
@Table(name = "sim_order")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SimOrder implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "agent_code")
    private String agentCode;

    @Column(name = "address")
    private String address;

    @Column(name = "city")
    private String city;

    @Column(name = "contact")
    private String contact;

    @Column(name = "country")
    private String country;

    @Column(name = "courier_tracking_url")
    private String courierTrackingUrl;

    @Column(name = "digital_plan_id")
    private Long digitalPlanId;

    @Column(name = "donor_telco")
    private String donorTelco;

    @Column(name = "email")
    private String email;

    @Column(name = "lat_long")
    private String latLong;

    @Column(name = "msisdn")
    private String msisdn;

    @Column(name = "name")
    private String name;

    @Column(name = "postcode")
    private String postcode;

    @Column(name = "source")
    private String source;

    @Column(name = "state")
    private String state;

    @Column(name = "topup_amount")
    private Double topupAmount;

    @Column(name = "api_key")
    private String apiKey;

    @Column(name = "code")
    private String code;

    @Column(name = "message")
    private String message;

    @Column(name = "created_date")
    private ZonedDateTime createdDate;

    @Column(name = "modified_date")
    private ZonedDateTime modifiedDate;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "modified_by")
    private String modifiedBy;

    @ManyToOne
    @JsonIgnoreProperties("simOrders")
    private Agent agent;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public SimOrder agentCode(String agentCode) {
        this.agentCode = agentCode;
        return this;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getAddress() {
        return address;
    }

    public SimOrder address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public SimOrder city(String city) {
        this.city = city;
        return this;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getContact() {
        return contact;
    }

    public SimOrder contact(String contact) {
        this.contact = contact;
        return this;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getCountry() {
        return country;
    }

    public SimOrder country(String country) {
        this.country = country;
        return this;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCourierTrackingUrl() {
        return courierTrackingUrl;
    }

    public SimOrder courierTrackingUrl(String courierTrackingUrl) {
        this.courierTrackingUrl = courierTrackingUrl;
        return this;
    }

    public void setCourierTrackingUrl(String courierTrackingUrl) {
        this.courierTrackingUrl = courierTrackingUrl;
    }

    public Long getDigitalPlanId() {
        return digitalPlanId;
    }

    public SimOrder digitalPlanId(Long digitalPlanId) {
        this.digitalPlanId = digitalPlanId;
        return this;
    }

    public void setDigitalPlanId(Long digitalPlanId) {
        this.digitalPlanId = digitalPlanId;
    }

    public String getDonorTelco() {
        return donorTelco;
    }

    public SimOrder donorTelco(String donorTelco) {
        this.donorTelco = donorTelco;
        return this;
    }

    public void setDonorTelco(String donorTelco) {
        this.donorTelco = donorTelco;
    }

    public String getEmail() {
        return email;
    }

    public SimOrder email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLatLong() {
        return latLong;
    }

    public SimOrder latLong(String latLong) {
        this.latLong = latLong;
        return this;
    }

    public void setLatLong(String latLong) {
        this.latLong = latLong;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public SimOrder msisdn(String msisdn) {
        this.msisdn = msisdn;
        return this;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getName() {
        return name;
    }

    public SimOrder name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPostcode() {
        return postcode;
    }

    public SimOrder postcode(String postcode) {
        this.postcode = postcode;
        return this;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getSource() {
        return source;
    }

    public SimOrder source(String source) {
        this.source = source;
        return this;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getState() {
        return state;
    }

    public SimOrder state(String state) {
        this.state = state;
        return this;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Double getTopupAmount() {
        return topupAmount;
    }

    public SimOrder topupAmount(Double topupAmount) {
        this.topupAmount = topupAmount;
        return this;
    }

    public void setTopupAmount(Double topupAmount) {
        this.topupAmount = topupAmount;
    }

    public String getApiKey() {
        return apiKey;
    }

    public SimOrder apiKey(String apiKey) {
        this.apiKey = apiKey;
        return this;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getCode() {
        return code;
    }

    public SimOrder code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public SimOrder message(String message) {
        this.message = message;
        return this;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public SimOrder createdDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public ZonedDateTime getModifiedDate() {
        return modifiedDate;
    }

    public SimOrder modifiedDate(ZonedDateTime modifiedDate) {
        this.modifiedDate = modifiedDate;
        return this;
    }

    public void setModifiedDate(ZonedDateTime modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public SimOrder createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public SimOrder modifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
        return this;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Agent getAgent() {
        return agent;
    }

    public SimOrder agent(Agent agent) {
        this.agent = agent;
        return this;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SimOrder simOrder = (SimOrder) o;
        if (simOrder.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), simOrder.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SimOrder{" +
            "id=" + getId() +
            ", agentCode='" + getAgentCode() + "'" +
            ", address='" + getAddress() + "'" +
            ", city='" + getCity() + "'" +
            ", contact='" + getContact() + "'" +
            ", country='" + getCountry() + "'" +
            ", courierTrackingUrl='" + getCourierTrackingUrl() + "'" +
            ", digitalPlanId=" + getDigitalPlanId() +
            ", donorTelco='" + getDonorTelco() + "'" +
            ", email='" + getEmail() + "'" +
            ", latLong='" + getLatLong() + "'" +
            ", msisdn='" + getMsisdn() + "'" +
            ", name='" + getName() + "'" +
            ", postcode='" + getPostcode() + "'" +
            ", source='" + getSource() + "'" +
            ", state='" + getState() + "'" +
            ", topupAmount=" + getTopupAmount() +
            ", apiKey='" + getApiKey() + "'" +
            ", code='" + getCode() + "'" +
            ", message='" + getMessage() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", modifiedDate='" + getModifiedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", modifiedBy='" + getModifiedBy() + "'" +
            "}";
    }
}
