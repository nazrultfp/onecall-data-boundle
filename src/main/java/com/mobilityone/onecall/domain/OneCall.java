package com.mobilityone.onecall.domain;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A OneCall.
 */
@Entity
@Table(name = "one_call")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class OneCall implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "channel_id")
    private String channelId;

    @NotNull
    @Column(name = "merchant_id", nullable = false)
    private String merchantId;

    @Column(name = "merchant_order_no")
    private String merchantOrderNo;

    @Column(name = "msisdn")
    private String msisdn;

    @Column(name = "topup_deno")
    private Integer topupDeno;

    @Column(name = "transaction_id")
    private Long transactionId;

    @Column(name = "api_key")
    private String apiKey;

    @Column(name = "balance")
    private String balance;

    @Column(name = "code")
    private String code;

    @Column(name = "message")
    private String message;

    @Column(name = "created_date")
    private ZonedDateTime createdDate;

    @Column(name = "modified_date")
    private ZonedDateTime modifiedDate;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "modified_by")
    private String modifiedBy;

    @OneToMany(mappedBy = "oneCall", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH})
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<MsisdnValue> msisdnValues = new HashSet<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getChannelId() {
        return channelId;
    }

    public OneCall channelId(String channelId) {
        this.channelId = channelId;
        return this;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public OneCall merchantId(String merchantId) {
        this.merchantId = merchantId;
        return this;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantOrderNo() {
        return merchantOrderNo;
    }

    public OneCall merchantOrderNo(String merchantOrderNo) {
        this.merchantOrderNo = merchantOrderNo;
        return this;
    }

    public void setMerchantOrderNo(String merchantOrderNo) {
        this.merchantOrderNo = merchantOrderNo;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public OneCall msisdn(String msisdn) {
        this.msisdn = msisdn;
        return this;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public Integer getTopupDeno() {
        return topupDeno;
    }

    public OneCall topupDeno(Integer topupDeno) {
        this.topupDeno = topupDeno;
        return this;
    }

    public void setTopupDeno(Integer topupDeno) {
        this.topupDeno = topupDeno;
    }

    public Long getTransactionId() {
        return transactionId;
    }

    public OneCall transactionId(Long transactionId) {
        this.transactionId = transactionId;
        return this;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public String getApiKey() {
        return apiKey;
    }

    public OneCall apiKey(String apiKey) {
        this.apiKey = apiKey;
        return this;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getBalance() {
        return balance;
    }

    public OneCall balance(String balance) {
        this.balance = balance;
        return this;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getCode() {
        return code;
    }

    public OneCall code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public OneCall message(String message) {
        this.message = message;
        return this;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public OneCall createdDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public ZonedDateTime getModifiedDate() {
        return modifiedDate;
    }

    public OneCall modifiedDate(ZonedDateTime modifiedDate) {
        this.modifiedDate = modifiedDate;
        return this;
    }

    public void setModifiedDate(ZonedDateTime modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public OneCall createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public OneCall modifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
        return this;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Set<MsisdnValue> getMsisdnValues() {
        return msisdnValues;
    }

    public OneCall msisdnValues(Set<MsisdnValue> msisdnValues) {
        this.msisdnValues = msisdnValues;
        return this;
    }

    public OneCall addMsisdnValues(MsisdnValue msisdnValue) {
        this.msisdnValues.add(msisdnValue);
        msisdnValue.setOneCall(this);
        return this;
    }

    public OneCall removeMsisdnValues(MsisdnValue msisdnValue) {
        this.msisdnValues.remove(msisdnValue);
        msisdnValue.setOneCall(null);
        return this;
    }

    public void setMsisdnValues(Set<MsisdnValue> msisdnValues) {
        this.msisdnValues = msisdnValues;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        OneCall oneCall = (OneCall) o;
        if (oneCall.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), oneCall.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "OneCall{" +
            "id=" + getId() +
            ", channelId='" + getChannelId() + "'" +
            ", merchantId='" + getMerchantId() + "'" +
            ", merchantOrderNo='" + getMerchantOrderNo() + "'" +
            ", msisdn='" + getMsisdn() + "'" +
            ", topupDeno=" + getTopupDeno() +
            ", transactionId=" + getTransactionId() +
            ", apiKey='" + getApiKey() + "'" +
            ", balance='" + getBalance() + "'" +
            ", code='" + getCode() + "'" +
            ", message='" + getMessage() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", modifiedDate='" + getModifiedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", modifiedBy='" + getModifiedBy() + "'" +
            "}";
    }
}
