package com.mobilityone.onecall.service.dto;

public class SimCancelPortInReqDTO {
    private Long portInId;
    private String reason;

    public Long getPortInId() {
        return portInId;
    }

    public void setPortInId(Long portInId) {
        this.portInId = portInId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public String toString() {
        return "SimCancelPortInReqDTO{" +
            "portInId=" + portInId +
            ", reason='" + reason + '\'' +
            '}';
    }
}
