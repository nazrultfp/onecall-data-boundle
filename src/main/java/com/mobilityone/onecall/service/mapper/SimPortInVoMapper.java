package com.mobilityone.onecall.service.mapper;

import com.mobilityone.onecall.domain.*;
import com.mobilityone.onecall.service.dto.SimPortInVoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity SimPortInVo and its DTO SimPortInVoDTO.
 */
@Mapper(componentModel = "spring", uses = {SimPortInMapper.class})
public interface SimPortInVoMapper extends EntityMapper<SimPortInVoDTO, SimPortInVo> {

    @Mapping(source = "simPortIn.id", target = "simPortInId")
    SimPortInVoDTO toDto(SimPortInVo simPortInVo);

    @Mapping(source = "simPortInId", target = "simPortIn")
    SimPortInVo toEntity(SimPortInVoDTO simPortInVoDTO);

    default SimPortInVo fromId(Long id) {
        if (id == null) {
            return null;
        }
        SimPortInVo simPortInVo = new SimPortInVo();
        simPortInVo.setId(id);
        return simPortInVo;
    }
}
