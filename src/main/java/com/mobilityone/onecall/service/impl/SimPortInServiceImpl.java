package com.mobilityone.onecall.service.impl;

import com.mobilityone.onecall.config.Constants;
import com.mobilityone.onecall.domain.DealerLogin;
import com.mobilityone.onecall.domain.SimPortIn;
import com.mobilityone.onecall.domain.SimPortInVo;
import com.mobilityone.onecall.domain.enumeration.SimStatus;
import com.mobilityone.onecall.repository.DealerLoginRepository;
import com.mobilityone.onecall.repository.SimPortInRepository;
import com.mobilityone.onecall.security.SecurityUtils;
import com.mobilityone.onecall.service.SimPortInService;
import com.mobilityone.onecall.service.TuneTalkService;
import com.mobilityone.onecall.service.dto.*;
import com.mobilityone.onecall.service.mapper.SimPortInMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Service Implementation for managing SimPortIn.
 */
@Service
@Transactional
public class SimPortInServiceImpl implements SimPortInService {

    private final Logger log = LoggerFactory.getLogger(SimPortInServiceImpl.class);

    private final SimPortInRepository simPortInRepository;

    private final SimPortInMapper simPortInMapper;

    private final DealerLoginRepository dealerLoginRepository;

    private final TuneTalkService tuneTalkService;

    public SimPortInServiceImpl(SimPortInRepository simPortInRepository, SimPortInMapper simPortInMapper,
                                DealerLoginRepository dealerLoginRepository, TuneTalkService tuneTalkService) {
        this.simPortInRepository = simPortInRepository;
        this.simPortInMapper = simPortInMapper;
        this.dealerLoginRepository = dealerLoginRepository;
        this.tuneTalkService = tuneTalkService;
    }

    /**
     * Save a simPortIn.
     *
     * @param simPortInDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public SimPortInDTO save(SimPortInDTO simPortInDTO) {
        log.debug("Request to save SimPortIn : {}", simPortInDTO);
        SimPortIn simPortIn = simPortInMapper.toEntity(simPortInDTO);
        simPortIn = simPortInRepository.save(simPortIn);
        return simPortInMapper.toDto(simPortIn);
    }

    /**
     * Get all the simPortIns.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<SimPortInDTO> findAll(Pageable pageable) {
        log.debug("Request to get all SimPortIns");
        return simPortInRepository.findAll(pageable)
            .map(simPortInMapper::toDto);
    }


    /**
     * Get one simPortIn by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<SimPortInDTO> findOne(Long id) {
        log.debug("Request to get SimPortIn : {}", id);
        return simPortInRepository.findById(id)
            .map(simPortInMapper::toDto);
    }

    /**
     * Delete the simPortIn by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete SimPortIn : {}", id);
        simPortInRepository.deleteById(id);
    }

    @Override
    public SimPortInDTO save(SimPortInReqDTO simPortInReqDTO) {
        log.debug("Request to save SimPortIn by req : {}", simPortInReqDTO);

        SimPortIn simPortIn = new SimPortIn();

        simPortIn.setAgentCode(getCurrentUserAgentCode());
        simPortIn.setDonorTelco(simPortInReqDTO.getDonorTelco());
        simPortIn.setIdNumber(simPortInReqDTO.getIdNumber());

        for (SimPortInVosReqDTO sim : simPortInReqDTO.getSimPortInVos()) {
            SimPortInVo port = new SimPortInVo();
            port.setSimNumber(sim.getSimNumber());
            port.setMsisdn(sim.getMsisdn());
            port.setStatus(SimStatus.PENDING.toString());
            port.setSimPortIn(simPortIn);
            simPortIn.getSimPortInVos().add(port);
        }

        simPortIn = simPortInRepository.save(simPortIn);

        log.info("saved data : {}", simPortIn);

        return simPortInMapper.toDto(simPortIn);
    }

    private String getCurrentUserAgentCode() {
        String username = SecurityUtils.getCurrentUserLogin().orElse(Constants.ANONYMOUS_USER);
        Optional<DealerLogin> dealerLoginOptional = dealerLoginRepository.findByDealerUserNameAndTokenExpiredFalse(username);
        return dealerLoginOptional.map(DealerLogin::getRespLstAgentCode).orElse(null);
    }

    @Override
    public SimPortInDTO save(Long simPortInId, JSONObject jsonObject) {
        log.debug("Request to save response for SimPortIn by Id : {} and resp : {}", simPortInId, jsonObject);

        Optional<SimPortIn> simPortInOptional = simPortInRepository.findById(simPortInId);
        if (!simPortInOptional.isPresent()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "port in not found");
        }

        SimPortIn simPortIn = simPortInOptional.get();

        if (!jsonObject.isNull("apiKey"))
            simPortIn.setApiKey(jsonObject.getString("apiKey"));
        if (!jsonObject.isNull("code"))
            simPortIn.setCode(jsonObject.getString("code"));
        if (!jsonObject.isNull("message"))
            simPortIn.setMessage(jsonObject.getString("message"));

        if (!jsonObject.getString("code").equals("200")) {
            simPortIn.getSimPortInVos().forEach(simPortInVo -> {
                simPortInVo.setStatus(SimStatus.FAILED.toString());
                simPortInVo.setModifiedDate(ZonedDateTime.now());
                simPortInVo.setModifiedBy(SecurityUtils.getCurrentUserLogin().orElse(Constants.ANONYMOUS_USER));
            });
        }

        simPortIn.setModifiedDate(ZonedDateTime.now());
        simPortIn.setModifiedBy(SecurityUtils.getCurrentUserLogin().orElse(Constants.ANONYMOUS_USER));

        simPortIn = simPortInRepository.save(simPortIn);

        log.info("saved data : {}", simPortIn);

        return simPortInMapper.toDto(simPortIn);
    }

    @Override
    public Page<SimPortInDTO> findAllByAgentId(Pageable pageable, Long agentId) {
        log.debug("Request to get all SimPortIns");
        return simPortInRepository.findByAgentIdOrderByCreatedDateDesc(pageable, agentId)
            .map(simPortInMapper::toDto);
    }

    @Override
    public SimPortInDTO recallSimPortIn(Long portInId) {
        log.debug("Request to recall TT SimPortIn by PortIn Id : {}", portInId);

        Optional<SimPortIn> portInOptional = simPortInRepository.findById(portInId);
        if (!portInOptional.isPresent()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "port in not found");
        }

        JSONObject object = tuneTalkService.simPortIn(getSimPortInJson(portInOptional.get()));

        SimPortInDTO simPortInDTO = save(portInOptional.get().getId(), object);

        return simPortInDTO;
    }

    @Override
    public JSONObject getTotalPortInsByDate(Integer year, Integer month, Integer day) {
        log.info("Request to get total number of port-in sims for {}-{}-{}.", year, month, day);

        if (month < 1 || month > 12 || day < 1 || day > 31) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "date is not correct");
        }

        String date = String.valueOf(year);
        date = date.concat("-").concat(String.format("%02d", month));
        date = date.concat("-").concat(String.format("%02d", day));

        log.info("date format : {}", date);

        Integer count = simPortInRepository.countByDate(date);

        JSONObject object = new JSONObject();
        object.put("date", date);
        object.put("total", count);

        log.info("total number of port-in sims : {}", object);

        return object;
    }

    @Override
    public List<SimPortInDTO> getAllByAgentCode(String agentCode) {
        log.info("Request to get all port in sims by agent code : {}", agentCode);


        agentCode = String.format("%08d", Integer.valueOf(agentCode));
        log.info("String agentCode = {}", agentCode);

        List<SimPortIn> list = simPortInRepository.findByAgentCode(agentCode);

        log.info("total number of registered sims by {} is {}", agentCode, list.size());

        return simPortInMapper.toDto(list);
    }

    @Override
    public List<Map<String, Object>> getAllInRangeDate(String fromDate, String toDate) {
        log.info("Request to get all port in sims in range from {} to {}", fromDate, toDate);

        List<Map<String, Object>> list = simPortInRepository.findAllInRangeDate(fromDate, toDate);

        log.info("total number of port in sims in range from {} to {} is {}", fromDate, toDate, list.size());

        return list;
    }

    @Override
    public List<Map<String, Object>> getAgentDailyTotalSimRegInRange(String agentCode, String fromDate, String toDate) {
        log.info("Request to get agent {} daily total port in sims in range from {} to {}", agentCode, fromDate, toDate);

        agentCode = String.format("%08d", Integer.valueOf(agentCode));
        log.info("String agentCode = {}", agentCode);

        Optional<DealerLogin> dealerLoginOptional = dealerLoginRepository.findFirstByRespLstAgentCode(agentCode);
        if (!dealerLoginOptional.isPresent()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "dealer not found");
        }

        List<Map<String, Object>> list = simPortInRepository.getAgentDailyTotalSimRegInRange(agentCode, fromDate, toDate);

        log.info("total number of port in sims in range from {} to {} is {}", fromDate, toDate, list.size());

        return list;
    }

    private JSONObject getSimPortInJson(SimPortIn simPortIn) {

        JSONObject jsonObject = new JSONObject();

        if (simPortIn.getDonorTelco() != null) jsonObject.put("donorTelco", simPortIn.getDonorTelco());
        if (simPortIn.getIdNumber() != null) jsonObject.put("idNumber", simPortIn.getIdNumber());
        if (simPortIn.getSimPortInVos().size() > 0) {
            JSONArray array = new JSONArray();
            simPortIn.getSimPortInVos().forEach(simPortInVo -> {
                JSONObject object = new JSONObject();
                object.put("msisdn", simPortInVo.getMsisdn());
                object.put("simNumber", simPortInVo.getSimNumber());
                array.put(object);
            });
            jsonObject.put("simPortInVos", array);
        }

        return jsonObject;
    }
}
