package com.mobilityone.onecall.service.impl;

import com.mobilityone.onecall.config.Constants;
import com.mobilityone.onecall.domain.DealerLogin;
import com.mobilityone.onecall.repository.DealerLoginRepository;
import com.mobilityone.onecall.security.SecurityUtils;
import com.mobilityone.onecall.service.SimOrderService;
import com.mobilityone.onecall.domain.SimOrder;
import com.mobilityone.onecall.repository.SimOrderRepository;
import com.mobilityone.onecall.service.dto.SimOrderDTO;
import com.mobilityone.onecall.service.dto.SimOrderReqDTO;
import com.mobilityone.onecall.service.mapper.SimOrderMapper;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

/**
 * Service Implementation for managing SimOrder.
 */
@Service
@Transactional
public class SimOrderServiceImpl implements SimOrderService {

    private final Logger log = LoggerFactory.getLogger(SimOrderServiceImpl.class);

    private final SimOrderRepository simOrderRepository;

    private final SimOrderMapper simOrderMapper;

    private final DealerLoginRepository dealerLoginRepository;

    public SimOrderServiceImpl(SimOrderRepository simOrderRepository, SimOrderMapper simOrderMapper, DealerLoginRepository dealerLoginRepository) {
        this.simOrderRepository = simOrderRepository;
        this.simOrderMapper = simOrderMapper;
        this.dealerLoginRepository = dealerLoginRepository;
    }

    /**
     * Save a simOrder.
     *
     * @param simOrderDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public SimOrderDTO save(SimOrderDTO simOrderDTO) {
        log.debug("Request to save SimOrder : {}", simOrderDTO);
        SimOrder simOrder = simOrderMapper.toEntity(simOrderDTO);
        simOrder = simOrderRepository.save(simOrder);
        return simOrderMapper.toDto(simOrder);
    }

    /**
     * Get all the simOrders.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<SimOrderDTO> findAll(Pageable pageable) {
        log.debug("Request to get all SimOrders");
        return simOrderRepository.findAll(pageable)
            .map(simOrderMapper::toDto);
    }


    /**
     * Get one simOrder by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<SimOrderDTO> findOne(Long id) {
        log.debug("Request to get SimOrder : {}", id);
        return simOrderRepository.findById(id)
            .map(simOrderMapper::toDto);
    }

    /**
     * Delete the simOrder by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete SimOrder : {}", id);
        simOrderRepository.deleteById(id);
    }

    @Override
    public SimOrderDTO save(SimOrderReqDTO simOrderReqDTO) {
        log.debug("Request to save SimOrder by req : {}", simOrderReqDTO);

        SimOrderDTO orderDTO = new SimOrderDTO();
        orderDTO.setAgentCode(getCurrentUserAgentCode());
        orderDTO.setAddress(simOrderReqDTO.getAddress());
        orderDTO.setCity(simOrderReqDTO.getCity());
        orderDTO.setContact(simOrderReqDTO.getContact());
        orderDTO.setCountry(simOrderReqDTO.getCountry());
        orderDTO.setCourierTrackingUrl(simOrderReqDTO.getCourierTrackingUrl());
        orderDTO.setDigitalPlanId(simOrderReqDTO.getAgentId());
        orderDTO.setDonorTelco(simOrderReqDTO.getDonorTelco());
        orderDTO.setEmail(simOrderReqDTO.getEmail());
        orderDTO.setLatLong(simOrderReqDTO.getLatLong());
        orderDTO.setMsisdn(simOrderReqDTO.getMsisdn());
        orderDTO.setName(simOrderReqDTO.getName());
        orderDTO.setPostcode(simOrderReqDTO.getPostcode());
        orderDTO.setSource(simOrderReqDTO.getSource());
        orderDTO.setState(simOrderReqDTO.getState());
        orderDTO.setTopupAmount(simOrderReqDTO.getTopupAmount());
        if (simOrderReqDTO.getAgentId() != null)
            orderDTO.setAgentId(simOrderReqDTO.getAgentId());

        orderDTO = save(orderDTO);

        log.info("saved data : {}", orderDTO);

        return orderDTO;
    }

    private String getCurrentUserAgentCode() {
        String username = SecurityUtils.getCurrentUserLogin().orElse(Constants.ANONYMOUS_USER);
        Optional<DealerLogin> dealerLoginOptional = dealerLoginRepository.findByDealerUserNameAndTokenExpiredFalse(username);
        if (!dealerLoginOptional.isPresent()){
            return dealerLoginOptional.get().getRespLstAgentCode();
        }
        return null;
    }

    @Override
    public SimOrderDTO save(Long simOrderId, JSONObject jsonObject) {
        log.debug("Request to save response for SimOrder by Id : {} and resp : {}", simOrderId, jsonObject);

        Optional<SimOrder> simOrderOptional = simOrderRepository.findById(simOrderId);
        if (!simOrderOptional.isPresent()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "sim order not found");
        }

        SimOrder simOrder = simOrderOptional.get();

        if (!jsonObject.isNull("apiKey"))
            simOrder.setApiKey(jsonObject.getString("apiKey"));
        if (!jsonObject.isNull("code"))
            simOrder.setCode(jsonObject.getString("code"));
        if (!jsonObject.isNull("message"))
            simOrder.setMessage(jsonObject.getString("message"));

        simOrder = simOrderRepository.save(simOrder);

        log.info("saved data : {}", simOrder);

        return simOrderMapper.toDto(simOrder);
    }

    @Override
    public Page<SimOrderDTO> findAllByAgentId(Pageable pageable, Long agentId) {
        log.debug("Request to get all SimOrders by agentId : {}", agentId);
        return simOrderRepository.findByAgentIdOrderByCreatedDateDesc(pageable, agentId)
            .map(simOrderMapper::toDto);
    }
}
