package com.mobilityone.onecall.service.mapper;

import com.mobilityone.onecall.domain.SimPortIn;
import com.mobilityone.onecall.service.dto.SimPortInDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity SimPortIn and its DTO SimPortInDTO.
 */
@Mapper(componentModel = "spring", uses = {AgentMapper.class, SimPortInVoMapper.class})
public interface SimPortInMapper extends EntityMapper<SimPortInDTO, SimPortIn> {

    @Mapping(source = "agent.id", target = "agentId")
    SimPortInDTO toDto(SimPortIn simPortIn);

    //@Mapping(target = "simPortInVos", ignore = true)
    @Mapping(source = "agentId", target = "agent")
    SimPortIn toEntity(SimPortInDTO simPortInDTO);

    default SimPortIn fromId(Long id) {
        if (id == null) {
            return null;
        }
        SimPortIn simPortIn = new SimPortIn();
        simPortIn.setId(id);
        return simPortIn;
    }
}
