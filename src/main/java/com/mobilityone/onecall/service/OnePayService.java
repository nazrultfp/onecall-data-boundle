package com.mobilityone.onecall.service;

import com.mobilityone.onecall.service.dto.SimRegisterDTO;
import org.json.JSONObject;

public interface OnePayService {

    JSONObject registerCustomerByRegId(Long regId);

    JSONObject registerCustomer(SimRegisterDTO reqObject);
}
