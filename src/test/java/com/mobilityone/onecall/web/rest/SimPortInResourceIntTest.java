package com.mobilityone.onecall.web.rest;

import com.mobilityone.onecall.OneCallApp;

import com.mobilityone.onecall.domain.SimPortIn;
import com.mobilityone.onecall.repository.SimPortInRepository;
import com.mobilityone.onecall.service.SimPortInService;
import com.mobilityone.onecall.service.dto.SimPortInDTO;
import com.mobilityone.onecall.service.mapper.SimPortInMapper;
import com.mobilityone.onecall.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;


import static com.mobilityone.onecall.web.rest.TestUtil.sameInstant;
import static com.mobilityone.onecall.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SimPortInResource REST controller.
 *
 * @see SimPortInResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OneCallApp.class)
public class SimPortInResourceIntTest {

    private static final String DEFAULT_AGENT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_AGENT_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_DONOR_TELCO = "AAAAAAAAAA";
    private static final String UPDATED_DONOR_TELCO = "BBBBBBBBBB";

    private static final String DEFAULT_ID_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_ID_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_API_KEY = "AAAAAAAAAA";
    private static final String UPDATED_API_KEY = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_MESSAGE = "AAAAAAAAAA";
    private static final String UPDATED_MESSAGE = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_MODIFIED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_MODIFIED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_MODIFIED_BY = "BBBBBBBBBB";

    @Autowired
    private SimPortInRepository simPortInRepository;

    @Autowired
    private SimPortInMapper simPortInMapper;

    @Autowired
    private SimPortInService simPortInService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSimPortInMockMvc;

    private SimPortIn simPortIn;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SimPortInResource simPortInResource = new SimPortInResource(simPortInService);
        this.restSimPortInMockMvc = MockMvcBuilders.standaloneSetup(simPortInResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SimPortIn createEntity(EntityManager em) {
        SimPortIn simPortIn = new SimPortIn()
            .agentCode(DEFAULT_AGENT_CODE)
            .donorTelco(DEFAULT_DONOR_TELCO)
            .idNumber(DEFAULT_ID_NUMBER)
            .apiKey(DEFAULT_API_KEY)
            .code(DEFAULT_CODE)
            .message(DEFAULT_MESSAGE)
            .createdDate(DEFAULT_CREATED_DATE)
            .modifiedDate(DEFAULT_MODIFIED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .modifiedBy(DEFAULT_MODIFIED_BY);
        return simPortIn;
    }

    @Before
    public void initTest() {
        simPortIn = createEntity(em);
    }

    @Test
    @Transactional
    public void createSimPortIn() throws Exception {
        int databaseSizeBeforeCreate = simPortInRepository.findAll().size();

        // Create the SimPortIn
        SimPortInDTO simPortInDTO = simPortInMapper.toDto(simPortIn);
        restSimPortInMockMvc.perform(post("/api/sim-port-ins")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(simPortInDTO)))
            .andExpect(status().isCreated());

        // Validate the SimPortIn in the database
        List<SimPortIn> simPortInList = simPortInRepository.findAll();
        assertThat(simPortInList).hasSize(databaseSizeBeforeCreate + 1);
        SimPortIn testSimPortIn = simPortInList.get(simPortInList.size() - 1);
        assertThat(testSimPortIn.getAgentCode()).isEqualTo(DEFAULT_AGENT_CODE);
        assertThat(testSimPortIn.getDonorTelco()).isEqualTo(DEFAULT_DONOR_TELCO);
        assertThat(testSimPortIn.getIdNumber()).isEqualTo(DEFAULT_ID_NUMBER);
        assertThat(testSimPortIn.getApiKey()).isEqualTo(DEFAULT_API_KEY);
        assertThat(testSimPortIn.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testSimPortIn.getMessage()).isEqualTo(DEFAULT_MESSAGE);
        assertThat(testSimPortIn.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testSimPortIn.getModifiedDate()).isEqualTo(DEFAULT_MODIFIED_DATE);
        assertThat(testSimPortIn.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testSimPortIn.getModifiedBy()).isEqualTo(DEFAULT_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void createSimPortInWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = simPortInRepository.findAll().size();

        // Create the SimPortIn with an existing ID
        simPortIn.setId(1L);
        SimPortInDTO simPortInDTO = simPortInMapper.toDto(simPortIn);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSimPortInMockMvc.perform(post("/api/sim-port-ins")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(simPortInDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SimPortIn in the database
        List<SimPortIn> simPortInList = simPortInRepository.findAll();
        assertThat(simPortInList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSimPortIns() throws Exception {
        // Initialize the database
        simPortInRepository.saveAndFlush(simPortIn);

        // Get all the simPortInList
        restSimPortInMockMvc.perform(get("/api/sim-port-ins?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(simPortIn.getId().intValue())))
            .andExpect(jsonPath("$.[*].agentCode").value(hasItem(DEFAULT_AGENT_CODE.toString())))
            .andExpect(jsonPath("$.[*].donorTelco").value(hasItem(DEFAULT_DONOR_TELCO.toString())))
            .andExpect(jsonPath("$.[*].idNumber").value(hasItem(DEFAULT_ID_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].apiKey").value(hasItem(DEFAULT_API_KEY.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].message").value(hasItem(DEFAULT_MESSAGE.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(sameInstant(DEFAULT_CREATED_DATE))))
            .andExpect(jsonPath("$.[*].modifiedDate").value(hasItem(sameInstant(DEFAULT_MODIFIED_DATE))))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].modifiedBy").value(hasItem(DEFAULT_MODIFIED_BY.toString())));
    }
    
    @Test
    @Transactional
    public void getSimPortIn() throws Exception {
        // Initialize the database
        simPortInRepository.saveAndFlush(simPortIn);

        // Get the simPortIn
        restSimPortInMockMvc.perform(get("/api/sim-port-ins/{id}", simPortIn.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(simPortIn.getId().intValue()))
            .andExpect(jsonPath("$.agentCode").value(DEFAULT_AGENT_CODE.toString()))
            .andExpect(jsonPath("$.donorTelco").value(DEFAULT_DONOR_TELCO.toString()))
            .andExpect(jsonPath("$.idNumber").value(DEFAULT_ID_NUMBER.toString()))
            .andExpect(jsonPath("$.apiKey").value(DEFAULT_API_KEY.toString()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.message").value(DEFAULT_MESSAGE.toString()))
            .andExpect(jsonPath("$.createdDate").value(sameInstant(DEFAULT_CREATED_DATE)))
            .andExpect(jsonPath("$.modifiedDate").value(sameInstant(DEFAULT_MODIFIED_DATE)))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.modifiedBy").value(DEFAULT_MODIFIED_BY.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSimPortIn() throws Exception {
        // Get the simPortIn
        restSimPortInMockMvc.perform(get("/api/sim-port-ins/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSimPortIn() throws Exception {
        // Initialize the database
        simPortInRepository.saveAndFlush(simPortIn);

        int databaseSizeBeforeUpdate = simPortInRepository.findAll().size();

        // Update the simPortIn
        SimPortIn updatedSimPortIn = simPortInRepository.findById(simPortIn.getId()).get();
        // Disconnect from session so that the updates on updatedSimPortIn are not directly saved in db
        em.detach(updatedSimPortIn);
        updatedSimPortIn
            .agentCode(UPDATED_AGENT_CODE)
            .donorTelco(UPDATED_DONOR_TELCO)
            .idNumber(UPDATED_ID_NUMBER)
            .apiKey(UPDATED_API_KEY)
            .code(UPDATED_CODE)
            .message(UPDATED_MESSAGE)
            .createdDate(UPDATED_CREATED_DATE)
            .modifiedDate(UPDATED_MODIFIED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .modifiedBy(UPDATED_MODIFIED_BY);
        SimPortInDTO simPortInDTO = simPortInMapper.toDto(updatedSimPortIn);

        restSimPortInMockMvc.perform(put("/api/sim-port-ins")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(simPortInDTO)))
            .andExpect(status().isOk());

        // Validate the SimPortIn in the database
        List<SimPortIn> simPortInList = simPortInRepository.findAll();
        assertThat(simPortInList).hasSize(databaseSizeBeforeUpdate);
        SimPortIn testSimPortIn = simPortInList.get(simPortInList.size() - 1);
        assertThat(testSimPortIn.getAgentCode()).isEqualTo(UPDATED_AGENT_CODE);
        assertThat(testSimPortIn.getDonorTelco()).isEqualTo(UPDATED_DONOR_TELCO);
        assertThat(testSimPortIn.getIdNumber()).isEqualTo(UPDATED_ID_NUMBER);
        assertThat(testSimPortIn.getApiKey()).isEqualTo(UPDATED_API_KEY);
        assertThat(testSimPortIn.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testSimPortIn.getMessage()).isEqualTo(UPDATED_MESSAGE);
        assertThat(testSimPortIn.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testSimPortIn.getModifiedDate()).isEqualTo(UPDATED_MODIFIED_DATE);
        assertThat(testSimPortIn.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testSimPortIn.getModifiedBy()).isEqualTo(UPDATED_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void updateNonExistingSimPortIn() throws Exception {
        int databaseSizeBeforeUpdate = simPortInRepository.findAll().size();

        // Create the SimPortIn
        SimPortInDTO simPortInDTO = simPortInMapper.toDto(simPortIn);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSimPortInMockMvc.perform(put("/api/sim-port-ins")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(simPortInDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SimPortIn in the database
        List<SimPortIn> simPortInList = simPortInRepository.findAll();
        assertThat(simPortInList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSimPortIn() throws Exception {
        // Initialize the database
        simPortInRepository.saveAndFlush(simPortIn);

        int databaseSizeBeforeDelete = simPortInRepository.findAll().size();

        // Delete the simPortIn
        restSimPortInMockMvc.perform(delete("/api/sim-port-ins/{id}", simPortIn.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<SimPortIn> simPortInList = simPortInRepository.findAll();
        assertThat(simPortInList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SimPortIn.class);
        SimPortIn simPortIn1 = new SimPortIn();
        simPortIn1.setId(1L);
        SimPortIn simPortIn2 = new SimPortIn();
        simPortIn2.setId(simPortIn1.getId());
        assertThat(simPortIn1).isEqualTo(simPortIn2);
        simPortIn2.setId(2L);
        assertThat(simPortIn1).isNotEqualTo(simPortIn2);
        simPortIn1.setId(null);
        assertThat(simPortIn1).isNotEqualTo(simPortIn2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SimPortInDTO.class);
        SimPortInDTO simPortInDTO1 = new SimPortInDTO();
        simPortInDTO1.setId(1L);
        SimPortInDTO simPortInDTO2 = new SimPortInDTO();
        assertThat(simPortInDTO1).isNotEqualTo(simPortInDTO2);
        simPortInDTO2.setId(simPortInDTO1.getId());
        assertThat(simPortInDTO1).isEqualTo(simPortInDTO2);
        simPortInDTO2.setId(2L);
        assertThat(simPortInDTO1).isNotEqualTo(simPortInDTO2);
        simPortInDTO1.setId(null);
        assertThat(simPortInDTO1).isNotEqualTo(simPortInDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(simPortInMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(simPortInMapper.fromId(null)).isNull();
    }
}
