package com.mobilityone.onecall.service.mapper;

import com.mobilityone.onecall.domain.*;
import com.mobilityone.onecall.service.dto.DealerLoginDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity DealerLogin and its DTO DealerLoginDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface DealerLoginMapper extends EntityMapper<DealerLoginDTO, DealerLogin> {



    default DealerLogin fromId(Long id) {
        if (id == null) {
            return null;
        }
        DealerLogin dealerLogin = new DealerLogin();
        dealerLogin.setId(id);
        return dealerLogin;
    }
}
