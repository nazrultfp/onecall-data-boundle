package com.mobilityone.onecall.web.rest.util;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;

public class CommonUtils {

    private final static Logger log = LoggerFactory.getLogger(CommonUtils.class);

    public static Long get15DigitId(){
        Calendar cal = Calendar.getInstance();
        long time = cal.getTimeInMillis();

        String random = RandomStringUtils.randomNumeric(2);

        return Long.valueOf(time + random);
    }

    public static String getCode(int length) {
        return RandomStringUtils.randomAlphanumeric(length);
    }

    public static String getNumericCode(int length) {
        return RandomStringUtils.randomNumeric(length);
    }

    public static boolean isNullOrEmpty(String str) {
        return (str == null || str.trim().isEmpty());
    }


    public static boolean isStringOnlyAlphabet(String str)
    {
        return ((str != null)
            && (!str.equals(""))
            && (str.matches("^[a-zA-Z]*$")));
    }

    public static boolean isStringAlphanumeric(String str){
        return ((str != null)
            && (!str.equals(""))
            && (str.matches("^[a-zA-Z0-9]*$")));
    }


    public static boolean isEmailValid(String email){
        return ((email != null)
            && (!email.equals(""))
            && (email.matches("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")));
    }

    public static String generateSHA1(String originalString) throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance("SHA-1");
        byte[] bytes = digest.digest(originalString.getBytes(StandardCharsets.UTF_8));
        return Hex.encodeHexString(bytes);
    }

    public static String generateSHA512(String originalString) throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance("SHA-512");
        byte[] bytes = digest.digest(originalString.getBytes(StandardCharsets.UTF_8));
        return Hex.encodeHexString(bytes);
    }


    private static String toHexString(byte[] hash)
    {
        log.info("Hash in byte : {}", hash);
        // Convert byte array into signum representation
        BigInteger number = new BigInteger(1, hash);

        log.info("Hash in BigInt : {}", number);
        // Convert message digest into hex value
        StringBuilder hexString = new StringBuilder(number.toString(16));

        // Pad with leading zeros
        while (hexString.length() < 32)
        {
            hexString.insert(0, '0');
        }

        return hexString.toString();
    }

}
