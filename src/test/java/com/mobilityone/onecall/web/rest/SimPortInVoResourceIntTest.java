package com.mobilityone.onecall.web.rest;

import com.mobilityone.onecall.OneCallApp;

import com.mobilityone.onecall.domain.SimPortInVo;
import com.mobilityone.onecall.repository.SimPortInVoRepository;
import com.mobilityone.onecall.service.SimPortInVoService;
import com.mobilityone.onecall.service.dto.SimPortInVoDTO;
import com.mobilityone.onecall.service.mapper.SimPortInVoMapper;
import com.mobilityone.onecall.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;


import static com.mobilityone.onecall.web.rest.TestUtil.sameInstant;
import static com.mobilityone.onecall.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SimPortInVoResource REST controller.
 *
 * @see SimPortInVoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OneCallApp.class)
public class SimPortInVoResourceIntTest {

    private static final String DEFAULT_MSISDN = "AAAAAAAAAA";
    private static final String UPDATED_MSISDN = "BBBBBBBBBB";

    private static final String DEFAULT_SIM_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_SIM_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_MODIFIED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_MODIFIED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_MODIFIED_BY = "BBBBBBBBBB";

    @Autowired
    private SimPortInVoRepository simPortInVoRepository;

    @Autowired
    private SimPortInVoMapper simPortInVoMapper;

    @Autowired
    private SimPortInVoService simPortInVoService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSimPortInVoMockMvc;

    private SimPortInVo simPortInVo;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SimPortInVoResource simPortInVoResource = new SimPortInVoResource(simPortInVoService);
        this.restSimPortInVoMockMvc = MockMvcBuilders.standaloneSetup(simPortInVoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SimPortInVo createEntity(EntityManager em) {
        SimPortInVo simPortInVo = new SimPortInVo()
            .msisdn(DEFAULT_MSISDN)
            .simNumber(DEFAULT_SIM_NUMBER)
            .status(DEFAULT_STATUS)
            .createdDate(DEFAULT_CREATED_DATE)
            .modifiedDate(DEFAULT_MODIFIED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .modifiedBy(DEFAULT_MODIFIED_BY);
        return simPortInVo;
    }

    @Before
    public void initTest() {
        simPortInVo = createEntity(em);
    }

    @Test
    @Transactional
    public void createSimPortInVo() throws Exception {
        int databaseSizeBeforeCreate = simPortInVoRepository.findAll().size();

        // Create the SimPortInVo
        SimPortInVoDTO simPortInVoDTO = simPortInVoMapper.toDto(simPortInVo);
        restSimPortInVoMockMvc.perform(post("/api/sim-port-in-vos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(simPortInVoDTO)))
            .andExpect(status().isCreated());

        // Validate the SimPortInVo in the database
        List<SimPortInVo> simPortInVoList = simPortInVoRepository.findAll();
        assertThat(simPortInVoList).hasSize(databaseSizeBeforeCreate + 1);
        SimPortInVo testSimPortInVo = simPortInVoList.get(simPortInVoList.size() - 1);
        assertThat(testSimPortInVo.getMsisdn()).isEqualTo(DEFAULT_MSISDN);
        assertThat(testSimPortInVo.getSimNumber()).isEqualTo(DEFAULT_SIM_NUMBER);
        assertThat(testSimPortInVo.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testSimPortInVo.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testSimPortInVo.getModifiedDate()).isEqualTo(DEFAULT_MODIFIED_DATE);
        assertThat(testSimPortInVo.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testSimPortInVo.getModifiedBy()).isEqualTo(DEFAULT_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void createSimPortInVoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = simPortInVoRepository.findAll().size();

        // Create the SimPortInVo with an existing ID
        simPortInVo.setId(1L);
        SimPortInVoDTO simPortInVoDTO = simPortInVoMapper.toDto(simPortInVo);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSimPortInVoMockMvc.perform(post("/api/sim-port-in-vos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(simPortInVoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SimPortInVo in the database
        List<SimPortInVo> simPortInVoList = simPortInVoRepository.findAll();
        assertThat(simPortInVoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSimPortInVos() throws Exception {
        // Initialize the database
        simPortInVoRepository.saveAndFlush(simPortInVo);

        // Get all the simPortInVoList
        restSimPortInVoMockMvc.perform(get("/api/sim-port-in-vos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(simPortInVo.getId().intValue())))
            .andExpect(jsonPath("$.[*].msisdn").value(hasItem(DEFAULT_MSISDN.toString())))
            .andExpect(jsonPath("$.[*].simNumber").value(hasItem(DEFAULT_SIM_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(sameInstant(DEFAULT_CREATED_DATE))))
            .andExpect(jsonPath("$.[*].modifiedDate").value(hasItem(sameInstant(DEFAULT_MODIFIED_DATE))))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].modifiedBy").value(hasItem(DEFAULT_MODIFIED_BY.toString())));
    }
    
    @Test
    @Transactional
    public void getSimPortInVo() throws Exception {
        // Initialize the database
        simPortInVoRepository.saveAndFlush(simPortInVo);

        // Get the simPortInVo
        restSimPortInVoMockMvc.perform(get("/api/sim-port-in-vos/{id}", simPortInVo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(simPortInVo.getId().intValue()))
            .andExpect(jsonPath("$.msisdn").value(DEFAULT_MSISDN.toString()))
            .andExpect(jsonPath("$.simNumber").value(DEFAULT_SIM_NUMBER.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.createdDate").value(sameInstant(DEFAULT_CREATED_DATE)))
            .andExpect(jsonPath("$.modifiedDate").value(sameInstant(DEFAULT_MODIFIED_DATE)))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.modifiedBy").value(DEFAULT_MODIFIED_BY.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSimPortInVo() throws Exception {
        // Get the simPortInVo
        restSimPortInVoMockMvc.perform(get("/api/sim-port-in-vos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSimPortInVo() throws Exception {
        // Initialize the database
        simPortInVoRepository.saveAndFlush(simPortInVo);

        int databaseSizeBeforeUpdate = simPortInVoRepository.findAll().size();

        // Update the simPortInVo
        SimPortInVo updatedSimPortInVo = simPortInVoRepository.findById(simPortInVo.getId()).get();
        // Disconnect from session so that the updates on updatedSimPortInVo are not directly saved in db
        em.detach(updatedSimPortInVo);
        updatedSimPortInVo
            .msisdn(UPDATED_MSISDN)
            .simNumber(UPDATED_SIM_NUMBER)
            .status(UPDATED_STATUS)
            .createdDate(UPDATED_CREATED_DATE)
            .modifiedDate(UPDATED_MODIFIED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .modifiedBy(UPDATED_MODIFIED_BY);
        SimPortInVoDTO simPortInVoDTO = simPortInVoMapper.toDto(updatedSimPortInVo);

        restSimPortInVoMockMvc.perform(put("/api/sim-port-in-vos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(simPortInVoDTO)))
            .andExpect(status().isOk());

        // Validate the SimPortInVo in the database
        List<SimPortInVo> simPortInVoList = simPortInVoRepository.findAll();
        assertThat(simPortInVoList).hasSize(databaseSizeBeforeUpdate);
        SimPortInVo testSimPortInVo = simPortInVoList.get(simPortInVoList.size() - 1);
        assertThat(testSimPortInVo.getMsisdn()).isEqualTo(UPDATED_MSISDN);
        assertThat(testSimPortInVo.getSimNumber()).isEqualTo(UPDATED_SIM_NUMBER);
        assertThat(testSimPortInVo.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testSimPortInVo.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testSimPortInVo.getModifiedDate()).isEqualTo(UPDATED_MODIFIED_DATE);
        assertThat(testSimPortInVo.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testSimPortInVo.getModifiedBy()).isEqualTo(UPDATED_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void updateNonExistingSimPortInVo() throws Exception {
        int databaseSizeBeforeUpdate = simPortInVoRepository.findAll().size();

        // Create the SimPortInVo
        SimPortInVoDTO simPortInVoDTO = simPortInVoMapper.toDto(simPortInVo);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSimPortInVoMockMvc.perform(put("/api/sim-port-in-vos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(simPortInVoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SimPortInVo in the database
        List<SimPortInVo> simPortInVoList = simPortInVoRepository.findAll();
        assertThat(simPortInVoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSimPortInVo() throws Exception {
        // Initialize the database
        simPortInVoRepository.saveAndFlush(simPortInVo);

        int databaseSizeBeforeDelete = simPortInVoRepository.findAll().size();

        // Delete the simPortInVo
        restSimPortInVoMockMvc.perform(delete("/api/sim-port-in-vos/{id}", simPortInVo.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<SimPortInVo> simPortInVoList = simPortInVoRepository.findAll();
        assertThat(simPortInVoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SimPortInVo.class);
        SimPortInVo simPortInVo1 = new SimPortInVo();
        simPortInVo1.setId(1L);
        SimPortInVo simPortInVo2 = new SimPortInVo();
        simPortInVo2.setId(simPortInVo1.getId());
        assertThat(simPortInVo1).isEqualTo(simPortInVo2);
        simPortInVo2.setId(2L);
        assertThat(simPortInVo1).isNotEqualTo(simPortInVo2);
        simPortInVo1.setId(null);
        assertThat(simPortInVo1).isNotEqualTo(simPortInVo2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SimPortInVoDTO.class);
        SimPortInVoDTO simPortInVoDTO1 = new SimPortInVoDTO();
        simPortInVoDTO1.setId(1L);
        SimPortInVoDTO simPortInVoDTO2 = new SimPortInVoDTO();
        assertThat(simPortInVoDTO1).isNotEqualTo(simPortInVoDTO2);
        simPortInVoDTO2.setId(simPortInVoDTO1.getId());
        assertThat(simPortInVoDTO1).isEqualTo(simPortInVoDTO2);
        simPortInVoDTO2.setId(2L);
        assertThat(simPortInVoDTO1).isNotEqualTo(simPortInVoDTO2);
        simPortInVoDTO1.setId(null);
        assertThat(simPortInVoDTO1).isNotEqualTo(simPortInVoDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(simPortInVoMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(simPortInVoMapper.fromId(null)).isNull();
    }
}
