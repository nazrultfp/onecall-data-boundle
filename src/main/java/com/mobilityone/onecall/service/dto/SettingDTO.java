package com.mobilityone.onecall.service.dto;
import com.mobilityone.onecall.config.Constants;
import com.mobilityone.onecall.security.SecurityUtils;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Setting entity.
 */
public class SettingDTO implements Serializable {

    private Long id;

    private Integer versionAndroid;

    private String versionIos;

    private Boolean isAndroidCritical = Boolean.FALSE;

    private Boolean isIosCritical = Boolean.FALSE;

    private String tncUrl;

    private Integer telcoVersion;

    private ZonedDateTime createdDate = ZonedDateTime.now();

    private ZonedDateTime modifiedDate = ZonedDateTime.now();

    private String createdBy = SecurityUtils.getCurrentUserLogin().orElse(Constants.ANONYMOUS_USER);

    private String modifiedBy = SecurityUtils.getCurrentUserLogin().orElse(Constants.ANONYMOUS_USER);


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersionAndroid() {
        return versionAndroid;
    }

    public void setVersionAndroid(Integer versionAndroid) {
        this.versionAndroid = versionAndroid;
    }

    public String getVersionIos() {
        return versionIos;
    }

    public void setVersionIos(String versionIos) {
        this.versionIos = versionIos;
    }

    public Boolean isIsAndroidCritical() {
        return isAndroidCritical;
    }

    public void setIsAndroidCritical(Boolean isAndroidCritical) {
        this.isAndroidCritical = isAndroidCritical;
    }

    public Boolean isIsIosCritical() {
        return isIosCritical;
    }

    public void setIsIosCritical(Boolean isIosCritical) {
        this.isIosCritical = isIosCritical;
    }

    public String getTncUrl() {
        return tncUrl;
    }

    public void setTncUrl(String tncUrl) {
        this.tncUrl = tncUrl;
    }

    public Integer getTelcoVersion() {
        return telcoVersion;
    }

    public void setTelcoVersion(Integer telcoVersion) {
        this.telcoVersion = telcoVersion;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public ZonedDateTime getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(ZonedDateTime modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SettingDTO settingDTO = (SettingDTO) o;
        if (settingDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), settingDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SettingDTO{" +
            "id=" + getId() +
            ", versionAndroid=" + getVersionAndroid() +
            ", versionIos='" + getVersionIos() + "'" +
            ", isAndroidCritical='" + isIsAndroidCritical() + "'" +
            ", isIosCritical='" + isIsIosCritical() + "'" +
            ", tncUrl='" + getTncUrl() + "'" +
            ", telcoVersion=" + getTelcoVersion() +
            ", createdDate='" + getCreatedDate() + "'" +
            ", modifiedDate='" + getModifiedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", modifiedBy='" + getModifiedBy() + "'" +
            "}";
    }
}
