package com.mobilityone.onecall.repository;

import com.mobilityone.onecall.domain.OneCall;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;


/**
 * Spring Data  repository for the OneCall entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OneCallRepository extends JpaRepository<OneCall, Long> {

    Optional<OneCall> findByMerchantOrderNo(String merchantOrderNo);

    Optional<OneCall> findTopByMerchantIdAndMerchantOrderNoOrderByCreatedDateDesc(String merchantId, String merchantOrderNo);

    Optional<OneCall> findByMerchantIdAndMerchantOrderNoAndCode(String merchantId, String merchantOrderNo, String code);

    Page<OneCall> findByMerchantIdAndCode(Pageable pageable, String merchantId, String code);


    @Query(value = "select sum(oc.topup_deno) as totalPrice , date_format(CONVERT_TZ(oc.created_date , '+00:00', '+08:00'), '%Y-%m-%d') as createDate from one_call oc  " +
        " where oc.code = '200' and oc.created_by like :client and date_format(CONVERT_TZ(oc.created_date , '+00:00', '+08:00'), '%Y-%m-%d') between :fromDate and :toDate  " +
        " group by createDate " , nativeQuery = true) //mysql
//    @Query(value = "select sum(oc.topup_deno) as totalPrice , FORMATDATETIME(DATEADD(HOUR, 8, oc.created_date) , 'yyyy-MM-dd') as createDate from one_call oc  " +
//        " where oc.code = '200' and oc.created_by like :client and FORMATDATETIME(DATEADD(HOUR, 8, oc.created_date) , 'yyyy-MM-dd') between :fromDate and :toDate  " +
//        " group by FORMATDATETIME(DATEADD(HOUR, 8, oc.created_date) , 'yyyy-MM-dd') ", nativeQuery = true)
    List<Map<String, Object>> getDailyTotalTopupInRangeForClient(@Param("client")String client, @Param("fromDate") String fromDate, @Param("toDate") String toDate);
}
