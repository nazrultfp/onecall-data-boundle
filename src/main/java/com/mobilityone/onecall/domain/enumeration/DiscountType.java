package com.mobilityone.onecall.domain.enumeration;

/**
 * The DiscountType enumeration.
 */
public enum DiscountType {
    AMOUNT, PERCENT
}
