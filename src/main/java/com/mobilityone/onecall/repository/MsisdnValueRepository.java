package com.mobilityone.onecall.repository;

import com.mobilityone.onecall.domain.MsisdnValue;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the MsisdnValue entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MsisdnValueRepository extends JpaRepository<MsisdnValue, Long> {

}
