package com.mobilityone.onecall.service.mapper;

import com.mobilityone.onecall.domain.OneCall;
import com.mobilityone.onecall.service.dto.OneCallDTO;
import com.mobilityone.onecall.service.dto.TopupDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {})
public interface TopupMapper extends EntityMapper<TopupDTO, OneCallDTO> {

    @Mapping(target = "msisdnValues", ignore = true)
    OneCallDTO toEntity(TopupDTO topupDTO);

    default OneCallDTO fromId(Long id) {
        if (id == null) {
            return null;
        }
        OneCallDTO oneCallDTO = new OneCallDTO();
        oneCallDTO.setId(id);
        return oneCallDTO;
    }
}
