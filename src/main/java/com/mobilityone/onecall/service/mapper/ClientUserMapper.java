package com.mobilityone.onecall.service.mapper;

import com.mobilityone.onecall.domain.*;
import com.mobilityone.onecall.service.dto.ClientUserDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ClientUser and its DTO ClientUserDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ClientUserMapper extends EntityMapper<ClientUserDTO, ClientUser> {



    default ClientUser fromId(Long id) {
        if (id == null) {
            return null;
        }
        ClientUser clientUser = new ClientUser();
        clientUser.setId(id);
        return clientUser;
    }
}
