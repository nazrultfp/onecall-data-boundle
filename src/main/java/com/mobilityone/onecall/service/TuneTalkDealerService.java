package com.mobilityone.onecall.service;

import com.mobilityone.onecall.service.dto.*;
import org.json.JSONObject;

public interface TuneTalkDealerService {

    JSONObject simHistory(HistoryReqDTO historyReqDTO);

    JSONObject portInStatus(String msisdn);

    JSONObject simCancelPortIn(SimCancelPortInReqDTO simCancelPortInReqDTO);

    TuneTalkRespDTO simResubmitPortIn(Long portInId);

    TuneTalkRespDTO performSubscription(DealerSubscriptionReqDTO dealerSubscriptionReqDTO);

    JSONObject getSubscriptionStatusByTransactionId(String transactionId);

    JSONObject getTelcoList();

    TuneTalkTopupRespDTO performTopup(DealerTopupReqDTO dealerTopupReqDTO);

    JSONObject getTopupValues(String msisdn);
}
