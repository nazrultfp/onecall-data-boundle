package com.mobilityone.onecall.service.dto;

import com.mobilityone.onecall.config.Constants;
import com.mobilityone.onecall.domain.SimPortInVo;
import com.mobilityone.onecall.security.SecurityUtils;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A DTO for the SimPortIn entity.
 */
public class SimPortInDTO implements Serializable {

    private Long id;

    private String agentCode;

    private String donorTelco;

    private String idNumber;

    private String apiKey;

    private String code;

    private String message;

    private ZonedDateTime createdDate = ZonedDateTime.now();

    private ZonedDateTime modifiedDate = ZonedDateTime.now();

    private String createdBy = SecurityUtils.getCurrentUserLogin().orElse(Constants.ANONYMOUS_USER);

    private String modifiedBy = SecurityUtils.getCurrentUserLogin().orElse(Constants.ANONYMOUS_USER);

    private Set<SimPortInVoDTO> simPortInVos = new HashSet<>();

    private Long agentId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getDonorTelco() {
        return donorTelco;
    }

    public void setDonorTelco(String donorTelco) {
        this.donorTelco = donorTelco;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public ZonedDateTime getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(ZonedDateTime modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Set<SimPortInVoDTO> getSimPortInVos() {
        return simPortInVos;
    }

    public void setSimPortInVos(Set<SimPortInVoDTO> simPortInVos) {
        this.simPortInVos = simPortInVos;
    }

    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SimPortInDTO simPortInDTO = (SimPortInDTO) o;
        if (simPortInDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), simPortInDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SimPortInDTO{" +
            "id=" + getId() +
            ", agentCode='" + getAgentCode() + "'" +
            ", donorTelco='" + getDonorTelco() + "'" +
            ", idNumber='" + getIdNumber() + "'" +
            ", apiKey='" + getApiKey() + "'" +
            ", code='" + getCode() + "'" +
            ", message='" + getMessage() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", modifiedDate='" + getModifiedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", modifiedBy='" + getModifiedBy() + "'" +
            ", simPortInVos=" + getSimPortInVos() +
            ", agent=" + getAgentId() +
            "}";
    }
}
