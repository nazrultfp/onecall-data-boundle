package com.mobilityone.onecall.service;

import com.mobilityone.onecall.service.dto.BigShotConvertReqDTO;
import com.mobilityone.onecall.service.dto.BigShotDTO;

import com.mobilityone.onecall.service.dto.TuneTalkRespDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing BigShot.
 */
public interface BigShotService {

    /**
     * Save a bigShot.
     *
     * @param bigShotDTO the entity to save
     * @return the persisted entity
     */
    BigShotDTO save(BigShotDTO bigShotDTO);

    /**
     * Get all the bigShots.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<BigShotDTO> findAll(Pageable pageable);


    /**
     * Get the "id" bigShot.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<BigShotDTO> findOne(Long id);

    /**
     * Delete the "id" bigShot.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    BigShotDTO save(BigShotConvertReqDTO bigShotConvertReqDTO, TuneTalkRespDTO tuneTalkRespDTO);
}
