package com.mobilityone.onecall.service;

import com.mobilityone.onecall.service.dto.TopupTransactionDTO;
import com.mobilityone.onecall.service.dto.TopupTransactionReportDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing TopupTransaction.
 */
public interface TopupTransactionService {

    /**
     * Save a topupTransaction.
     *
     * @param topupTransactionDTO the entity to save
     * @return the persisted entity
     */
    TopupTransactionDTO save(TopupTransactionDTO topupTransactionDTO);

    /**
     * Get all the topupTransactions.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<TopupTransactionDTO> findAll(Pageable pageable);


    /**
     * Get the "id" topupTransaction.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<TopupTransactionDTO> findOne(Long id);

    /**
     * Delete the "id" topupTransaction.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    Page<TopupTransactionReportDTO> getTopupReport(Pageable pageable);
}
