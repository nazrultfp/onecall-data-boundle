package com.mobilityone.onecall.service.mapper;

import com.mobilityone.onecall.domain.PlanTransaction;
import com.mobilityone.onecall.service.dto.PlanTransactionDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity PlanTransaction and its DTO PlanTransactionDTO.
 */
@Mapper(componentModel = "spring", uses = {MerchantMapper.class, PlanMapper.class})
public interface PlanTransactionMapper extends EntityMapper<PlanTransactionDTO, PlanTransaction> {

    @Mapping(source = "merchant.id", target = "merchantId")
    @Mapping(source = "plan.id", target = "planId")
    PlanTransactionDTO toDto(PlanTransaction planTransaction);

    @Mapping(source = "merchantId", target = "merchant")
    @Mapping(source = "planId", target = "plan")
    PlanTransaction toEntity(PlanTransactionDTO planTransactionDTO);

    default PlanTransaction fromId(Long id) {
        if (id == null) {
            return null;
        }
        PlanTransaction planTransaction = new PlanTransaction();
        planTransaction.setId(id);
        return planTransaction;
    }
}
