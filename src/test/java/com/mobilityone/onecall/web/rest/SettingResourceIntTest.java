package com.mobilityone.onecall.web.rest;

import com.mobilityone.onecall.OneCallApp;

import com.mobilityone.onecall.domain.Setting;
import com.mobilityone.onecall.repository.SettingRepository;
import com.mobilityone.onecall.service.SettingService;
import com.mobilityone.onecall.service.dto.SettingDTO;
import com.mobilityone.onecall.service.mapper.SettingMapper;
import com.mobilityone.onecall.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;


import static com.mobilityone.onecall.web.rest.TestUtil.sameInstant;
import static com.mobilityone.onecall.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SettingResource REST controller.
 *
 * @see SettingResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OneCallApp.class)
public class SettingResourceIntTest {

    private static final Integer DEFAULT_VERSION_ANDROID = 1;
    private static final Integer UPDATED_VERSION_ANDROID = 2;

    private static final String DEFAULT_VERSION_IOS = "AAAAAAAAAA";
    private static final String UPDATED_VERSION_IOS = "BBBBBBBBBB";

    private static final Boolean DEFAULT_IS_ANDROID_CRITICAL = false;
    private static final Boolean UPDATED_IS_ANDROID_CRITICAL = true;

    private static final Boolean DEFAULT_IS_IOS_CRITICAL = false;
    private static final Boolean UPDATED_IS_IOS_CRITICAL = true;

    private static final String DEFAULT_TNC_URL = "AAAAAAAAAA";
    private static final String UPDATED_TNC_URL = "BBBBBBBBBB";

    private static final Integer DEFAULT_TELCO_VERSION = 1;
    private static final Integer UPDATED_TELCO_VERSION = 2;

    private static final ZonedDateTime DEFAULT_CREATED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_MODIFIED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_MODIFIED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_MODIFIED_BY = "BBBBBBBBBB";

    @Autowired
    private SettingRepository settingRepository;

    @Autowired
    private SettingMapper settingMapper;

    @Autowired
    private SettingService settingService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSettingMockMvc;

    private Setting setting;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SettingResource settingResource = new SettingResource(settingService);
        this.restSettingMockMvc = MockMvcBuilders.standaloneSetup(settingResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Setting createEntity(EntityManager em) {
        Setting setting = new Setting()
            .versionAndroid(DEFAULT_VERSION_ANDROID)
            .versionIos(DEFAULT_VERSION_IOS)
            .isAndroidCritical(DEFAULT_IS_ANDROID_CRITICAL)
            .isIosCritical(DEFAULT_IS_IOS_CRITICAL)
            .tncUrl(DEFAULT_TNC_URL)
            .telcoVersion(DEFAULT_TELCO_VERSION)
            .createdDate(DEFAULT_CREATED_DATE)
            .modifiedDate(DEFAULT_MODIFIED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .modifiedBy(DEFAULT_MODIFIED_BY);
        return setting;
    }

    @Before
    public void initTest() {
        setting = createEntity(em);
    }

    @Test
    @Transactional
    public void createSetting() throws Exception {
        int databaseSizeBeforeCreate = settingRepository.findAll().size();

        // Create the Setting
        SettingDTO settingDTO = settingMapper.toDto(setting);
        restSettingMockMvc.perform(post("/api/settings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(settingDTO)))
            .andExpect(status().isCreated());

        // Validate the Setting in the database
        List<Setting> settingList = settingRepository.findAll();
        assertThat(settingList).hasSize(databaseSizeBeforeCreate + 1);
        Setting testSetting = settingList.get(settingList.size() - 1);
        assertThat(testSetting.getVersionAndroid()).isEqualTo(DEFAULT_VERSION_ANDROID);
        assertThat(testSetting.getVersionIos()).isEqualTo(DEFAULT_VERSION_IOS);
        assertThat(testSetting.isIsAndroidCritical()).isEqualTo(DEFAULT_IS_ANDROID_CRITICAL);
        assertThat(testSetting.isIsIosCritical()).isEqualTo(DEFAULT_IS_IOS_CRITICAL);
        assertThat(testSetting.getTncUrl()).isEqualTo(DEFAULT_TNC_URL);
        assertThat(testSetting.getTelcoVersion()).isEqualTo(DEFAULT_TELCO_VERSION);
        assertThat(testSetting.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testSetting.getModifiedDate()).isEqualTo(DEFAULT_MODIFIED_DATE);
        assertThat(testSetting.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testSetting.getModifiedBy()).isEqualTo(DEFAULT_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void createSettingWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = settingRepository.findAll().size();

        // Create the Setting with an existing ID
        setting.setId(1L);
        SettingDTO settingDTO = settingMapper.toDto(setting);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSettingMockMvc.perform(post("/api/settings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(settingDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Setting in the database
        List<Setting> settingList = settingRepository.findAll();
        assertThat(settingList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSettings() throws Exception {
        // Initialize the database
        settingRepository.saveAndFlush(setting);

        // Get all the settingList
        restSettingMockMvc.perform(get("/api/settings?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(setting.getId().intValue())))
            .andExpect(jsonPath("$.[*].versionAndroid").value(hasItem(DEFAULT_VERSION_ANDROID)))
            .andExpect(jsonPath("$.[*].versionIos").value(hasItem(DEFAULT_VERSION_IOS.toString())))
            .andExpect(jsonPath("$.[*].isAndroidCritical").value(hasItem(DEFAULT_IS_ANDROID_CRITICAL.booleanValue())))
            .andExpect(jsonPath("$.[*].isIosCritical").value(hasItem(DEFAULT_IS_IOS_CRITICAL.booleanValue())))
            .andExpect(jsonPath("$.[*].tncUrl").value(hasItem(DEFAULT_TNC_URL.toString())))
            .andExpect(jsonPath("$.[*].telcoVersion").value(hasItem(DEFAULT_TELCO_VERSION)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(sameInstant(DEFAULT_CREATED_DATE))))
            .andExpect(jsonPath("$.[*].modifiedDate").value(hasItem(sameInstant(DEFAULT_MODIFIED_DATE))))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].modifiedBy").value(hasItem(DEFAULT_MODIFIED_BY.toString())));
    }
    
    @Test
    @Transactional
    public void getSetting() throws Exception {
        // Initialize the database
        settingRepository.saveAndFlush(setting);

        // Get the setting
        restSettingMockMvc.perform(get("/api/settings/{id}", setting.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(setting.getId().intValue()))
            .andExpect(jsonPath("$.versionAndroid").value(DEFAULT_VERSION_ANDROID))
            .andExpect(jsonPath("$.versionIos").value(DEFAULT_VERSION_IOS.toString()))
            .andExpect(jsonPath("$.isAndroidCritical").value(DEFAULT_IS_ANDROID_CRITICAL.booleanValue()))
            .andExpect(jsonPath("$.isIosCritical").value(DEFAULT_IS_IOS_CRITICAL.booleanValue()))
            .andExpect(jsonPath("$.tncUrl").value(DEFAULT_TNC_URL.toString()))
            .andExpect(jsonPath("$.telcoVersion").value(DEFAULT_TELCO_VERSION))
            .andExpect(jsonPath("$.createdDate").value(sameInstant(DEFAULT_CREATED_DATE)))
            .andExpect(jsonPath("$.modifiedDate").value(sameInstant(DEFAULT_MODIFIED_DATE)))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.modifiedBy").value(DEFAULT_MODIFIED_BY.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSetting() throws Exception {
        // Get the setting
        restSettingMockMvc.perform(get("/api/settings/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSetting() throws Exception {
        // Initialize the database
        settingRepository.saveAndFlush(setting);

        int databaseSizeBeforeUpdate = settingRepository.findAll().size();

        // Update the setting
        Setting updatedSetting = settingRepository.findById(setting.getId()).get();
        // Disconnect from session so that the updates on updatedSetting are not directly saved in db
        em.detach(updatedSetting);
        updatedSetting
            .versionAndroid(UPDATED_VERSION_ANDROID)
            .versionIos(UPDATED_VERSION_IOS)
            .isAndroidCritical(UPDATED_IS_ANDROID_CRITICAL)
            .isIosCritical(UPDATED_IS_IOS_CRITICAL)
            .tncUrl(UPDATED_TNC_URL)
            .telcoVersion(UPDATED_TELCO_VERSION)
            .createdDate(UPDATED_CREATED_DATE)
            .modifiedDate(UPDATED_MODIFIED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .modifiedBy(UPDATED_MODIFIED_BY);
        SettingDTO settingDTO = settingMapper.toDto(updatedSetting);

        restSettingMockMvc.perform(put("/api/settings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(settingDTO)))
            .andExpect(status().isOk());

        // Validate the Setting in the database
        List<Setting> settingList = settingRepository.findAll();
        assertThat(settingList).hasSize(databaseSizeBeforeUpdate);
        Setting testSetting = settingList.get(settingList.size() - 1);
        assertThat(testSetting.getVersionAndroid()).isEqualTo(UPDATED_VERSION_ANDROID);
        assertThat(testSetting.getVersionIos()).isEqualTo(UPDATED_VERSION_IOS);
        assertThat(testSetting.isIsAndroidCritical()).isEqualTo(UPDATED_IS_ANDROID_CRITICAL);
        assertThat(testSetting.isIsIosCritical()).isEqualTo(UPDATED_IS_IOS_CRITICAL);
        assertThat(testSetting.getTncUrl()).isEqualTo(UPDATED_TNC_URL);
        assertThat(testSetting.getTelcoVersion()).isEqualTo(UPDATED_TELCO_VERSION);
        assertThat(testSetting.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testSetting.getModifiedDate()).isEqualTo(UPDATED_MODIFIED_DATE);
        assertThat(testSetting.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testSetting.getModifiedBy()).isEqualTo(UPDATED_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void updateNonExistingSetting() throws Exception {
        int databaseSizeBeforeUpdate = settingRepository.findAll().size();

        // Create the Setting
        SettingDTO settingDTO = settingMapper.toDto(setting);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSettingMockMvc.perform(put("/api/settings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(settingDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Setting in the database
        List<Setting> settingList = settingRepository.findAll();
        assertThat(settingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSetting() throws Exception {
        // Initialize the database
        settingRepository.saveAndFlush(setting);

        int databaseSizeBeforeDelete = settingRepository.findAll().size();

        // Delete the setting
        restSettingMockMvc.perform(delete("/api/settings/{id}", setting.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Setting> settingList = settingRepository.findAll();
        assertThat(settingList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Setting.class);
        Setting setting1 = new Setting();
        setting1.setId(1L);
        Setting setting2 = new Setting();
        setting2.setId(setting1.getId());
        assertThat(setting1).isEqualTo(setting2);
        setting2.setId(2L);
        assertThat(setting1).isNotEqualTo(setting2);
        setting1.setId(null);
        assertThat(setting1).isNotEqualTo(setting2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SettingDTO.class);
        SettingDTO settingDTO1 = new SettingDTO();
        settingDTO1.setId(1L);
        SettingDTO settingDTO2 = new SettingDTO();
        assertThat(settingDTO1).isNotEqualTo(settingDTO2);
        settingDTO2.setId(settingDTO1.getId());
        assertThat(settingDTO1).isEqualTo(settingDTO2);
        settingDTO2.setId(2L);
        assertThat(settingDTO1).isNotEqualTo(settingDTO2);
        settingDTO1.setId(null);
        assertThat(settingDTO1).isNotEqualTo(settingDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(settingMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(settingMapper.fromId(null)).isNull();
    }
}
