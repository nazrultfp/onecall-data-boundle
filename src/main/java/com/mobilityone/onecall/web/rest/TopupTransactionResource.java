package com.mobilityone.onecall.web.rest;

import com.mobilityone.onecall.service.TopupTransactionService;
import com.mobilityone.onecall.service.dto.TopupTransactionDTO;
import com.mobilityone.onecall.service.dto.TopupTransactionReportDTO;
import com.mobilityone.onecall.web.rest.errors.BadRequestAlertException;
import com.mobilityone.onecall.web.rest.util.HeaderUtil;
import com.mobilityone.onecall.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing TopupTransaction.
 */
@RestController
@RequestMapping("/api")
public class TopupTransactionResource {

    private final Logger log = LoggerFactory.getLogger(TopupTransactionResource.class);

    private static final String ENTITY_NAME = "oneCallTopupTransaction";

    private final TopupTransactionService topupTransactionService;

    public TopupTransactionResource(TopupTransactionService topupTransactionService) {
        this.topupTransactionService = topupTransactionService;
    }

    /**
     * POST  /topup-transactions : Create a new topupTransaction.
     *
     * @param topupTransactionDTO the topupTransactionDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new topupTransactionDTO, or with status 400 (Bad Request) if the topupTransaction has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    //@PostMapping("/topup-transactions")
    public ResponseEntity<TopupTransactionDTO> createTopupTransaction(@RequestBody TopupTransactionDTO topupTransactionDTO) throws URISyntaxException {
        log.debug("REST request to save TopupTransaction : {}", topupTransactionDTO);
        if (topupTransactionDTO.getId() != null) {
            throw new BadRequestAlertException("A new topupTransaction cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TopupTransactionDTO result = topupTransactionService.save(topupTransactionDTO);
        return ResponseEntity.created(new URI("/api/topup-transactions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /topup-transactions : Updates an existing topupTransaction.
     *
     * @param topupTransactionDTO the topupTransactionDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated topupTransactionDTO,
     * or with status 400 (Bad Request) if the topupTransactionDTO is not valid,
     * or with status 500 (Internal Server Error) if the topupTransactionDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    //@PutMapping("/topup-transactions")
    public ResponseEntity<TopupTransactionDTO> updateTopupTransaction(@RequestBody TopupTransactionDTO topupTransactionDTO) throws URISyntaxException {
        log.debug("REST request to update TopupTransaction : {}", topupTransactionDTO);
        if (topupTransactionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TopupTransactionDTO result = topupTransactionService.save(topupTransactionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, topupTransactionDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /topup-transactions : get all the topupTransactions.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of topupTransactions in body
     */
    @GetMapping("/topup-transactions")
    public ResponseEntity<List<TopupTransactionDTO>> getAllTopupTransactions(Pageable pageable) {
        log.debug("REST request to get a page of TopupTransactions");
        Page<TopupTransactionDTO> page = topupTransactionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/topup-transactions");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /topup-transactions/:id : get the "id" topupTransaction.
     *
     * @param id the id of the topupTransactionDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the topupTransactionDTO, or with status 404 (Not Found)
     */
    @GetMapping("/topup-transactions/{id}")
    public ResponseEntity<TopupTransactionDTO> getTopupTransaction(@PathVariable Long id) {
        log.debug("REST request to get TopupTransaction : {}", id);
        Optional<TopupTransactionDTO> topupTransactionDTO = topupTransactionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(topupTransactionDTO);
    }

    /**
     * DELETE  /topup-transactions/:id : delete the "id" topupTransaction.
     *
     * @param id the id of the topupTransactionDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    //@DeleteMapping("/topup-transactions/{id}")
    public ResponseEntity<Void> deleteTopupTransaction(@PathVariable Long id) {
        log.debug("REST request to delete TopupTransaction : {}", id);
        topupTransactionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * GET  /topup-transactions/report : get the topupTransaction report.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of topupTransactions in body
     */
    @GetMapping("/topup-transactions/report")
    public ResponseEntity<List<TopupTransactionReportDTO>> getTopupTransactionReport(Pageable pageable) {
        log.debug("REST request to get a page of TopupTransactions");
        Page<TopupTransactionReportDTO> page = topupTransactionService.getTopupReport(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/topup-transactions/report");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
