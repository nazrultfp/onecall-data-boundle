package com.mobilityone.onecall.service.impl;

import com.mobilityone.onecall.domain.PlanTransaction;
import com.mobilityone.onecall.repository.PlanTransactionRepository;
import com.mobilityone.onecall.service.PlanTransactionService;
import com.mobilityone.onecall.service.dto.PlanTransactionDTO;
import com.mobilityone.onecall.service.dto.PlanTransactionReportDTO;
import com.mobilityone.onecall.service.mapper.PlanTransactionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing PlanTransaction.
 */
@Service
@Transactional
public class PlanTransactionServiceImpl implements PlanTransactionService {

    private final Logger log = LoggerFactory.getLogger(PlanTransactionServiceImpl.class);

    private final PlanTransactionRepository planTransactionRepository;

    private final PlanTransactionMapper planTransactionMapper;

    public PlanTransactionServiceImpl(PlanTransactionRepository planTransactionRepository, PlanTransactionMapper planTransactionMapper) {
        this.planTransactionRepository = planTransactionRepository;
        this.planTransactionMapper = planTransactionMapper;
    }

    /**
     * Save a planTransaction.
     *
     * @param planTransactionDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public PlanTransactionDTO save(PlanTransactionDTO planTransactionDTO) {
        log.debug("Request to save PlanTransaction : {}", planTransactionDTO);
        PlanTransaction planTransaction = planTransactionMapper.toEntity(planTransactionDTO);
        planTransaction = planTransactionRepository.save(planTransaction);
        return planTransactionMapper.toDto(planTransaction);
    }

    /**
     * Get all the planTransactions.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PlanTransactionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PlanTransactions");
        return planTransactionRepository.findAll(pageable)
            .map(planTransactionMapper::toDto);
    }


    /**
     * Get one planTransaction by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<PlanTransactionDTO> findOne(Long id) {
        log.debug("Request to get PlanTransaction : {}", id);
        return planTransactionRepository.findById(id)
            .map(planTransactionMapper::toDto);
    }

    /**
     * Delete the planTransaction by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PlanTransaction : {}", id);
        planTransactionRepository.deleteById(id);
    }

    @Override
    public Page<PlanTransactionReportDTO> getPlanTransactionReport(Pageable pageable) {
        log.debug("Request to get all PlanTransactions");
        return planTransactionRepository.getPlanTransactionReport(pageable);
    }
}
