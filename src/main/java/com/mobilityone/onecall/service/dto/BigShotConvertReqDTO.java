package com.mobilityone.onecall.service.dto;

public class BigShotConvertReqDTO {

    private String bigshotId;
    private String msisdn;
    private Integer point;
    private String transactionId;
    private String version;

    public String getBigshotId() {
        return bigshotId;
    }

    public void setBigshotId(String bigshotId) {
        this.bigshotId = bigshotId;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public Integer getPoint() {
        return point;
    }

    public void setPoint(Integer point) {
        this.point = point;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "BigShotConvertReqDTO{" +
            "bigshotId='" + bigshotId + '\'' +
            ", msisdn='" + msisdn + '\'' +
            ", point=" + point +
            ", transactionId='" + transactionId + '\'' +
            ", version='" + version + '\'' +
            '}';
    }
}
