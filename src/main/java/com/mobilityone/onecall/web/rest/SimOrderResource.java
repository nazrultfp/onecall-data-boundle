package com.mobilityone.onecall.web.rest;
import com.mobilityone.onecall.service.SimOrderService;
import com.mobilityone.onecall.web.rest.errors.BadRequestAlertException;
import com.mobilityone.onecall.web.rest.util.HeaderUtil;
import com.mobilityone.onecall.web.rest.util.PaginationUtil;
import com.mobilityone.onecall.service.dto.SimOrderDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SimOrder.
 */
@RestController
@RequestMapping("/api")
public class SimOrderResource {

    private final Logger log = LoggerFactory.getLogger(SimOrderResource.class);

    private static final String ENTITY_NAME = "oneCallSimOrder";

    private final SimOrderService simOrderService;

    public SimOrderResource(SimOrderService simOrderService) {
        this.simOrderService = simOrderService;
    }

    /**
     * POST  /sim-orders : Create a new simOrder.
     *
     * @param simOrderDTO the simOrderDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new simOrderDTO, or with status 400 (Bad Request) if the simOrder has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    //@PostMapping("/sim-orders")
    public ResponseEntity<SimOrderDTO> createSimOrder(@RequestBody SimOrderDTO simOrderDTO) throws URISyntaxException {
        log.debug("REST request to save SimOrder : {}", simOrderDTO);
        if (simOrderDTO.getId() != null) {
            throw new BadRequestAlertException("A new simOrder cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SimOrderDTO result = simOrderService.save(simOrderDTO);
        return ResponseEntity.created(new URI("/api/sim-orders/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /sim-orders : Updates an existing simOrder.
     *
     * @param simOrderDTO the simOrderDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated simOrderDTO,
     * or with status 400 (Bad Request) if the simOrderDTO is not valid,
     * or with status 500 (Internal Server Error) if the simOrderDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    //@PutMapping("/sim-orders")
    public ResponseEntity<SimOrderDTO> updateSimOrder(@RequestBody SimOrderDTO simOrderDTO) throws URISyntaxException {
        log.debug("REST request to update SimOrder : {}", simOrderDTO);
        if (simOrderDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SimOrderDTO result = simOrderService.save(simOrderDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, simOrderDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /sim-orders : get all the simOrders.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of simOrders in body
     */
    @GetMapping("/sim-orders")
    public ResponseEntity<List<SimOrderDTO>> getAllSimOrders(Pageable pageable) {
        log.debug("REST request to get a page of SimOrders");
        Page<SimOrderDTO> page = simOrderService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sim-orders");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /sim-orders/:id : get the "id" simOrder.
     *
     * @param id the id of the simOrderDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the simOrderDTO, or with status 404 (Not Found)
     */
    @GetMapping("/sim-orders/{id}")
    public ResponseEntity<SimOrderDTO> getSimOrder(@PathVariable Long id) {
        log.debug("REST request to get SimOrder : {}", id);
        Optional<SimOrderDTO> simOrderDTO = simOrderService.findOne(id);
        return ResponseUtil.wrapOrNotFound(simOrderDTO);
    }

    /**
     * DELETE  /sim-orders/:id : delete the "id" simOrder.
     *
     * @param id the id of the simOrderDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    //@DeleteMapping("/sim-orders/{id}")
    public ResponseEntity<Void> deleteSimOrder(@PathVariable Long id) {
        log.debug("REST request to delete SimOrder : {}", id);
        simOrderService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }


    @GetMapping("/sim-orders/by-agent/{agentId}")
    public ResponseEntity<List<SimOrderDTO>> getAllSimOrdersByAgent(Pageable pageable, @PathVariable Long agentId) {
        log.debug("REST request to get a page of SimOrders by agentid : {}", agentId);
        Page<SimOrderDTO> page = simOrderService.findAllByAgentId(pageable, agentId);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sim-orders/by-agent");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
