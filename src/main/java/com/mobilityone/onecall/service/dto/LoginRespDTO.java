package com.mobilityone.onecall.service.dto;

public class LoginRespDTO {

    private String access_token;
    private String token_type;
    private String full_name;
    private String credit_balance;
    private String msisdn;
    private String agentCode;


    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getCredit_balance() {
        return credit_balance;
    }

    public void setCredit_balance(String credit_balance) {
        this.credit_balance = credit_balance;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    @Override
    public String toString() {
        return "LoginRespDTO{" +
            "access_token='" + access_token + '\'' +
            ", token_type='" + token_type + '\'' +
            ", full_name='" + full_name + '\'' +
            ", credit_balance='" + credit_balance + '\'' +
            ", msisdn='" + msisdn + '\'' +
            ", agentCode='" + agentCode + '\'' +
            '}';
    }
}
