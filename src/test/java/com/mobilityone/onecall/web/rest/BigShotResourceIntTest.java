package com.mobilityone.onecall.web.rest;

import com.mobilityone.onecall.OneCallApp;

import com.mobilityone.onecall.domain.BigShot;
import com.mobilityone.onecall.repository.BigShotRepository;
import com.mobilityone.onecall.service.BigShotService;
import com.mobilityone.onecall.service.dto.BigShotDTO;
import com.mobilityone.onecall.service.mapper.BigShotMapper;
import com.mobilityone.onecall.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;


import static com.mobilityone.onecall.web.rest.TestUtil.sameInstant;
import static com.mobilityone.onecall.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the BigShotResource REST controller.
 *
 * @see BigShotResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OneCallApp.class)
public class BigShotResourceIntTest {

    private static final String DEFAULT_BIGSHOT_ID = "AAAAAAAAAA";
    private static final String UPDATED_BIGSHOT_ID = "BBBBBBBBBB";

    private static final String DEFAULT_MSISDN = "AAAAAAAAAA";
    private static final String UPDATED_MSISDN = "BBBBBBBBBB";

    private static final Integer DEFAULT_POINT = 1;
    private static final Integer UPDATED_POINT = 2;

    private static final String DEFAULT_TRANSACTION_ID = "AAAAAAAAAA";
    private static final String UPDATED_TRANSACTION_ID = "BBBBBBBBBB";

    private static final String DEFAULT_VERSION = "AAAAAAAAAA";
    private static final String UPDATED_VERSION = "BBBBBBBBBB";

    private static final String DEFAULT_API_KEY = "AAAAAAAAAA";
    private static final String UPDATED_API_KEY = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_MESSAGE = "AAAAAAAAAA";
    private static final String UPDATED_MESSAGE = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_MODIFIED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_MODIFIED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_MODIFIED_BY = "BBBBBBBBBB";

    @Autowired
    private BigShotRepository bigShotRepository;

    @Autowired
    private BigShotMapper bigShotMapper;

    @Autowired
    private BigShotService bigShotService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restBigShotMockMvc;

    private BigShot bigShot;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final BigShotResource bigShotResource = new BigShotResource(bigShotService);
        this.restBigShotMockMvc = MockMvcBuilders.standaloneSetup(bigShotResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BigShot createEntity(EntityManager em) {
        BigShot bigShot = new BigShot()
            .bigshotId(DEFAULT_BIGSHOT_ID)
            .msisdn(DEFAULT_MSISDN)
            .point(DEFAULT_POINT)
            .transactionId(DEFAULT_TRANSACTION_ID)
            .version(DEFAULT_VERSION)
            .apiKey(DEFAULT_API_KEY)
            .code(DEFAULT_CODE)
            .message(DEFAULT_MESSAGE)
            .createdDate(DEFAULT_CREATED_DATE)
            .modifiedDate(DEFAULT_MODIFIED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .modifiedBy(DEFAULT_MODIFIED_BY);
        return bigShot;
    }

    @Before
    public void initTest() {
        bigShot = createEntity(em);
    }

    @Test
    @Transactional
    public void createBigShot() throws Exception {
        int databaseSizeBeforeCreate = bigShotRepository.findAll().size();

        // Create the BigShot
        BigShotDTO bigShotDTO = bigShotMapper.toDto(bigShot);
        restBigShotMockMvc.perform(post("/api/big-shots")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bigShotDTO)))
            .andExpect(status().isCreated());

        // Validate the BigShot in the database
        List<BigShot> bigShotList = bigShotRepository.findAll();
        assertThat(bigShotList).hasSize(databaseSizeBeforeCreate + 1);
        BigShot testBigShot = bigShotList.get(bigShotList.size() - 1);
        assertThat(testBigShot.getBigshotId()).isEqualTo(DEFAULT_BIGSHOT_ID);
        assertThat(testBigShot.getMsisdn()).isEqualTo(DEFAULT_MSISDN);
        assertThat(testBigShot.getPoint()).isEqualTo(DEFAULT_POINT);
        assertThat(testBigShot.getTransactionId()).isEqualTo(DEFAULT_TRANSACTION_ID);
        assertThat(testBigShot.getVersion()).isEqualTo(DEFAULT_VERSION);
        assertThat(testBigShot.getApiKey()).isEqualTo(DEFAULT_API_KEY);
        assertThat(testBigShot.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testBigShot.getMessage()).isEqualTo(DEFAULT_MESSAGE);
        assertThat(testBigShot.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testBigShot.getModifiedDate()).isEqualTo(DEFAULT_MODIFIED_DATE);
        assertThat(testBigShot.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testBigShot.getModifiedBy()).isEqualTo(DEFAULT_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void createBigShotWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = bigShotRepository.findAll().size();

        // Create the BigShot with an existing ID
        bigShot.setId(1L);
        BigShotDTO bigShotDTO = bigShotMapper.toDto(bigShot);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBigShotMockMvc.perform(post("/api/big-shots")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bigShotDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BigShot in the database
        List<BigShot> bigShotList = bigShotRepository.findAll();
        assertThat(bigShotList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllBigShots() throws Exception {
        // Initialize the database
        bigShotRepository.saveAndFlush(bigShot);

        // Get all the bigShotList
        restBigShotMockMvc.perform(get("/api/big-shots?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bigShot.getId().intValue())))
            .andExpect(jsonPath("$.[*].bigshotId").value(hasItem(DEFAULT_BIGSHOT_ID.toString())))
            .andExpect(jsonPath("$.[*].msisdn").value(hasItem(DEFAULT_MSISDN.toString())))
            .andExpect(jsonPath("$.[*].point").value(hasItem(DEFAULT_POINT)))
            .andExpect(jsonPath("$.[*].transactionId").value(hasItem(DEFAULT_TRANSACTION_ID.toString())))
            .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION.toString())))
            .andExpect(jsonPath("$.[*].apiKey").value(hasItem(DEFAULT_API_KEY.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].message").value(hasItem(DEFAULT_MESSAGE.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(sameInstant(DEFAULT_CREATED_DATE))))
            .andExpect(jsonPath("$.[*].modifiedDate").value(hasItem(sameInstant(DEFAULT_MODIFIED_DATE))))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].modifiedBy").value(hasItem(DEFAULT_MODIFIED_BY.toString())));
    }
    
    @Test
    @Transactional
    public void getBigShot() throws Exception {
        // Initialize the database
        bigShotRepository.saveAndFlush(bigShot);

        // Get the bigShot
        restBigShotMockMvc.perform(get("/api/big-shots/{id}", bigShot.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(bigShot.getId().intValue()))
            .andExpect(jsonPath("$.bigshotId").value(DEFAULT_BIGSHOT_ID.toString()))
            .andExpect(jsonPath("$.msisdn").value(DEFAULT_MSISDN.toString()))
            .andExpect(jsonPath("$.point").value(DEFAULT_POINT))
            .andExpect(jsonPath("$.transactionId").value(DEFAULT_TRANSACTION_ID.toString()))
            .andExpect(jsonPath("$.version").value(DEFAULT_VERSION.toString()))
            .andExpect(jsonPath("$.apiKey").value(DEFAULT_API_KEY.toString()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.message").value(DEFAULT_MESSAGE.toString()))
            .andExpect(jsonPath("$.createdDate").value(sameInstant(DEFAULT_CREATED_DATE)))
            .andExpect(jsonPath("$.modifiedDate").value(sameInstant(DEFAULT_MODIFIED_DATE)))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.modifiedBy").value(DEFAULT_MODIFIED_BY.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingBigShot() throws Exception {
        // Get the bigShot
        restBigShotMockMvc.perform(get("/api/big-shots/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBigShot() throws Exception {
        // Initialize the database
        bigShotRepository.saveAndFlush(bigShot);

        int databaseSizeBeforeUpdate = bigShotRepository.findAll().size();

        // Update the bigShot
        BigShot updatedBigShot = bigShotRepository.findById(bigShot.getId()).get();
        // Disconnect from session so that the updates on updatedBigShot are not directly saved in db
        em.detach(updatedBigShot);
        updatedBigShot
            .bigshotId(UPDATED_BIGSHOT_ID)
            .msisdn(UPDATED_MSISDN)
            .point(UPDATED_POINT)
            .transactionId(UPDATED_TRANSACTION_ID)
            .version(UPDATED_VERSION)
            .apiKey(UPDATED_API_KEY)
            .code(UPDATED_CODE)
            .message(UPDATED_MESSAGE)
            .createdDate(UPDATED_CREATED_DATE)
            .modifiedDate(UPDATED_MODIFIED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .modifiedBy(UPDATED_MODIFIED_BY);
        BigShotDTO bigShotDTO = bigShotMapper.toDto(updatedBigShot);

        restBigShotMockMvc.perform(put("/api/big-shots")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bigShotDTO)))
            .andExpect(status().isOk());

        // Validate the BigShot in the database
        List<BigShot> bigShotList = bigShotRepository.findAll();
        assertThat(bigShotList).hasSize(databaseSizeBeforeUpdate);
        BigShot testBigShot = bigShotList.get(bigShotList.size() - 1);
        assertThat(testBigShot.getBigshotId()).isEqualTo(UPDATED_BIGSHOT_ID);
        assertThat(testBigShot.getMsisdn()).isEqualTo(UPDATED_MSISDN);
        assertThat(testBigShot.getPoint()).isEqualTo(UPDATED_POINT);
        assertThat(testBigShot.getTransactionId()).isEqualTo(UPDATED_TRANSACTION_ID);
        assertThat(testBigShot.getVersion()).isEqualTo(UPDATED_VERSION);
        assertThat(testBigShot.getApiKey()).isEqualTo(UPDATED_API_KEY);
        assertThat(testBigShot.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testBigShot.getMessage()).isEqualTo(UPDATED_MESSAGE);
        assertThat(testBigShot.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testBigShot.getModifiedDate()).isEqualTo(UPDATED_MODIFIED_DATE);
        assertThat(testBigShot.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testBigShot.getModifiedBy()).isEqualTo(UPDATED_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void updateNonExistingBigShot() throws Exception {
        int databaseSizeBeforeUpdate = bigShotRepository.findAll().size();

        // Create the BigShot
        BigShotDTO bigShotDTO = bigShotMapper.toDto(bigShot);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBigShotMockMvc.perform(put("/api/big-shots")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(bigShotDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BigShot in the database
        List<BigShot> bigShotList = bigShotRepository.findAll();
        assertThat(bigShotList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBigShot() throws Exception {
        // Initialize the database
        bigShotRepository.saveAndFlush(bigShot);

        int databaseSizeBeforeDelete = bigShotRepository.findAll().size();

        // Delete the bigShot
        restBigShotMockMvc.perform(delete("/api/big-shots/{id}", bigShot.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<BigShot> bigShotList = bigShotRepository.findAll();
        assertThat(bigShotList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BigShot.class);
        BigShot bigShot1 = new BigShot();
        bigShot1.setId(1L);
        BigShot bigShot2 = new BigShot();
        bigShot2.setId(bigShot1.getId());
        assertThat(bigShot1).isEqualTo(bigShot2);
        bigShot2.setId(2L);
        assertThat(bigShot1).isNotEqualTo(bigShot2);
        bigShot1.setId(null);
        assertThat(bigShot1).isNotEqualTo(bigShot2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(BigShotDTO.class);
        BigShotDTO bigShotDTO1 = new BigShotDTO();
        bigShotDTO1.setId(1L);
        BigShotDTO bigShotDTO2 = new BigShotDTO();
        assertThat(bigShotDTO1).isNotEqualTo(bigShotDTO2);
        bigShotDTO2.setId(bigShotDTO1.getId());
        assertThat(bigShotDTO1).isEqualTo(bigShotDTO2);
        bigShotDTO2.setId(2L);
        assertThat(bigShotDTO1).isNotEqualTo(bigShotDTO2);
        bigShotDTO1.setId(null);
        assertThat(bigShotDTO1).isNotEqualTo(bigShotDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(bigShotMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(bigShotMapper.fromId(null)).isNull();
    }
}
