package com.mobilityone.onecall.domain.enumeration;

/**
 * The PlanType enumeration.
 */
public enum PlanType {
    Data, Call, Validity
}
