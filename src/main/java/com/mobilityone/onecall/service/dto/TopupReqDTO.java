package com.mobilityone.onecall.service.dto;

import javax.validation.constraints.NotNull;

public class TopupReqDTO {
    private String msisdn;
    private Integer topUpDeno;
    private String transactionId;
    private String channelId;
    private String merchantId;
    private String merchantOrderNo;

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantOrderNo() {
        return merchantOrderNo;
    }

    public void setMerchantOrderNo(String merchantOrderNo) {
        this.merchantOrderNo = merchantOrderNo;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public Integer getTopUpDeno() {
        return topUpDeno;
    }

    public void setTopUpDeno(Integer topUpDeno) {
        this.topUpDeno = topUpDeno;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    @Override
    public String toString() {
        return "TopupReqDTO{" +
            "msisdn='" + msisdn + '\'' +
            ", topUpDeno=" + topUpDeno +
            ", transactionId='" + transactionId + '\'' +
            ", channelId='" + channelId + '\'' +
            ", merchantId='" + merchantId + '\'' +
            ", merchantOrderNo='" + merchantOrderNo + '\'' +
            '}';
    }
}
