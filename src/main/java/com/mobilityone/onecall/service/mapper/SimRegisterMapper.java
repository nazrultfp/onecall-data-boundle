package com.mobilityone.onecall.service.mapper;

import com.mobilityone.onecall.domain.*;
import com.mobilityone.onecall.service.dto.SimRegisterDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity SimRegister and its DTO SimRegisterDTO.
 */
@Mapper(componentModel = "spring", uses = {AgentMapper.class, PhotoMapper.class})
public interface SimRegisterMapper extends EntityMapper<SimRegisterDTO, SimRegister> {

    @Mapping(source = "agent.id", target = "agentId")
    SimRegisterDTO toDto(SimRegister simRegister);

    //@Mapping(target = "photos", ignore = true)
    @Mapping(source = "agentId", target = "agent")
    SimRegister toEntity(SimRegisterDTO simRegisterDTO);

    default SimRegister fromId(Long id) {
        if (id == null) {
            return null;
        }
        SimRegister simRegister = new SimRegister();
        simRegister.setId(id);
        return simRegister;
    }
}
