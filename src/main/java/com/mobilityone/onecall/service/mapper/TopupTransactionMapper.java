package com.mobilityone.onecall.service.mapper;

import com.mobilityone.onecall.domain.TopupTransaction;
import com.mobilityone.onecall.service.dto.TopupTransactionDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity TopupTransaction and its DTO TopupTransactionDTO.
 */
@Mapper(componentModel = "spring", uses = {MerchantMapper.class})
public interface TopupTransactionMapper extends EntityMapper<TopupTransactionDTO, TopupTransaction> {

    @Mapping(source = "merchant.id", target = "merchantId")
    TopupTransactionDTO toDto(TopupTransaction topupTransaction);

    @Mapping(source = "merchantId", target = "merchant")
    TopupTransaction toEntity(TopupTransactionDTO topupTransactionDTO);

    default TopupTransaction fromId(Long id) {
        if (id == null) {
            return null;
        }
        TopupTransaction topupTransaction = new TopupTransaction();
        topupTransaction.setId(id);
        return topupTransaction;
    }
}
