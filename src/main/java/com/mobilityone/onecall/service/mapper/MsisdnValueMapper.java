package com.mobilityone.onecall.service.mapper;

import com.mobilityone.onecall.domain.*;
import com.mobilityone.onecall.service.dto.MsisdnValueDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity MsisdnValue and its DTO MsisdnValueDTO.
 */
@Mapper(componentModel = "spring", uses = {OneCallMapper.class})
public interface MsisdnValueMapper extends EntityMapper<MsisdnValueDTO, MsisdnValue> {

    @Mapping(source = "oneCall.id", target = "oneCallId")
    MsisdnValueDTO toDto(MsisdnValue msisdnValue);

    @Mapping(source = "oneCallId", target = "oneCall")
    MsisdnValue toEntity(MsisdnValueDTO msisdnValueDTO);

    default MsisdnValue fromId(Long id) {
        if (id == null) {
            return null;
        }
        MsisdnValue msisdnValue = new MsisdnValue();
        msisdnValue.setId(id);
        return msisdnValue;
    }
}
