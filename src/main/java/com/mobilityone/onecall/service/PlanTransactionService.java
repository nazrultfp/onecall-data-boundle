package com.mobilityone.onecall.service;

import com.mobilityone.onecall.service.dto.PlanTransactionDTO;
import com.mobilityone.onecall.service.dto.PlanTransactionReportDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing PlanTransaction.
 */
public interface PlanTransactionService {

    /**
     * Save a planTransaction.
     *
     * @param planTransactionDTO the entity to save
     * @return the persisted entity
     */
    PlanTransactionDTO save(PlanTransactionDTO planTransactionDTO);

    /**
     * Get all the planTransactions.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<PlanTransactionDTO> findAll(Pageable pageable);


    /**
     * Get the "id" planTransaction.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<PlanTransactionDTO> findOne(Long id);

    /**
     * Delete the "id" planTransaction.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    Page<PlanTransactionReportDTO> getPlanTransactionReport(Pageable pageable);
}
