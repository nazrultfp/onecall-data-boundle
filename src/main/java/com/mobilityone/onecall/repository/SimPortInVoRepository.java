package com.mobilityone.onecall.repository;

import com.mobilityone.onecall.domain.SimPortInVo;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Spring Data  repository for the SimPortInVo entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SimPortInVoRepository extends JpaRepository<SimPortInVo, Long> {

    Optional<SimPortInVo> findFirstByMsisdn(String msisdn);
}
