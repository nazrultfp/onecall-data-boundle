package com.mobilityone.onecall.repository;

import com.mobilityone.onecall.domain.BigShot;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the BigShot entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BigShotRepository extends JpaRepository<BigShot, Long> {

}
