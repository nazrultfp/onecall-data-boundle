package com.mobilityone.onecall.web.rest;

import com.mobilityone.onecall.OneCallApp;
import com.mobilityone.onecall.domain.TopupTransaction;
import com.mobilityone.onecall.domain.enumeration.Status;
import com.mobilityone.onecall.repository.TopupTransactionRepository;
import com.mobilityone.onecall.service.TopupTransactionService;
import com.mobilityone.onecall.service.dto.TopupTransactionDTO;
import com.mobilityone.onecall.service.mapper.TopupTransactionMapper;
import com.mobilityone.onecall.web.rest.errors.ExceptionTranslator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;

import static com.mobilityone.onecall.web.rest.TestUtil.createFormattingConversionService;
import static com.mobilityone.onecall.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
/**
 * Test class for the TopupTransactionResource REST controller.
 *
 * @see TopupTransactionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OneCallApp.class)
public class TopupTransactionResourceIntTest {

    private static final Double DEFAULT_AMOUNT = 1D;
    private static final Double UPDATED_AMOUNT = 2D;

    private static final ZonedDateTime DEFAULT_TRANSACTION_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_TRANSACTION_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_TRANSACTION_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TRANSACTION_TYPE = "BBBBBBBBBB";

    private static final Status DEFAULT_STATUS = Status.SUCCESSFUL;
    private static final Status UPDATED_STATUS = Status.UNSUCCESSFUL;

    private static final String DEFAULT_MESSAGE = "AAAAAAAAAA";
    private static final String UPDATED_MESSAGE = "BBBBBBBBBB";

    @Autowired
    private TopupTransactionRepository topupTransactionRepository;

    @Autowired
    private TopupTransactionMapper topupTransactionMapper;

    @Autowired
    private TopupTransactionService topupTransactionService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTopupTransactionMockMvc;

    private TopupTransaction topupTransaction;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TopupTransactionResource topupTransactionResource = new TopupTransactionResource(topupTransactionService);
        this.restTopupTransactionMockMvc = MockMvcBuilders.standaloneSetup(topupTransactionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TopupTransaction createEntity(EntityManager em) {
        TopupTransaction topupTransaction = new TopupTransaction()
            .amount(DEFAULT_AMOUNT)
            .transactionDate(DEFAULT_TRANSACTION_DATE)
            .transactionType(DEFAULT_TRANSACTION_TYPE)
            .status(DEFAULT_STATUS)
            .message(DEFAULT_MESSAGE);
        return topupTransaction;
    }

    @Before
    public void initTest() {
        topupTransaction = createEntity(em);
    }

    @Test
    @Transactional
    public void createTopupTransaction() throws Exception {
        int databaseSizeBeforeCreate = topupTransactionRepository.findAll().size();

        // Create the TopupTransaction
        TopupTransactionDTO topupTransactionDTO = topupTransactionMapper.toDto(topupTransaction);
        restTopupTransactionMockMvc.perform(post("/api/topup-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(topupTransactionDTO)))
            .andExpect(status().isCreated());

        // Validate the TopupTransaction in the database
        List<TopupTransaction> topupTransactionList = topupTransactionRepository.findAll();
        assertThat(topupTransactionList).hasSize(databaseSizeBeforeCreate + 1);
        TopupTransaction testTopupTransaction = topupTransactionList.get(topupTransactionList.size() - 1);
        assertThat(testTopupTransaction.getAmount()).isEqualTo(DEFAULT_AMOUNT);
        assertThat(testTopupTransaction.getTransactionDate()).isEqualTo(DEFAULT_TRANSACTION_DATE);
        assertThat(testTopupTransaction.getTransactionType()).isEqualTo(DEFAULT_TRANSACTION_TYPE);
        assertThat(testTopupTransaction.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testTopupTransaction.getMessage()).isEqualTo(DEFAULT_MESSAGE);
    }

    @Test
    @Transactional
    public void createTopupTransactionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = topupTransactionRepository.findAll().size();

        // Create the TopupTransaction with an existing ID
        topupTransaction.setId(1L);
        TopupTransactionDTO topupTransactionDTO = topupTransactionMapper.toDto(topupTransaction);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTopupTransactionMockMvc.perform(post("/api/topup-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(topupTransactionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TopupTransaction in the database
        List<TopupTransaction> topupTransactionList = topupTransactionRepository.findAll();
        assertThat(topupTransactionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllTopupTransactions() throws Exception {
        // Initialize the database
        topupTransactionRepository.saveAndFlush(topupTransaction);

        // Get all the topupTransactionList
        restTopupTransactionMockMvc.perform(get("/api/topup-transactions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(topupTransaction.getId().intValue())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].transactionDate").value(hasItem(sameInstant(DEFAULT_TRANSACTION_DATE))))
            .andExpect(jsonPath("$.[*].transactionType").value(hasItem(DEFAULT_TRANSACTION_TYPE.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].message").value(hasItem(DEFAULT_MESSAGE.toString())));
    }

    @Test
    @Transactional
    public void getTopupTransaction() throws Exception {
        // Initialize the database
        topupTransactionRepository.saveAndFlush(topupTransaction);

        // Get the topupTransaction
        restTopupTransactionMockMvc.perform(get("/api/topup-transactions/{id}", topupTransaction.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(topupTransaction.getId().intValue()))
            .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT.doubleValue()))
            .andExpect(jsonPath("$.transactionDate").value(sameInstant(DEFAULT_TRANSACTION_DATE)))
            .andExpect(jsonPath("$.transactionType").value(DEFAULT_TRANSACTION_TYPE.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.message").value(DEFAULT_MESSAGE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTopupTransaction() throws Exception {
        // Get the topupTransaction
        restTopupTransactionMockMvc.perform(get("/api/topup-transactions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTopupTransaction() throws Exception {
        // Initialize the database
        topupTransactionRepository.saveAndFlush(topupTransaction);

        int databaseSizeBeforeUpdate = topupTransactionRepository.findAll().size();

        // Update the topupTransaction
        TopupTransaction updatedTopupTransaction = topupTransactionRepository.findById(topupTransaction.getId()).get();
        // Disconnect from session so that the updates on updatedTopupTransaction are not directly saved in db
        em.detach(updatedTopupTransaction);
        updatedTopupTransaction
            .amount(UPDATED_AMOUNT)
            .transactionDate(UPDATED_TRANSACTION_DATE)
            .transactionType(UPDATED_TRANSACTION_TYPE)
            .status(UPDATED_STATUS)
            .message(UPDATED_MESSAGE);
        TopupTransactionDTO topupTransactionDTO = topupTransactionMapper.toDto(updatedTopupTransaction);

        restTopupTransactionMockMvc.perform(put("/api/topup-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(topupTransactionDTO)))
            .andExpect(status().isOk());

        // Validate the TopupTransaction in the database
        List<TopupTransaction> topupTransactionList = topupTransactionRepository.findAll();
        assertThat(topupTransactionList).hasSize(databaseSizeBeforeUpdate);
        TopupTransaction testTopupTransaction = topupTransactionList.get(topupTransactionList.size() - 1);
        assertThat(testTopupTransaction.getAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testTopupTransaction.getTransactionDate()).isEqualTo(UPDATED_TRANSACTION_DATE);
        assertThat(testTopupTransaction.getTransactionType()).isEqualTo(UPDATED_TRANSACTION_TYPE);
        assertThat(testTopupTransaction.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testTopupTransaction.getMessage()).isEqualTo(UPDATED_MESSAGE);
    }

    @Test
    @Transactional
    public void updateNonExistingTopupTransaction() throws Exception {
        int databaseSizeBeforeUpdate = topupTransactionRepository.findAll().size();

        // Create the TopupTransaction
        TopupTransactionDTO topupTransactionDTO = topupTransactionMapper.toDto(topupTransaction);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTopupTransactionMockMvc.perform(put("/api/topup-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(topupTransactionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TopupTransaction in the database
        List<TopupTransaction> topupTransactionList = topupTransactionRepository.findAll();
        assertThat(topupTransactionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTopupTransaction() throws Exception {
        // Initialize the database
        topupTransactionRepository.saveAndFlush(topupTransaction);

        int databaseSizeBeforeDelete = topupTransactionRepository.findAll().size();

        // Delete the topupTransaction
        restTopupTransactionMockMvc.perform(delete("/api/topup-transactions/{id}", topupTransaction.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<TopupTransaction> topupTransactionList = topupTransactionRepository.findAll();
        assertThat(topupTransactionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TopupTransaction.class);
        TopupTransaction topupTransaction1 = new TopupTransaction();
        topupTransaction1.setId(1L);
        TopupTransaction topupTransaction2 = new TopupTransaction();
        topupTransaction2.setId(topupTransaction1.getId());
        assertThat(topupTransaction1).isEqualTo(topupTransaction2);
        topupTransaction2.setId(2L);
        assertThat(topupTransaction1).isNotEqualTo(topupTransaction2);
        topupTransaction1.setId(null);
        assertThat(topupTransaction1).isNotEqualTo(topupTransaction2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TopupTransactionDTO.class);
        TopupTransactionDTO topupTransactionDTO1 = new TopupTransactionDTO();
        topupTransactionDTO1.setId(1L);
        TopupTransactionDTO topupTransactionDTO2 = new TopupTransactionDTO();
        assertThat(topupTransactionDTO1).isNotEqualTo(topupTransactionDTO2);
        topupTransactionDTO2.setId(topupTransactionDTO1.getId());
        assertThat(topupTransactionDTO1).isEqualTo(topupTransactionDTO2);
        topupTransactionDTO2.setId(2L);
        assertThat(topupTransactionDTO1).isNotEqualTo(topupTransactionDTO2);
        topupTransactionDTO1.setId(null);
        assertThat(topupTransactionDTO1).isNotEqualTo(topupTransactionDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(topupTransactionMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(topupTransactionMapper.fromId(null)).isNull();
    }
}
