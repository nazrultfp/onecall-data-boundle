package com.mobilityone.onecall.domain.enumeration;

/**
 * The Gender enumeration.
 */
public enum Gender {
    MALE, FEMALE
}
