package com.mobilityone.onecall.service.dto;

public class TtMsidnPlanDTO {

    private String name;
    private String expiry;
    private String expiryEndDate;
    private Double dataBalance;
    private Double dataQuota;
    private Double voiceOffnetBalance;
    private Double voiceOffnetQuota;
    private Double voiceOnnetBalance;
    private Double voiceOnnetQuota;
    private Double smsBalance;
    private Double smsQuota;
    private Double bundleCreditBalance;
    private Double bundleCreditQuota;
    private Boolean isPending;
    private Boolean autoRenew;
    private Boolean isBasicInternet;
    private Double addOnDataBalance;
    private Double addOnDataQuota;
    private String addOnName;
    private Double addOnSmsBalance;
    private Double addOnSmsQuota;
    private Double addOnVoiceOffnetBalance;
    private Double addOnVoiceOffnetQuota;
    private Double addOnVoiceOnnetBalance;
    private Double addOnVoiceOnnetQuota;
    private Double chatBalance;
    private Double chatQuota;
    private Double musicBalance;
    private Double musicQuota;
    private Double socialBalance;
    private Double socialQuota;
    private Double videoBalance;
    private Double videoQuota;
    private Boolean pending;
    private Boolean basicInternet;

    public Boolean getBasicInternet() {
        return basicInternet;
    }

    public void setBasicInternet(Boolean basicInternet) {
        this.basicInternet = basicInternet;
    }

    public Boolean getPending() {
        return pending;
    }

    public void setPending(Boolean pending) {
        this.pending = pending;
    }

    public Double getAddOnDataBalance() {
        return addOnDataBalance;
    }

    public void setAddOnDataBalance(Double addOnDataBalance) {
        this.addOnDataBalance = addOnDataBalance;
    }

    public Double getAddOnDataQuota() {
        return addOnDataQuota;
    }

    public void setAddOnDataQuota(Double addOnDataQuota) {
        this.addOnDataQuota = addOnDataQuota;
    }

    public String getAddOnName() {
        return addOnName;
    }

    public void setAddOnName(String addOnName) {
        this.addOnName = addOnName;
    }

    public Double getAddOnSmsBalance() {
        return addOnSmsBalance;
    }

    public void setAddOnSmsBalance(Double addOnSmsBalance) {
        this.addOnSmsBalance = addOnSmsBalance;
    }

    public Double getAddOnSmsQuota() {
        return addOnSmsQuota;
    }

    public void setAddOnSmsQuota(Double addOnSmsQuota) {
        this.addOnSmsQuota = addOnSmsQuota;
    }

    public Double getAddOnVoiceOffnetBalance() {
        return addOnVoiceOffnetBalance;
    }

    public void setAddOnVoiceOffnetBalance(Double addOnVoiceOffnetBalance) {
        this.addOnVoiceOffnetBalance = addOnVoiceOffnetBalance;
    }

    public Double getAddOnVoiceOffnetQuota() {
        return addOnVoiceOffnetQuota;
    }

    public void setAddOnVoiceOffnetQuota(Double addOnVoiceOffnetQuota) {
        this.addOnVoiceOffnetQuota = addOnVoiceOffnetQuota;
    }

    public Double getAddOnVoiceOnnetBalance() {
        return addOnVoiceOnnetBalance;
    }

    public void setAddOnVoiceOnnetBalance(Double addOnVoiceOnnetBalance) {
        this.addOnVoiceOnnetBalance = addOnVoiceOnnetBalance;
    }

    public Double getAddOnVoiceOnnetQuota() {
        return addOnVoiceOnnetQuota;
    }

    public void setAddOnVoiceOnnetQuota(Double addOnVoiceOnnetQuota) {
        this.addOnVoiceOnnetQuota = addOnVoiceOnnetQuota;
    }

    public Boolean getAutoRenew() {
        return autoRenew;
    }

    public void setAutoRenew(Boolean autoRenew) {
        this.autoRenew = autoRenew;
    }

    public Boolean getIsBasicInternet() {
        return isBasicInternet;
    }

    public void setIsBasicInternet(Boolean basicInternet) {
        this.isBasicInternet = basicInternet;
    }

    public Boolean getIsPending() {
        return isPending;
    }

    public void setIsPending(Boolean pending) {
        isPending = pending;
    }

    public Double getSmsBalance() {
        return smsBalance;
    }

    public void setSmsBalance(Double smsBalance) {
        this.smsBalance = smsBalance;
    }

    public Double getSmsQuota() {
        return smsQuota;
    }

    public void setSmsQuota(Double smsQuota) {
        this.smsQuota = smsQuota;
    }

    public Double getSocialBalance() {
        return socialBalance;
    }

    public void setSocialBalance(Double socialBalance) {
        this.socialBalance = socialBalance;
    }

    public Double getSocialQuota() {
        return socialQuota;
    }

    public void setSocialQuota(Double socialQuota) {
        this.socialQuota = socialQuota;
    }

    public Double getVideoBalance() {
        return videoBalance;
    }

    public void setVideoBalance(Double videoBalance) {
        this.videoBalance = videoBalance;
    }

    public Double getVideoQuota() {
        return videoQuota;
    }

    public void setVideoQuota(Double videoQuota) {
        this.videoQuota = videoQuota;
    }

    public Double getVoiceOffnetBalance() {
        return voiceOffnetBalance;
    }

    public void setVoiceOffnetBalance(Double voiceOffnetBalance) {
        this.voiceOffnetBalance = voiceOffnetBalance;
    }

    public Double getVoiceOffnetQuota() {
        return voiceOffnetQuota;
    }

    public void setVoiceOffnetQuota(Double voiceOffnetQuota) {
        this.voiceOffnetQuota = voiceOffnetQuota;
    }

    public Double getVoiceOnnetBalance() {
        return voiceOnnetBalance;
    }

    public void setVoiceOnnetBalance(Double voiceOnnetBalance) {
        this.voiceOnnetBalance = voiceOnnetBalance;
    }

    public Double getVoiceOnnetQuota() {
        return voiceOnnetQuota;
    }

    public void setVoiceOnnetQuota(Double voiceOnnetQuota) {
        this.voiceOnnetQuota = voiceOnnetQuota;
    }

    public Double getMusicBalance() {
        return musicBalance;
    }

    public void setMusicBalance(Double musicBalance) {
        this.musicBalance = musicBalance;
    }

    public Double getMusicQuota() {
        return musicQuota;
    }

    public void setMusicQuota(Double musicQuota) {
        this.musicQuota = musicQuota;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getBundleCreditBalance() {
        return bundleCreditBalance;
    }

    public void setBundleCreditBalance(Double bundleCreditBalance) {
        this.bundleCreditBalance = bundleCreditBalance;
    }

    public Double getBundleCreditQuota() {
        return bundleCreditQuota;
    }

    public void setBundleCreditQuota(Double bundleCreditQuota) {
        this.bundleCreditQuota = bundleCreditQuota;
    }

    public Double getChatBalance() {
        return chatBalance;
    }

    public void setChatBalance(Double chatBalance) {
        this.chatBalance = chatBalance;
    }

    public Double getChatQuota() {
        return chatQuota;
    }

    public void setChatQuota(Double chatQuota) {
        this.chatQuota = chatQuota;
    }

    public Double getDataBalance() {
        return dataBalance;
    }

    public void setDataBalance(Double dataBalance) {
        this.dataBalance = dataBalance;
    }

    public Double getDataQuota() {
        return dataQuota;
    }

    public void setDataQuota(Double dataQuota) {
        this.dataQuota = dataQuota;
    }

    public String getExpiry() {
        return expiry;
    }

    public void setExpiry(String expiry) {
        this.expiry = expiry;
    }

    public String getExpiryEndDate() {
        return expiryEndDate;
    }

    public void setExpiryEndDate(String expiryEndDate) {
        this.expiryEndDate = expiryEndDate;
    }

    @Override
    public String toString() {
        return "TtMsidnPlanDTO{" +
            "addOnDataBalance=" + addOnDataBalance +
            ", addOnDataQuota=" + addOnDataQuota +
            ", addOnName='" + addOnName + '\'' +
            ", addOnSmsBalance=" + addOnSmsBalance +
            ", addOnSmsQuota=" + addOnSmsQuota +
            ", addOnVoiceOffnetBalance=" + addOnVoiceOffnetBalance +
            ", addOnVoiceOffnetQuota=" + addOnVoiceOffnetQuota +
            ", addOnVoiceOnnetBalance=" + addOnVoiceOnnetBalance +
            ", addOnVoiceOnnetQuota=" + addOnVoiceOnnetQuota +
            ", autoRenew=" + autoRenew +
            ", basicInternet=" + basicInternet +
            ", bundleCreditBalance=" + bundleCreditBalance +
            ", bundleCreditQuota=" + bundleCreditQuota +
            ", chatBalance=" + chatBalance +
            ", chatQuota=" + chatQuota +
            ", dataBalance=" + dataBalance +
            ", dataQuota=" + dataQuota +
            ", expiry=" + expiry +
            ", expiryEndDate=" + expiryEndDate +
            ", isBasicInternet=" + isBasicInternet +
            ", isPending=" + isPending +
            ", musicBalance=" + musicBalance +
            ", musicQuota=" + musicQuota +
            ", name='" + name + '\'' +
            ", pending=" + pending +
            ", smsBalance=" + smsBalance +
            ", smsQuota=" + smsQuota +
            ", socialBalance=" + socialBalance +
            ", socialQuota=" + socialQuota +
            ", videoBalance=" + videoBalance +
            ", videoQuota=" + videoQuota +
            ", voiceOffnetBalance=" + voiceOffnetBalance +
            ", voiceOffnetQuota=" + voiceOffnetQuota +
            ", voiceOnnetBalance=" + voiceOnnetBalance +
            ", voiceOnnetQuota=" + voiceOnnetQuota +
            '}';
    }
}
