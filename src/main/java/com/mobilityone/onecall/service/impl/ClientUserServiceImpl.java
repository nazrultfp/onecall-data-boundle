package com.mobilityone.onecall.service.impl;

import com.mobilityone.onecall.service.ClientUserService;
import com.mobilityone.onecall.domain.ClientUser;
import com.mobilityone.onecall.repository.ClientUserRepository;
import com.mobilityone.onecall.service.dto.ClientUserDTO;
import com.mobilityone.onecall.service.mapper.ClientUserMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing ClientUser.
 */
@Service
@Transactional
public class ClientUserServiceImpl implements ClientUserService {

    private final Logger log = LoggerFactory.getLogger(ClientUserServiceImpl.class);

    private final ClientUserRepository clientUserRepository;

    private final ClientUserMapper clientUserMapper;

    public ClientUserServiceImpl(ClientUserRepository clientUserRepository, ClientUserMapper clientUserMapper) {
        this.clientUserRepository = clientUserRepository;
        this.clientUserMapper = clientUserMapper;
    }

    /**
     * Save a clientUser.
     *
     * @param clientUserDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public ClientUserDTO save(ClientUserDTO clientUserDTO) {
        log.debug("Request to save ClientUser : {}", clientUserDTO);
        ClientUser clientUser = clientUserMapper.toEntity(clientUserDTO);
        clientUser = clientUserRepository.save(clientUser);
        return clientUserMapper.toDto(clientUser);
    }

    /**
     * Get all the clientUsers.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ClientUserDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ClientUsers");
        return clientUserRepository.findAll(pageable)
            .map(clientUserMapper::toDto);
    }


    /**
     * Get one clientUser by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ClientUserDTO> findOne(Long id) {
        log.debug("Request to get ClientUser : {}", id);
        return clientUserRepository.findById(id)
            .map(clientUserMapper::toDto);
    }

    /**
     * Delete the clientUser by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ClientUser : {}", id);        clientUserRepository.deleteById(id);
    }
}
