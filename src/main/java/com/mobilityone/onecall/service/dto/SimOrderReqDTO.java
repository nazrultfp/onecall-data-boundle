package com.mobilityone.onecall.service.dto;

import javax.validation.constraints.NotNull;

public class SimOrderReqDTO {

    private Long agentId;

//    @NotNull
//    private String agentCode;
    private String address;
    private String city;
    private String contact;
    private String country;
    private String courierTrackingUrl;
    private Double digitalPlanId;
    private String donorTelco;
    private String email;
    private String latLong;
    private String msisdn;
    private String name;
    private String postcode;
    private String source;
    private String state;
    private Double topupAmount;

    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

//    public String getAgentCode() {
//        return agentCode;
//    }
//
//    public void setAgentCode(String agentCode) {
//        this.agentCode = agentCode;
//    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCourierTrackingUrl() {
        return courierTrackingUrl;
    }

    public void setCourierTrackingUrl(String courierTrackingUrl) {
        this.courierTrackingUrl = courierTrackingUrl;
    }

    public Double getDigitalPlanId() {
        return digitalPlanId;
    }

    public void setDigitalPlanId(Double digitalPlanId) {
        this.digitalPlanId = digitalPlanId;
    }

    public String getDonorTelco() {
        return donorTelco;
    }

    public void setDonorTelco(String donorTelco) {
        this.donorTelco = donorTelco;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLatLong() {
        return latLong;
    }

    public void setLatLong(String latLong) {
        this.latLong = latLong;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Double getTopupAmount() {
        return topupAmount;
    }

    public void setTopupAmount(Double topupAmount) {
        this.topupAmount = topupAmount;
    }

    @Override
    public String toString() {
        return "SimOrderReqDTO{" +
            "agentId=" + agentId +
//            ", agentCode='" + agentCode + '\'' +
            ", address='" + address + '\'' +
            ", city='" + city + '\'' +
            ", contact='" + contact + '\'' +
            ", country='" + country + '\'' +
            ", courierTrackingUrl='" + courierTrackingUrl + '\'' +
            ", digitalPlanId=" + digitalPlanId +
            ", donorTelco='" + donorTelco + '\'' +
            ", email='" + email + '\'' +
            ", latLong='" + latLong + '\'' +
            ", msisdn='" + msisdn + '\'' +
            ", name='" + name + '\'' +
            ", postcode='" + postcode + '\'' +
            ", source='" + source + '\'' +
            ", state='" + state + '\'' +
            ", topupAmount=" + topupAmount +
            '}';
    }
}
