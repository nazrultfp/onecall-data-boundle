package com.mobilityone.onecall.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mobilityone.onecall.domain.enumeration.DiscountType;
import com.mobilityone.onecall.domain.enumeration.Status;

import java.time.ZonedDateTime;

public class PlanTransactionReportDTO {

    private ZonedDateTime transactionDate;
    private String orderType;
    private String orderCode;
    private String merchantCode;
    private String merchantName;
    private String description;
    private Long quantity;
    private Double totalAmount;
    private Double amount;
    private Double discount;
    @JsonIgnore
    private DiscountType discountType;
    private Double subTotal;
    private Double processingFee;
    private Double netAmount;
    private Status status;
    private String transactionId;

    public PlanTransactionReportDTO(ZonedDateTime transactionDate, String orderType, String orderCode, String merchantCode,
                                    String merchantName, String description, Long quantity, Double totalAmount,
                                    Double amount, Double discount, DiscountType discountType, Double processingFee,
                                    Double netAmount, Status status, String transactionId) {
        this.transactionDate = transactionDate;
        this.orderType = orderType;
        this.orderCode = orderCode;
        this.merchantCode = merchantCode;
        this.merchantName = merchantName;
        this.description = description;
        this.quantity = quantity;
        this.totalAmount = totalAmount;
        this.amount = amount;
        this.discount = discount;
        this.discountType = discountType;
        this.processingFee = processingFee;
        this.netAmount = netAmount;
        this.status = status;
        this.transactionId = transactionId;

        if (discountType.equals(DiscountType.PERCENT))
            subTotal = amount - (amount * discount / 100);
        else
            subTotal = amount - discount;
    }

    public ZonedDateTime getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(ZonedDateTime transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getMerchantCode() {
        return merchantCode;
    }

    public void setMerchantCode(String merchantCode) {
        this.merchantCode = merchantCode;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public DiscountType getDiscountType() {
        return discountType;
    }

    public void setDiscountType(DiscountType discountType) {
        this.discountType = discountType;
    }

    public Double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Double subTotal) {
        this.subTotal = subTotal;
    }

    public Double getProcessingFee() {
        return processingFee;
    }

    public void setProcessingFee(Double processingFee) {
        this.processingFee = processingFee;
    }

    public Double getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(Double netAmount) {
        this.netAmount = netAmount;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
}
