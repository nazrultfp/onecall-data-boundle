package com.mobilityone.onecall.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Favorite entity.
 */
public class FavoriteDTO implements Serializable {

    private Long id;

    private String phoneNo;

    private String prodCode;

    private String prodSubCode;

    private String catType;

    private String prodDesc;

    private String prodCodeAlt;

    private String prodDescLong;

    private String accountNumber;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getProdCode() {
        return prodCode;
    }

    public void setProdCode(String prodCode) {
        this.prodCode = prodCode;
    }

    public String getProdSubCode() {
        return prodSubCode;
    }

    public void setProdSubCode(String prodSubCode) {
        this.prodSubCode = prodSubCode;
    }

    public String getCatType() {
        return catType;
    }

    public void setCatType(String catType) {
        this.catType = catType;
    }

    public String getProdDesc() {
        return prodDesc;
    }

    public void setProdDesc(String prodDesc) {
        this.prodDesc = prodDesc;
    }

    public String getProdCodeAlt() {
        return prodCodeAlt;
    }

    public void setProdCodeAlt(String prodCodeAlt) {
        this.prodCodeAlt = prodCodeAlt;
    }

    public String getProdDescLong() {
        return prodDescLong;
    }

    public void setProdDescLong(String prodDescLong) {
        this.prodDescLong = prodDescLong;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FavoriteDTO favoriteDTO = (FavoriteDTO) o;
        if (favoriteDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), favoriteDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "FavoriteDTO{" +
            "id=" + getId() +
            ", phoneNo='" + getPhoneNo() + "'" +
            ", prodCode='" + getProdCode() + "'" +
            ", prodSubCode='" + getProdSubCode() + "'" +
            ", catType='" + getCatType() + "'" +
            ", prodDesc='" + getProdDesc() + "'" +
            ", prodCodeAlt='" + getProdCodeAlt() + "'" +
            ", prodDescLong='" + getProdDescLong() + "'" +
            ", accountNumber='" + getAccountNumber() + "'" +
            "}";
    }
}
