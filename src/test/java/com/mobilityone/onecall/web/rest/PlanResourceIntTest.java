package com.mobilityone.onecall.web.rest;

import com.mobilityone.onecall.OneCallApp;

import com.mobilityone.onecall.domain.Plan;
import com.mobilityone.onecall.repository.PlanRepository;
import com.mobilityone.onecall.service.PlanService;
import com.mobilityone.onecall.service.dto.PlanDTO;
import com.mobilityone.onecall.service.mapper.PlanMapper;
import com.mobilityone.onecall.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.mobilityone.onecall.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.mobilityone.onecall.domain.enumeration.PlanRenewal;
import com.mobilityone.onecall.domain.enumeration.PlanType;
import com.mobilityone.onecall.domain.enumeration.DataMode;
/**
 * Test class for the PlanResource REST controller.
 *
 * @see PlanResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OneCallApp.class)
public class PlanResourceIntTest {

    private static final String DEFAULT_KEYWORD = "AAAAAAAAAA";
    private static final String UPDATED_KEYWORD = "BBBBBBBBBB";

    private static final PlanRenewal DEFAULT_PLAN_RENEWAL = PlanRenewal.Daily;
    private static final PlanRenewal UPDATED_PLAN_RENEWAL = PlanRenewal.Weekly;

    private static final PlanType DEFAULT_PLAN_TYPE = PlanType.Data;
    private static final PlanType UPDATED_PLAN_TYPE = PlanType.Call;

    private static final String DEFAULT_AMOUNT = "AAAAAAAAAA";
    private static final String UPDATED_AMOUNT = "BBBBBBBBBB";

    private static final String DEFAULT_PRICE = "AAAAAAAAAA";
    private static final String UPDATED_PRICE = "BBBBBBBBBB";

    private static final DataMode DEFAULT_DATA_MODE = DataMode.VOICE;
    private static final DataMode UPDATED_DATA_MODE = DataMode.DATA;

    private static final String DEFAULT_EXPIRE_DAYS = "AAAAAAAAAA";
    private static final String UPDATED_EXPIRE_DAYS = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ENABLE = false;
    private static final Boolean UPDATED_ENABLE = true;

    private static final String DEFAULT_TITLE_A = "AAAAAAAAAA";
    private static final String UPDATED_TITLE_A = "BBBBBBBBBB";

    private static final String DEFAULT_SUBTITLE_A = "AAAAAAAAAA";
    private static final String UPDATED_SUBTITLE_A = "BBBBBBBBBB";

    private static final String DEFAULT_TITLE_B = "AAAAAAAAAA";
    private static final String UPDATED_TITLE_B = "BBBBBBBBBB";

    private static final String DEFAULT_SUBTITLE_B = "AAAAAAAAAA";
    private static final String UPDATED_SUBTITLE_B = "BBBBBBBBBB";

    private static final Boolean DEFAULT_UNLIMITED_CALL = false;
    private static final Boolean UPDATED_UNLIMITED_CALL = true;

    @Autowired
    private PlanRepository planRepository;

    @Autowired
    private PlanMapper planMapper;

    @Autowired
    private PlanService planService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restPlanMockMvc;

    private Plan plan;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PlanResource planResource = new PlanResource(planService);
        this.restPlanMockMvc = MockMvcBuilders.standaloneSetup(planResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Plan createEntity(EntityManager em) {
        Plan plan = new Plan()
            .keyword(DEFAULT_KEYWORD)
            .planRenewal(DEFAULT_PLAN_RENEWAL)
            .planType(DEFAULT_PLAN_TYPE)
            .amount(DEFAULT_AMOUNT)
            .price(DEFAULT_PRICE)
            .dataMode(DEFAULT_DATA_MODE)
            .expireDays(DEFAULT_EXPIRE_DAYS)
            .description(DEFAULT_DESCRIPTION)
            .enable(DEFAULT_ENABLE)
            .titleA(DEFAULT_TITLE_A)
            .subtitleA(DEFAULT_SUBTITLE_A)
            .titleB(DEFAULT_TITLE_B)
            .subtitleB(DEFAULT_SUBTITLE_B)
            .unlimitedCall(DEFAULT_UNLIMITED_CALL);
        return plan;
    }

    @Before
    public void initTest() {
        plan = createEntity(em);
    }

    @Test
    @Transactional
    public void createPlan() throws Exception {
        int databaseSizeBeforeCreate = planRepository.findAll().size();

        // Create the Plan
        PlanDTO planDTO = planMapper.toDto(plan);
        restPlanMockMvc.perform(post("/api/plans")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(planDTO)))
            .andExpect(status().isCreated());

        // Validate the Plan in the database
        List<Plan> planList = planRepository.findAll();
        assertThat(planList).hasSize(databaseSizeBeforeCreate + 1);
        Plan testPlan = planList.get(planList.size() - 1);
        assertThat(testPlan.getKeyword()).isEqualTo(DEFAULT_KEYWORD);
        assertThat(testPlan.getPlanRenewal()).isEqualTo(DEFAULT_PLAN_RENEWAL);
        assertThat(testPlan.getPlanType()).isEqualTo(DEFAULT_PLAN_TYPE);
        assertThat(testPlan.getAmount()).isEqualTo(DEFAULT_AMOUNT);
        assertThat(testPlan.getPrice()).isEqualTo(DEFAULT_PRICE);
        assertThat(testPlan.getDataMode()).isEqualTo(DEFAULT_DATA_MODE);
        assertThat(testPlan.getExpireDays()).isEqualTo(DEFAULT_EXPIRE_DAYS);
        assertThat(testPlan.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testPlan.isEnable()).isEqualTo(DEFAULT_ENABLE);
        assertThat(testPlan.getTitleA()).isEqualTo(DEFAULT_TITLE_A);
        assertThat(testPlan.getSubtitleA()).isEqualTo(DEFAULT_SUBTITLE_A);
        assertThat(testPlan.getTitleB()).isEqualTo(DEFAULT_TITLE_B);
        assertThat(testPlan.getSubtitleB()).isEqualTo(DEFAULT_SUBTITLE_B);
        assertThat(testPlan.isUnlimitedCall()).isEqualTo(DEFAULT_UNLIMITED_CALL);
    }

    @Test
    @Transactional
    public void createPlanWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = planRepository.findAll().size();

        // Create the Plan with an existing ID
        plan.setId(1L);
        PlanDTO planDTO = planMapper.toDto(plan);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPlanMockMvc.perform(post("/api/plans")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(planDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Plan in the database
        List<Plan> planList = planRepository.findAll();
        assertThat(planList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllPlans() throws Exception {
        // Initialize the database
        planRepository.saveAndFlush(plan);

        // Get all the planList
        restPlanMockMvc.perform(get("/api/plans?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(plan.getId().intValue())))
            .andExpect(jsonPath("$.[*].keyword").value(hasItem(DEFAULT_KEYWORD.toString())))
            .andExpect(jsonPath("$.[*].planRenewal").value(hasItem(DEFAULT_PLAN_RENEWAL.toString())))
            .andExpect(jsonPath("$.[*].planType").value(hasItem(DEFAULT_PLAN_TYPE.toString())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT.toString())))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE.toString())))
            .andExpect(jsonPath("$.[*].dataMode").value(hasItem(DEFAULT_DATA_MODE.toString())))
            .andExpect(jsonPath("$.[*].expireDays").value(hasItem(DEFAULT_EXPIRE_DAYS.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].enable").value(hasItem(DEFAULT_ENABLE.booleanValue())))
            .andExpect(jsonPath("$.[*].titleA").value(hasItem(DEFAULT_TITLE_A.toString())))
            .andExpect(jsonPath("$.[*].subtitleA").value(hasItem(DEFAULT_SUBTITLE_A.toString())))
            .andExpect(jsonPath("$.[*].titleB").value(hasItem(DEFAULT_TITLE_B.toString())))
            .andExpect(jsonPath("$.[*].subtitleB").value(hasItem(DEFAULT_SUBTITLE_B.toString())))
            .andExpect(jsonPath("$.[*].unlimitedCall").value(hasItem(DEFAULT_UNLIMITED_CALL.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getPlan() throws Exception {
        // Initialize the database
        planRepository.saveAndFlush(plan);

        // Get the plan
        restPlanMockMvc.perform(get("/api/plans/{id}", plan.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(plan.getId().intValue()))
            .andExpect(jsonPath("$.keyword").value(DEFAULT_KEYWORD.toString()))
            .andExpect(jsonPath("$.planRenewal").value(DEFAULT_PLAN_RENEWAL.toString()))
            .andExpect(jsonPath("$.planType").value(DEFAULT_PLAN_TYPE.toString()))
            .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT.toString()))
            .andExpect(jsonPath("$.price").value(DEFAULT_PRICE.toString()))
            .andExpect(jsonPath("$.dataMode").value(DEFAULT_DATA_MODE.toString()))
            .andExpect(jsonPath("$.expireDays").value(DEFAULT_EXPIRE_DAYS.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.enable").value(DEFAULT_ENABLE.booleanValue()))
            .andExpect(jsonPath("$.titleA").value(DEFAULT_TITLE_A.toString()))
            .andExpect(jsonPath("$.subtitleA").value(DEFAULT_SUBTITLE_A.toString()))
            .andExpect(jsonPath("$.titleB").value(DEFAULT_TITLE_B.toString()))
            .andExpect(jsonPath("$.subtitleB").value(DEFAULT_SUBTITLE_B.toString()))
            .andExpect(jsonPath("$.unlimitedCall").value(DEFAULT_UNLIMITED_CALL.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingPlan() throws Exception {
        // Get the plan
        restPlanMockMvc.perform(get("/api/plans/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePlan() throws Exception {
        // Initialize the database
        planRepository.saveAndFlush(plan);

        int databaseSizeBeforeUpdate = planRepository.findAll().size();

        // Update the plan
        Plan updatedPlan = planRepository.findById(plan.getId()).get();
        // Disconnect from session so that the updates on updatedPlan are not directly saved in db
        em.detach(updatedPlan);
        updatedPlan
            .keyword(UPDATED_KEYWORD)
            .planRenewal(UPDATED_PLAN_RENEWAL)
            .planType(UPDATED_PLAN_TYPE)
            .amount(UPDATED_AMOUNT)
            .price(UPDATED_PRICE)
            .dataMode(UPDATED_DATA_MODE)
            .expireDays(UPDATED_EXPIRE_DAYS)
            .description(UPDATED_DESCRIPTION)
            .enable(UPDATED_ENABLE)
            .titleA(UPDATED_TITLE_A)
            .subtitleA(UPDATED_SUBTITLE_A)
            .titleB(UPDATED_TITLE_B)
            .subtitleB(UPDATED_SUBTITLE_B)
            .unlimitedCall(UPDATED_UNLIMITED_CALL);
        PlanDTO planDTO = planMapper.toDto(updatedPlan);

        restPlanMockMvc.perform(put("/api/plans")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(planDTO)))
            .andExpect(status().isOk());

        // Validate the Plan in the database
        List<Plan> planList = planRepository.findAll();
        assertThat(planList).hasSize(databaseSizeBeforeUpdate);
        Plan testPlan = planList.get(planList.size() - 1);
        assertThat(testPlan.getKeyword()).isEqualTo(UPDATED_KEYWORD);
        assertThat(testPlan.getPlanRenewal()).isEqualTo(UPDATED_PLAN_RENEWAL);
        assertThat(testPlan.getPlanType()).isEqualTo(UPDATED_PLAN_TYPE);
        assertThat(testPlan.getAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testPlan.getPrice()).isEqualTo(UPDATED_PRICE);
        assertThat(testPlan.getDataMode()).isEqualTo(UPDATED_DATA_MODE);
        assertThat(testPlan.getExpireDays()).isEqualTo(UPDATED_EXPIRE_DAYS);
        assertThat(testPlan.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testPlan.isEnable()).isEqualTo(UPDATED_ENABLE);
        assertThat(testPlan.getTitleA()).isEqualTo(UPDATED_TITLE_A);
        assertThat(testPlan.getSubtitleA()).isEqualTo(UPDATED_SUBTITLE_A);
        assertThat(testPlan.getTitleB()).isEqualTo(UPDATED_TITLE_B);
        assertThat(testPlan.getSubtitleB()).isEqualTo(UPDATED_SUBTITLE_B);
        assertThat(testPlan.isUnlimitedCall()).isEqualTo(UPDATED_UNLIMITED_CALL);
    }

    @Test
    @Transactional
    public void updateNonExistingPlan() throws Exception {
        int databaseSizeBeforeUpdate = planRepository.findAll().size();

        // Create the Plan
        PlanDTO planDTO = planMapper.toDto(plan);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPlanMockMvc.perform(put("/api/plans")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(planDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Plan in the database
        List<Plan> planList = planRepository.findAll();
        assertThat(planList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePlan() throws Exception {
        // Initialize the database
        planRepository.saveAndFlush(plan);

        int databaseSizeBeforeDelete = planRepository.findAll().size();

        // Delete the plan
        restPlanMockMvc.perform(delete("/api/plans/{id}", plan.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Plan> planList = planRepository.findAll();
        assertThat(planList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Plan.class);
        Plan plan1 = new Plan();
        plan1.setId(1L);
        Plan plan2 = new Plan();
        plan2.setId(plan1.getId());
        assertThat(plan1).isEqualTo(plan2);
        plan2.setId(2L);
        assertThat(plan1).isNotEqualTo(plan2);
        plan1.setId(null);
        assertThat(plan1).isNotEqualTo(plan2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PlanDTO.class);
        PlanDTO planDTO1 = new PlanDTO();
        planDTO1.setId(1L);
        PlanDTO planDTO2 = new PlanDTO();
        assertThat(planDTO1).isNotEqualTo(planDTO2);
        planDTO2.setId(planDTO1.getId());
        assertThat(planDTO1).isEqualTo(planDTO2);
        planDTO2.setId(2L);
        assertThat(planDTO1).isNotEqualTo(planDTO2);
        planDTO1.setId(null);
        assertThat(planDTO1).isNotEqualTo(planDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(planMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(planMapper.fromId(null)).isNull();
    }
}
