package com.mobilityone.onecall.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TuneTalkRespDTO {
    private String apiKey;
    private String code;
    private String message;

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "TuneTalkRespDTO{" +
            "apiKey='" + apiKey + '\'' +
            ", code='" + code + '\'' +
            ", message='" + message + '\'' +
            '}';
    }
}
