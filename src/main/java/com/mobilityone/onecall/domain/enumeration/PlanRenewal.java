package com.mobilityone.onecall.domain.enumeration;

/**
 * The PlanRenewal enumeration.
 */
public enum PlanRenewal {
    Daily, Weekly, Monthly, Yearly, Booster
}
