package com.mobilityone.onecall.config;

import com.mobilityone.onecall.config.jwtfilter.JwtAuthenticationFilter;
import com.mobilityone.onecall.config.jwtfilter.JwtAuthorizationFilter;
import com.mobilityone.onecall.security.*;
import com.mobilityone.onecall.security.jwt.*;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.zalando.problem.spring.web.advice.security.SecurityProblemSupport;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
@Import(SecurityProblemSupport.class)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final TokenProvider tokenProvider;

    private final SecurityProblemSupport problemSupport;

    private final ApplicationProperties properties;

//    @Autowired
    private final UserDetailsService jwtUserDetailService;

    public SecurityConfiguration(TokenProvider tokenProvider, SecurityProblemSupport problemSupport, ApplicationProperties properties, UserDetailsService jwtUserDetailService) {
        this.tokenProvider = tokenProvider;
        this.problemSupport = problemSupport;
        this.properties = properties;
        this.jwtUserDetailService = jwtUserDetailService;
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
            .antMatchers(HttpMethod.OPTIONS, "/**")
            .antMatchers("/h2-console/**")
            .antMatchers("/swagger-ui/index.html")
            .antMatchers("/test/**");
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
            .csrf()
            .disable()
            .exceptionHandling()
            .authenticationEntryPoint(problemSupport)
            .accessDeniedHandler(problemSupport)
        .and()
            .headers()
            .frameOptions()
            .disable()
        .and()
            .sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
            .authorizeRequests()
            .antMatchers(HttpMethod.POST,"/api/dealer-login").permitAll()
            .antMatchers(HttpMethod.POST,"/api/client-login").permitAll()
            .antMatchers("/api/one-calls/telco-list").permitAll()
            .antMatchers("/api/settings").permitAll()
            .antMatchers("/api/agents").permitAll()  // TODO remove on prod
            .antMatchers("/api/agents/**").permitAll()  // TODO remove on prod
            .antMatchers("/api/merchants").permitAll() // TODO remove on prod
            .antMatchers("/api/merchants/**").permitAll() // TODO remove on prod
            .antMatchers("/api/**").authenticated()
            .antMatchers("/management/health").permitAll()
            .antMatchers("/management/info").permitAll()
            .antMatchers("/management/**").hasAuthority(AuthoritiesConstants.ADMIN)
        .and()
            .addFilter(new JwtAuthenticationFilter(authenticationManager(), properties))
            .addFilter(new JwtAuthorizationFilter(authenticationManager(), properties));
//            .apply(securityConfigurerAdapter());

    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(jwtUserDetailService).passwordEncoder(passwordEncoder());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    protected AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

    private JWTConfigurer securityConfigurerAdapter() {
        return new JWTConfigurer(tokenProvider);
    }
}
