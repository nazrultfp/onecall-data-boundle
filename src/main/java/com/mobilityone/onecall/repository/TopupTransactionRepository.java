package com.mobilityone.onecall.repository;

import com.mobilityone.onecall.domain.TopupTransaction;
import com.mobilityone.onecall.service.dto.TopupTransactionReportDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the TopupTransaction entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TopupTransactionRepository extends JpaRepository<TopupTransaction, Long> {

    @Query("select new com.mobilityone.onecall.service.dto.TopupTransactionReportDTO(tt.transactionDate, m.merchantName, " +
        "m.merchantCode, tt.amount, tt.status, tt.message) from TopupTransaction tt inner join Merchant m on tt.merchant.id = m.id ")
    Page<TopupTransactionReportDTO> getTopupReport(Pageable pageable);
}
