package com.mobilityone.onecall.web.rest;

import com.mobilityone.onecall.service.OneCallService;
import com.mobilityone.onecall.service.TuneTalkService;
import com.mobilityone.onecall.service.dto.*;
import com.mobilityone.onecall.service.mapper.TopupMapper;
import com.mobilityone.onecall.web.rest.errors.BadRequestAlertException;
import com.mobilityone.onecall.web.rest.util.HeaderUtil;
import com.mobilityone.onecall.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * REST controller for managing OneCall.
 */
@RestController
@RequestMapping("/api")
public class OneCallResource {

    private final Logger log = LoggerFactory.getLogger(OneCallResource.class);

    private static final String ENTITY_NAME = "oneCallOneCall";

    private final OneCallService oneCallService;

    private final TuneTalkService tuneTalkService;

    private final TopupMapper topupMapper;

    public OneCallResource(OneCallService oneCallService, TuneTalkService tuneTalkService, TopupMapper topupMapper) {
        this.oneCallService = oneCallService;
        this.tuneTalkService = tuneTalkService;
        this.topupMapper = topupMapper;
    }

    /**
     * POST  /one-calls : Create a new oneCall.
     *
     * @param oneCallDTO the oneCallDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new oneCallDTO, or with status 400 (Bad Request) if the oneCall has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    //@PostMapping("/one-calls")
    public ResponseEntity<OneCallDTO> createOneCall(@Valid @RequestBody OneCallDTO oneCallDTO) throws URISyntaxException {
        log.debug("REST request to save OneCall : {}", oneCallDTO);
        if (oneCallDTO.getId() != null) {
            throw new BadRequestAlertException("A new oneCall cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OneCallDTO result = oneCallService.save(oneCallDTO);
        return ResponseEntity.created(new URI("/api/one-calls/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /one-calls : Updates an existing oneCall.
     *
     * @param oneCallDTO the oneCallDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated oneCallDTO,
     * or with status 400 (Bad Request) if the oneCallDTO is not valid,
     * or with status 500 (Internal Server Error) if the oneCallDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    //@PutMapping("/one-calls")
    public ResponseEntity<OneCallDTO> updateOneCall(@Valid @RequestBody OneCallDTO oneCallDTO) throws URISyntaxException {
        log.debug("REST request to update OneCall : {}", oneCallDTO);
        if (oneCallDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        OneCallDTO result = oneCallService.save(oneCallDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, oneCallDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /one-calls : get all the oneCalls.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of oneCalls in body
     */
    @GetMapping("/one-calls")
    public ResponseEntity<List<OneCallDTO>> getAllOneCalls(Pageable pageable) {
        log.debug("REST request to get a page of OneCalls");
        Page<OneCallDTO> page = oneCallService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/one-calls");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * GET  /one-calls/:id : get the "id" oneCall.
     *
     * @param id the id of the oneCallDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the oneCallDTO, or with status 404 (Not Found)
     */
    @GetMapping("/one-calls/{id}")
    public ResponseEntity<OneCallDTO> getOneCall(@PathVariable Long id) {
        log.debug("REST request to get OneCall : {}", id);
        Optional<OneCallDTO> oneCallDTO = oneCallService.findOne(id);
        return ResponseUtil.wrapOrNotFound(oneCallDTO);
    }

    /**
     * DELETE  /one-calls/:id : delete the "id" oneCall.
     *
     * @param id the id of the oneCallDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    //@DeleteMapping("/one-calls/{id}")
    public ResponseEntity<Void> deleteOneCall(@PathVariable Long id) {
        log.debug("REST request to delete OneCall : {}", id);
        oneCallService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @ApiOperation(value = "Get Account Balance By ICCID")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Request Successful!"),
        @ApiResponse(code = 401, message = "Empty Secret Key!"),
        @ApiResponse(code = 403, message = "Invalid Secret Key! or Restricted Access!"),
        @ApiResponse(code = 500, message = "Request Failed!")
    })
    @GetMapping(value = "/one-calls/iccid-balance/{iccid}", produces = "application/json")
    public ResponseEntity<TtBalabceRespDTO> getBalanceByIccid(@PathVariable String iccid) {
        log.info("Reuest to Get Balance for {} Iccid.", iccid);

        TtBalabceRespDTO balance = oneCallService.getBalanceByIccid(iccid);

//        log.info("Response : {}", balance.toString());

        int abs = Math.abs(Integer.parseInt(balance.getCode()));
        abs = abs < 0 ? abs * -1 : abs;
        log.info("code abs : {}", abs);


        return ResponseEntity.status(HttpStatus.valueOf(abs))
            .header("Content-Type: application/json; charset=UTF-8").body(balance);
    }

    @ApiOperation(value = "Get Account Balance By IMSI Number")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Request Successful!"),
        @ApiResponse(code = 401, message = "Empty Secret Key!"),
        @ApiResponse(code = 403, message = "Invalid Secret Key! or Restricted Access!"),
        @ApiResponse(code = 500, message = "Request Failed!")
    })
    @GetMapping(value = "/one-calls/imsi-balance/{imsi}", produces = "application/json")
    public ResponseEntity<TtBalabceRespDTO> getBalanceByImsi(@PathVariable String imsi) {
        log.info("Reuest to Get Balance for {} Imsi.", imsi);

        TtBalabceRespDTO balance = oneCallService.getBalanceByImsi(imsi);

//        log.info("Response : {}", balance.toString());

        int abs = Math.abs(Integer.parseInt(balance.getCode()));
        abs = abs < 0 ? abs * -1 : abs;
        log.info("code abs : {}", abs);


        return ResponseEntity.status(HttpStatus.valueOf(abs))
            .header("Content-Type: application/json; charset=UTF-8").body(balance);
    }

    @ApiOperation(value = "Get Account Balance By Msisdn")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Request Successful!"),
        @ApiResponse(code = 401, message = "Empty Secret Key!"),
        @ApiResponse(code = 403, message = "Invalid Secret Key! or Restricted Access!"),
        @ApiResponse(code = 500, message = "Request Failed!")
    })
    @GetMapping(value = "/one-calls/msisdn-balance/{msisdn}", produces = "application/json")
    public ResponseEntity<TtBalabceRespDTO> getBalanceByMsisdn(@PathVariable String msisdn) {
        log.info("Reuest to Get Balance for {} Msisdn.", msisdn);

        TtBalabceRespDTO balance = oneCallService.getBalanceByMsisdn(msisdn);

//        log.info("Response : {}", balance.toString());

        int abs = Math.abs(Integer.parseInt(balance.getCode()));
        abs = abs < 0 ? abs * -1 : abs;
        log.info("code abs : {}", abs);

        return ResponseEntity.status(HttpStatus.valueOf(abs))
            .header("Content-Type: application/json; charset=UTF-8").body(balance);
    }

    @ApiOperation(value = "Convert BIG Points")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Request Successful!"),
        @ApiResponse(code = 401, message = "Empty Secret Key!"),
        @ApiResponse(code = 403, message = "Invalid Secret Key! or Restricted Access!"),
        @ApiResponse(code = 500, message = "Request Failed!")
    })
    @PostMapping(value = "/one-calls/bigshot", produces = "application/json")
    public ResponseEntity<TuneTalkRespDTO> bigShotConvert(@Valid @RequestBody BigShotConvertReqDTO bigShotConvertReqDTO) {
        log.info("Convert BIG Points. {}", bigShotConvertReqDTO.toString());

        TuneTalkRespDTO tuneTalkRespDTO = oneCallService.bigShotConvert(bigShotConvertReqDTO);

//        TuneTalkRespDTO tuneTalkRespDTO = tuneTalkService.bigShotConvert(bigShotConvertReqDTO);

        log.info("Response : {}", tuneTalkRespDTO.toString());

        int abs = Math.abs(Integer.parseInt(tuneTalkRespDTO.getCode()));
        abs = abs < 0 ? abs * -1 : abs;
        log.info("code abs : {}", abs);


        return ResponseEntity.status(HttpStatus.valueOf(abs))
            .header("Content-Type: application/json; charset=UTF-8").body(tuneTalkRespDTO);
    }

    @ApiOperation(value = "Get BIG Reward account via email")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Request Successful!"),
        @ApiResponse(code = 401, message = "Empty Secret Key!"),
        @ApiResponse(code = 403, message = "Invalid Secret Key! or Restricted Access!"),
        @ApiResponse(code = 500, message = "Request Failed!")
    })
    @PostMapping(value = "/one-calls/bigshot-lookup/{email}")
    public ResponseEntity<TuneTalkRespDTO> lookupByEmail(@PathVariable String email) {
        log.info("Look Up for Big Shot by Email : {}", email);

        TuneTalkRespDTO tuneTalkRespDTO = oneCallService.lookupByEmail(email);

        log.info("Response : {}", tuneTalkRespDTO.toString());

        int abs = Math.abs(Integer.parseInt(tuneTalkRespDTO.getCode()));
        abs = abs < 0 ? abs * -1 : abs;
        log.info("code abs : {}", abs);


        return ResponseEntity.status(HttpStatus.valueOf(abs))
            .header("Content-Type: application/json; charset=UTF-8").body(tuneTalkRespDTO);
    }

    @ApiOperation(value = "Cancel location by MSISDN number")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Request Successful!"),
        @ApiResponse(code = 401, message = "Empty Secret Key!"),
        @ApiResponse(code = 403, message = "Invalid Secret Key! or Restricted Access!"),
        @ApiResponse(code = 500, message = "Request Failed!")
    })
    @GetMapping(value = "/one-calls/cancel-location/{msisdn}", produces = "application/json")
    public ResponseEntity<TuneTalkRespDTO> cancelLocationByMsidn(@PathVariable String msisdn) {
        log.info("Cancel location by MSISDN number : {}", msisdn);

        TuneTalkRespDTO tuneTalkRespDTO = tuneTalkService.cancelLocationByMsidn(msisdn);

        log.info("Response : {}", tuneTalkRespDTO.toString());

        int abs = Math.abs(Integer.parseInt(tuneTalkRespDTO.getCode()));
        abs = abs < 0 ? abs * -1 : abs;
        log.info("code abs : {}", abs);


        return ResponseEntity.status(HttpStatus.valueOf(abs))
            .header("Content-Type: application/json; charset=UTF-8").body(tuneTalkRespDTO);
    }

    @ApiOperation(value = "Debit Balance")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Request Successful!"),
        @ApiResponse(code = 401, message = "Empty Secret Key!"),
        @ApiResponse(code = 403, message = "Invalid Secret Key! or Restricted Access!"),
        @ApiResponse(code = 500, message = "Request Failed!")
    })
    @PostMapping(value = "/one-calls/debit-balance", produces = "application/json")
    public ResponseEntity<DebitBalanceRespDTO> debitBalance(@Valid @RequestBody DebitBalanceReqDTO debitBalanceReqDTO) {
        log.info("Debit Balance : {}", debitBalanceReqDTO.toString());

        DebitBalanceRespDTO debitBalanceRespDTO = tuneTalkService.debitBalance(debitBalanceReqDTO);

        log.info("Response : {}", debitBalanceRespDTO.toString());

        int abs = Math.abs(Integer.parseInt(debitBalanceRespDTO.getCode()));
        abs = abs < 0 ? abs * -1 : abs;
        log.info("code abs : {}", abs);


        return ResponseEntity.status(HttpStatus.valueOf(abs))
            .header("Content-Type: application/json; charset=UTF-8").body(debitBalanceRespDTO);
    }


    @ApiOperation(value = "Get Msisdn List")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Request Successful!"),
        @ApiResponse(code = 401, message = "Empty Secret Key!"),
        @ApiResponse(code = 403, message = "Invalid Secret Key! or Restricted Access!"),
        @ApiResponse(code = 500, message = "Request Failed!")
    })
    @GetMapping(value = "/one-calls/msisdn-list/{idNumber}", produces = "application/json")
    public ResponseEntity<?> getMsisdnListByIdNumber(@PathVariable String idNumber) {
        log.info("Get Msisdn List for : {}", idNumber);

        JSONObject jsonObject = tuneTalkService.getMsisdnListByIdNumber(idNumber);

        log.info("Response : {}", jsonObject.toString());

        int abs = Math.abs(Integer.parseInt(jsonObject.getString("code")));
        abs = abs < 0 ? abs * -1 : abs;
        log.info("code abs : {}", abs);


        return ResponseEntity.status(HttpStatus.valueOf(abs))
            .header("Content-Type: application/json; charset=UTF-8").body(jsonObject.toString());
    }

    @ApiOperation(value = "Get Port In Status By MSISDN")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Request Successful!"),
        @ApiResponse(code = 401, message = "Empty Secret Key!"),
        @ApiResponse(code = 403, message = "Invalid Secret Key! or Restricted Access!"),
        @ApiResponse(code = 500, message = "Request Failed!")
    })
    @GetMapping(value = "/one-calls/port-in-status/{msisdn}", produces = "application/json")
    public ResponseEntity<?> getPortStatusByMsisdn(@PathVariable String msisdn) {
        log.info("Get Port In Status By MSISDN : {}", msisdn);

        JSONObject jsonObject = tuneTalkService.getPortStatusByMsisdn(msisdn);

        log.info("Response : {}", jsonObject.toString());

        if (jsonObject.has("code") && "200".equals(jsonObject.getString("code")))
            oneCallService.setPortInStatusByMsisdn(msisdn, jsonObject.getString("statusResp"));

        int abs = Math.abs(Integer.parseInt(jsonObject.getString("code")));
        abs = abs < 0 ? abs * -1 : abs;
        log.info("code abs : {}", abs);


        return ResponseEntity.status(HttpStatus.valueOf(abs))
            .header("Content-Type: application/json; charset=UTF-8").body(jsonObject.toString());
    }

    @ApiOperation(value = "Get List of Prefer Number")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Request Successful!"),
        @ApiResponse(code = 401, message = "Empty Secret Key!"),
        @ApiResponse(code = 403, message = "Invalid Secret Key! or Restricted Access!"),
        @ApiResponse(code = 500, message = "Request Failed!")
    })
    @GetMapping(value = "/one-calls/prefer-number/{prefix}", produces = "application/json")
    public ResponseEntity<?> getPreferNumberList(@PathVariable String prefix) {
        log.info("Get list of prefer number with prefix : {}", prefix);

        JSONObject jsonObject = tuneTalkService.getPreferNumberList(prefix);

        log.info("Response : {}", jsonObject.toString());

        int abs = Math.abs(Integer.parseInt(jsonObject.getString("code")));
        abs = abs < 0 ? abs * -1 : abs;
        log.info("code abs : {}", abs);


        return ResponseEntity.status(HttpStatus.valueOf(abs))
            .header("Content-Type: application/json; charset=UTF-8").body(jsonObject.toString());
    }

    @ApiOperation(value = "Get Member Profile by ICCID")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Request Successful!"),
        @ApiResponse(code = 401, message = "Empty Secret Key!"),
        @ApiResponse(code = 403, message = "Invalid Secret Key! or Restricted Access!"),
        @ApiResponse(code = 500, message = "Request Failed!")
    })
    @GetMapping(value = "/one-calls/profile-by-iccid/{iccid}", produces = "application/json")
    public ResponseEntity<?> getMemberProfileByIccid(@PathVariable String iccid) {
        log.info("Get Member Profile by ICCID : {}", iccid);

        JSONObject jsonObject = tuneTalkService.getMemberProfileByIccid(iccid);

        log.info("Response : {}", jsonObject.toString());

        int abs = Math.abs(Integer.parseInt(jsonObject.getString("code")));
        abs = abs < 0 ? abs * -1 : abs;
        log.info("code abs : {}", abs);


        return ResponseEntity.status(HttpStatus.valueOf(abs))
            .header("Content-Type: application/json; charset=UTF-8").body(jsonObject.toString());
    }

    @ApiOperation(value = "Get Member Profile by IMSI number")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Request Successful!"),
        @ApiResponse(code = 401, message = "Empty Secret Key!"),
        @ApiResponse(code = 403, message = "Invalid Secret Key! or Restricted Access!"),
        @ApiResponse(code = 500, message = "Request Failed!")
    })
    @GetMapping(value = "/one-calls/profile-by-imsi/{imsi}", produces = "application/json")
    public ResponseEntity<?> getMemberProfileByImsiNumber(@PathVariable String imsi) {
        log.info("Get Member Profile by IMSI number : {}", imsi);

        JSONObject jsonObject = tuneTalkService.getMemberProfileByImsiNumber(imsi);

        log.info("Response : {}", jsonObject.toString());

        int abs = Math.abs(Integer.parseInt(jsonObject.getString("code")));
        abs = abs < 0 ? abs * -1 : abs;
        log.info("code abs : {}", abs);


        return ResponseEntity.status(HttpStatus.valueOf(abs))
            .header("Content-Type: application/json; charset=UTF-8").body(jsonObject.toString());
    }

    @ApiOperation(value = "Get Member Profile by MSISDN")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Request Successful!"),
        @ApiResponse(code = 401, message = "Empty Secret Key!"),
        @ApiResponse(code = 403, message = "Invalid Secret Key! or Restricted Access!"),
        @ApiResponse(code = 500, message = "Request Failed!")
    })
    @GetMapping(value = "/one-calls/profile-by-msisdn/{msisdn}", produces = "application/json")
    public ResponseEntity<?> getMemberProfileByMsisdn(@PathVariable String msisdn) {
        log.info("Get Member Profile by MSISDN : {}", msisdn);

        JSONObject jsonObject = tuneTalkService.getMemberProfileByMsisdn(msisdn);

        log.info("Response : {}", jsonObject.toString());

        int abs = Math.abs(Integer.parseInt(jsonObject.getString("code")));
        abs = abs < 0 ? abs * -1 : abs;
        log.info("code abs : {}", abs);


        return ResponseEntity.status(HttpStatus.valueOf(abs))
            .header("Content-Type: application/json; charset=UTF-8").body(jsonObject.toString());
    }

    @ApiOperation(value = "Check SIM registered status")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Request Successful!"),
        @ApiResponse(code = 401, message = "Empty Secret Key!"),
        @ApiResponse(code = 403, message = "Invalid Secret Key! or Restricted Access!"),
        @ApiResponse(code = 500, message = "Request Failed!")
    })
    @GetMapping(value = "/one-calls/sim-registration-status/{simNumber}", produces = "application/json")
    public ResponseEntity<TuneTalkRespDTO> checkSimRegisteredStatus(@PathVariable String simNumber) {
        log.info("Check SIM registered status for SIM number : {}", simNumber);

        TuneTalkRespDTO tuneTalkRespDTO = tuneTalkService.checkSimRegisteredStatus(simNumber);

        log.info("Response : {}", tuneTalkRespDTO.toString());

        int abs = Math.abs(Integer.parseInt(tuneTalkRespDTO.getCode()));
        abs = abs < 0 ? abs * -1 : abs;
        log.info("code abs : {}", abs);


        return ResponseEntity.status(HttpStatus.valueOf(abs))
            .header("Content-Type: application/json; charset=UTF-8").body(tuneTalkRespDTO);
    }

    @ApiOperation(value = "SIM Order")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Request Successful!"),
        @ApiResponse(code = 401, message = "Empty Secret Key!"),
        @ApiResponse(code = 403, message = "Invalid Secret Key! or Restricted Access!"),
        @ApiResponse(code = 500, message = "Request Failed!")
    })
    @PostMapping(value = "/one-calls/sim-order", produces = "application/json")
    public ResponseEntity<?> simOrder(@Valid @RequestBody SimOrderReqDTO simOrderReqDTO) {
        log.info("SIM Order : {}", simOrderReqDTO.toString());

        Object object = oneCallService.simOrder(simOrderReqDTO);
//        JSONObject jsonObject = tuneTalkService.simOrder(simOrderReqDTO);

        log.info("Response : {}", object);

        SimOrderDTO simOrderDTO = null;
        if (object instanceof SimOrderDTO) {
            simOrderDTO = (SimOrderDTO) object;
        }
        else if (object instanceof JSONObject) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(((JSONObject) object).toString());
        }

        int abs = Math.abs(Integer.parseInt(simOrderDTO.getCode()));
        abs = abs < 0 ? abs * -1 : abs;
        log.info("code abs : {}", abs);


        return ResponseEntity.status(HttpStatus.valueOf(abs))
            .header("Content-Type: application/json; charset=UTF-8").body(simOrderDTO);
    }

    @ApiOperation(value = "Sim Port In")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Request Successful!"),
        @ApiResponse(code = 401, message = "Empty Secret Key!"),
        @ApiResponse(code = 403, message = "Invalid Secret Key! or Restricted Access!"),
        @ApiResponse(code = 500, message = "Request Failed!")
    })
    @PostMapping(value = "/one-calls/sim-port-in", produces = "application/json")
    public ResponseEntity<?> simPortIn(@Valid @RequestBody SimPortInReqDTO simPortInReqDTO) {
        log.info("Sim Port In : {}", simPortInReqDTO.toString());

        Object object = oneCallService.simPortIn(simPortInReqDTO);

//        TuneTalkRespDTO tuneTalkRespDTO = tuneTalkService.simPortIn(simPortInReqDTO);

        log.info("Response : {}", object);

        SimPortInDTO simPortInDTO = null;
        if (object instanceof SimPortInDTO) {
            simPortInDTO = (SimPortInDTO) object;
        }
        else if (object instanceof JSONObject) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(((JSONObject) object).toString());
        }

        int abs = Math.abs(Integer.parseInt(simPortInDTO.getCode()));
        abs = abs < 0 ? abs * -1 : abs;
        log.info("code abs : {}", abs);


        return ResponseEntity.status(HttpStatus.valueOf(abs))
            .header("Content-Type: application/json; charset=UTF-8").body(simPortInDTO);
    }

    @ApiOperation(value = "Sim Registration")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Request Successful!"),
        @ApiResponse(code = 401, message = "Empty Secret Key!"),
        @ApiResponse(code = 403, message = "Invalid Secret Key! or Restricted Access!"),
        @ApiResponse(code = 500, message = "Request Failed!")
    })
    @PostMapping(value = "/one-calls/sim-registration", produces = "application/json")
    public ResponseEntity<?> simRegistration(@Valid @RequestBody SimRegReqDTO simRegReqDTO) {
        log.info("Sim Registration : {}", simRegReqDTO.toString());

        Object object = oneCallService.simRegistration(simRegReqDTO);

        log.debug("Response from TT : {}", object);

        SimRegisterDTO simRegisterDTO = null;
        if (object instanceof SimRegisterDTO) {
            simRegisterDTO = (SimRegisterDTO) object;
        }
        else if (object instanceof JSONObject) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(((JSONObject) object).toString());
        }

//        JSONObject jsonObject = tuneTalkService.simRegistration(simRegReqDTO);

//        log.info("Response : {}", simRegisterDTO);

        int abs = Math.abs(Integer.parseInt(simRegisterDTO.getCode()));
        abs = abs < 0 ? abs * -1 : abs;
        log.info("code abs : {}", abs);


        return ResponseEntity.status(HttpStatus.valueOf(abs))
            .header("Content-Type: application/json; charset=UTF-8").body(simRegisterDTO);
    }

    @ApiOperation(value = "Sim Registration without KYC")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Request Successful!"),
        @ApiResponse(code = 401, message = "Empty Secret Key!"),
        @ApiResponse(code = 403, message = "Invalid Secret Key! or Restricted Access!"),
        @ApiResponse(code = 500, message = "Request Failed!")
    })
    @PostMapping(value = "/one-calls/sim-registration-without-kyc", produces = "application/json")
    public ResponseEntity<?> simRegistrationWithoutKYC (@Valid @RequestBody SimRegReqDTO simRegReqDTO) {
        log.info("Sim Registration without KYC : {}", simRegReqDTO.toString());

        Object object = oneCallService.simRegistrationWithoutKYC(simRegReqDTO);

        log.debug("Response from TT : {}", object);

        SimRegisterDTO simRegisterDTO = null;
        if (object instanceof SimRegisterDTO) {
            simRegisterDTO = (SimRegisterDTO) object;
        }
        else if (object instanceof JSONObject) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(((JSONObject) object).toString());
        }

//        JSONObject jsonObject = tuneTalkService.simRegistration(simRegReqDTO);

//        log.info("Response : {}", simRegisterDTO);

        int abs = Math.abs(Integer.parseInt(simRegisterDTO.getCode()));
        abs = abs < 0 ? abs * -1 : abs;
        log.info("code abs : {}", abs);


        return ResponseEntity.status(HttpStatus.valueOf(abs))
            .header("Content-Type: application/json; charset=UTF-8").body(simRegisterDTO);
    }

    @ApiOperation(value = "Perform subscription")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Request Successful!"),
        @ApiResponse(code = 401, message = "Empty Secret Key!"),
        @ApiResponse(code = 403, message = "Invalid Secret Key! or Restricted Access!"),
        @ApiResponse(code = 500, message = "Request Failed!")
    })
    @PostMapping(value = "/one-calls/perform-subscription", produces = "application/json")
    public ResponseEntity<SubscriptionPlanDTO> performSubscription(@Valid @RequestBody SubscriptionReqDTO subscriptionReqDTO) {
        log.info("REST Request to Perform subscription : {}", subscriptionReqDTO.toString());

        SubscriptionPlanDTO subscriptionPlanDTO = oneCallService.performSubscription(subscriptionReqDTO);

//        TuneTalkRespDTO tuneTalkRespDTO = tuneTalkService.performSubscription(subscriptionReqDTO);

        log.info("Response : {}", subscriptionPlanDTO.toString());

        int abs = Math.abs(Integer.parseInt(subscriptionPlanDTO.getCode()));
        abs = abs < 0 ? abs * -1 : abs;
        log.info("code abs : {}", abs);


        return ResponseEntity.status(HttpStatus.valueOf(abs))
            .header("Content-Type: application/json; charset=UTF-8").body(subscriptionPlanDTO);
    }

    @ApiOperation(value = "Get Subscription Status By Transaction Id")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Request Successful!"),
        @ApiResponse(code = 401, message = "Empty Secret Key!"),
        @ApiResponse(code = 403, message = "Invalid Secret Key! or Restricted Access!"),
        @ApiResponse(code = 500, message = "Request Failed!")
    })
    @GetMapping(value = "/one-calls/subscription-status/{transactionId}", produces = "application/json")
    public ResponseEntity<?> getSubscriptionStatusByTransactionId(@PathVariable String transactionId) {
        log.info("Get Subscription Status By Transaction Id : {}", transactionId);

        JSONObject jsonObject = tuneTalkService.getSubscriptionStatusByTransactionId(transactionId);

        log.info("Response : {}", jsonObject.toString());

        int abs = Math.abs(Integer.parseInt(jsonObject.getString("code")));
        abs = abs < 0 ? abs * -1 : abs;
        log.info("code abs : {}", abs);


        return ResponseEntity.status(HttpStatus.valueOf(abs))
            .header("Content-Type: application/json; charset=UTF-8").body(jsonObject.toString());
    }

    @ApiOperation(value = "Returns List of Telco for SIM Port In")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Request Successful!"),
        @ApiResponse(code = 401, message = "Empty Secret Key!"),
        @ApiResponse(code = 403, message = "Invalid Secret Key! or Restricted Access!"),
        @ApiResponse(code = 500, message = "Request Failed!")
    })
    @GetMapping(value = "/one-calls/telco-list", produces = "application/json")
    public ResponseEntity<?> getTelcoList() {
        log.info("Returns List of Telco for SIM Port In.");

        JSONObject jsonObject = tuneTalkService.getTelcoList();

        log.info("Response : {}", jsonObject.toString());

        int abs = Math.abs(Integer.parseInt(jsonObject.getString("code")));
        abs = abs < 0 ? abs * -1 : abs;
        log.info("code abs : {}", abs);


        return ResponseEntity.status(HttpStatus.valueOf(abs))
            .header("Content-Type: application/json; charset=UTF-8").body(jsonObject.toString(2));
    }

    @ApiOperation(value = "Perform topup")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Request Successful!"),
        @ApiResponse(code = 401, message = "Empty Secret Key!"),
        @ApiResponse(code = 403, message = "Invalid Secret Key! or Restricted Access!"),
        @ApiResponse(code = 500, message = "Request Failed!")
    })
    @PostMapping(value = "/one-calls/topup")
    public ResponseEntity<OneCallDTO> oneCallTopup(@Valid @RequestBody TopupDTO topupDTO) {
        log.debug("REST request for new Topup : {}", topupDTO);
        OneCallDTO result = oneCallService.doTopup(topupMapper.toEntity(topupDTO));

        log.info("Response : {}", result);

        int abs = Math.abs(Integer.parseInt(result.getCode()));
        abs = abs < 0 ? abs * -1 : abs;
        log.info("code abs : {}", abs);

//            TopupRespDTO respDTO = new TopupRespDTO();
//            respDTO.setApiKey(result.getApiKey());
//            respDTO.setBalance(result.getBalance());
//            respDTO.setCode(result.getCode());
//            respDTO.setMessage(result.getMessage());

        return ResponseEntity.status(HttpStatus.valueOf(abs))
            .header("Content-Type: application/json; charset=UTF-8").body(result);
    }

    @ApiOperation(value = "Perform topup status by Transaction Id")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Request Successful!"),
        @ApiResponse(code = 401, message = "Empty Secret Key!"),
        @ApiResponse(code = 403, message = "Invalid Secret Key! or Restricted Access!"),
        @ApiResponse(code = 500, message = "Request Failed!")
    })
    @GetMapping("/one-calls/topup-transaction")
    public ResponseEntity<OneCallDTO> getTopupByTransactionId(TopupOrderDTO topupOrderDTO) {
        log.debug("REST request to get Topup by MerchantOrderNo: {}", topupOrderDTO.toString());
        OneCallDTO result = oneCallService.getTopupByTransId(topupOrderDTO);

        log.info("Response : {}", result);

        int abs = Math.abs(Integer.parseInt(result.getCode()));
        abs = abs < 0 ? abs * -1 : abs;
        log.info("code abs : {}", abs);

        return ResponseEntity.status(HttpStatus.valueOf(abs))
            .header("Content-Type: application/json; charset=UTF-8").body(result);
    }

    @ApiOperation(value = "Get Topup Deno List via MSISDN Number")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Request Successful!"),
        @ApiResponse(code = 401, message = "Empty Secret Key!"),
        @ApiResponse(code = 403, message = "Invalid Secret Key! or Restricted Access!"),
        @ApiResponse(code = 500, message = "Request Failed!")
    })
    @PostMapping("/one-calls/topup-values")
    public ResponseEntity<OneCallDTO> getTopupValuesByMsisdn(@Valid @RequestBody TopupValuesDTO topupValuesDTO) {
        log.debug("REST request to get Topup Values by Msisdn: {}", topupValuesDTO.toString());
        OneCallDTO result = oneCallService.getTopupValuesByMsisdn(topupValuesDTO);

        log.info("Response : {}", result);

        int abs = Math.abs(Integer.parseInt(result.getCode()));
        abs = abs < 0 ? abs * -1 : abs;
        log.info("code abs : {}", abs);

        return ResponseEntity.status(HttpStatus.valueOf(abs))
            .header("Content-Type: application/json; charset=UTF-8").body(result);
    }

    @ApiOperation(value = "Update Topup Validity for a MSISDN")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Request Successful!"),
        @ApiResponse(code = 401, message = "Empty Secret Key!"),
        @ApiResponse(code = 403, message = "Invalid Secret Key! or Restricted Access!"),
        @ApiResponse(code = 500, message = "Request Failed!")
    })
    @GetMapping(value = "/one-calls/update-validity/{msisdn}/{days}", produces = "application/json")
    public ResponseEntity<TuneTalkRespDTO> updateTopupValidityForMsisdn(@PathVariable String msisdn, @PathVariable Integer days) {
        log.info("Update Topup Validity {} days for {}", days.toString(), msisdn);

        TuneTalkRespDTO tuneTalkRespDTO = tuneTalkService.updateTopupValidityForMsisdn(msisdn, days);

        log.info("Response : {}", tuneTalkRespDTO.toString());

        int abs = Math.abs(Integer.parseInt(tuneTalkRespDTO.getCode()));
        abs = abs < 0 ? abs * -1 : abs;
        log.info("code abs : {}", abs);


        return ResponseEntity.status(HttpStatus.valueOf(abs))
            .header("Content-Type: application/json; charset=UTF-8").body(tuneTalkRespDTO);
    }


    @GetMapping("/one-calls/all-transactions/{merchantId}")
    public ResponseEntity<List<OneCallDTO>> getAllTransactionsByMerchantId(Pageable pageable, @PathVariable String merchantId) {
        log.info("Request to get All Transactions By MerchantId : {}", merchantId);

        Page<OneCallDTO> page = oneCallService.getAllTransactionsByMerchantId(pageable, merchantId);

        log.info("Page Content : {}", page.getContent());

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/one-calls/all-transactions/{merchantId}");
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }


    @GetMapping("/one-calls/sim-history")
    public ResponseEntity<List<Map<String, Object>>> getSimHistory(){
        log.info("Request to get All Sim Histories");

        List<Map<String, Object>> history = oneCallService.getSimHistory();

        log.info("Page Content : {}", history);

//        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/one-calls/sim-history");
        return ResponseEntity.ok().body(history);
    }


    @GetMapping("/one-calls/sim-status-by-msisdn/{msisdn}")
    public ResponseEntity<List<Map<String, Object>>> getSimStatusByMsisdn(@PathVariable String msisdn){
        log.info("REST request to get sim status for msisdn : {}", msisdn);

        List<Map<String, Object>> list = oneCallService.getSimStatusByMsisdn(msisdn);

        return ResponseEntity.ok().body(list);
    }


    @GetMapping("/one-calls/daily-client-topup-in-range/{client}/{fromDate}/{toDate}")
    public ResponseEntity<List<Map<String, Object>>> getDailyTotalTopupInRangeForClient(@PathVariable String client, @PathVariable String fromDate, @PathVariable String toDate){
        log.info("REST request to get client {} daily total topup in date range from {} to {}", client, fromDate, toDate);

        List<Map<String, Object>> object = oneCallService.getDailyTotalTopupInRangeForClient(client, fromDate,toDate);

        return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON_UTF8).body(object);
    }
}
