package com.mobilityone.onecall.domain;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Favorite.
 */
@Entity
@Table(name = "favorite")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Favorite implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "phone_no")
    private String phoneNo;

    @Column(name = "prod_code")
    private String prodCode;

    @Column(name = "prod_sub_code")
    private String prodSubCode;

    @Column(name = "cat_type")
    private String catType;

    @Column(name = "prod_desc")
    private String prodDesc;

    @Column(name = "prod_code_alt")
    private String prodCodeAlt;

    @Column(name = "prod_desc_long")
    private String prodDescLong;

    @Column(name = "account_number")
    private String accountNumber;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public Favorite phoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
        return this;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getProdCode() {
        return prodCode;
    }

    public Favorite prodCode(String prodCode) {
        this.prodCode = prodCode;
        return this;
    }

    public void setProdCode(String prodCode) {
        this.prodCode = prodCode;
    }

    public String getProdSubCode() {
        return prodSubCode;
    }

    public Favorite prodSubCode(String prodSubCode) {
        this.prodSubCode = prodSubCode;
        return this;
    }

    public void setProdSubCode(String prodSubCode) {
        this.prodSubCode = prodSubCode;
    }

    public String getCatType() {
        return catType;
    }

    public Favorite catType(String catType) {
        this.catType = catType;
        return this;
    }

    public void setCatType(String catType) {
        this.catType = catType;
    }

    public String getProdDesc() {
        return prodDesc;
    }

    public Favorite prodDesc(String prodDesc) {
        this.prodDesc = prodDesc;
        return this;
    }

    public void setProdDesc(String prodDesc) {
        this.prodDesc = prodDesc;
    }

    public String getProdCodeAlt() {
        return prodCodeAlt;
    }

    public Favorite prodCodeAlt(String prodCodeAlt) {
        this.prodCodeAlt = prodCodeAlt;
        return this;
    }

    public void setProdCodeAlt(String prodCodeAlt) {
        this.prodCodeAlt = prodCodeAlt;
    }

    public String getProdDescLong() {
        return prodDescLong;
    }

    public Favorite prodDescLong(String prodDescLong) {
        this.prodDescLong = prodDescLong;
        return this;
    }

    public void setProdDescLong(String prodDescLong) {
        this.prodDescLong = prodDescLong;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public Favorite accountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
        return this;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Favorite favorite = (Favorite) o;
        if (favorite.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), favorite.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Favorite{" +
            "id=" + getId() +
            ", phoneNo='" + getPhoneNo() + "'" +
            ", prodCode='" + getProdCode() + "'" +
            ", prodSubCode='" + getProdSubCode() + "'" +
            ", catType='" + getCatType() + "'" +
            ", prodDesc='" + getProdDesc() + "'" +
            ", prodCodeAlt='" + getProdCodeAlt() + "'" +
            ", prodDescLong='" + getProdDescLong() + "'" +
            ", accountNumber='" + getAccountNumber() + "'" +
            "}";
    }
}
