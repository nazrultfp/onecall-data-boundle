package com.mobilityone.onecall.service.impl;

import com.mobilityone.onecall.config.ApplicationProperties;
import com.mobilityone.onecall.config.Constants;
import com.mobilityone.onecall.domain.Agent;
import com.mobilityone.onecall.repository.AgentRepository;
import com.mobilityone.onecall.security.AuthoritiesConstants;
import com.mobilityone.onecall.security.SecurityUtils;
import com.mobilityone.onecall.service.AgentService;
import com.mobilityone.onecall.service.TuneTalkService;
import com.mobilityone.onecall.service.dto.*;
import com.mobilityone.onecall.service.mapper.AgentMapper;
import com.mobilityone.onecall.web.rest.util.CommonUtils;
import org.json.JSONObject;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;

import javax.ws.rs.core.Response;
import java.time.ZonedDateTime;
import java.util.*;

/**
 * Service Implementation for managing Agent.
 */
@Service
@Transactional
public class AgentServiceImpl implements AgentService {

    private final Logger log = LoggerFactory.getLogger(AgentServiceImpl.class);

    private final ApplicationProperties properties;

    private final AgentRepository agentRepository;

    private final AgentMapper agentMapper;

    private final TuneTalkService tuneTalkService;

    public AgentServiceImpl(ApplicationProperties properties, AgentRepository agentRepository, AgentMapper agentMapper, TuneTalkService tuneTalkService) {
        this.properties = properties;
        this.agentRepository = agentRepository;
        this.agentMapper = agentMapper;
        this.tuneTalkService = tuneTalkService;
    }

    /**
     * Save a agent.
     *
     * @param agentDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public AgentDTO save(AgentDTO agentDTO) {
        log.debug("Request to save Agent : {}", agentDTO);
        Agent agent = agentMapper.toEntity(agentDTO);
        agent = agentRepository.save(agent);
        return agentMapper.toDto(agent);
    }

    /**
     * Get all the agents.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<AgentDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Agents");
        return agentRepository.findAll(pageable)
            .map(agentMapper::toDto);
    }


    /**
     * Get one agent by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<AgentDTO> findOne(Long id) {
        log.debug("Request to get Agent : {}", id);
        return agentRepository.findById(id)
            .map(agentMapper::toDto);
    }

    /**
     * Delete the agent by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Agent : {}", id);
        Optional<Agent> agentOptional = agentRepository.findById(id);
        if (!agentOptional.isPresent()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "agent not found");
        }

        Agent agent = agentOptional.get();
        agent.setActive(Boolean.FALSE);
        agentRepository.save(agent);
    }


    @Override
    public ResponseEntity registerAgent(AgentDTO agentDTO) {
        log.debug("Start register agent: {}", agentDTO);
//        if(!userDTO.getPassword().matches(Constants.PASS_REGEX)) {
//            Map<String, String> m = new HashMap<>();
//            m.put("errorMessage", "Password is weak! Select a strong password.");
//            return ResponseEntity
//                .status(HttpStatus.BAD_REQUEST)
//                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
//                .body(m);
//        }

        TtBalabceRespDTO balanceByMsisdn = tuneTalkService.getBalanceByMsisdn(agentDTO.getPhoneNo());
        if (!"200".equalsIgnoreCase(balanceByMsisdn.getCode()) || balanceByMsisdn.getCreditBalance().isEmpty() || Double.valueOf(balanceByMsisdn.getCreditBalance()).equals(0)) {
            Map<String, String> m = new HashMap<>();
            m.put("errorMessage", "Agent phone No is not OneCall.");
            return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
                .body(m);
        }

        if (CommonUtils.isNullOrEmpty(agentDTO.getEmail()) || !agentDTO.getEmail().matches(Constants.EMAIL_REGEX)) {
            Map<String, String> m = new HashMap<>();
            m.put("errorMessage", "Email isn't valid.");
            return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
                .body(m);
        }
        if (CommonUtils.isNullOrEmpty(agentDTO.getPhoneNo()) /*||
            !phoneUtil.isValidNumber(phoneUtil.parse(userDTO.getMobile(), ""))*/) {
            Map<String, String> m = new HashMap<>();
            m.put("errorMessage", "Mobile number isn't valid.");
            return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
                .body(m);
        }
        log.debug("Start register user: {}", agentDTO);
        Keycloak keycloak = KeycloakBuilder.builder() //
            .serverUrl(properties.getKeycloak().getBaseUrl()) //
            .realm(properties.getKeycloak().getRealm()) //
            .grantType(OAuth2Constants.PASSWORD) //
            .clientId(properties.getKeycloak().getClientId()) //
            .clientSecret(properties.getKeycloak().getClientSecret())
            .username(properties.getUsersAdmin().getUsername()) //
            .password(properties.getUsersAdmin().getPassword()) //
            .build();
        log.debug("Keycloak: {}", keycloak.realms());
        UserRepresentation user = new UserRepresentation();
        user.setEnabled(true);
        user.setUsername(agentDTO.getPhoneNo());
        user.setFirstName(agentDTO.getFirstName());
        user.setLastName(agentDTO.getLastName());
        user.setEmail(agentDTO.getEmail());
        user.setRealmRoles(Arrays.asList(AuthoritiesConstants.USER));
        Map<String, List<String>> attr = new HashMap<>();
        if (agentDTO.getPhoneNo() != null) {
            List<String> phoneNumber = new ArrayList<>();
            phoneNumber.add(agentDTO.getPhoneNo());
            attr.put("phoneNumber", phoneNumber);
        }
        if (agentDTO.getAddress() != null) {
            List<String> address = new ArrayList<>();
            address.add(agentDTO.getAddress());
            attr.put("address", address);
        }
        if (agentDTO.getLandLine() != null) {
            List<String> landLine = new ArrayList<>();
            landLine.add(agentDTO.getLandLine());
            attr.put("landLine", landLine);
        }
        user.setAttributes(attr);
        CredentialRepresentation credential = new CredentialRepresentation();
        credential.setTemporary(false);
        credential.setType(CredentialRepresentation.PASSWORD);
        credential.setValue(String.valueOf(agentDTO.getPassword()));
        user.setCredentials(Arrays.asList(credential));

        // Get realm
        RealmResource realmResource = keycloak.realm(properties.getKeycloak().getRealm());
        UsersResource userResource = realmResource.users();

        // Create user (requires manage-users role)
        Response response = userResource.create(user);
        log.info("Headers: {}", response.getHeaders());
        log.info("Status: {}", response.getStatus());
        log.info("Status Info: {}", response.getStatusInfo());
        log.info("StringHeaders: {}", response.getStringHeaders());
//        BufferedInputStream bis = new BufferedInputStream((InputStream) response.getEntity());
//        ByteArrayOutputStream buf = new ByteArrayOutputStream();
//        int result = bis.read();
//        while (result != -1) {
//            byte b = (byte) result;
//            buf.write(b);
//            result = bis.read();
//        }

        if (response.getStatus() == HttpStatus.CREATED.value()) {
            agentDTO.setDealerId(CommonUtils.get15DigitId().toString());
            agentDTO = save(agentDTO);
        } else {
            Map<String, String> m = new HashMap<>();
            m.put("errorMessage", response.getStatusInfo().getReasonPhrase());
            return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
                .body(m);
        }

        log.info("Registered agent : {}", agentDTO);

        return ResponseEntity
            .status(response.getStatus() == 201 ? 200 : response.getStatus())
            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
            .body(agentDTO);
    }

    @Override
    public LoginRespDTO login(LoginDTO loginDTO) {
        log.debug("Start login process for: {}", loginDTO.getUsername());

        try {
            RestTemplate loginTmp = new RestTemplate();
            MultiValueMap<String, String> formParams = new LinkedMultiValueMap<>();
            org.springframework.http.HttpHeaders headers = new org.springframework.http.HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            formParams.add("grant_type", properties.getKeycloak().getGrantType());
            formParams.add("username", loginDTO.getUsername());
            formParams.add("password", loginDTO.getPassword());
            formParams.add("client_id", properties.getKeycloak().getClientId());
            formParams.add("client_secret", properties.getKeycloak().getClientSecret());
            formParams.add("scope", properties.getKeycloak().getScope());
            org.springframework.http.HttpEntity<MultiValueMap<String, String>> entity = new org.springframework.http.HttpEntity<>(
                formParams, headers);
            String tokenURL = String.format("%s/realms/%s/protocol/openid-connect/token",
                properties.getKeycloak().getBaseUrl(),
                properties.getKeycloak().getRealm());
            log.info("Before calling {}", tokenURL);
            ResponseEntity<String> response = loginTmp.exchange(tokenURL, HttpMethod.POST, entity, String.class);
            log.info("Status code of Calling {} was {}.", tokenURL, response.getStatusCode());
            JSONObject result = new JSONObject(response.getBody());
//            JSONObject playload = new JSONObject(new String(Base64.getDecoder().decode(result.getString("access_token").split("\\.")[1])));
//            JSONArray rolesJsonArray = playload.getJSONObject("realm_access").getJSONArray("roles");
//            Set<String> roles = new HashSet<>(rolesJsonArray.length());
//            for (int i = 0; i < rolesJsonArray.length(); i++) {
//                roles.add((String)rolesJsonArray.get(i));
//            }
//            log.debug("User Roles: {}", roles);
//            if(loginDTO.getSource().equals(InitializeType.MERCHANT) &&
//                !roles.contains(AuthoritiesConstants.MERCHANT.toString())) {
//                throw new ResponseStatusException(HttpStatus.FORBIDDEN, "User isn't merchant.");
//            } else if (loginDTO.getSource().equals(InitializeType.CLIENT_APP) &&
//                roles.contains(AuthoritiesConstants.MERCHANT.toString())) {
//                throw new ResponseStatusException(HttpStatus.FORBIDDEN, "User isn't normal user.");
//            }

            Optional<Agent> agent = agentRepository.findFirstByPhoneNo(loginDTO.getUsername());
            if (!agent.isPresent())
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "agent not found");

            TtBalabceRespDTO balance = tuneTalkService.getBalanceByMsisdn(loginDTO.getUsername());

            LoginRespDTO loginRespDTO = new LoginRespDTO();
//            loginRespDTO.setAgent(agentMapper.toDto(agent.get()));
//            loginRespDTO.setBalance(balance);
            loginRespDTO.setAccess_token(result.getString("access_token"));
            loginRespDTO.setToken_type(result.getString("token_type"));

            return loginRespDTO;
        } catch (HttpClientErrorException e) {
            log.error("Error in calling login api: {}", e.getMessage());
            throw new ResponseStatusException(e.getStatusCode(), e.getStatusText(), e);
        }
    }

    @Override
    public AgentDTO updateAgentAddress(AgentUpdateDTO agentUpdateDTO) {
        log.debug("Request to update an Agent Address : {}", agentUpdateDTO);

        Optional<AgentDTO> agentOptional = findOne(agentUpdateDTO.getId());
        if (!agentOptional.isPresent()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "agent not found");
        }

        AgentDTO agentDTO = agentOptional.get();
        if (!CommonUtils.isNullOrEmpty(agentUpdateDTO.getLandLine()))
            agentDTO.setLandLine(agentUpdateDTO.getLandLine());
        if (!CommonUtils.isNullOrEmpty(agentUpdateDTO.getFax()))
            agentDTO.setFax(agentUpdateDTO.getFax());
        if (!CommonUtils.isNullOrEmpty(agentUpdateDTO.getAddress()))
            agentDTO.setAddress(agentUpdateDTO.getAddress());
        if (!CommonUtils.isNullOrEmpty(agentUpdateDTO.getCity()))
            agentDTO.setCity(agentUpdateDTO.getCity());
        if (!CommonUtils.isNullOrEmpty(agentUpdateDTO.getZipCode()))
            agentDTO.setZipCode(agentUpdateDTO.getZipCode());
        if (!CommonUtils.isNullOrEmpty(agentUpdateDTO.getState()))
            agentDTO.setState(agentUpdateDTO.getState());
        if (!CommonUtils.isNullOrEmpty(agentUpdateDTO.getCountry()))
            agentDTO.setCountry(agentUpdateDTO.getCountry());

        agentDTO.setModifiedDate(ZonedDateTime.now());
        agentDTO.setModifiedBy(SecurityUtils.getCurrentUserLogin().orElse(Constants.ANONYMOUS_USER));
        agentDTO = save(agentDTO);

        log.info("saved data : {}", agentDTO);

        return agentDTO;
    }

    @Override
    public void resetPassword(ResetPasswordDTO resetPasswordDTO) {
        log.debug("Request to reset password of an agent : {}", resetPasswordDTO);

        Optional<Agent> agent = agentRepository.findById(resetPasswordDTO.getAgentId());
        if (!agent.isPresent()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "agent not found");
        }

        if (!agent.get().getPassword().equals(resetPasswordDTO.getOldPassword())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "old password doesn't match");
        }

        UserRepresentation user = getUserInfoByUsername(agent.get().getPhoneNo())
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "User doesn't exist in keycloak"));
        Keycloak keycloak = KeycloakBuilder.builder() //
            .serverUrl(properties.getKeycloak().getBaseUrl()) //
            .realm(properties.getKeycloak().getRealm()) //
            .grantType(OAuth2Constants.PASSWORD) //
            .clientId(properties.getKeycloak().getClientId()) //
            .clientSecret(properties.getKeycloak().getClientSecret())
            .username(properties.getUsersAdmin().getUsername()) //
            .password(properties.getUsersAdmin().getPassword()) //
            .build();
        CredentialRepresentation newCreds = new CredentialRepresentation();
        newCreds.setType(CredentialRepresentation.PASSWORD);
        newCreds.setValue(resetPasswordDTO.getNewPassword());
        newCreds.setTemporary(false);
        keycloak.realm(properties.getKeycloak().getRealm()).users().get(user.getId()).resetPassword(newCreds);


        agent.get().setPassword(resetPasswordDTO.getNewPassword());
        agentRepository.save(agent.get());

    }

    @Override
    public LoginRespDTO loginAgent(LoginDTO loginDTO) {
        log.debug("Start login process for: {}", loginDTO.getUsername());

        String loginUrl = properties.getLoginTfp().getLoginUrl();

        RestTemplate restTemplate = new RestTemplate();

        // request header
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity<?> entity = new HttpEntity<>(headers);

        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(loginUrl)
            .queryParam("username", loginDTO.getUsername())
            .queryParam("password", loginDTO.getPassword());

        ResponseEntity<LoginRespDTO> responseEntity = restTemplate.exchange(
            uriBuilder.toUriString(),
            HttpMethod.GET,
            entity,
            LoginRespDTO.class
        );

//        LoginRespDTO body = responseEntity.getBody();
//        if (body.getExist().equals(Boolean.FALSE)) {
//            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "user not found.");
//        }



        return null;
    }

    private Optional<UserRepresentation> getUserInfoByUsername(String username) {
        log.debug("Get full name of {}", username);
        if (CommonUtils.isNullOrEmpty(username))
            return Optional.empty();
        Keycloak keycloak = KeycloakBuilder.builder().serverUrl(properties.getUsersAdmin().getServerUrl())
            .realm(properties.getUsersAdmin().getRealm()).grantType(OAuth2Constants.PASSWORD)
            .clientId(properties.getUsersAdmin().getClientId())
            .clientSecret(properties.getKeycloak().getClientSecret())
            .username(properties.getUsersAdmin().getUsername())
            .password(properties.getUsersAdmin().getPassword()).build();
        List<UserRepresentation> users = keycloak.realm(properties.getUsersAdmin().getRealm()).users()
            .search(username);
        if (users.size() == 0)
            return Optional.empty();
        return Optional.of(users.stream().filter(u -> u.getUsername().equalsIgnoreCase(username)).findAny()
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Couldn't find user.")));
    }
}
