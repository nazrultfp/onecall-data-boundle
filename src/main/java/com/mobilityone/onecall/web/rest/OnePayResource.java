package com.mobilityone.onecall.web.rest;

import com.mobilityone.onecall.service.OnePayService;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * REST controller for managing OneCall.
 */
@RestController
@RequestMapping("/api")
public class OnePayResource {

    private final Logger log = LoggerFactory.getLogger(OnePayResource.class);

    private final OnePayService onePayService;

    public OnePayResource(OnePayService onePayService) {
        this.onePayService = onePayService;
    }


    @GetMapping("/one-pay/register/{regId}")
    public ResponseEntity<String> customerRegisterByRegId(@PathVariable Long regId){
        log.debug("REST Request to call TPM Customer Register by regId : {}", regId);

        JSONObject object = onePayService.registerCustomerByRegId(regId);

        HttpStatus status;
        if (object.has("ErrCode") && "0".equals(object.getString("ErrCode")))
            status = HttpStatus.OK;
        else
            status = HttpStatus.BAD_REQUEST;

        return ResponseEntity.status(status).contentType(MediaType.APPLICATION_JSON_UTF8).body(object.toString());
    }


    @GetMapping("/one-pay/register-list/{regIdList}")
    public ResponseEntity<Void> registerCustomersByRegIds(@PathVariable List<Long> regIdList){
        log.debug("REST Request to call TPM Customer Register for a list of regIds : {}", regIdList);

        regIdList.forEach(onePayService::registerCustomerByRegId);

        return ResponseEntity.ok().build();
    }
}
