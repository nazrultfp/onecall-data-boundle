package com.mobilityone.onecall.web.rest;

import com.mobilityone.onecall.OneCallApp;

import com.mobilityone.onecall.domain.SimRegister;
import com.mobilityone.onecall.repository.SimRegisterRepository;
import com.mobilityone.onecall.service.SimRegisterService;
import com.mobilityone.onecall.service.dto.SimRegisterDTO;
import com.mobilityone.onecall.service.mapper.SimRegisterMapper;
import com.mobilityone.onecall.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;


import static com.mobilityone.onecall.web.rest.TestUtil.sameInstant;
import static com.mobilityone.onecall.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.mobilityone.onecall.domain.enumeration.Gender;
/**
 * Test class for the SimRegisterResource REST controller.
 *
 * @see SimRegisterResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OneCallApp.class)
public class SimRegisterResourceIntTest {

    private static final String DEFAULT_AGENT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_AGENT_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_TRANSACTION_ID = "AAAAAAAAAA";
    private static final String UPDATED_TRANSACTION_ID = "BBBBBBBBBB";

    private static final String DEFAULT_FIRST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBBBBBBB";

    private static final Gender DEFAULT_GENDER = Gender.MALE;
    private static final Gender UPDATED_GENDER = Gender.FEMALE;

    private static final String DEFAULT_DOB = "AAAAAAAAAA";
    private static final String UPDATED_DOB = "BBBBBBBBBB";

    private static final String DEFAULT_ID_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_ID_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_ID_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_ID_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_NATIONALITY = "AAAAAAAAAA";
    private static final String UPDATED_NATIONALITY = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL_ADDR = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL_ADDR = "BBBBBBBBBB";

    private static final String DEFAULT_BLOCKED_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_BLOCKED_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_CITY = "AAAAAAAAAA";
    private static final String UPDATED_CITY = "BBBBBBBBBB";

    private static final String DEFAULT_STATE = "AAAAAAAAAA";
    private static final String UPDATED_STATE = "BBBBBBBBBB";

    private static final String DEFAULT_COUNTRY = "AAAAAAAAAA";
    private static final String UPDATED_COUNTRY = "BBBBBBBBBB";

    private static final String DEFAULT_POST_CODE = "AAAAAAAAAA";
    private static final String UPDATED_POST_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_SIM_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_SIM_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_PARTNER_DEALER_CODE = "AAAAAAAAAA";
    private static final String UPDATED_PARTNER_DEALER_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_API_KEY = "AAAAAAAAAA";
    private static final String UPDATED_API_KEY = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_MESSAGE = "AAAAAAAAAA";
    private static final String UPDATED_MESSAGE = "BBBBBBBBBB";

    private static final String DEFAULT_MSISDN = "AAAAAAAAAA";
    private static final String UPDATED_MSISDN = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_MODIFIED_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_MODIFIED_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_MODIFIED_BY = "BBBBBBBBBB";

    @Autowired
    private SimRegisterRepository simRegisterRepository;

    @Autowired
    private SimRegisterMapper simRegisterMapper;

    @Autowired
    private SimRegisterService simRegisterService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSimRegisterMockMvc;

    private SimRegister simRegister;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SimRegisterResource simRegisterResource = new SimRegisterResource(simRegisterService);
        this.restSimRegisterMockMvc = MockMvcBuilders.standaloneSetup(simRegisterResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SimRegister createEntity(EntityManager em) {
        SimRegister simRegister = new SimRegister()
            .agentCode(DEFAULT_AGENT_CODE)
            .transactionId(DEFAULT_TRANSACTION_ID)
            .firstName(DEFAULT_FIRST_NAME)
            .lastName(DEFAULT_LAST_NAME)
            .gender(DEFAULT_GENDER)
            .dob(DEFAULT_DOB)
            .idNumber(DEFAULT_ID_NUMBER)
            .idType(DEFAULT_ID_TYPE)
            .nationality(DEFAULT_NATIONALITY)
            .address(DEFAULT_ADDRESS)
            .emailAddr(DEFAULT_EMAIL_ADDR)
            .blockedNumber(DEFAULT_BLOCKED_NUMBER)
            .city(DEFAULT_CITY)
            .state(DEFAULT_STATE)
            .country(DEFAULT_COUNTRY)
            .postCode(DEFAULT_POST_CODE)
            .simNumber(DEFAULT_SIM_NUMBER)
            .partnerDealerCode(DEFAULT_PARTNER_DEALER_CODE)
            .apiKey(DEFAULT_API_KEY)
            .code(DEFAULT_CODE)
            .message(DEFAULT_MESSAGE)
            .msisdn(DEFAULT_MSISDN)
            .status(DEFAULT_STATUS)
            .createdDate(DEFAULT_CREATED_DATE)
            .modifiedDate(DEFAULT_MODIFIED_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .modifiedBy(DEFAULT_MODIFIED_BY);
        return simRegister;
    }

    @Before
    public void initTest() {
        simRegister = createEntity(em);
    }

    @Test
    @Transactional
    public void createSimRegister() throws Exception {
        int databaseSizeBeforeCreate = simRegisterRepository.findAll().size();

        // Create the SimRegister
        SimRegisterDTO simRegisterDTO = simRegisterMapper.toDto(simRegister);
        restSimRegisterMockMvc.perform(post("/api/sim-registers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(simRegisterDTO)))
            .andExpect(status().isCreated());

        // Validate the SimRegister in the database
        List<SimRegister> simRegisterList = simRegisterRepository.findAll();
        assertThat(simRegisterList).hasSize(databaseSizeBeforeCreate + 1);
        SimRegister testSimRegister = simRegisterList.get(simRegisterList.size() - 1);
        assertThat(testSimRegister.getAgentCode()).isEqualTo(DEFAULT_AGENT_CODE);
        assertThat(testSimRegister.getTransactionId()).isEqualTo(DEFAULT_TRANSACTION_ID);
        assertThat(testSimRegister.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testSimRegister.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testSimRegister.getGender()).isEqualTo(DEFAULT_GENDER);
        assertThat(testSimRegister.getDob()).isEqualTo(DEFAULT_DOB);
        assertThat(testSimRegister.getIdNumber()).isEqualTo(DEFAULT_ID_NUMBER);
        assertThat(testSimRegister.getIdType()).isEqualTo(DEFAULT_ID_TYPE);
        assertThat(testSimRegister.getNationality()).isEqualTo(DEFAULT_NATIONALITY);
        assertThat(testSimRegister.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testSimRegister.getEmailAddr()).isEqualTo(DEFAULT_EMAIL_ADDR);
        assertThat(testSimRegister.getBlockedNumber()).isEqualTo(DEFAULT_BLOCKED_NUMBER);
        assertThat(testSimRegister.getCity()).isEqualTo(DEFAULT_CITY);
        assertThat(testSimRegister.getState()).isEqualTo(DEFAULT_STATE);
        assertThat(testSimRegister.getCountry()).isEqualTo(DEFAULT_COUNTRY);
        assertThat(testSimRegister.getPostCode()).isEqualTo(DEFAULT_POST_CODE);
        assertThat(testSimRegister.getSimNumber()).isEqualTo(DEFAULT_SIM_NUMBER);
        assertThat(testSimRegister.getPartnerDealerCode()).isEqualTo(DEFAULT_PARTNER_DEALER_CODE);
        assertThat(testSimRegister.getApiKey()).isEqualTo(DEFAULT_API_KEY);
        assertThat(testSimRegister.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testSimRegister.getMessage()).isEqualTo(DEFAULT_MESSAGE);
        assertThat(testSimRegister.getMsisdn()).isEqualTo(DEFAULT_MSISDN);
        assertThat(testSimRegister.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testSimRegister.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testSimRegister.getModifiedDate()).isEqualTo(DEFAULT_MODIFIED_DATE);
        assertThat(testSimRegister.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testSimRegister.getModifiedBy()).isEqualTo(DEFAULT_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void createSimRegisterWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = simRegisterRepository.findAll().size();

        // Create the SimRegister with an existing ID
        simRegister.setId(1L);
        SimRegisterDTO simRegisterDTO = simRegisterMapper.toDto(simRegister);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSimRegisterMockMvc.perform(post("/api/sim-registers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(simRegisterDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SimRegister in the database
        List<SimRegister> simRegisterList = simRegisterRepository.findAll();
        assertThat(simRegisterList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSimRegisters() throws Exception {
        // Initialize the database
        simRegisterRepository.saveAndFlush(simRegister);

        // Get all the simRegisterList
        restSimRegisterMockMvc.perform(get("/api/sim-registers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(simRegister.getId().intValue())))
            .andExpect(jsonPath("$.[*].agentCode").value(hasItem(DEFAULT_AGENT_CODE.toString())))
            .andExpect(jsonPath("$.[*].transactionId").value(hasItem(DEFAULT_TRANSACTION_ID.toString())))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME.toString())))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME.toString())))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER.toString())))
            .andExpect(jsonPath("$.[*].dob").value(hasItem(DEFAULT_DOB.toString())))
            .andExpect(jsonPath("$.[*].idNumber").value(hasItem(DEFAULT_ID_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].idType").value(hasItem(DEFAULT_ID_TYPE.toString())))
            .andExpect(jsonPath("$.[*].nationality").value(hasItem(DEFAULT_NATIONALITY.toString())))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].emailAddr").value(hasItem(DEFAULT_EMAIL_ADDR.toString())))
            .andExpect(jsonPath("$.[*].blockedNumber").value(hasItem(DEFAULT_BLOCKED_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY.toString())))
            .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE.toString())))
            .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY.toString())))
            .andExpect(jsonPath("$.[*].postCode").value(hasItem(DEFAULT_POST_CODE.toString())))
            .andExpect(jsonPath("$.[*].simNumber").value(hasItem(DEFAULT_SIM_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].partnerDealerCode").value(hasItem(DEFAULT_PARTNER_DEALER_CODE.toString())))
            .andExpect(jsonPath("$.[*].apiKey").value(hasItem(DEFAULT_API_KEY.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].message").value(hasItem(DEFAULT_MESSAGE.toString())))
            .andExpect(jsonPath("$.[*].msisdn").value(hasItem(DEFAULT_MSISDN.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(sameInstant(DEFAULT_CREATED_DATE))))
            .andExpect(jsonPath("$.[*].modifiedDate").value(hasItem(sameInstant(DEFAULT_MODIFIED_DATE))))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].modifiedBy").value(hasItem(DEFAULT_MODIFIED_BY.toString())));
    }
    
    @Test
    @Transactional
    public void getSimRegister() throws Exception {
        // Initialize the database
        simRegisterRepository.saveAndFlush(simRegister);

        // Get the simRegister
        restSimRegisterMockMvc.perform(get("/api/sim-registers/{id}", simRegister.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(simRegister.getId().intValue()))
            .andExpect(jsonPath("$.agentCode").value(DEFAULT_AGENT_CODE.toString()))
            .andExpect(jsonPath("$.transactionId").value(DEFAULT_TRANSACTION_ID.toString()))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME.toString()))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME.toString()))
            .andExpect(jsonPath("$.gender").value(DEFAULT_GENDER.toString()))
            .andExpect(jsonPath("$.dob").value(DEFAULT_DOB.toString()))
            .andExpect(jsonPath("$.idNumber").value(DEFAULT_ID_NUMBER.toString()))
            .andExpect(jsonPath("$.idType").value(DEFAULT_ID_TYPE.toString()))
            .andExpect(jsonPath("$.nationality").value(DEFAULT_NATIONALITY.toString()))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS.toString()))
            .andExpect(jsonPath("$.emailAddr").value(DEFAULT_EMAIL_ADDR.toString()))
            .andExpect(jsonPath("$.blockedNumber").value(DEFAULT_BLOCKED_NUMBER.toString()))
            .andExpect(jsonPath("$.city").value(DEFAULT_CITY.toString()))
            .andExpect(jsonPath("$.state").value(DEFAULT_STATE.toString()))
            .andExpect(jsonPath("$.country").value(DEFAULT_COUNTRY.toString()))
            .andExpect(jsonPath("$.postCode").value(DEFAULT_POST_CODE.toString()))
            .andExpect(jsonPath("$.simNumber").value(DEFAULT_SIM_NUMBER.toString()))
            .andExpect(jsonPath("$.partnerDealerCode").value(DEFAULT_PARTNER_DEALER_CODE.toString()))
            .andExpect(jsonPath("$.apiKey").value(DEFAULT_API_KEY.toString()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.message").value(DEFAULT_MESSAGE.toString()))
            .andExpect(jsonPath("$.msisdn").value(DEFAULT_MSISDN.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.createdDate").value(sameInstant(DEFAULT_CREATED_DATE)))
            .andExpect(jsonPath("$.modifiedDate").value(sameInstant(DEFAULT_MODIFIED_DATE)))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.modifiedBy").value(DEFAULT_MODIFIED_BY.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSimRegister() throws Exception {
        // Get the simRegister
        restSimRegisterMockMvc.perform(get("/api/sim-registers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSimRegister() throws Exception {
        // Initialize the database
        simRegisterRepository.saveAndFlush(simRegister);

        int databaseSizeBeforeUpdate = simRegisterRepository.findAll().size();

        // Update the simRegister
        SimRegister updatedSimRegister = simRegisterRepository.findById(simRegister.getId()).get();
        // Disconnect from session so that the updates on updatedSimRegister are not directly saved in db
        em.detach(updatedSimRegister);
        updatedSimRegister
            .agentCode(UPDATED_AGENT_CODE)
            .transactionId(UPDATED_TRANSACTION_ID)
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .gender(UPDATED_GENDER)
            .dob(UPDATED_DOB)
            .idNumber(UPDATED_ID_NUMBER)
            .idType(UPDATED_ID_TYPE)
            .nationality(UPDATED_NATIONALITY)
            .address(UPDATED_ADDRESS)
            .emailAddr(UPDATED_EMAIL_ADDR)
            .blockedNumber(UPDATED_BLOCKED_NUMBER)
            .city(UPDATED_CITY)
            .state(UPDATED_STATE)
            .country(UPDATED_COUNTRY)
            .postCode(UPDATED_POST_CODE)
            .simNumber(UPDATED_SIM_NUMBER)
            .partnerDealerCode(UPDATED_PARTNER_DEALER_CODE)
            .apiKey(UPDATED_API_KEY)
            .code(UPDATED_CODE)
            .message(UPDATED_MESSAGE)
            .msisdn(UPDATED_MSISDN)
            .status(UPDATED_STATUS)
            .createdDate(UPDATED_CREATED_DATE)
            .modifiedDate(UPDATED_MODIFIED_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .modifiedBy(UPDATED_MODIFIED_BY);
        SimRegisterDTO simRegisterDTO = simRegisterMapper.toDto(updatedSimRegister);

        restSimRegisterMockMvc.perform(put("/api/sim-registers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(simRegisterDTO)))
            .andExpect(status().isOk());

        // Validate the SimRegister in the database
        List<SimRegister> simRegisterList = simRegisterRepository.findAll();
        assertThat(simRegisterList).hasSize(databaseSizeBeforeUpdate);
        SimRegister testSimRegister = simRegisterList.get(simRegisterList.size() - 1);
        assertThat(testSimRegister.getAgentCode()).isEqualTo(UPDATED_AGENT_CODE);
        assertThat(testSimRegister.getTransactionId()).isEqualTo(UPDATED_TRANSACTION_ID);
        assertThat(testSimRegister.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testSimRegister.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testSimRegister.getGender()).isEqualTo(UPDATED_GENDER);
        assertThat(testSimRegister.getDob()).isEqualTo(UPDATED_DOB);
        assertThat(testSimRegister.getIdNumber()).isEqualTo(UPDATED_ID_NUMBER);
        assertThat(testSimRegister.getIdType()).isEqualTo(UPDATED_ID_TYPE);
        assertThat(testSimRegister.getNationality()).isEqualTo(UPDATED_NATIONALITY);
        assertThat(testSimRegister.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testSimRegister.getEmailAddr()).isEqualTo(UPDATED_EMAIL_ADDR);
        assertThat(testSimRegister.getBlockedNumber()).isEqualTo(UPDATED_BLOCKED_NUMBER);
        assertThat(testSimRegister.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testSimRegister.getState()).isEqualTo(UPDATED_STATE);
        assertThat(testSimRegister.getCountry()).isEqualTo(UPDATED_COUNTRY);
        assertThat(testSimRegister.getPostCode()).isEqualTo(UPDATED_POST_CODE);
        assertThat(testSimRegister.getSimNumber()).isEqualTo(UPDATED_SIM_NUMBER);
        assertThat(testSimRegister.getPartnerDealerCode()).isEqualTo(UPDATED_PARTNER_DEALER_CODE);
        assertThat(testSimRegister.getApiKey()).isEqualTo(UPDATED_API_KEY);
        assertThat(testSimRegister.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testSimRegister.getMessage()).isEqualTo(UPDATED_MESSAGE);
        assertThat(testSimRegister.getMsisdn()).isEqualTo(UPDATED_MSISDN);
        assertThat(testSimRegister.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testSimRegister.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testSimRegister.getModifiedDate()).isEqualTo(UPDATED_MODIFIED_DATE);
        assertThat(testSimRegister.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testSimRegister.getModifiedBy()).isEqualTo(UPDATED_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void updateNonExistingSimRegister() throws Exception {
        int databaseSizeBeforeUpdate = simRegisterRepository.findAll().size();

        // Create the SimRegister
        SimRegisterDTO simRegisterDTO = simRegisterMapper.toDto(simRegister);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSimRegisterMockMvc.perform(put("/api/sim-registers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(simRegisterDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SimRegister in the database
        List<SimRegister> simRegisterList = simRegisterRepository.findAll();
        assertThat(simRegisterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSimRegister() throws Exception {
        // Initialize the database
        simRegisterRepository.saveAndFlush(simRegister);

        int databaseSizeBeforeDelete = simRegisterRepository.findAll().size();

        // Delete the simRegister
        restSimRegisterMockMvc.perform(delete("/api/sim-registers/{id}", simRegister.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<SimRegister> simRegisterList = simRegisterRepository.findAll();
        assertThat(simRegisterList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SimRegister.class);
        SimRegister simRegister1 = new SimRegister();
        simRegister1.setId(1L);
        SimRegister simRegister2 = new SimRegister();
        simRegister2.setId(simRegister1.getId());
        assertThat(simRegister1).isEqualTo(simRegister2);
        simRegister2.setId(2L);
        assertThat(simRegister1).isNotEqualTo(simRegister2);
        simRegister1.setId(null);
        assertThat(simRegister1).isNotEqualTo(simRegister2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SimRegisterDTO.class);
        SimRegisterDTO simRegisterDTO1 = new SimRegisterDTO();
        simRegisterDTO1.setId(1L);
        SimRegisterDTO simRegisterDTO2 = new SimRegisterDTO();
        assertThat(simRegisterDTO1).isNotEqualTo(simRegisterDTO2);
        simRegisterDTO2.setId(simRegisterDTO1.getId());
        assertThat(simRegisterDTO1).isEqualTo(simRegisterDTO2);
        simRegisterDTO2.setId(2L);
        assertThat(simRegisterDTO1).isNotEqualTo(simRegisterDTO2);
        simRegisterDTO1.setId(null);
        assertThat(simRegisterDTO1).isNotEqualTo(simRegisterDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(simRegisterMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(simRegisterMapper.fromId(null)).isNull();
    }
}
