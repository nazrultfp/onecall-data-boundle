package com.mobilityone.onecall.web.rest;

import com.mobilityone.onecall.OneCallApp;

import com.mobilityone.onecall.domain.DealerLogin;
import com.mobilityone.onecall.repository.DealerLoginRepository;
import com.mobilityone.onecall.service.DealerLoginService;
import com.mobilityone.onecall.service.dto.DealerLoginDTO;
import com.mobilityone.onecall.service.mapper.DealerLoginMapper;
import com.mobilityone.onecall.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;


import static com.mobilityone.onecall.web.rest.TestUtil.sameInstant;
import static com.mobilityone.onecall.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DealerLoginResource REST controller.
 *
 * @see DealerLoginResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OneCallApp.class)
public class DealerLoginResourceIntTest {

    private static final String DEFAULT_DEALER_USER_NAME = "AAAAAAAAAA";
    private static final String UPDATED_DEALER_USER_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DEALER_PASSWORD = "AAAAAAAAAA";
    private static final String UPDATED_DEALER_PASSWORD = "BBBBBBBBBB";

    private static final Boolean DEFAULT_RESP_ERROR = false;
    private static final Boolean UPDATED_RESP_ERROR = true;

    private static final String DEFAULT_RESP_ERROR_DESC = "AAAAAAAAAA";
    private static final String UPDATED_RESP_ERROR_DESC = "BBBBBBBBBB";

    private static final Boolean DEFAULT_RESP_EXIST = false;
    private static final Boolean UPDATED_RESP_EXIST = true;

    private static final String DEFAULT_RESP_CATCH_DESC = "AAAAAAAAAA";
    private static final String UPDATED_RESP_CATCH_DESC = "BBBBBBBBBB";

    private static final String DEFAULT_RESP_LST_MSISDN = "AAAAAAAAAA";
    private static final String UPDATED_RESP_LST_MSISDN = "BBBBBBBBBB";

    private static final String DEFAULT_RESP_LST_FULLNAME = "AAAAAAAAAA";
    private static final String UPDATED_RESP_LST_FULLNAME = "BBBBBBBBBB";

    private static final String DEFAULT_RESP_LST_CREDITBALANCE = "AAAAAAAAAA";
    private static final String UPDATED_RESP_LST_CREDITBALANCE = "BBBBBBBBBB";

    private static final String DEFAULT_RESP_LST_AGENT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_RESP_LST_AGENT_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_TOKEN = "AAAAAAAAAA";
    private static final String UPDATED_TOKEN = "BBBBBBBBBB";

    private static final String DEFAULT_TOKEN_ROLE = "AAAAAAAAAA";
    private static final String UPDATED_TOKEN_ROLE = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_TOKEN_CREATE_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_TOKEN_CREATE_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_TOKEN_EXPIRE_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_TOKEN_EXPIRE_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Boolean DEFAULT_TOKEN_EXPIRED = false;
    private static final Boolean UPDATED_TOKEN_EXPIRED = true;

    @Autowired
    private DealerLoginRepository dealerLoginRepository;

    @Autowired
    private DealerLoginMapper dealerLoginMapper;

    @Autowired
    private DealerLoginService dealerLoginService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restDealerLoginMockMvc;

    private DealerLogin dealerLogin;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DealerLoginResource dealerLoginResource = new DealerLoginResource(dealerLoginService);
        this.restDealerLoginMockMvc = MockMvcBuilders.standaloneSetup(dealerLoginResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DealerLogin createEntity(EntityManager em) {
        DealerLogin dealerLogin = new DealerLogin()
            .dealerUserName(DEFAULT_DEALER_USER_NAME)
            .dealerPassword(DEFAULT_DEALER_PASSWORD)
            .respError(DEFAULT_RESP_ERROR)
            .respErrorDesc(DEFAULT_RESP_ERROR_DESC)
            .respExist(DEFAULT_RESP_EXIST)
            .respCatchDesc(DEFAULT_RESP_CATCH_DESC)
            .respLstMsisdn(DEFAULT_RESP_LST_MSISDN)
            .respLstFullname(DEFAULT_RESP_LST_FULLNAME)
            .respLstCreditbalance(DEFAULT_RESP_LST_CREDITBALANCE)
            .respLstAgentCode(DEFAULT_RESP_LST_AGENT_CODE)
            .token(DEFAULT_TOKEN)
            .tokenRole(DEFAULT_TOKEN_ROLE)
            .tokenCreateDate(DEFAULT_TOKEN_CREATE_DATE)
            .tokenExpireDate(DEFAULT_TOKEN_EXPIRE_DATE)
            .tokenExpired(DEFAULT_TOKEN_EXPIRED);
        return dealerLogin;
    }

    @Before
    public void initTest() {
        dealerLogin = createEntity(em);
    }

    @Test
    @Transactional
    public void createDealerLogin() throws Exception {
        int databaseSizeBeforeCreate = dealerLoginRepository.findAll().size();

        // Create the DealerLogin
        DealerLoginDTO dealerLoginDTO = dealerLoginMapper.toDto(dealerLogin);
        restDealerLoginMockMvc.perform(post("/api/dealer-logins")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dealerLoginDTO)))
            .andExpect(status().isCreated());

        // Validate the DealerLogin in the database
        List<DealerLogin> dealerLoginList = dealerLoginRepository.findAll();
        assertThat(dealerLoginList).hasSize(databaseSizeBeforeCreate + 1);
        DealerLogin testDealerLogin = dealerLoginList.get(dealerLoginList.size() - 1);
        assertThat(testDealerLogin.getDealerUserName()).isEqualTo(DEFAULT_DEALER_USER_NAME);
        assertThat(testDealerLogin.getDealerPassword()).isEqualTo(DEFAULT_DEALER_PASSWORD);
        assertThat(testDealerLogin.isRespError()).isEqualTo(DEFAULT_RESP_ERROR);
        assertThat(testDealerLogin.getRespErrorDesc()).isEqualTo(DEFAULT_RESP_ERROR_DESC);
        assertThat(testDealerLogin.isRespExist()).isEqualTo(DEFAULT_RESP_EXIST);
        assertThat(testDealerLogin.getRespCatchDesc()).isEqualTo(DEFAULT_RESP_CATCH_DESC);
        assertThat(testDealerLogin.getRespLstMsisdn()).isEqualTo(DEFAULT_RESP_LST_MSISDN);
        assertThat(testDealerLogin.getRespLstFullname()).isEqualTo(DEFAULT_RESP_LST_FULLNAME);
        assertThat(testDealerLogin.getRespLstCreditbalance()).isEqualTo(DEFAULT_RESP_LST_CREDITBALANCE);
        assertThat(testDealerLogin.getRespLstAgentCode()).isEqualTo(DEFAULT_RESP_LST_AGENT_CODE);
        assertThat(testDealerLogin.getToken()).isEqualTo(DEFAULT_TOKEN);
        assertThat(testDealerLogin.getTokenRole()).isEqualTo(DEFAULT_TOKEN_ROLE);
        assertThat(testDealerLogin.getTokenCreateDate()).isEqualTo(DEFAULT_TOKEN_CREATE_DATE);
        assertThat(testDealerLogin.getTokenExpireDate()).isEqualTo(DEFAULT_TOKEN_EXPIRE_DATE);
        assertThat(testDealerLogin.isTokenExpired()).isEqualTo(DEFAULT_TOKEN_EXPIRED);
    }

    @Test
    @Transactional
    public void createDealerLoginWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = dealerLoginRepository.findAll().size();

        // Create the DealerLogin with an existing ID
        dealerLogin.setId(1L);
        DealerLoginDTO dealerLoginDTO = dealerLoginMapper.toDto(dealerLogin);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDealerLoginMockMvc.perform(post("/api/dealer-logins")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dealerLoginDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DealerLogin in the database
        List<DealerLogin> dealerLoginList = dealerLoginRepository.findAll();
        assertThat(dealerLoginList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllDealerLogins() throws Exception {
        // Initialize the database
        dealerLoginRepository.saveAndFlush(dealerLogin);

        // Get all the dealerLoginList
        restDealerLoginMockMvc.perform(get("/api/dealer-logins?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dealerLogin.getId().intValue())))
            .andExpect(jsonPath("$.[*].dealerUserName").value(hasItem(DEFAULT_DEALER_USER_NAME.toString())))
            .andExpect(jsonPath("$.[*].dealerPassword").value(hasItem(DEFAULT_DEALER_PASSWORD.toString())))
            .andExpect(jsonPath("$.[*].respError").value(hasItem(DEFAULT_RESP_ERROR.booleanValue())))
            .andExpect(jsonPath("$.[*].respErrorDesc").value(hasItem(DEFAULT_RESP_ERROR_DESC.toString())))
            .andExpect(jsonPath("$.[*].respExist").value(hasItem(DEFAULT_RESP_EXIST.booleanValue())))
            .andExpect(jsonPath("$.[*].respCatchDesc").value(hasItem(DEFAULT_RESP_CATCH_DESC.toString())))
            .andExpect(jsonPath("$.[*].respLstMsisdn").value(hasItem(DEFAULT_RESP_LST_MSISDN.toString())))
            .andExpect(jsonPath("$.[*].respLstFullname").value(hasItem(DEFAULT_RESP_LST_FULLNAME.toString())))
            .andExpect(jsonPath("$.[*].respLstCreditbalance").value(hasItem(DEFAULT_RESP_LST_CREDITBALANCE.toString())))
            .andExpect(jsonPath("$.[*].respLstAgentCode").value(hasItem(DEFAULT_RESP_LST_AGENT_CODE.toString())))
            .andExpect(jsonPath("$.[*].token").value(hasItem(DEFAULT_TOKEN.toString())))
            .andExpect(jsonPath("$.[*].tokenRole").value(hasItem(DEFAULT_TOKEN_ROLE.toString())))
            .andExpect(jsonPath("$.[*].tokenCreateDate").value(hasItem(sameInstant(DEFAULT_TOKEN_CREATE_DATE))))
            .andExpect(jsonPath("$.[*].tokenExpireDate").value(hasItem(sameInstant(DEFAULT_TOKEN_EXPIRE_DATE))))
            .andExpect(jsonPath("$.[*].tokenExpired").value(hasItem(DEFAULT_TOKEN_EXPIRED.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getDealerLogin() throws Exception {
        // Initialize the database
        dealerLoginRepository.saveAndFlush(dealerLogin);

        // Get the dealerLogin
        restDealerLoginMockMvc.perform(get("/api/dealer-logins/{id}", dealerLogin.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(dealerLogin.getId().intValue()))
            .andExpect(jsonPath("$.dealerUserName").value(DEFAULT_DEALER_USER_NAME.toString()))
            .andExpect(jsonPath("$.dealerPassword").value(DEFAULT_DEALER_PASSWORD.toString()))
            .andExpect(jsonPath("$.respError").value(DEFAULT_RESP_ERROR.booleanValue()))
            .andExpect(jsonPath("$.respErrorDesc").value(DEFAULT_RESP_ERROR_DESC.toString()))
            .andExpect(jsonPath("$.respExist").value(DEFAULT_RESP_EXIST.booleanValue()))
            .andExpect(jsonPath("$.respCatchDesc").value(DEFAULT_RESP_CATCH_DESC.toString()))
            .andExpect(jsonPath("$.respLstMsisdn").value(DEFAULT_RESP_LST_MSISDN.toString()))
            .andExpect(jsonPath("$.respLstFullname").value(DEFAULT_RESP_LST_FULLNAME.toString()))
            .andExpect(jsonPath("$.respLstCreditbalance").value(DEFAULT_RESP_LST_CREDITBALANCE.toString()))
            .andExpect(jsonPath("$.respLstAgentCode").value(DEFAULT_RESP_LST_AGENT_CODE.toString()))
            .andExpect(jsonPath("$.token").value(DEFAULT_TOKEN.toString()))
            .andExpect(jsonPath("$.tokenRole").value(DEFAULT_TOKEN_ROLE.toString()))
            .andExpect(jsonPath("$.tokenCreateDate").value(sameInstant(DEFAULT_TOKEN_CREATE_DATE)))
            .andExpect(jsonPath("$.tokenExpireDate").value(sameInstant(DEFAULT_TOKEN_EXPIRE_DATE)))
            .andExpect(jsonPath("$.tokenExpired").value(DEFAULT_TOKEN_EXPIRED.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingDealerLogin() throws Exception {
        // Get the dealerLogin
        restDealerLoginMockMvc.perform(get("/api/dealer-logins/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDealerLogin() throws Exception {
        // Initialize the database
        dealerLoginRepository.saveAndFlush(dealerLogin);

        int databaseSizeBeforeUpdate = dealerLoginRepository.findAll().size();

        // Update the dealerLogin
        DealerLogin updatedDealerLogin = dealerLoginRepository.findById(dealerLogin.getId()).get();
        // Disconnect from session so that the updates on updatedDealerLogin are not directly saved in db
        em.detach(updatedDealerLogin);
        updatedDealerLogin
            .dealerUserName(UPDATED_DEALER_USER_NAME)
            .dealerPassword(UPDATED_DEALER_PASSWORD)
            .respError(UPDATED_RESP_ERROR)
            .respErrorDesc(UPDATED_RESP_ERROR_DESC)
            .respExist(UPDATED_RESP_EXIST)
            .respCatchDesc(UPDATED_RESP_CATCH_DESC)
            .respLstMsisdn(UPDATED_RESP_LST_MSISDN)
            .respLstFullname(UPDATED_RESP_LST_FULLNAME)
            .respLstCreditbalance(UPDATED_RESP_LST_CREDITBALANCE)
            .respLstAgentCode(UPDATED_RESP_LST_AGENT_CODE)
            .token(UPDATED_TOKEN)
            .tokenRole(UPDATED_TOKEN_ROLE)
            .tokenCreateDate(UPDATED_TOKEN_CREATE_DATE)
            .tokenExpireDate(UPDATED_TOKEN_EXPIRE_DATE)
            .tokenExpired(UPDATED_TOKEN_EXPIRED);
        DealerLoginDTO dealerLoginDTO = dealerLoginMapper.toDto(updatedDealerLogin);

        restDealerLoginMockMvc.perform(put("/api/dealer-logins")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dealerLoginDTO)))
            .andExpect(status().isOk());

        // Validate the DealerLogin in the database
        List<DealerLogin> dealerLoginList = dealerLoginRepository.findAll();
        assertThat(dealerLoginList).hasSize(databaseSizeBeforeUpdate);
        DealerLogin testDealerLogin = dealerLoginList.get(dealerLoginList.size() - 1);
        assertThat(testDealerLogin.getDealerUserName()).isEqualTo(UPDATED_DEALER_USER_NAME);
        assertThat(testDealerLogin.getDealerPassword()).isEqualTo(UPDATED_DEALER_PASSWORD);
        assertThat(testDealerLogin.isRespError()).isEqualTo(UPDATED_RESP_ERROR);
        assertThat(testDealerLogin.getRespErrorDesc()).isEqualTo(UPDATED_RESP_ERROR_DESC);
        assertThat(testDealerLogin.isRespExist()).isEqualTo(UPDATED_RESP_EXIST);
        assertThat(testDealerLogin.getRespCatchDesc()).isEqualTo(UPDATED_RESP_CATCH_DESC);
        assertThat(testDealerLogin.getRespLstMsisdn()).isEqualTo(UPDATED_RESP_LST_MSISDN);
        assertThat(testDealerLogin.getRespLstFullname()).isEqualTo(UPDATED_RESP_LST_FULLNAME);
        assertThat(testDealerLogin.getRespLstCreditbalance()).isEqualTo(UPDATED_RESP_LST_CREDITBALANCE);
        assertThat(testDealerLogin.getRespLstAgentCode()).isEqualTo(UPDATED_RESP_LST_AGENT_CODE);
        assertThat(testDealerLogin.getToken()).isEqualTo(UPDATED_TOKEN);
        assertThat(testDealerLogin.getTokenRole()).isEqualTo(UPDATED_TOKEN_ROLE);
        assertThat(testDealerLogin.getTokenCreateDate()).isEqualTo(UPDATED_TOKEN_CREATE_DATE);
        assertThat(testDealerLogin.getTokenExpireDate()).isEqualTo(UPDATED_TOKEN_EXPIRE_DATE);
        assertThat(testDealerLogin.isTokenExpired()).isEqualTo(UPDATED_TOKEN_EXPIRED);
    }

    @Test
    @Transactional
    public void updateNonExistingDealerLogin() throws Exception {
        int databaseSizeBeforeUpdate = dealerLoginRepository.findAll().size();

        // Create the DealerLogin
        DealerLoginDTO dealerLoginDTO = dealerLoginMapper.toDto(dealerLogin);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDealerLoginMockMvc.perform(put("/api/dealer-logins")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dealerLoginDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DealerLogin in the database
        List<DealerLogin> dealerLoginList = dealerLoginRepository.findAll();
        assertThat(dealerLoginList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDealerLogin() throws Exception {
        // Initialize the database
        dealerLoginRepository.saveAndFlush(dealerLogin);

        int databaseSizeBeforeDelete = dealerLoginRepository.findAll().size();

        // Delete the dealerLogin
        restDealerLoginMockMvc.perform(delete("/api/dealer-logins/{id}", dealerLogin.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<DealerLogin> dealerLoginList = dealerLoginRepository.findAll();
        assertThat(dealerLoginList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DealerLogin.class);
        DealerLogin dealerLogin1 = new DealerLogin();
        dealerLogin1.setId(1L);
        DealerLogin dealerLogin2 = new DealerLogin();
        dealerLogin2.setId(dealerLogin1.getId());
        assertThat(dealerLogin1).isEqualTo(dealerLogin2);
        dealerLogin2.setId(2L);
        assertThat(dealerLogin1).isNotEqualTo(dealerLogin2);
        dealerLogin1.setId(null);
        assertThat(dealerLogin1).isNotEqualTo(dealerLogin2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DealerLoginDTO.class);
        DealerLoginDTO dealerLoginDTO1 = new DealerLoginDTO();
        dealerLoginDTO1.setId(1L);
        DealerLoginDTO dealerLoginDTO2 = new DealerLoginDTO();
        assertThat(dealerLoginDTO1).isNotEqualTo(dealerLoginDTO2);
        dealerLoginDTO2.setId(dealerLoginDTO1.getId());
        assertThat(dealerLoginDTO1).isEqualTo(dealerLoginDTO2);
        dealerLoginDTO2.setId(2L);
        assertThat(dealerLoginDTO1).isNotEqualTo(dealerLoginDTO2);
        dealerLoginDTO1.setId(null);
        assertThat(dealerLoginDTO1).isNotEqualTo(dealerLoginDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(dealerLoginMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(dealerLoginMapper.fromId(null)).isNull();
    }
}
