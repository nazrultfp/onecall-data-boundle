package com.mobilityone.onecall.service.dto;

public class TuneTalkTopupReqDTO {

    private String msisdn;
    private Integer topUpDeno;
    private String transactionId;

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public Integer getTopUpDeno() {
        return topUpDeno;
    }

    public void setTopUpDeno(Integer topUpDeno) {
        this.topUpDeno = topUpDeno;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    @Override
    public String toString() {
        return "TuneTalkTopupReqDTO{" +
            "msisdn='" + msisdn + '\'' +
            ", topUpDeno=" + topUpDeno +
            ", transactionId='" + transactionId + '\'' +
            '}';
    }
}
