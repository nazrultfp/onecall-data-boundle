package com.mobilityone.onecall.service.dto;
import com.mobilityone.onecall.domain.MsisdnValue;

import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A DTO for the OneCall entity.
 */
public class OneCallDTO implements Serializable {

    private Long id;

    private String channelId;

    @NotNull
    private String merchantId;

    private String merchantOrderNo;

    private String msisdn;

    private Integer topupDeno;

    private Long transactionId;

    private String apiKey;

    private String balance;

    private String code;

    private String message;

    private ZonedDateTime createdDate;

    private ZonedDateTime modifiedDate;

    private String createdBy;

    private String modifiedBy;

    private Set<MsisdnValueDTO> msisdnValues = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantOrderNo() {
        return merchantOrderNo;
    }

    public void setMerchantOrderNo(String merchantOrderNo) {
        this.merchantOrderNo = merchantOrderNo;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public Integer getTopupDeno() {
        return topupDeno;
    }

    public void setTopupDeno(Integer topupDeno) {
        this.topupDeno = topupDeno;
    }

    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public ZonedDateTime getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(ZonedDateTime modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Set<MsisdnValueDTO> getMsisdnValues() {
        return msisdnValues;
    }

    public void setMsisdnValues(Set<MsisdnValueDTO> msisdnValues) {
        this.msisdnValues = msisdnValues;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OneCallDTO oneCallDTO = (OneCallDTO) o;
        if (oneCallDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), oneCallDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "OneCallDTO{" +
            "id=" + getId() +
            ", channelId='" + getChannelId() + "'" +
            ", merchantId='" + getMerchantId() + "'" +
            ", merchantOrderNo='" + getMerchantOrderNo() + "'" +
            ", msisdn='" + getMsisdn() + "'" +
            ", topupDeno=" + getTopupDeno() +
            ", transactionId=" + getTransactionId() +
            ", apiKey='" + getApiKey() + "'" +
            ", balance='" + getBalance() + "'" +
            ", code='" + getCode() + "'" +
            ", message='" + getMessage() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", modifiedDate='" + getModifiedDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", modifiedBy='" + getModifiedBy() + "'" +
            "}";
    }
}
