package com.mobilityone.onecall.service.impl;

import com.mobilityone.onecall.config.Constants;
import com.mobilityone.onecall.domain.Agent;
import com.mobilityone.onecall.domain.DealerLogin;
import com.mobilityone.onecall.domain.Photo;
import com.mobilityone.onecall.domain.enumeration.Gender;
import com.mobilityone.onecall.domain.enumeration.SimStatus;
import com.mobilityone.onecall.repository.AgentRepository;
import com.mobilityone.onecall.repository.DealerLoginRepository;
import com.mobilityone.onecall.security.SecurityUtils;
import com.mobilityone.onecall.service.SimRegisterService;
import com.mobilityone.onecall.domain.SimRegister;
import com.mobilityone.onecall.repository.SimRegisterRepository;
import com.mobilityone.onecall.service.dto.SimRegReqDTO;
import com.mobilityone.onecall.service.dto.SimRegisterDTO;
import com.mobilityone.onecall.service.dto.SimRegisterReportDTO;
import com.mobilityone.onecall.service.mapper.PhotoMapper;
import com.mobilityone.onecall.service.mapper.SimRegisterMapper;
import com.mobilityone.onecall.service.mapper.SimRegisterReportMapper;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Service Implementation for managing SimRegister.
 */
@Service
@Transactional
public class SimRegisterServiceImpl implements SimRegisterService {

    private final Logger log = LoggerFactory.getLogger(SimRegisterServiceImpl.class);

    private final SimRegisterRepository simRegisterRepository;

    private final SimRegisterMapper simRegisterMapper;

    private final SimRegisterReportMapper simRegisterReportMapper;

    private final PhotoMapper photoMapper;

    private final AgentRepository agentRepository;

    private final DealerLoginRepository dealerLoginRepository;

    public SimRegisterServiceImpl(SimRegisterRepository simRegisterRepository, SimRegisterMapper simRegisterMapper, SimRegisterReportMapper simRegisterReportMapper, PhotoMapper photoMapper, AgentRepository agentRepository, DealerLoginRepository dealerLoginRepository) {
        this.simRegisterRepository = simRegisterRepository;
        this.simRegisterMapper = simRegisterMapper;
        this.simRegisterReportMapper = simRegisterReportMapper;
        this.photoMapper = photoMapper;
        this.agentRepository = agentRepository;
        this.dealerLoginRepository = dealerLoginRepository;
    }

    /**
     * Save a simRegister.
     *
     * @param simRegisterDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public SimRegisterDTO save(SimRegisterDTO simRegisterDTO) {
        log.debug("Request to save SimRegister : {}", simRegisterDTO);
        SimRegister simRegister = simRegisterMapper.toEntity(simRegisterDTO);
        simRegister = simRegisterRepository.save(simRegister);
        return simRegisterMapper.toDto(simRegister);
    }

    /**
     * Get all the simRegisters.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<SimRegisterDTO> findAll(Pageable pageable) {
        log.debug("Request to get all SimRegisters");
        return simRegisterRepository.findAll(pageable)
            .map(simRegisterMapper::toDto);
    }


    /**
     * Get one simRegister by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<SimRegisterDTO> findOne(Long id) {
        log.debug("Request to get SimRegister : {}", id);
        return simRegisterRepository.findById(id)
            .map(simRegisterMapper::toDto);
    }

//    @Override
//    public Optional<SimRegisterDTO> findOneByRegId(Long regId) {
//        log.debug("Request to get SimRegister : {}", regId);
//
//        Optional<SimRegister> optionalSimRegister = simRegisterRepository.findById(regId);
//        if (!optionalSimRegister.isPresent()){
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "sim register not found");
//        }
//
//        SimRegister simRegister = optionalSimRegister.get();
//        SimRegisterDTO simRegisterDTO = simRegisterMapper.toDto(simRegister);
//        Set<PhotoDTO> photoDTOS = simRegister.getPhotos().stream().map(photoMapper::toDto).collect(Collectors.toSet());
//        log.debug("Photos : {}", photoDTOS);
//
//        simRegisterDTO.setPhotos(photoDTOS);
//
//        log.debug("SimRegisterDTO : {}", simRegisterDTO);
//
//        return Optional.of(simRegisterDTO);
//    }

    /**
     * Delete the simRegister by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete SimRegister : {}", id);
        simRegisterRepository.deleteById(id);
    }

    @Override
    public SimRegisterDTO save(SimRegReqDTO simRegReqDTO) {
        log.debug("Request to save SimRegister by req : {} ", simRegReqDTO);
        SimRegister simRegister = new SimRegister();
        simRegister.setAgentCode(getCurrentUserAgentCode());
        simRegister.setTransactionId(simRegReqDTO.getTransactionId());
        simRegister.setFirstName(simRegReqDTO.getFirstName());
        simRegister.setLastName(simRegReqDTO.getLastName());
        simRegister.setGender(Gender.valueOf(simRegReqDTO.getGender()));
        simRegister.setDob(simRegReqDTO.getDob());
        simRegister.setIdNumber(simRegReqDTO.getIdNumber());
        simRegister.setIdType(simRegReqDTO.getIdType());
        simRegister.setNationality(simRegReqDTO.getNationality());
        simRegister.setAddress(simRegReqDTO.getAddress());
        simRegister.setEmailAddr(simRegReqDTO.getEmailAddr());
        simRegister.setBlockedNumber(simRegReqDTO.getBlockedNumber());
        simRegister.setCity(simRegReqDTO.getCity());
        simRegister.setState(simRegReqDTO.getState());
        simRegister.setCountry(simRegReqDTO.getCountry());
        simRegister.setPostCode(simRegReqDTO.getPostCode());
        simRegister.setSimNumber(simRegReqDTO.getSimNumber());
        simRegister.setStatus(SimStatus.KYC_PENDING.toString());
        simRegister.setCreatedDate(ZonedDateTime.now());
        simRegister.setCreatedBy(SecurityUtils.getCurrentUserLogin().orElse(Constants.ANONYMOUS_USER));
        simRegister.setModifiedDate(ZonedDateTime.now());
        simRegister.setModifiedBy(SecurityUtils.getCurrentUserLogin().orElse(Constants.ANONYMOUS_USER));
        simRegister.getPhotos().add(new Photo().photo(simRegReqDTO.getPhoto()).simRegister(simRegister));

        if (simRegReqDTO.getAgentId() != null) {
            Optional<Agent> agent = agentRepository.findById(simRegReqDTO.getAgentId());
            if (!agent.isPresent()) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "agent not found");
            }
            simRegister.setAgent(agent.get());
        }


        simRegister = simRegisterRepository.save(simRegister);

        log.debug("Saved data : {}", simRegister);

        return simRegisterMapper.toDto(simRegister);
    }

    private String getCurrentUserAgentCode() {
        String username = SecurityUtils.getCurrentUserLogin().orElse(Constants.ANONYMOUS_USER);
        Optional<DealerLogin> dealerLoginOptional = dealerLoginRepository.findByDealerUserNameAndTokenExpiredFalse(username);
        return dealerLoginOptional.map(DealerLogin::getRespLstAgentCode).orElse(null);
    }

    @Override
    public SimRegisterDTO save(Long simRegId, JSONObject jsonObject) {
        log.debug("Request to save response for SimRegister by id : {} and resp : {}", simRegId, jsonObject);

        Optional<SimRegister> registerOptional = simRegisterRepository.findById(simRegId);
        if (!registerOptional.isPresent()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "sim register not found");
        }

        SimRegister simRegister = registerOptional.get();
        if (!jsonObject.isNull("apiKey"))
            simRegister.setApiKey(jsonObject.getString("apiKey"));
        if (!jsonObject.isNull("code"))
            simRegister.setCode(jsonObject.getString("code"));
        if (!jsonObject.isNull("message"))
            simRegister.setMessage(jsonObject.getString("message"));
        if (!jsonObject.isNull("msisdn"))
            simRegister.setMsisdn(jsonObject.getString("msisdn"));

        simRegister = simRegisterRepository.save(simRegister);

        log.debug("Saved data : {}", simRegister);

        return simRegisterMapper.toDto(simRegister);
    }

//    @Override
//    public SimRegisterDTO save(SimRegReqDTO simRegReqDTO, JSONObject jsonObject) {
//        log.debug("Request to save SimRegister by req : {} and resp : {}", simRegReqDTO, jsonObject);
//
//        SimRegister simRegister = new SimRegister();
//        simRegister.setAgentCode(simRegReqDTO.getAgentCode());
//        simRegister.setTransactionId(simRegReqDTO.getTransactionId());
//        simRegister.setFirstName(simRegReqDTO.getFirstName());
//        simRegister.setLastName(simRegReqDTO.getLastName());
//        simRegister.setGender(Gender.valueOf(simRegReqDTO.getGender()));
//        simRegister.setDob(simRegReqDTO.getDob());
//        simRegister.setIdNumber(simRegReqDTO.getIdNumber());
//        simRegister.setIdType(simRegReqDTO.getIdType());
//        simRegister.setNationality(simRegReqDTO.getNationality());
//        simRegister.setAddress(simRegReqDTO.getAddress());
//        simRegister.setEmailAddr(simRegReqDTO.getEmailAddr());
//        simRegister.setBlockedNumber(simRegReqDTO.getBlockedNumber());
//        simRegister.setCity(simRegReqDTO.getCity());
//        simRegister.setState(simRegReqDTO.getState());
//        simRegister.setCountry(simRegReqDTO.getCountry());
//        simRegister.setPostCode(simRegReqDTO.getPostCode());
//        simRegister.setSimNumber(simRegReqDTO.getSimNumber());
//        simRegister.setCreatedDate(ZonedDateTime.now());
//        simRegister.setCreatedBy(SecurityUtils.getCurrentUserLogin().orElse(Constants.ANONYMOUS_USER));
//        simRegister.setModifiedDate(ZonedDateTime.now());
//        simRegister.setModifiedBy(SecurityUtils.getCurrentUserLogin().orElse(Constants.ANONYMOUS_USER));
////        simRegister.setPartnerDealerCode(simRegReqDTO.getPartnerDealerCode());
//        if (!jsonObject.isNull("apiKey"))
//            simRegister.setApiKey(jsonObject.getString("apiKey"));
//        if (!jsonObject.isNull("code"))
//            simRegister.setCode(jsonObject.getString("code"));
//        if (!jsonObject.isNull("message"))
//            simRegister.setMessage(jsonObject.getString("message"));
//        if (!jsonObject.isNull("msisdn"))
//            simRegister.setMsisdn(jsonObject.getString("msisdn"));
//        simRegister.getPhotos().add(new Photo().photo(simRegReqDTO.getPhoto()));
//
//        if (simRegReqDTO.getAgentId() != null) {
//            Optional<Agent> agent = agentRepository.findById(simRegReqDTO.getAgentId());
//            if (!agent.isPresent()) {
//                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "agent not found");
//            }
//            simRegister.setAgent(agent.get());
//        }
//
//        simRegister = simRegisterRepository.save(simRegister);
//
//        log.debug("Saved data : {}", simRegister);
//
//        return simRegisterMapper.toDto(simRegister);
//    }

    @Override
    public Page<SimRegisterDTO> findByAgentId(Pageable pageable, Long agentId) {
        log.debug("Request to get all SimRegisters for agentId : {}", agentId);
        return simRegisterRepository.findByAgentIdOrderByCreatedDateDesc(pageable, agentId)
            .map(simRegisterMapper::toDto);
    }

    @Override
    public List<Map<String, Object>> simHistory() {
        log.debug("Request to get all Sims History");
        List<Map<String, Object>> content = simRegisterRepository.simHistory(SecurityUtils.getCurrentUserLogin().orElse(Constants.ANONYMOUS_USER));

//        for (Map<String, Object> data : content){
//            Set<Map.Entry<String, Object>> entries = data.entrySet();
//            for (Map.Entry<String, Object> entry : entries) {
//                log.info("==>> {} : {}", entry.getKey(), entry.getValue());
//            }
//        }

        return content;
    }

    @Override
    public List<Map<String, Object>> getSimStatusByMsisdn(String msisdn) {
        log.info("Request to get sim status for msisdn : {}", msisdn);

        return simRegisterRepository.simStatusByMsisdn(msisdn);
    }

    @Override
    public Page<SimRegisterReportDTO> getReportOfAllSimRegisters(Pageable pageable) {
        log.debug("Request to get all SimRegisters");
        return simRegisterRepository.findAll(pageable)
            .map(simRegisterReportMapper::toDto);
    }

    @Override
    public JSONObject getTotalNoByDate(Integer year, Integer month, Integer day) {
        log.info("Request to get total number of registered sims for {}-{}-{}.", year, month, day);

        if (month < 1 || month > 12 || day < 1 || day > 31) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "date is not correct");
        }

        String date = String.valueOf(year);
        date = date.concat("-").concat(String.format("%02d", month));
        date = date.concat("-").concat(String.format("%02d", day));

        log.info("date format : {}", date);

        Integer count = simRegisterRepository.countByDate(date);

        JSONObject object = new JSONObject();
        object.put("date", date);
        object.put("total", count);

        log.info("result : {}", object);

        return object;
    }

    @Override
    public List<SimRegisterReportDTO> getAllByAgentCode(String agentCode) {
        log.info("Request to get all registered sims by agent code : {}", agentCode);

        agentCode = String.format("%08d", Integer.valueOf(agentCode));
        log.info("String agentCode = {}", agentCode);

        Optional<DealerLogin> dealerLoginOptional = dealerLoginRepository.findFirstByRespLstAgentCode(agentCode);
        if (!dealerLoginOptional.isPresent()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "dealer not found");
        }

        List<SimRegister> simRegisterList = simRegisterRepository.findByCreatedBy(dealerLoginOptional.get().getDealerUserName());

        log.info("total number of registered sims by {} is {}", dealerLoginOptional.get().getDealerUserName(), simRegisterList.size());

        return simRegisterReportMapper.toDto(simRegisterList);
    }

    @Override
    public List<Map<String, Object>> getAllInRangeDate(String fromDate, String toDate) {
        log.info("Request to get all registered sims in range from {} to {}", fromDate, toDate);

        List <Map<String, Object>> list = simRegisterRepository.findAllInRangeDate(fromDate,toDate);

        log.info("total number of registered sims in range from {} to {} is {}" , fromDate, toDate, list.size());

        return list;
    }

    @Override
    public List<Map<String, Object>> getSimStatusByAgentCode(String agentCode) {
        log.info("Request to get all registered sims status by agent code : {}", agentCode);

        agentCode = String.format("%08d", Integer.valueOf(agentCode));
        log.info("String agentCode = {}", agentCode);

        Optional<DealerLogin> dealerLoginOptional = dealerLoginRepository.findFirstByRespLstAgentCode(agentCode);
        if (!dealerLoginOptional.isPresent()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "dealer not found");
        }

        List<Map<String, Object>> list = simRegisterRepository.getSimStatusByAgentCode(agentCode);

        log.info("total number of registered sims by {} is {}", dealerLoginOptional.get().getDealerUserName(), list.size());

        return list;
    }

    @Override
    public List<Map<String, Object>> getDailyTotalSimInRange(String fromDate, String toDate) {
        log.info("Request to get daily total registered sims and port in sims in range from {} to {}", fromDate, toDate);

        List <Map<String, Object>> list = simRegisterRepository.getDailyTotalSimInRange(fromDate,toDate);

        log.info("total number of daily report in range from {} to {} is {}" , fromDate, toDate, list.size());

        return list;
    }

    @Override
    public List<Map<String, Object>> getAgentDailyTotalSimRegInRange(String agentCode, String fromDate, String toDate) {
        log.info("Request to get agent {} daily total registered sims in range from {} to {}", agentCode, fromDate, toDate);

        agentCode = String.format("%08d", Integer.valueOf(agentCode));
        log.info("String agentCode = {}", agentCode);

        Optional<DealerLogin> dealerLoginOptional = dealerLoginRepository.findFirstByRespLstAgentCode(agentCode);
        if (!dealerLoginOptional.isPresent()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "dealer not found");
        }

        List <Map<String, Object>> list = simRegisterRepository.getAgentDailyTotalSimRegInRange(agentCode, fromDate,toDate);

        log.info("total number of daily report in range from {} to {} is {}" , fromDate, toDate, list.size());

        return list;
    }

    @Override
    public List<Map<String, Object>> getSimRegsInRange(String fromDate, String toDate) {
        log.info("Request to get registered sims in range from {} to {}", fromDate, toDate);

        List <Map<String, Object>> list = simRegisterRepository.getSimRegsInRange(fromDate,toDate);

        log.info("total sim regs in range from {} to {} is {}" , fromDate, toDate, list.size());

        return list;
    }
}
